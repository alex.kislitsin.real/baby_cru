SELECT u.*,

(if(`1`=0 OR `1`=3,1,0)+if(`2`=0 OR `2`=3,1,0)+if(`3`=0 OR `3`=3,1,0)+if(`4`=0 OR `4`=3,1,0)+if(`5`=0 OR `5`=3,1,0)+if(`6`=0 OR `6`=3,1,0)+if(`7`=0 OR `7`=3,1,0)
+if(`8`=0 OR `8`=3,1,0)+if(`9`=0 OR `9`=3,1,0)+if(`10`=0 OR `10`=3,1,0)+if(`11`=0 OR `11`=3,1,0)+if(`12`=0 OR `12`=3,1,0)+if(`13`=0 OR `13`=3,1,0)+if(`14`=0 OR `14`=3,1,0)
+if(`15`=0 OR `15`=3,1,0)+if(`16`=0 OR `16`=3,1,0)+if(`17`=0 OR `17`=3,1,0)+if(`18`=0 OR `18`=3,1,0)+if(`19`=0 OR `19`=3,1,0)+if(`20`=0 OR `20`=3,1,0)+if(`21`=0 OR `21`=3,1,0)
+if(`22`=0 OR `22`=3,1,0)+if(`23`=0 OR `23`=3,1,0)+if(`24`=0 OR `24`=3,1,0)+if(`25`=0 OR `25`=3,1,0)+if(`26`=0 OR `26`=3,1,0)+if(`27`=0 OR `27`=3,1,0)+if(`28`=0 OR `28`=3,1,0)
+if(`29`=0 OR `29`=3,1,0)+if(`30`=0 OR `30`=3,1,0)+if(`31`=0 OR `31`=3,1,0)) AS `пришли`,

(if(`1`=1,1,0)+if(`2`=1,1,0)+if(`3`=1,1,0)+if(`4`=1,1,0)+if(`5`=1,1,0)+if(`6`=1,1,0)+if(`7`=1,1,0)
+if(`8`=1,1,0)+if(`9`=1,1,0)+if(`10`=1,1,0)+if(`11`=1,1,0)+if(`12`=1,1,0)+if(`13`=1,1,0)+if(`14`=1,1,0)
+if(`15`=1,1,0)+if(`16`=1,1,0)+if(`17`=1,1,0)+if(`18`=1,1,0)+if(`19`=1,1,0)+if(`20`=1,1,0)+if(`21`=1,1,0)
+if(`22`=1,1,0)+if(`23`=1,1,0)+if(`24`=1,1,0)+if(`25`=1,1,0)+if(`26`=1,1,0)+if(`27`=1,1,0)+if(`28`=1,1,0)
+if(`29`=1,1,0)+if(`30`=1,1,0)+if(`31`=1,1,0)) AS `пропуски`

FROM (
SELECT 
a.id AS `id`,
rtrim(a.`name`) AS `name`,`schet` as `number_schet`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-01') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-01') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-01')) > 4 OR CONCAT(:yea,'-',:mon,'-01') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-01') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `1`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-02') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-02') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-02')) > 4 OR CONCAT(:yea,'-',:mon,'-02') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-02') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `2`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-03') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-03') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-03')) > 4 OR CONCAT(:yea,'-',:mon,'-03') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-03') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `3`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-04') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-04') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-04')) > 4 OR CONCAT(:yea,'-',:mon,'-04') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-04') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `4`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-05') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-05') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-05')) > 4 OR CONCAT(:yea,'-',:mon,'-05') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-05') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `5`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-06') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-06') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-06')) > 4 OR CONCAT(:yea,'-',:mon,'-06') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-06') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `6`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-07') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-07') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-07')) > 4 OR CONCAT(:yea,'-',:mon,'-07') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-07') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `7`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-08') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-08') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-08')) > 4 OR CONCAT(:yea,'-',:mon,'-08') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-08') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `8`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-09') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-09') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-09')) > 4 OR CONCAT(:yea,'-',:mon,'-09') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-09') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `9`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-10') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-10') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-10')) > 4 OR CONCAT(:yea,'-',:mon,'-10') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-10') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `10`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-11') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-11') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-11')) > 4 OR CONCAT(:yea,'-',:mon,'-11') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-11') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `11`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-12') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-12') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-12')) > 4 OR CONCAT(:yea,'-',:mon,'-12') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-12') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `12`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-13') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-13') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-13')) > 4 OR CONCAT(:yea,'-',:mon,'-13') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-13') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `13`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-14') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-14') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-14')) > 4 OR CONCAT(:yea,'-',:mon,'-14') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-14') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `14`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-15') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-15') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-15')) > 4 OR CONCAT(:yea,'-',:mon,'-15') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-15') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `15`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-16') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-16') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-16')) > 4 OR CONCAT(:yea,'-',:mon,'-16') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-16') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `16`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-17') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-17') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-17')) > 4 OR CONCAT(:yea,'-',:mon,'-17') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-17') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `17`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-18') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-18') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-18')) > 4 OR CONCAT(:yea,'-',:mon,'-18') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-18') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `18`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-19') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-19') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-19')) > 4 OR CONCAT(:yea,'-',:mon,'-19') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-19') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `19`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-20') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-20') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-20')) > 4 OR CONCAT(:yea,'-',:mon,'-20') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-20') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `20`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-21') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-21') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-21')) > 4 OR CONCAT(:yea,'-',:mon,'-21') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-21') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `21`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-22') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-22') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-22')) > 4 OR CONCAT(:yea,'-',:mon,'-22') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-22') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `22`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-23') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-23') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-23')) > 4 OR CONCAT(:yea,'-',:mon,'-23') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-23') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `23`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-24') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-24') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-24')) > 4 OR CONCAT(:yea,'-',:mon,'-24') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-24') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `24`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-25') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-25') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-25')) > 4 OR CONCAT(:yea,'-',:mon,'-25') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-25') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `25`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-26') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-26') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-26')) > 4 OR CONCAT(:yea,'-',:mon,'-26') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-26') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `26`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-27') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-27') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-27')) > 4 OR CONCAT(:yea,'-',:mon,'-27') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-27') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `27`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-28') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-28') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-28')) > 4 OR CONCAT(:yea,'-',:mon,'-28') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-28') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `28`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-29') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-29') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-29')) > 4 OR CONCAT(:yea,'-',:mon,'-29') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-29') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `29`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-30') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-30') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-30')) > 4 OR CONCAT(:yea,'-',:mon,'-30') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-30') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `30`,
case 
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-31') and propuskS LIKE '%приду%') then 3/*сегодня*/
when a.id IN (select gogos.id from gogos where dateS = CONCAT(:yea,'-',:mon,'-31') and rtrim(propuskS) = 'H') then 1/*пропуск прочее*/
when (weekday(CONCAT(:yea,'-',:mon,'-31')) > 4 OR CONCAT(:yea,'-',:mon,'-31') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-31') not in (select date_work from works_day) then 5/*выходной*/ 
ELSE 0
END AS `31`

FROM (SELECT id,`name`,`schet` from sotrudniki where 
((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
and (((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL and pitanie like '%пит%'))a)u ORDER BY u.`name`


