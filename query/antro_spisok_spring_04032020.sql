SELECT a.id,a.`name`,a.rozd,
if(
(SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),
date_format((SELECT max(dat) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),'%d.%m.%Y'),
'01.01.1900') AS dat,
if(
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),
(SELECT max(rost) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),
0) AS rost,
if(
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),
(SELECT max(ves) FROM rostves WHERE id_child = a.id AND dat BETWEEN CONCAT(:year,'-','01-01') AND LAST_DAY(CONCAT(:year,'-','06-01'))),
0) AS ves
FROM (SELECT id,`name`,date_format(rozd,'%d.%m.%Y') AS rozd from deti WHERE `out` is NULL AND id_gruppa = :id_group)a ORDER BY a.`name`