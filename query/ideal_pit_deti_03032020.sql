SELECT 
aa.id AS `id_all`,
aa.`name` AS `name_all`,
aa.`start`,
aa.alergia,
IFNULL(MONTH(aa.`in`),0) AS `in`,
IFNULL(MONTH(aa.`out`),0) AS `out`,
IFNULL(year(aa.`in`),0) AS `yin`,
IFNULL(year(aa.`out`),0) AS `yout`,
case when aa.id not IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200)) then 1 ELSE 0 END AS `st`,
case when aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200)) then 1 ELSE 0 END AS `sn`,
case when (aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200))) AND 
(select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200) AND id_child = aa.id) > 0
then (select name from reason where id = (select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200) AND id_child = aa.id))
when (aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200))) AND 
(select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200) AND id_child = aa.id) = 0
then (select name from reason where id = (select gogo.id from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200) AND id_child = aa.id)) 
ELSE 0 END AS `sn_reason`,
case when aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id NOT IN (100,200) AND surprise LIKE '%+%') then 1 ELSE 0 END AS `sur`,
 
case when aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id IN (100,200)) then 1 ELSE 0 END AS `today`,
case when (aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id IN (100,200))) AND 
(select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id IN (100,200) AND id_child = aa.id) > 0
then (select name from reason where id = (select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id IN (100,200) AND id_child = aa.id))
when (aa.id IN (select gogo.id_child from gogo where datenotgo = :date_in and gogo.id IN (100,200))) AND 
(select gogo.sovsem from gogo where datenotgo = :date_in and gogo.id IN (100,200) AND id_child = aa.id) = 0
then (select name from reason where id = (select gogo.id from gogo where datenotgo = :date_in and gogo.id IN (100,200) AND id_child = aa.id)) 
ELSE 0 END AS `today_reason`
FROM (

SELECT 
id,`name`,
case
when :date_in < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa
end as id_gruppa,
old_id_gruppa,
perevod,`start`,
`in`,
`out`,
alergia
 from deti where 
((month(:date_in)>=MONTH(`in`) and year(:date_in)=year(`in`)) or year(:date_in)>year(`in`))
and (((month(:date_in)<=month(`out`) and year(:date_in)<=year(`out`)) or year(:date_in)<year(`out`)) or `out` is NULL))aa WHERE aa.id_gruppa = :id_group ORDER BY aa.`name`