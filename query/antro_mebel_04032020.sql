SELECT a.id AS id_gruppa,a.`name` AS namegr,
(SELECT COUNT(*) FROM deti WHERE id_gruppa = a.id AND `out` IS null) AS countgr,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost > 1 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS zzz,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 1 AND 85 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y1,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 85.01 and 100 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y2,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 100.01 and 115 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y3,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 115.01 and 130 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y4,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 130.01 and 145 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y5,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 145.01 and 160 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = a.id AND deti.`out` IS null) AS y6
FROM (SELECT * FROM gruppa)a
UNION
SELECT '','Итого',SUM(countgr),SUM(zzz),SUM(y1),SUM(y2),SUM(y3),SUM(y4),SUM(y5),SUM(y6) FROM
(SELECT b.id AS id_gruppa,b.`name` AS namegr,
(SELECT COUNT(*) FROM deti WHERE id_gruppa = b.id AND `out` IS null) AS countgr,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost > 1 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS zzz,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 1 AND 85 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y1,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 85.01 and 100 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y2,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 100.01 and 115 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y3,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 115.01 and 130 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y4,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 130.01 and 145 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y5,
(SELECT COUNT(*) FROM rostves LEFT OUTER JOIN deti ON rostves.id_child = deti.id 
WHERE rost between 145.01 and 160 AND month(dat) between :m1 AND :m2 and year(dat) = :yea AND deti.id_gruppa = b.id AND deti.`out` IS null) AS y6
FROM (SELECT * FROM gruppa)b)m