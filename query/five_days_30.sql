
SELECT t.*,
(IFNULL(k1,0)+IFNULL(k2,0)+IFNULL(k3,0)+IFNULL(k4,0)+IFNULL(k5,0)+IFNULL(k6,0)+IFNULL(k7,0)+IFNULL(k8,0)+IFNULL(k9,0)+IFNULL(k10,0)
+IFNULL(k11,0)+IFNULL(k12,0)+IFNULL(k13,0)+IFNULL(k14,0)+IFNULL(k15,0)+IFNULL(k16,0)+IFNULL(k17,0)+IFNULL(k18,0)+IFNULL(k19,0)+IFNULL(k20,0)
+IFNULL(k21,0)+IFNULL(k22,0)+IFNULL(k23,0)+IFNULL(k24,0)+IFNULL(k25,0)+IFNULL(k26,0)+IFNULL(k27,0)+IFNULL(k28,0)+IFNULL(k29,0)+IFNULL(k30,0)) AS `summ`


 FROM (
SELECT  g.*,
(SELECT COUNT(*) FROM (
SELECT id,`name`,case when LAST_DAY(CONCAT(:yea,'-',if(:mon < 10,CONCAT('0',:mon),:mon),'-01')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id) AS `kolvse`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-01')) > 4 OR CONCAT(:yea,'-',:mon,'-01') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-01') not in (select date_work from works_day) then null
when :v1=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-01') and gogo.id not in (100,200)))) end AS `k1`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-02')) > 4 OR CONCAT(:yea,'-',:mon,'-02') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-02') not in (select date_work from works_day) then null
when :v2=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-02') and gogo.id not in (100,200)))) end AS `k2`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-03')) > 4 OR CONCAT(:yea,'-',:mon,'-03') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-03') not in (select date_work from works_day) then null
when :v3=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-03') and gogo.id not in (100,200)))) end AS `k3`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-04')) > 4 OR CONCAT(:yea,'-',:mon,'-04') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-04') not in (select date_work from works_day) then null
when :v4=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-04') and gogo.id not in (100,200)))) end AS `k4`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-05')) > 4 OR CONCAT(:yea,'-',:mon,'-05') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-05') not in (select date_work from works_day) then null
when :v5=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-05') and gogo.id not in (100,200)))) end AS `k5`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-06')) > 4 OR CONCAT(:yea,'-',:mon,'-06') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-06') not in (select date_work from works_day) then null
when :v6=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-06') and gogo.id not in (100,200)))) end AS `k6`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-07')) > 4 OR CONCAT(:yea,'-',:mon,'-07') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-07') not in (select date_work from works_day) then null
when :v7=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-07') and gogo.id not in (100,200)))) end AS `k7`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-08')) > 4 OR CONCAT(:yea,'-',:mon,'-08') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-08') not in (select date_work from works_day) then null
when :v8=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-08') and gogo.id not in (100,200)))) end AS `k8`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-09')) > 4 OR CONCAT(:yea,'-',:mon,'-09') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-09') not in (select date_work from works_day) then null
when :v9=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-09') and gogo.id not in (100,200)))) end AS `k9`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-10')) > 4 OR CONCAT(:yea,'-',:mon,'-10') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-10') not in (select date_work from works_day) then null
when :v10=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-10') and gogo.id not in (100,200)))) end AS `k10`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-11')) > 4 OR CONCAT(:yea,'-',:mon,'-11') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-11') not in (select date_work from works_day) then null
when :v11=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-11') and gogo.id not in (100,200)))) end AS `k11`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-12')) > 4 OR CONCAT(:yea,'-',:mon,'-12') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-12') not in (select date_work from works_day) then null
when :v12=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-12') and gogo.id not in (100,200)))) end AS `k12`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-13')) > 4 OR CONCAT(:yea,'-',:mon,'-13') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-13') not in (select date_work from works_day) then null
when :v13=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-13') and gogo.id not in (100,200)))) end AS `k13`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-14')) > 4 OR CONCAT(:yea,'-',:mon,'-14') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-14') not in (select date_work from works_day) then null
when :v14=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-14') and gogo.id not in (100,200)))) end AS `k14`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-15')) > 4 OR CONCAT(:yea,'-',:mon,'-15') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-15') not in (select date_work from works_day) then null
when :v15=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-15') and gogo.id not in (100,200)))) end AS `k15`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-16')) > 4 OR CONCAT(:yea,'-',:mon,'-16') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-16') not in (select date_work from works_day) then null
when :v16=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-16') and gogo.id not in (100,200)))) end AS `k16`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-17')) > 4 OR CONCAT(:yea,'-',:mon,'-17') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-17') not in (select date_work from works_day) then null
when :v17=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-17') and gogo.id not in (100,200)))) end AS `k17`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-18')) > 4 OR CONCAT(:yea,'-',:mon,'-18') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-18') not in (select date_work from works_day) then null
when :v18=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-18') and gogo.id not in (100,200)))) end AS `k18`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-19')) > 4 OR CONCAT(:yea,'-',:mon,'-19') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-19') not in (select date_work from works_day) then null
when :v19=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-19') and gogo.id not in (100,200)))) end AS `k19`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-20')) > 4 OR CONCAT(:yea,'-',:mon,'-20') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-20') not in (select date_work from works_day) then null
when :v20=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-20') and gogo.id not in (100,200)))) end AS `k20`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-21')) > 4 OR CONCAT(:yea,'-',:mon,'-21') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-21') not in (select date_work from works_day) then null
when :v21=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-21') and gogo.id not in (100,200)))) end AS `k21`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-22')) > 4 OR CONCAT(:yea,'-',:mon,'-22') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-22') not in (select date_work from works_day) then null
when :v22=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-22') and gogo.id not in (100,200)))) end AS `k22`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-23')) > 4 OR CONCAT(:yea,'-',:mon,'-23') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-23') not in (select date_work from works_day) then null
when :v23=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-23') and gogo.id not in (100,200)))) end AS `k23`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-24')) > 4 OR CONCAT(:yea,'-',:mon,'-24') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-24') not in (select date_work from works_day) then null
when :v24=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-24') and gogo.id not in (100,200)))) end AS `k24`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-25')) > 4 OR CONCAT(:yea,'-',:mon,'-25') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-25') not in (select date_work from works_day) then null
when :v25=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-25') and gogo.id not in (100,200)))) end AS `k25`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-26')) > 4 OR CONCAT(:yea,'-',:mon,'-26') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-26') not in (select date_work from works_day) then null
when :v26=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-26') and gogo.id not in (100,200)))) end AS `k26`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-27')) > 4 OR CONCAT(:yea,'-',:mon,'-27') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-27') not in (select date_work from works_day) then null
when :v27=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-27') and gogo.id not in (100,200)))) end AS `k27`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-28')) > 4 OR CONCAT(:yea,'-',:mon,'-28') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-28') not in (select date_work from works_day) then null
when :v28=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-28') and gogo.id not in (100,200)))) end AS `k28`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-29')) > 4 OR CONCAT(:yea,'-',:mon,'-29') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-29') not in (select date_work from works_day) then null
when :v29=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-29') and gogo.id not in (100,200)))) end AS `k29`,
case
when (weekday(CONCAT(:yea,'-',:mon,'-30')) > 4 OR CONCAT(:yea,'-',:mon,'-30') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-30') not in (select date_work from works_day) then null
when :v30=0 then 0
ELSE ((select count(*) from deti WHERE deti.id_gruppa = g.id and ((:mon>=month(`in`) and :yea>=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL) AND deti.id NOT IN (SELECT id_child FROM gogo WHERE datenotgo = CONCAT(:yea,'-',:mon,'-30') and gogo.id not in (100,200)))) end AS `k30`,0 AS `k31`

FROM (SELECT id,RTRIM(`name`) AS `name` FROM gruppa)g)t


UNION

SELECT j.*,
(IFNULL(k1,0)+IFNULL(k2,0)+IFNULL(k3,0)+IFNULL(k4,0)+IFNULL(k5,0)+IFNULL(k6,0)+IFNULL(k7,0)+IFNULL(k8,0)+IFNULL(k9,0)+IFNULL(k10,0)
+IFNULL(k11,0)+IFNULL(k12,0)+IFNULL(k13,0)+IFNULL(k14,0)+IFNULL(k15,0)+IFNULL(k16,0)+IFNULL(k17,0)+IFNULL(k18,0)+IFNULL(k19,0)+IFNULL(k20,0)
+IFNULL(k21,0)+IFNULL(k22,0)+IFNULL(k23,0)+IFNULL(k24,0)+IFNULL(k25,0)+IFNULL(k26,0)+IFNULL(k27,0)+IFNULL(k28,0)+IFNULL(k29,0)+IFNULL(k30,0)) AS `summ`
 FROM (
SELECT null,'Сотрудники',
(SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%'),

case
when (weekday(CONCAT(:yea,'-',:mon,'-01')) > 4 OR CONCAT(:yea,'-',:mon,'-01') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-01') not in (select date_work from works_day) then null
when :v1=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-01')  and rtrim(propuskS) = 'H')) end AS `k1`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-02')) > 4 OR CONCAT(:yea,'-',:mon,'-02') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-02') not in (select date_work from works_day) then null
when :v2=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-02')  and rtrim(propuskS) = 'H')) end AS `k2`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-03')) > 4 OR CONCAT(:yea,'-',:mon,'-03') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-03') not in (select date_work from works_day) then null
when :v3=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-03')  and rtrim(propuskS) = 'H')) end AS `k3`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-4')) > 4 OR CONCAT(:yea,'-',:mon,'-4') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-4') not in (select date_work from works_day) then null
when :v4=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-4')  and rtrim(propuskS) = 'H')) end AS `k4`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-5')) > 4 OR CONCAT(:yea,'-',:mon,'-5') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-5') not in (select date_work from works_day) then null
when :v5=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-5')  and rtrim(propuskS) = 'H')) end AS `k5`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-6')) > 4 OR CONCAT(:yea,'-',:mon,'-6') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-6') not in (select date_work from works_day) then null
when :v6=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-6')  and rtrim(propuskS) = 'H')) end AS `k6`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-7')) > 4 OR CONCAT(:yea,'-',:mon,'-7') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-7') not in (select date_work from works_day) then null
when :v7=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-7')  and rtrim(propuskS) = 'H')) end AS `k7`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-8')) > 4 OR CONCAT(:yea,'-',:mon,'-8') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-8') not in (select date_work from works_day) then null
when :v8=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-8')  and rtrim(propuskS) = 'H')) end AS `k8`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-9')) > 4 OR CONCAT(:yea,'-',:mon,'-9') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-9') not in (select date_work from works_day) then null
when :v9=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-9')  and rtrim(propuskS) = 'H')) end AS `k9`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-10')) > 4 OR CONCAT(:yea,'-',:mon,'-10') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-10') not in (select date_work from works_day) then null
when :v10=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-10')  and rtrim(propuskS) = 'H')) end AS `k10`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-11')) > 4 OR CONCAT(:yea,'-',:mon,'-11') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-11') not in (select date_work from works_day) then null
when :v11=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-11')  and rtrim(propuskS) = 'H')) end AS `k11`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-12')) > 4 OR CONCAT(:yea,'-',:mon,'-12') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-12') not in (select date_work from works_day) then null
when :v12=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-12')  and rtrim(propuskS) = 'H')) end AS `k12`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-13')) > 4 OR CONCAT(:yea,'-',:mon,'-13') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-13') not in (select date_work from works_day) then null
when :v13=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-13')  and rtrim(propuskS) = 'H')) end AS `k13`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-14')) > 4 OR CONCAT(:yea,'-',:mon,'-14') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-14') not in (select date_work from works_day) then null
when :v14=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-14')  and rtrim(propuskS) = 'H')) end AS `k14`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-15')) > 4 OR CONCAT(:yea,'-',:mon,'-15') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-15') not in (select date_work from works_day) then null
when :v15=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-15')  and rtrim(propuskS) = 'H')) end AS `k15`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-16')) > 4 OR CONCAT(:yea,'-',:mon,'-16') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-16') not in (select date_work from works_day) then null
when :v16=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-16')  and rtrim(propuskS) = 'H')) end AS `k16`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-17')) > 4 OR CONCAT(:yea,'-',:mon,'-17') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-17') not in (select date_work from works_day) then null
when :v17=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-17')  and rtrim(propuskS) = 'H')) end AS `k17`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-18')) > 4 OR CONCAT(:yea,'-',:mon,'-18') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-18') not in (select date_work from works_day) then null
when :v18=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-18')  and rtrim(propuskS) = 'H')) end AS `k18`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-19')) > 4 OR CONCAT(:yea,'-',:mon,'-19') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-19') not in (select date_work from works_day) then null
when :v19=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-19')  and rtrim(propuskS) = 'H')) end AS `k19`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-20')) > 4 OR CONCAT(:yea,'-',:mon,'-20') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-20') not in (select date_work from works_day) then null
when :v20=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-20')  and rtrim(propuskS) = 'H')) end AS `k20`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-21')) > 4 OR CONCAT(:yea,'-',:mon,'-21') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-21') not in (select date_work from works_day) then null
when :v21=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-21')  and rtrim(propuskS) = 'H')) end AS `k21`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-22')) > 4 OR CONCAT(:yea,'-',:mon,'-22') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-22') not in (select date_work from works_day) then null
when :v22=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-22')  and rtrim(propuskS) = 'H')) end AS `k22`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-23')) > 4 OR CONCAT(:yea,'-',:mon,'-23') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-23') not in (select date_work from works_day) then null
when :v23=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-23')  and rtrim(propuskS) = 'H')) end AS `k23`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-24')) > 4 OR CONCAT(:yea,'-',:mon,'-24') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-24') not in (select date_work from works_day) then null
when :v24=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-24')  and rtrim(propuskS) = 'H')) end AS `k24`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-25')) > 4 OR CONCAT(:yea,'-',:mon,'-25') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-25') not in (select date_work from works_day) then null
when :v25=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-25')  and rtrim(propuskS) = 'H')) end AS `k25`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-26')) > 4 OR CONCAT(:yea,'-',:mon,'-26') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-26') not in (select date_work from works_day) then null
when :v26=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-26')  and rtrim(propuskS) = 'H')) end AS `k26`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-27')) > 4 OR CONCAT(:yea,'-',:mon,'-27') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-27') not in (select date_work from works_day) then null
when :v27=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-27')  and rtrim(propuskS) = 'H')) end AS `k27`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-28')) > 4 OR CONCAT(:yea,'-',:mon,'-28') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-28') not in (select date_work from works_day) then null
when :v28=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-28')  and rtrim(propuskS) = 'H')) end AS `k28`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-29')) > 4 OR CONCAT(:yea,'-',:mon,'-29') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-29') not in (select date_work from works_day) then null
when :v29=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-29')  and rtrim(propuskS) = 'H')) end AS `k29`,

case
when (weekday(CONCAT(:yea,'-',:mon,'-30')) > 4 OR CONCAT(:yea,'-',:mon,'-30') IN (select date_hol from holidays)) 
AND CONCAT(:yea,'-',:mon,'-30') not in (select date_work from works_day) then null
when :v30=0 then 0
ELSE (SELECT COUNT(*) from sotrudniki WHERE 
(((:mon>=MONTH(`inS`) and :yea=year(`inS`)) or :yea>year(`inS`))
AND ((:mon<=month(`outS`) and :yea<=year(`outS`)) or :yea<year(`outS`)) or `outS` is NULL) and pitanie like '%пит%' AND sotrudniki.id NOT IN (SELECT id FROM gogos WHERE dateS = CONCAT(:yea,'-',:mon,'-30')  and rtrim(propuskS) = 'H')) end AS `k30`,0 AS `k31`)j




      