SELECT w.*,
case
when w.progress1 like '%Отрицат%' AND w.progress like '%Положит%' then 'B!'
else ''
END AS `viraj`
from (
SELECT 
id_child,`name`,rozd,
`year4`,`year4_2`,`dat4`,`coment4`,
case
when year4_2 = 0 and year4_2 != '' then 'Отрицат.'
when (year4_2 > 0 and year4_2 < 5) or year4 like 'ar%' then 'Сомнит.'
when year4_2 > 4 and year4_2 < 17 and year4 like 'p%' then 'Положит.'
when year4_2 > 16 and year4 like 'p%' then 'Гипер.'
ELSE '' END AS `progress4`,

`year3`,`year3_2`,`dat3`,`coment3`,
case
when year3_2 = 0 and year3_2 != '' then 'Отрицат.'
when (year3_2 > 0 and year3_2 < 5) or year3 like 'ar%' then 'Сомнит.'
when year3_2 > 3 and year3_2 < 17 and year3 like 'p%' then 'Положит.'
when year3_2 > 16 and year3 like 'p%' then 'Гипер.'
ELSE '' END AS `progress3`,

`year2`,`year2_2`,`dat2`,`coment2`,
case
when year2_2 = 0 and year2_2 != '' then 'Отрицат.'
when (year2_2 > 0 and year2_2 < 5) or year2 like 'ar%' then 'Сомнит.'
when year2_2 > 2 and year2_2 < 17 and year2 like 'p%' then 'Положит.'
when year2_2 > 16 and year2 like 'p%' then 'Гипер.'
ELSE '' END AS `progress2`,

`year1`,`year1_2`,`dat1`,`coment1`,
case
when year1_2 = 0 and year1_2 != '' then 'Отрицат.'
when (year1_2 > 0 and year1_2 < 5) or year1 like 'ar%' then 'Сомнит.'
when year1_2 > 1 and year1_2 < 17 and year1 like 'p%' then 'Положит.'
when year1_2 > 16 and year1 like 'p%' then 'Гипер.'
ELSE '' END AS `progress1`,

`currentyear1`,`currentyear1_2`,`dat`,`coment`,
case
when currentyear1_2 = 0 and currentyear1_2 != '' then 'Отрицат.'
when (currentyear1_2 > 0 and currentyear1_2 < 5) or currentyear1 like 'ar%' then 'Сомнит.'
when currentyear1_2 > 4 and currentyear1_2 < 17 and currentyear1 like 'p%' then 'Положит.'
when currentyear1_2 > 16 and currentyear1 like 'p%' then 'Гипер.'
ELSE '' END AS `progress`,

`med`,`medotvod`

FROM (
SELECT 
a.id AS `id_child`,
rtrim(a.`name`) AS `name`,`rozd`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),(SELECT rtrim(el1) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),'') AS `year4`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),(SELECT rtrim(el2) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),'') AS `year4_2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),(SELECT dat FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),'1900-01-01') AS `dat4`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),(SELECT rtrim(coment) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-4),'') AS `coment4`,

if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),(SELECT rtrim(el1) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),'') AS `year3`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),(SELECT rtrim(el2) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),'') AS `year3_2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),(SELECT dat FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),'1900-01-01') AS `dat3`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),(SELECT rtrim(coment) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-3),'') AS `coment3`,

if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),(SELECT rtrim(el1) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),'') AS `year2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),(SELECT rtrim(el2) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),'') AS `year2_2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),(SELECT dat FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),'1900-01-01') AS `dat2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),(SELECT rtrim(coment) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-2),'') AS `coment2`,

if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),(SELECT rtrim(el1) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),'') AS `year1`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),(SELECT rtrim(el2) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),'') AS `year1_2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),(SELECT dat FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),'1900-01-01') AS `dat1`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),(SELECT rtrim(coment) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())-1),'') AS `coment1`,

if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),(SELECT rtrim(el1) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),'') AS `currentyear1`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),(SELECT rtrim(el2) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),'') AS `currentyear1_2`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),(SELECT dat FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),'1900-01-01') AS `dat`,
if((SELECT year(max(dat)) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),(SELECT rtrim(coment) FROM mantu WHERE id_child = a.id AND YEAR(dat)=YEAR(NOW())),'') AS `coment`,

(SELECT MAX(date_inj) FROM injection WHERE id_child = a.id) AS `med`,

case
when DATE_ADD((SELECT MAX(date_inj) FROM injection WHERE id_child = a.id),INTERVAL 30 DAY) > NOW() 
then concat(DATE_FORMAT(DATE_ADD((SELECT MAX(date_inj) FROM injection WHERE id_child = a.id),INTERVAL 30 DAY),'%d.%m.%Y'),' (прививка)')

when (SELECT MAX(medotvod) FROM gogo WHERE gogo.id_child = a.id) > NOW()
then CONCAT((DATE_FORMAT((SELECT MAX(medotvod) FROM gogo WHERE id_child = a.id),'%d.%m.%Y')),' (заболевание)')


when (SELECT MAX(date_sled_yavki) FROM mantu_n WHERE mantu_n.id_child = a.id AND status_ptd = 1) = (SELECT MAX(date_to) FROM mantu_n WHERE mantu_n.id_child = a.id AND status_ptd = 0)
then CONCAT(DATE_FORMAT((SELECT MAX(date_to) FROM mantu_n WHERE mantu_n.id_child = a.id),'%d.%m.%Y'),' (учёт в ОПТД)')

ELSE ''
end AS `medotvod`

FROM (SELECT * from deti WHERE `out` is NULL)a WHERE a.id_gruppa = :id_group)m)w
ORDER BY w.`name`

