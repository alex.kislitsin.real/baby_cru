select * from (
select deti.id,rtrim(deti.NAME) AS `name`,gruppa.id AS `id_gruppa`,rtrim(gruppa.NAME) AS `namegr`,rtrim(deti.alergia) AS `alergia`, 
case when deti.id not in (select id_child AS `id` from 
gogo where datenotgo = :dat and gogo.id not in (100,200)) then 1 else 0 end as today,
case when deti.id in (select id_child`id` from 
gogo where datenotgo = :dat and gogo.id = 200) then 1 else 0 END as notnot,
case when deti.id in (select id_child`id` from 
gogo where datenotgo = :dat and gogo.id = 100) then 1 else 0 END as opozd 
from deti left outer join gruppa on deti.id_gruppa = gruppa.id 
where length(rtrim(alergia)) > 0 AND `out` is null order by deti.name)d where d.today > 0