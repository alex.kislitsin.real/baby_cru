
SELECT a.id as `id_child`,a.`name`,a.`rozd`,
IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=1),'1900-01-01') AS `bcg`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=1 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=1)),0) AS `v_bcg`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=1 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=1)),0) AS `r_bcg`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=1 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=1)),'1900-01-01') AS `napravlen_bcg`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=1 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=1)),0) AS `otkaz_bcg`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=2),'1900-01-01') AS `opv`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=2 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=2)),0) AS `v_opv`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=2 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=2)),0) AS `r_opv`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=2 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=2)),'1900-01-01') AS `napravlen_opv`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=2 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=2)),0) AS `otkaz_opv`,


IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=3),'1900-01-01') AS `akds`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=3 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=3)),0) AS `v_akds`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=3 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=3)),0) AS `r_akds`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=3 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=3)),'1900-01-01') AS `napravlen_akds`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=3 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=3)),0) AS `otkaz_akds`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=4),'1900-01-01') AS `gepatit_b`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=4 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=4)),0) AS `v_gepatit_b`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=4 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=4)),0) AS `r_gepatit_b`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=4 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=4)),'1900-01-01') AS `napravlen_gepatit_b`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=4 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=4)),0) AS `otkaz_gepatit_b`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=5),'1900-01-01') AS `kor`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=5 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=5)),0) AS `v_kor`,
IFNULL((SELECT distinct(r) from injection where id_child=a.id and id_inj=5 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=5)),0) AS `r_kor`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=5 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=5)),'1900-01-01') AS `napravlen_kor`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=5 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=5)),0) AS `otkaz_kor`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=6),'1900-01-01') AS `red`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=6 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=6)),0) AS `v_red`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=6 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=6)),0) AS `r_red`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=6 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=6)),'1900-01-01') AS `napravlen_red`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=6 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=6)),0) AS `otkaz_red`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=7),'1900-01-01') AS `parotit`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=7 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=7)),0) AS `v_parotit`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=7 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=7)),0) AS `r_parotit`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=7 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=7)),'1900-01-01') AS `napravlen_parotit`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=7 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=7)),0) AS `otkaz_parotit`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=8),'1900-01-01') AS `gomofil`,
IFNULL((SELECT distinct(v) from injection where id_child=a.id and id_inj=8 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=8)),0) AS `v_gomofil`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=8 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=8)),0) AS `r_gomofil`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=8 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=8)),'1900-01-01') AS `napravlen_gomofil`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=8 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=8)),0) AS `otkaz_gomofil`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=9),'1900-01-01') AS `prevenar`,
IFNULL((select distinct(v) from injection where id_child=a.id and id_inj=9 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=9)),0) AS `v_prevenar`,
IFNULL((select distinct(r) from injection where id_child=a.id and id_inj=9 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=9)),0) AS `r_prevenar`,
IFNULL((select distinct(napravlen) from injection where id_child=a.id and id_inj=9 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=9)),'1900-01-01') AS `napravlen_prevenar`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=9 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=9)),0) AS `otkaz_prevenar`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=10),'1900-01-01') AS `gripp`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=10 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=10)),0) AS `otkaz_gripp`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=11),'1900-01-01') AS `encefalit`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=11 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=11)),0) AS `otkaz_encefalit`,

IFNULL((select max(date_inj) from injection where id_child=a.id and id_inj=12),'1900-01-01') AS `vo`,
IFNULL((select distinct(otkaz) from injection where id_child=a.id and id_inj=12 and date_inj = (select max(date_inj) from injection where id_child=a.id and id_inj=12)),0) AS `otkaz_vo`

FROM (SELECT id,`name`,`rozd`,case when LAST_DAY(CONCAT(:yea,'-',:mon,'-04')) < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where 
((:mon>=MONTH(`in`) and :yea=year(`in`)) or :yea>year(`in`))
and (((:mon<=month(`out`) and :yea<=year(`out`)) or :yea<year(`out`)) or `out` is NULL))a WHERE a.id_gruppa = :id_group