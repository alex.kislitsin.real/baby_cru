
SELECT t.*, 
(t.kolvse-t.snyati)AS`stoyat`


FROM (
SELECT g.*, 
(SELECT COUNT(*) FROM (
SELECT id,`name`,case when :dat < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id) AS `kolvse`,

(SELECT COUNT(*) FROM gogo WHERE datenotgo = :dat and gogo.id not in (100,200) AND id_child IN ((SELECT d.id FROM (
SELECT id,`name`,case when :dat < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id))) AS `snyati`,

(SELECT COUNT(*) FROM gogo WHERE datenotgo = :dat and gogo.id = 4 AND id_child IN ((SELECT d.id FROM (
SELECT id,`name`,case when :dat < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id))) AS `orz`,

(SELECT COUNT(*) FROM gogo WHERE datenotgo = :dat and gogo.id NOT IN (0,100,200) AND id_child IN ((SELECT d.id FROM (
SELECT id,`name`,case when :dat < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id))) AS `bolezni`,

(SELECT COUNT(*) FROM gogo WHERE datenotgo = :dat and gogo.id = 0 AND id_child IN ((SELECT d.id FROM (
SELECT id,`name`,case when :dat < perevod AND old_id_gruppa > 0 then old_id_gruppa ELSE id_gruppa end as id_gruppa from deti where ((MONTH(:dat)>=MONTH(`in`) and YEAR(:dat)=year(`in`)) or YEAR(:dat)>year(`in`))
and (((MONTH(:dat)<=month(`out`) and YEAR(:dat)<=year(`out`)) or YEAR(:dat)<year(`out`)) or `out` is NULL))d WHERE d.id_gruppa = g.id))) AS `prochee`

FROM (
SELECT id,RTRIM(`name`) AS `name` FROM gruppa)g)t
