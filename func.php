<?php

use yii\helpers\ArrayHelper;

function getColumnLetter( $number ){
    $prefix = '';
    $suffix = '';
    $prefNum = intval( $number/26 );
    if( $number > 25 ){
        $prefix = getColumnLetter( $prefNum - 1 );
    }
    $suffix = chr( fmod( $number, 26 )+65 );
    return $prefix.$suffix;
}

function show_print($arr){
    echo '<pre>' . print_r($arr,true) . '</pre>';
}

function debug($arr){
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

function cl_print_r ($var, $label = '')
{
    $str = json_encode(print_r ($var, true));
    echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
}

function change_db_attr()
{
    /*$var = Yii::$app->request->cookies->getValue('name_base');
    $var = preg_replace('/[^A-Za-z0-9_]/','',$var);
    Yii::$app->db->close();
    Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=$var";
    Yii::$app->db->username=$var;*/
}

function change_db_attr_read_only()
{
    /*$var = Yii::$app->request->cookies->getValue('name_base');
    $var = preg_replace('/[^A-Za-z0-9_]/','',$var);
    $var2 = "u0601128_434535read";
    Yii::$app->db->close();
    Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=$var";
    Yii::$app->db->username=$var2;*/
}

function connect_to_db($var){
    /*Yii::$app->db->close();
    Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=$var";
    Yii::$app->db->username=$var;*/
}

function verification_user(){
//    $var_name_base = Yii::$app->request->cookies->getValue('user');
    return Yii::$app->request->cookies->getValue('user');
//    return $var;
}

function vul($d){
    $name_base = Yii::$app->request->cookies->getValue('name_base');
    $us = Yii::$app->request->cookies->getValue('user');
    $id_u = Yii::$app->request->cookies->getValue('id_user');
    if(empty($name_base)||empty($us)||empty($id_u)){
        return $d->redirect(['site/login']);
    }
}

function verifi_date_today($d,$model_d_date,$model_d_antidate){                 //в параметрах передаем выбранную дату и массив с праздниками из базы

    static $last_work_todaymonth,$last_work_todaymonth1,$last_work_todaymonth2,$work_day_after_today,$last_work_todaymonth3,$last_work_todaymonth4;

    $day_tabel = Yii::$app->request->cookies->getValue('day_tabel');
    !isset($day_tabel) ? $day_tabel = 3 : null;

    $today = date("Y-m-d");                     //сегодня
    $hour = date('H');                          //текущий час
    $month_item = date('n', strtotime($d));     //месяц выбранной даты
    $month_today = date("n");                   //месяц сегодня (без нуля в начале)
    $year_item = date('Y', strtotime($d));     //год выбранной даты
    $year_today = date("Y");                   //год сегодня (4 цифры)
    $day_item_week = date('N', strtotime($d));//день на неделе выбранной даты
    $day_today_week = date('N');//день на неделе сегодняшней даты
    $count_days = date('t');//***
    $last_day = date('Y-m-'.$count_days);//***
    $first_day = date('Y-m-01');//***
    $iter_date = $last_day;//***

    if($d < $today)return 0;                    //нельзя ставить отметку
    if($d == $today && $hour < 12)return 1;     //можно ставить сегодняшнюю отметку
    if($d == $today && $hour >= 12)return 0;    //нельзя ставить сегодняшнюю отметку

    switch($day_tabel){
        case 2:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            /*if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth && ($today == $last_work_todaymonth || ($today == $last_work_todaymonth1 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 && ($today == $last_work_todaymonth1 && $hour >= 12))return 4;//табель закрыт
                }
            }*/
            break;
        case 3:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth && ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || ($today == $last_work_todaymonth2 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 && ($today == $last_work_todaymonth1 || ($today == $last_work_todaymonth2 && $hour >= 12)))return 4;//табель закрыт
                }
            }
            break;
        case 4:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth3 = $iter_date;//получаем 4 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth &&
                        ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 &&
                        ($today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth2 &&
                        ($today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт

                }
            }
            break;
        case 5:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth3 = $iter_date;//получаем 4 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth4 = $iter_date;//получаем 5 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth &&
                        ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 &&
                        ($today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth2 &&
                        ($today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth3 &&
                        ($today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                }
            }
            break;

    }





    $iter_date = date('Y-m-d',strtotime($d. " - 1 day"));
    while($iter_date >= $today){
        if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
            $work_day_before_item_date = $iter_date;//получаем ближайший рабочий день до выбранной даты
            if($d > $today){
                if($today == $work_day_before_item_date && $hour >= 12)return 0;
            }
            break;
        }else{
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
        }
    }

    if(((date('N', strtotime($today)) > 5) || (in_array($today,$model_d_date))) && !in_array($iter_date,$model_d_antidate)){
        $iter_date = date('Y-m-d',strtotime($today. " 1 day"));
        while(true){
            if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                $work_day_after_today = $iter_date;//получаем ближайший рабочий день после сегодня
                break;
            }else{
                $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
            }
        }
        if($d == $work_day_after_today)return 0;
    }

    return 999;
}

function verifi_date_five($d,$model_d_date,$model_d_antidate){                 //в параметрах передаем выбранную дату и массив с праздниками из базы

    static $last_work_todaymonth,$last_work_todaymonth1,$last_work_todaymonth2,$work_day_after_today,$last_work_todaymonth3,$last_work_todaymonth4;

    $day_tabel = Yii::$app->request->cookies->getValue('day_tabel');
    !isset($day_tabel) ? $day_tabel = 3 : null;

    $today = date("Y-m-d");                     //сегодня
    $hour = date('H');                          //текущий час
    $month_item = date('n', strtotime($d));     //месяц выбранной даты
    $month_today = date("n");                   //месяц сегодня (без нуля в начале)
    $count_days = date('t');//***
    $last_day = date('Y-m-'.$count_days);//***
    $first_day = date('Y-m-01');//***
    $iter_date = $last_day;//***

    switch($day_tabel){
        case 2:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            break;
        case 3:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth && ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || ($today == $last_work_todaymonth2 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 && ($today == $last_work_todaymonth1 || ($today == $last_work_todaymonth2 && $hour >= 12)))return 4;//табель закрыт
                }
            }
            break;
        case 4:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth3 = $iter_date;//получаем 4 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth &&
                        ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 &&
                        ($today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth2 &&
                        ($today == $last_work_todaymonth2 || ($today == $last_work_todaymonth3 && $hour >= 12)))return 4;//табель закрыт

                }
            }
            break;
        case 5:
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth = $iter_date;//получаем последний рабочий день в текущем месяце
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth1 = $iter_date;//получаем 2 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth2 = $iter_date;//получаем 3 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth3 = $iter_date;//получаем 4 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
            while($iter_date >= $first_day){
                if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                    $last_work_todaymonth4 = $iter_date;//получаем 5 рабочий день в текущем месяце с конца
                    break;
                }else{
                    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
                }
            }

            if($d >= $today){
                if($month_item == $month_today){
                    if($d == $last_work_todaymonth &&
                        ($today == $last_work_todaymonth || $today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth1 &&
                        ($today == $last_work_todaymonth1 || $today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth2 &&
                        ($today == $last_work_todaymonth2 || $today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                    if($d == $last_work_todaymonth3 &&
                        ($today == $last_work_todaymonth3 || ($today == $last_work_todaymonth4 && $hour >= 12)))return 4;//табель закрыт
                }
            }
            break;

    }





    $iter_date = date('Y-m-d',strtotime($d. " - 1 day"));
    while($iter_date >= $today){
        if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
            $work_day_before_item_date = $iter_date;//получаем ближайший рабочий день до выбранной даты
            if($d > $today){
                if($today == $work_day_before_item_date && $hour >= 12)return 0;
            }
            break;
        }else{
            $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
        }
    }

    if(((date('N', strtotime($today)) > 5) || (in_array($today,$model_d_date))) && !in_array($iter_date,$model_d_antidate)){
        $iter_date = date('Y-m-d',strtotime($today. " 1 day"));
        while(true){
            if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                $work_day_after_today = $iter_date;//получаем ближайший рабочий день после сегодня
                break;
            }else{
                $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
            }
        }
        if($d == $work_day_after_today)return 0;
    }

    return 1;
}

function verifi_date_today_med($d){
    $today = date("Y-m-d");                     //сегодня
    $hour = date('H');                          //текущий час
    $month_item = date('n', strtotime($d));     //месяц выбранной даты
    $month_today = date("n");                   //месяц сегодня (без нуля в начале)
    $year_item = date('Y', strtotime($d));     //год выбранной даты
    $year_today = date("Y");                   //год сегодня (4 цифры)
    $day_item_week = date('N', strtotime($d));//день на неделе выбранной даты
    $day_today_week = date('N');//день на неделе сегодняшней даты

    if(($month_item < $month_today && $year_item == $year_today) || $year_item < $year_today)return 3;//нельзя ставить отметку для медсестры

    return 555;
}

function rdate($param, $time=0) {
    if(intval($time)==0)$time=time();
    $MonthNames=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
    if(strpos($param,'M')===false) return date($param, $time);
    else return date(str_replace('M',$MonthNames[date('n',$time)-1],$param), $time);
}

function get_dol(){
    return $dol = ["Заведующий",
        "Ст.воспитатель",
        "Ст.медсестра",
        "Медсестра",
        "Делопроизводитель",
        "Воспитатель",
        "Пом. воспитателя",
        "Пом. воспитателя (вечерний)",
        "Муз. руководитель",
        "Слесарь-сантехник",
        "Повар",
        "Шеф-повар",
        "Зам.зав.по АХЧ",
        "Дворник",
        "Машинист по ремонту и стирке",
        "Зав.складом",
        "Сторож-вахтёр",
        "Кухонный рабочий",
        "Рабочий по комплексному обслуживанию здания",
        "Зав.хозяйством",
        "Педагог-психолог",
        "Логопед",
        "Инструктор по физ.культуре","Платные услуги"];

    /*$dol = [0 => "Заведующий",
              1 => "Ст.воспитатель",
              2 => "Ст.медсестра",
              3 => "Медсестра",
              4 => "Делопроизводитель",
              5 => "Воспитатель",
              6 => "Пом. воспитателя",
              7 => "Пом. воспитателя (вечерний)",
              8 => "Муз. руководитель",
              9 => "Слесарь-сантехник",
              10 => "Повар",
              11 => "Шеф-повар",
              12 => "Зам.зав.по АХЧ",
              13 => "Дворник",
              14 => "Машинист по ремонту и стирке",
              15 => "Зав.складом",
              16 => "Сторож-вахтёр",
              17 => "Кухонный рабочий",
              18 => "Рабочий по комплексному обслуживанию здания",
              19 => "Зав.хозяйством",
              20 => "Педагог-психолог",
              21 => "Логопед",
              22 => "Инструктор по физ.культуре"];*/
}

function getgroup(){

    $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);

    if (empty($array_gruppa)){
        change_db_attr();
        $query_gruppa = "select * from gruppa order by id";
        $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
        $array_gruppa = ArrayHelper::index($array_gruppa,'id');
        $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

        $json_array_group = json_encode($array_gruppa);
        $cookie = new \yii\web\Cookie([
            'name' => 'array_group',
            'value' => $json_array_group,
        ]);
        Yii::$app->response->cookies->add($cookie);
    }

    return $array_gruppa;
}

function show_layout_light($t){
    if (verification_user()!=555){
        $t->layout = '@app/views/layouts/main_v';
    }
}
function redirect_dostup($t){
    switch(verification_user()){
        case 1:
            return $t->redirect(['sp/spview']);
            break;
        case 2:
            return $t->redirect(['rod/indexrod']);
            break;
        case 1425:
            return $t->redirect(['crujki/cru']);
            break;
    }
}

function out_is_null($t){
    change_db_attr();
    $id_u = Yii::$app->request->cookies->getValue('id_user');
    $us = Yii::$app->request->cookies->getValue('user');
    if ($us==2){
        $query = 'select isnull(year([out]),0)[out] from deti where id=:id';
    }else{
        $query = 'select isnull(year([outS]),0)[out] from sotrudniki where id=:id';
    }
    $value = Yii::$app->db->createCommand($query,['id' => $id_u])->queryOne();
    if ($value['out'] > 0){
        return $t->redirect(['site/index']);
    }
}

function num2str($num) {
    $nul='ноль';
    $ten=array(
        array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
        array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
    );
    $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
    $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
    $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
    $unit=array( // Units
        array('копейка' ,'копейки' ,'копеек',	 1),
        array('рубль'   ,'рубля'   ,'рублей'    ,0),
        array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
        array('миллион' ,'миллиона','миллионов' ,0),
        array('миллиард','милиарда','миллиардов',0),
    );
    //
    list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
    $out = array();
    if (intval($rub)>0) {
        foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
            if (!intval($v)) continue;
            $uk = sizeof($unit)-$uk-1; // unit key
            $gender = $unit[$uk][3];
            list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
            // mega-logic
            $out[] = $hundred[$i1]; # 1xx-9xx
            if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
            else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
            // units without rub & kop
            if ($uk>1) $out[]= morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
        } //foreach
    }
    else $out[] = $nul;
    $out[] = morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
    $out[] = $kop.' '.morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
    return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
}

/**
 * Склоняем словоформу
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5) {
    $n = abs(intval($n)) % 100;
    if ($n>10 && $n<20) return $f5;
    $n = $n % 10;
    if ($n>1 && $n<5) return $f2;
    if ($n==1) return $f1;
    return $f5;
}

function insert_data($value){
    if (Yii::$app->request->userIP != '127.0.0.1' && Yii::$app->request->userIP != '178.141.182.27' ){
        $var = 'u0601128_baby';
        Yii::$app->db->close();
        Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=$var";
        Yii::$app->db->username=$var;
        $query_insert = "insert into count_people (ipo,name_page) values (:ipo,:name_page)";
        Yii::$app->db->createCommand($query_insert,['ipo' => Yii::$app->request->userIP,'name_page' => $value,])->execute();
    }
}

function counter_peoples(){
    $var = 'u0601128_baby';
    Yii::$app->db->close();
    Yii::$app->db->dsn="sqlsrv:server=31.31.196.80;Database=$var";
    Yii::$app->db->username=$var;
    $i = Yii::$app->request->userIP;
    $id = Yii::$app->db->createCommand("select isnull((select distinct(max(id))[id] from count_people where ipo like :ipo),0)[id]",['ipo' => $i])->queryOne();
//    print $id['id'];
//    return;
    if ($id['id'] == 0){
        $count = Yii::$app->db->createCommand("select count(distinct(ipo))[count] from count_people")->queryOne();
        $count = $count['count'] + 1;
    }else{
        $count = 0;
    }
    return $count;
}


function add_month($time) {
    $d=date('j',$time);  // день
    $m=date('n',$time);  // месяц
    $y=date('Y',$time);  // год

    // Прибавить месяц
    $m++;
    if ($m>12) { $y++; $m=1; }

    // Это последний день месяца?
    if ($d==date('t',$time)) {
        $d=31;
    }
    // Открутить дату до последнего дня месяца
    if (!checkdate($m,$d,$y)){
        $d=date('t',mktime(0,0,0,$m,1,$y));
    }
    // Вернуть новую дату в TIMESTAMP
    return mktime(0,0,0,$m,$d,$y);
}


function checking($t){
    /*$date = Yii::$app->request->cookies->getValue('check_date');
    if (!empty($date)){
        $date = date('Y-m-d',strtotime($date));
        if ($date != date('Y-m-d')){
            $cookie = new \yii\web\Cookie([
                'name' => 'check_date',
                'value' => date('Y-m-d'),
                'expire' => time() + 3600 * 24 * 365
            ]);
            Yii::$app->response->cookies->add($cookie);
            return $t->redirect(['site/login']);
        }
    }else{
        $cookie = new \yii\web\Cookie([
            'name' => 'check_date',
            'value' => date('Y-m-d'),
            'expire' => time() + 3600 * 24 * 365
        ]);
        Yii::$app->response->cookies->add($cookie);
    }*/
}

function checking_table_deti(){
    return;

    $current_year = date('Y');
    $current_month = date('n');
    if ($current_month != 1){
        $current_month = $current_month - 1;
        $i_create_table_deti = 'deti'.$current_month.$current_year;
        if (Yii::$app->request->cookies->getValue($i_create_table_deti)!=1){
//            $query = "IF OBJECT_ID('$i_create_table_deti','U') IS NULL begin select * into $i_create_table_deti from deti; end;";
            $query = "CREATE TABLE IF NOT EXISTS $i_create_table_deti SELECT * FROM `deti`;";
            Yii::$app->db->createCommand($query)->execute();
            $cookie = new \yii\web\Cookie([
                'name' => $i_create_table_deti,
                'value' => 1,
                'expire' => time() + 3600 * 24 * 365
            ]);
            Yii::$app->response->cookies->add($cookie);
        }
    }elseif($current_month == 1){
        $current_month = 12;
        $current_year = $current_year - 1;
        $i_create_table_deti = 'deti'.$current_month.$current_year;
        if (Yii::$app->request->cookies->getValue($i_create_table_deti)!=1){
            $query = "CREATE TABLE IF NOT EXISTS $i_create_table_deti SELECT * FROM `deti`;";
            Yii::$app->db->createCommand($query)->execute();
            $cookie = new \yii\web\Cookie([
                'name' => $i_create_table_deti,
                'value' => 1,
                'expire' => time() + 3600 * 24 * 365
            ]);
            Yii::$app->response->cookies->add($cookie);
        }
    }
}