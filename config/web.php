<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mydb = require __DIR__ . '/mydb.php';
$mail = require __DIR__ . '/mail.php';

$config = [

    'id' => 'basic',
    'basePath' => dirname(__DIR__),
//    'defaultRoute' => 'injection/inj',
//    'defaultRoute' => 'start/system',
    'defaultRoute' => 'site/login',
//    'defaultRoute' => 'sp/spview',
//    'homeUrl' => '[sp/spview]',
    'bootstrap' => ['log'],
    //'layout'=>'basic',
    'language' => 'ru-Ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [

        'mobileDetect' => [
            'class' => '\skeeks\yii2\mobiledetect\MobileDetect'
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gdfghdfghfghhfgh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['pochtovi2007@yandex.ru' => 'pochtovi2007'],
            ],
            'transport' => $mail,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'mydb' => $mydb,
//        'dby' => [
//            'class' => 'yii\db\Connection',
//            'dsn' => "sqlsrv:server=31.31.196.80;Database=u0601128_43453500",
//            'username' => 'u0601128_434535',
//            'password' => '0000a!',
//            'charset' => 'utf8',
//        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'defaultTimeZone' => 'Europe/Moscow',
//            'timeZone' => 'GMT+3',
            'dateFormat'     => 'php:d.m.Y',
//            'date2Format'     => 'php:Y-m-d',
            'datetimeFormat' => 'php:d.m.Y  [H:i:s]',
            'timeFormat'     => 'php:Y-m-d',
//            'timeFormat'     => 'php:H:i:s',
        ],

        /*'urlManager' => [
            'enablePrettyUrl' => TRUE,
            'showScriptName' => FALSE,
//            'enableStrictParsing' => TRUE,
            'rules' => [
                '' => 'sp/spview',
                '<action>' => 'sp/<action>',
//                '<action>' => 'site/<action>',
//                '' => '/index.php?r=sp/spview',
//                '' => '/index.php?r=sp%2Fspview'
            ],
        ],*/
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => '6LdCa8kUAAAAAEGgYwKomIAMVyZqDBUsWUM2q5mj',
            'secretV2' => '6LdCa8kUAAAAAEuHC7OaU_QrBCNRGeNKIDJtEoJ_',
//            'siteKeyV3' => 'your siteKey v3',
//            'secretV3' => 'your secret key v3',
        ],
//        'yiimorphy' => [
//            'class' => 'maxodrom\phpmorphy\components\YiiMorphy',
//             'language' => 'ru', // or 'uk', or 'de'
//            // 'options' => [], // your options which will be passed to \phpMorphy's constructor
//        ],
//        'session' => [
//            'class' => 'yii\web\Session',
//            'cookieParams' => ['lifetime' => 5],
////            'cookieParams' => ['lifetime' => 3600*24*30*12],
//            'timeout' => 5,
////            'timeout' => 3600*24*30*12,
//            'useCookies' => true,
//        ],


    ],

    'params' => $params,
];

if (!true) {
    // configuration adjustments for 'dev' environment

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    /*$config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
//        // uncomment the following to add your IP if you are not connecting from localhost.
//        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];*/

}

return $config;
