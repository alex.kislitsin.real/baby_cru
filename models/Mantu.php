<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Mantu extends Model
{
    public $id_child;//id ребенка
    public $name;
    public $dat;//дата отметки
    public $ye;//инфильтрат
    public $yea;//размер
    public $coment;
    public $year;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['dat'], 'required','message' => false],
            [['id_child','year'], 'integer'],
//            [['number_schet','old_id_gruppa'], 'default','value' => 0],
//            [['ye'],'in','range' => ["ar","p","отр"]],
            [['dat'], 'date','format' => 'php:d.m.Y','message' => false],
            [['dat'], 'default','value' => ''],

            [['ye','yea','coment'], 'trim'],
            [['ye','yea','coment'], 'string'],
            [['ye','yea','coment'],'default','value' => ''],

            ['name','safe'],

            [['coment'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    return $result;
                }],

            [['id_child','year'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],

            [['yea'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9\s]/','',$value);
                    return $result;
                }],

            [['dat'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],





        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя',
            'dat' => 'Дата проведения',
            'ye' => 'Инфильтрат',
            'yea' => 'Размер, мм',
            'coment' => 'Примечание',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
