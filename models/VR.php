<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 17.11.19
 * Time: 14:06
 */

namespace app\models;


use yii\base\Model;
use Yii;

class VR extends Model{

    public $v1;
    public $v2;
    public $v3;
    public $r1;
    public $r2;
    public $r3;

    public function rules(){
        return [
//            [['v1','r1','v2','r2','v3','r3'],'safe'],
            [['v1','r1','v2','r2','v3','r3'],'integer'],
            [['v1','r1','v2','r2','v3','r3'], 'default','value' => 0],
        ];
    }

    public function attributeLabels(){
        return [
            'v1' => 'V',
            'r1' => 'R',
        ];
    }

}

