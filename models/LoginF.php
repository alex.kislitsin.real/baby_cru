<?php

namespace app\models;

use Yii;
use yii\base\Model;

class LoginF extends Model
{
    public $region;
    public $city;
    public $sad;
    public $snils;
    public $polis;
    public $captcha;

    public function attributeLabels(){
        return [
            'region' => 'Область',
            'city' => 'Город',
            'sad' => 'Номер детского сада',
            'snils' => 'Снилс',
            'polis' => 'Полис',
            'captcha' => 'Введите проверочный код',
        ];
    }

    public function rules()
    {
        return [
            [['region', 'city','sad','snils','polis','captcha'], 'required','message' => false],
//            ['region','safe'],
//            ['city','safe'],
//            ['sad','safe'],
//            ['snils','safe'],
//            ['polis','safe'],
            [['region', 'city','sad'], 'integer'],
            [['region', 'city','sad'], 'default','value' => 0],
//            [['region', 'city','sad'], 'string'],
//            [['region', 'city','sad'], 'default','value' => ''],


            ['captcha', 'captcha'],
            [['captcha','region','city','sad'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],
            [['snils'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9\s\-]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],
            [['polis'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9\s]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],

        ];
    }



}
