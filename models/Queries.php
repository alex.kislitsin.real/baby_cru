<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Queries extends Model
{
    public function rules()
    {
        return [];
    }



    public function show_sp($date,$id_group){
        $id_group = preg_replace('/[^0-9]/','',$id_group);
        $date = Yii::$app->formatter->asTime($date);
        $file_query = dirname(__DIR__, 2).'\test\query\ideal_pit_deti_03032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'date_in' => $date,
            'id_group' => $id_group,
        ])->queryAll();
        return $array;
    }

    public function show_sp_calendar($id,$date){
        $id = preg_replace('/[^0-9]/','',$id);
        $date = Yii::$app->formatter->asTime($date);
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $query = "select gogo.*,reason.NAME AS reason from gogo LEFT OUTER JOIN reason ON if(sovsem > 0, sovsem=reason.id, gogo.id=reason.id)  where id_child =:id and month(datenotgo)=:mon and year(datenotgo)=:yea;";
        $array = Yii::$app->db->createCommand($query,[
                'id' => $id,
                'mon' => $month,
                'yea' => $year,
            ]
        )->queryAll();
        return $array;
    }




    public function show_sp_so($date){
        $date = Yii::$app->formatter->asTime($date);
        $file_query = dirname(__DIR__, 2).'\test\query\ideal_pit_so_03032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'date_in' => $date,
        ])->queryAll();
        return $array;
    }

    public function show_sp_antro($period,$year,$id_group){

        switch ($period){
            case 1:
                $file_query = dirname(__DIR__, 2).'\test\query\antro_spisok_spring_04032020.sql';
                break;
            case 2:
                $file_query = dirname(__DIR__, 2).'\test\query\antro_spisok_otem_04032020.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'year' => $year,
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_sp_antro_mebel($period,$year){

        switch ($period){
            case 1:
                $m1 = 1;
                $m2 = 6;
                break;
            case 2:
                $m1 = 7;
                $m2 = 12;
                break;
        }
        $file_query = dirname(__DIR__, 2).'\test\query\antro_mebel_04032020.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'm1' => $m1,
            'm2' => $m2
        ])->queryAll();
        return $array;
    }

    public function show_tabel_deti($year,$month,$id_group){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_28.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_29.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_30.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_31.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }


    public function show_tabel_so($year,$month){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_28_so.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_29_so.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_30_so.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_works_31_so.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_spravka($year,$month){

        $transaction = Yii::$app->db->beginTransaction();
        try {
            Yii::$app->db->createCommand("CALL count_works_days (:yea,:mon,@c);",[
                'yea' => $year,
                'mon' => $month,
            ])->execute();
            $array_count = Yii::$app->db->createCommand("select @c as `c`;")->queryOne();
            $file_query = dirname(__DIR__, 2).'\test\query\spravka08032020.sql';
            $query = file_get_contents($file_query);
            $array = Yii::$app->db->createCommand($query,[
                'yea' => $year,
                'mon' => $month,
                'c' => $array_count['c'],
            ])->queryAll();
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
        }

        return $array;
    }

    public function show_mantu($id_group){
        $file_query = dirname(__DIR__, 2).'\test\query\journal_mantu.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'id_group' => $id_group,
        ])->queryAll();
        return $array;

    }

    public function show_mantu_pokazateli(){
        $file_query = dirname(__DIR__, 2).'\test\query\pokazateli_mantu.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query)->queryAll();
        return $array;
    }

    public function show_five($year,$month){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        for($i=0;$i<=31;$i++){
            $day = $i < 10 ? '0'.$i : $i;
            $iter_date = date('Y-m-d',strtotime(date($year.'-'.$month.'-'.$day)));

            /*$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
            $model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

            if(empty($model_d_date)){
                $model_d_date = array();
            }
            if(empty($model_d_antidate)){
                $model_d_antidate = array();
            }*/

//            $now = '2020-03-04';

            $now = date('Y-m-d');
            $now2 = $now;

            /*while(true){
                if(((date('N', strtotime($now)) < 6) && (!in_array($now,$model_d_date))) || in_array($now,$model_d_antidate)){
                    break;
                }else{
                    $now = date('Y-m-d',strtotime($now. " 1 day"));
                }
            }*/

            if ($iter_date > $now2){
                ${'v'.$i} = 0;
            }else{
                ${'v'.$i} = 1;
            }
        }


        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_28.sql';

                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_29.sql';

                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_30.sql';

                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\five_days_31.sql';

                break;
        }



        $query = file_get_contents($file_query);



        $transaction = Yii::$app->db->beginTransaction();
        try{
            if (($month < date('n') && $year == date('Y')) || $year < date('Y')){
                $deti = 'deti'.$month.$year;
                $query = str_replace('deti',$deti,$query);

                $array = Yii::$app->db->createCommand('show tables like "%'.$deti.'%";')->queryOne();
                if (empty($array)){
                    $transaction->rollBack();
                    return 400;
                }
            }

            $array = Yii::$app->db->createCommand($query,[
                'yea' => $year,
                'mon' => $month,
                'v1' => $v1,'v2' => $v2,'v3' => $v3,'v4' => $v4,'v5' => $v5,
                'v6' => $v6,'v7' => $v7,'v8' => $v8,'v9' => $v9,'v10' => $v10,
                'v11' => $v11,'v12' => $v12,'v13' => $v13,'v14' => $v14,'v15' => $v15,
                'v16' => $v16,'v17' => $v17,'v18' => $v18,'v19' => $v19,'v20' => $v20,
                'v21' => $v21,'v22' => $v22,'v23' => $v23,'v24' => $v24,'v25' => $v25,
                'v26' => $v26,'v27' => $v27,'v28' => $v28,'v29' => $v29,'v30' => $v30,
                'v31' => $v31
            ])->queryAll();

            $transaction->commit();
        }catch (Exception $e){
            $transaction->rollBack();
        }

        return $array;
    }


    public function show_test(){
        $file_query = dirname(__DIR__, 2).'\test\query\test.sql';
        $query = file_get_contents($file_query);
        $query = str_replace('det','deti',$query);
        $array = Yii::$app->db->createCommand($query)->queryAll();
        return $array;
    }

    public function show_kal($id_group){
        $file_query = dirname(__DIR__, 2).'\test\query\journal_kal.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_journal_injection($id_group){

        $file_query = dirname(__DIR__, 2).'\test\query\journal_injection.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => date('Y'),
            'mon' => date('n'),
            'id_group' => $id_group
        ])->queryAll();
        return $array;
    }

    public function show_tabel_cru($year,$month,$id_crujok){

        $count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));

        switch ($count_days_all){
            case 28:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 29:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 30:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
            case 31:
                $file_query = dirname(__DIR__, 2).'\test\query\tabel_cru_31.sql';
                break;
        }

        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
            'id_crujok' => $id_crujok
        ])->queryAll();
        return $array;
    }

    public function show_plan_injection(){
        $array = Yii::$app->db->createCommand("call Show_plan_injection_run ();")->queryAll();
        return $array;
    }

    public function show_cru_akt($year,$month){
        $file_query = dirname(__DIR__, 2).'\test\query\cru_akt.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_cru_dogovora($year,$month){
        $file_query = dirname(__DIR__, 2).'\test\query\cru_dogovora.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'yea' => $year,
            'mon' => $month,
        ])->queryAll();
        return $array;
    }

    public function show_dvijenie($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\dvijenie.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

    public function show_dvijenie_alergiki($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\show_alergiki_ander_dvij.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

    public function show_hot_button_orz($dat){
        $file_query = dirname(__DIR__, 2).'\test\query\hot_button_orz.sql';
        $query = file_get_contents($file_query);
        $array = Yii::$app->db->createCommand($query,[
            'dat' => $dat,
        ])->queryAll();
        return $array;
    }

}
