<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Optd extends Model
{
    public $id_child;//id ребенка
    public $id;//id записи
    public $name;
    public $name_edit;
    public $date_to;//дата направления в ОПТД
    public $pred_diagnoz;//предварительный диагноз
    public $date_osmotr_ptd;//дата осмотра в ОПТД
    public $finish_diagnoz_ptd;//окончательный диагноз в ОПТД
    public $status_ptd;//статус учета
    public $date_sled_yavki;//date_sled_yavki
    public $medotvod;
    public $date_vidacha;//дата выдачи направления родителю
    public $coment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['id_child','date_to'], 'required','message' => false],
            [['name','id_child','status_ptd','id'], 'integer'],
            [['status_ptd','id'], 'default','value' => 0],
            [['status_ptd'],'in','range' => [0,1,2,5]],
            [['date_to','date_osmotr_ptd','date_sled_yavki','medotvod','date_vidacha'], 'date','format' => 'php:d.m.Y','message' => false],
            [['date_to','date_osmotr_ptd','date_sled_yavki','medotvod','date_vidacha'], 'default','value' => ''],

            [['name_edit','pred_diagnoz','finish_diagnoz_ptd','coment'], 'trim'],
            [['name_edit','pred_diagnoz','finish_diagnoz_ptd','coment'], 'string'],
            [['name_edit','pred_diagnoz','finish_diagnoz_ptd','coment'],'default','value' => ''],

//            [['date_to','date_osmotr_ptd','date_sled_yavki','medotvod','date_vidacha',
//                'pred_diagnoz','finish_diagnoz_ptd','coment','status_ptd','name','id_child','id_child','date_to'],'safe'],

            [['name_edit','pred_diagnoz','finish_diagnoz_ptd','coment'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\;]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],

            [['name','id_child','status_ptd','id'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],

            [['date_to','date_osmotr_ptd','date_sled_yavki','medotvod','date_vidacha'], 'filter', 'filter' => function ($value) {
                    if (strlen($value)>0){
                        $result = Yii::$app->formatter->asTime($value);
                        $result = str_replace('--','',$result);
                        return $result;
                    }
                    return $result = "";
                }],





        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя',
            'name_edit' => 'Фамилия Имя',
            'date_to' => 'Дата направления',
            'pred_diagnoz' => 'Предварительный диагноз',
            'date_osmotr_ptd' => 'Дата осмотра в ОПТД',
            'finish_diagnoz_ptd' => 'Окончательный диагноз ОПТД',
            'status_ptd' => 'Статус',
            'date_sled_yavki' => 'Дата следующей явки',
            'medotvod' => 'Медотвод',
            'date_vidacha' => 'Дата выдачи направления на руки',
            'coment' => 'Примечание',

        ];
    }

    public function show_table_optd(){
        $query = "select mantu_n.id,id_child,id_gruppa,name,rozd,date_to,pred_diagnoz,date_osmotr_ptd,finish_diagnoz_ptd,status_ptd,date_sled_yavki,medotvod,date_vidacha,coment from mantu_n left outer join deti on mantu_n.id_child = deti.id order by id_gruppa,name,date_to";
        $model = Yii::$app->db->createCommand($query)->queryAll();
        return $model;
    }

    public function show_table_optd_control_yavka(){
        $query = "select id_gruppa,name,rozd,date_to from mantu_n left outer join deti on mantu_n.id_child = deti.id where year(date_sled_yavki)=1900 and status_ptd in (0,1) and year(date_to) <= year(getdate()) and month(date_to) <= month(getdate()) order by id_gruppa,name,date_to";
        $model = Yii::$app->db->createCommand($query)->queryAll();
        return $model;
    }




    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
