<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Presentation extends Model
{
    public $name;
    public $tel;
    public $name_org;
    public $email;
    public $region;
    public $city;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

//            [['name','name_org','region','city','email'],'safe'],

            [['name'], 'required','message' => 'Обязательно введите ваше имя'],
            [['email'], 'required','message' => 'Обязательно введите вашу электронную почту'],
            [['region'], 'required','message' => 'Обязательно введите ваш регион'],
            [['city'], 'required','message' => 'Обязательно введите ваш населенный пункт'],
            [['email'], 'email'],

            [['name','name_org','region','city','tel'], 'trim'],
            [['name','name_org','region','city','tel'], 'string'],
            [['name','name_org','region','city','tel'],'default','value' => ''],

            [['name','name_org','region','email','city'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\;]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],
            ['captcha', 'captcha','captchaAction' => Url::to('/start/captcha')],
            [['captcha'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],

            [['tel'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9\+\(\)\s+]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],





        ];
    }

    public function attributeLabels()
    {
        return [
            'captcha' => 'Введите проверочный код',
            'name' => 'Имя',
            'name_org' => 'Название организации',
            'tel' => 'Номер телефона',
            'email' => 'Электронная почта',
            'region' => 'Регион',
            'city' => 'Населенный пункт',

        ];
    }



}
