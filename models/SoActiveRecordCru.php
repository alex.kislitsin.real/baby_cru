<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.12.19
 * Time: 16:51
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class SoActiveRecordCru extends ActiveRecord{

    public $id;
    public $name;
//    public $price;
//    public $pedagog;
//    public $pn;
//    public $vt;
//    public $sr;
//    public $ch;
//    public $pt;
//    public $zp;

    public static function tableName(){
        return 'sotrudniki';
    }

    public function rules(){
        return[
//            [['name','pedagog'],'trim'],
//            [['name','pedagog'],'string'],
//            [['name','pedagog'],'default','value' => ''],
            [['name'],'default','value' => 0],
            [['name'],'integer'],
            /*[['name','pedagog'],'filter','filter' => function($value){
                    $value = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    $result = str_replace('--','',$value);
                    return $result;
                }],*/
            [['name'],'filter','filter' => function($value){
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],
            /*[['zp'],'filter','filter' => function($value){
                    $result = preg_replace('/[^0-9]/','',$value);
                    if ($result > 100){
                        $x = $result - 100;
                        return $result = $result - $x;
                    }else{
                        return $result;
                    }
                }],*/

        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Педагог',
//            'price' => 'Стоимость',
        ];
    }
} 