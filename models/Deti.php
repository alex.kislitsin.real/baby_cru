<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Deti extends Model
{
    public $id;
    public $name;
    public $id_gruppa;
    public $number_schet;
    public $start;
    public $in;
    public $out;
    public $rozd;
    public $otche;
    public $pol;
    public $polis;
    public $snils;
    public $adress;
    public $old_id_gruppa;
    public $perevod;
    public $perevod2;
    public $alergia;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [





            [['name'], 'required','message' => false],
            [['id','number_schet','id_gruppa','old_id_gruppa','pol'], 'integer'],
            [['number_schet','id_gruppa','old_id_gruppa'], 'default','value' => 0],
            [['pol'],'in','range' => [1,2]],

            [['in','rozd','perevod','perevod2'], 'date','format' => 'php:d.m.Y','message' => false],
            [['in','rozd','perevod','perevod2'], 'default','value' => date('1900-01-01')],
            ['out','default','value' => null],

            [['name','otche','adress','snils','start','polis','alergia'], 'trim'],
            [['name','otche','adress','snils','start','polis','alergia'], 'string'],
            [['name','otche','adress','snils','start','polis','alergia'],'default','value' => ''],


            [['name','otche','adress','alergia'], 'filter', 'filter' => function ($value) {
                    $value = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    $result = mb_strtoupper($value,'utf-8');
                    return $result;
                }],

            [['id','number_schet','id_gruppa','old_id_gruppa','pol'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],

            [['in','rozd','perevod','perevod2'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],

            [['out'], 'filter', 'filter' => function ($value) {
                    $value === null ? $result = null : $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],



        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя',
            'id_gruppa' => 'id_gruppa',
            'number_schet' => 'Номер лиц.счета',
            'in' => 'Дата зачисления',
            'out' => 'Дата отчисления',
            'rozd' => 'Дата рождения',
            'start' => 'start',
            'pol' => 'Пол',
            'otche' => 'Отчество',
            'adress' => 'Адрес',
            'polis' => 'Полис',
            'snils' => 'Снилс',
            'old_id_gruppa' => 'old_id_gruppa',
            'perevod' => 'Дата перевода',
            'perevod2' => 'Дата перевода',
            'alergia' => 'Аллергия',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
