<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.12.19
 * Time: 16:51
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "category_cru".
 * @property int $id
 * @property string $name
 */
class Category_id extends ActiveRecord{

    public $id;
    public $name;

    public static function tableName(){
        return 'category_cru';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => 'Категория кружка',
        ];
    }
} 