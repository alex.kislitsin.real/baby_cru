<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Cru_add_child extends Model
{
    public $id_child;//id ребенка
    public $name;
    public $in_cru;
    public $in_cru_old;
    public $out_cru;//дата направления в ОПТД
    public $id_crujok;//предварительный диагноз
    public $id_crujok_old;//предварительный диагноз

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['id_child'], 'required','message' => false],
            [['id_crujok','id_child','id_crujok_old'], 'integer'],
            [['id_crujok','id_crujok_old'], 'default','value' => 0],
//            [['status_ptd'],'in','range' => [0,1,2,5]],
            [['out_cru','in_cru','in_cru_old'], 'date','format' => 'php:d.m.Y','message' => false],
            [['out_cru'], 'default','value' => null],

            [['id_crujok','id_child','id_crujok_old'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],

            [['in_cru','in_cru_old'], 'filter', 'filter' => function ($value) {
                    return Yii::$app->formatter->asTime($value);
                }],
            [['out_cru'], 'filter', 'filter' => function ($value) {
                    $value === null ? $result = null : $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Фамилия Имя',
            'in_cru' => 'date',
            'out_cru' => 'date',
            'id_crujok' => 'id_crujok',
        ];
    }

    public function show_table_modal_add($id_crujok){
        /*$query = "select C.id,rtrim(C.NAME) AS`name`,
case when (out_cru is null or (month(out_cru)=month(now()) and year(out_cru)=year(now())) or out_cru > now()) and in_cru is not null then id_crujok else 0 END AS `id_crujok`,
case when (out_cru is null or (month(out_cru)=month(now()) and year(out_cru)=year(now())) or out_cru > now()) and in_cru is not null  then in_cru else null END AS `in_cru`,
case when (out_cru is null or (month(out_cru)=month(now()) and year(out_cru)=year(now())) or out_cru > now()) and in_cru is not null  then out_cru else null END AS `out_cru`

from (select id,name from deti where `out` is null)C left outer join (select A.id_child,A.id_crujok,A.in_cru,B.out_cru from (select id_child,id_crujok,max(in_cru) AS `in_cru` from crujki_add_deti group by id_child,id_crujok)A left outer join (select * from crujki_add_deti)B on A.id_child=B.id_child and A.in_cru=B.in_cru and A.id_crujok=B.id_crujok where A.id_crujok=:id_crujok)B on C.id = B.id_child order by B.id_crujok desc,C.name asc";
        $array_add_child = Yii::$app->db->createCommand($query,[
            'id_crujok' => $id_crujok
        ])->queryAll();
        return $array_add_child;*/

        $file_query = dirname(__DIR__, 2).'\test\query\show_deti_add_deti_cru_new_30032020.sql';
//        $file_query = dirname(__DIR__, 2).'\test\query\new_query_cru_add_child.sql';
        $query = file_get_contents($file_query);

        $array_add_child = Yii::$app->db->createCommand($query,[
            'id_crujok' => $id_crujok
        ])->queryAll();
        return $array_add_child;
    }

    /*public function show_table_optd_control_yavka(){
        $query = "select id_gruppa,name,rozd,date_to from mantu_n left outer join deti on mantu_n.id_child = deti.id where year(date_sled_yavki)=1900 and status_ptd in (0,1) and year(date_to) <= year(getdate()) and month(date_to) <= month(getdate()) order by id_gruppa,name,date_to";
        $model = Yii::$app->db->createCommand($query)->queryAll();
        return $model;
    }*/




    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
