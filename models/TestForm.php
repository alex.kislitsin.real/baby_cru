<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class TestForm extends Model{

    public $name;

    public function attributeLabels(){
        return [
            'name' => 'Имя',
        ];
    }

    public function rules(){
        return [
//            [['name','email'],'required','message' => 'Обязательно для заполнения'],
            ['name','safe'],
//            ['name','string','min' => 2,'tooShort' => 'Слишком короткое'],
//            ['name','string','max' => 10,'tooLong' => 'Слишком длинное'],
            //['text','trim']
        ];
    }

//    public function myRule($attr){
//        if(!in_array($this->$attr, ['hello','world'])){
//            $this->addError($attr,'Введите корректные данные');
//        }
//    }

} 