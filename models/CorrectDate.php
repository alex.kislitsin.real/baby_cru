<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CorrectDate extends Model
{
    public $id_child;//id ребенка
    public $datbegin;
    public $datend;
    public $reasonold;
    public $reasonnew;
    public $search;
    public $medotvod;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            [['datbegin','datend'], 'required','message' => false],
            [['datbegin','datend'], 'date','format' => 'php:d.m.Y','message' => false],

            [['reasonold','search'], 'trim'],
            [['reasonold','search'], 'string'],
            [['reasonold','search'],'default','value' => ''],

            ['medotvod','safe'],


            [['reasonold','search'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    return $result;
                }],

            [['id_child','reasonnew','medotvod'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],

            [['datbegin','datend'], 'filter', 'filter' => function ($value) {
                    $result = Yii::$app->formatter->asTime($value);
                    return $result;
                }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'datbegin' => 'начальная дата',
            'datend' => 'конечная дата',
            'search' => 'введите данные для быстрого поиска ребенка',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
