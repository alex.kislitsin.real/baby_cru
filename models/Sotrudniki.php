<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Sotrudniki extends Model
{
    public $id;
    public $name;
    public $schet;
    public $ins;
    public $outs;
    public $rozdso;
    public $dol;
    public $pitanie;
    public $pol;
    public $tel;
    public $address;
    public $coment;
    public $snils;
    public $prikaz_in;
    public $prikaz_out;
    public $polis;

    public $seria;
    public $num_passport;
    public $kem_vidan;
    public $date_vidacha;
    public $inn;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name'], 'required','message' => false],
            [['id','schet','prikaz_in','prikaz_out','pitanie','pol','seria','num_passport','inn'], 'integer'],
            [['schet','prikaz_in','prikaz_out','seria','num_passport','inn'], 'default','value' => 0],
            [['pitanie','pol'],'in','range' => [1,2]],

            [['ins','rozdso','date_vidacha'], 'date','format' => 'php:d.m.Y'],
            [['ins','rozdso','date_vidacha'], 'default','value' => date('1900-01-01')],
            ['outs','default','value' => null],
            ['date_vidacha','default','value' => ''],

            ['tel', 'safe'],
            ['snils', 'safe'],
//            ['dol', 'default','value' => ''],

//            ['outs', 'safe'],

            [['name','coment','address','dol','kem_vidan'], 'trim'],
            [['name','coment','address','dol','kem_vidan'], 'string'],
            [['name','coment','address','dol','kem_vidan'],'default','value' => ''],

            [['name'], 'filter', 'filter' => function ($value) {
                    $value = preg_replace('/[a-zA-Z\'\"\;]/','',$value);
                    $value = mb_strtolower($value,'utf-8');
                    $result = mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
                    $result = str_replace('--','',$result);
//                    $result = preg_replace("/(\+7)(\d{3})(\d{3})(\d{2})(\d{2})/", "$1 ($2) $3-$4-$5", $value);
                    return $result;
                }],
            [['polis','coment','address','dol','kem_vidan'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[\'\"\;]/','',$value);
                    $result = str_replace('--','',$result);
                    return $result;
                }],
            [['date_vidacha'], 'filter', 'filter' => function ($value) {
                    if (strlen($value)>0){
                        $result = Yii::$app->formatter->asTime($value);
                        return $result;
                    }
                    return $result = "";
                }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'ФИО',
            'dol' => 'Должность',
            'schet' => 'Номер лиц.счета',
            'ins' => 'Дата зачисления',
            'outs' => 'Дата увольнения',
            'rozdso' => 'Дата рождения',
            'pitanie' => 'На питании',
            'pol' => 'Пол',
            'tel' => 'Телефон',
            'address' => 'Адрес',
            'coment' => 'Примечание',
            'snils' => 'Снилс',
            'polis' => 'Полис',
            'prikaz_in' => 'Приказ о зачислении',
            'prikaz_out' => 'Приказ об увольн.',
            'seria' => 'Серия паспорта',
            'num_passport' => 'Номер паспорта',
            'kem_vidan' => 'Кем выдан',
            'date_vidacha' => 'Дата выдачи',
            'inn' => 'ИНН',
        ];
    }


    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */

}
