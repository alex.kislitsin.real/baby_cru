<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 03.09.19
 * Time: 15:25
 */

namespace app\models;
use yii\base\Model;

class Gruppa extends Model{

    public $id;
    public $name;

    public function attributeLabels(){
        return [
            'id' => '№',
            'name' => 'Название группы',
        ];
    }

    public function rules(){
        return [
            [['id'],'integer'],
            ['name','safe'],
            [['id'], 'filter', 'filter' => function ($value) {
                    $result = preg_replace('/[^0-9]/','',$value);
                    return $result;
                }],
        ];
    }

}