<?php

use yii\db\Migration;

/**
 * Class m201022_123608_fix_fk_crujki_gogo_ibfk_2
 */
class m201022_123608_fix_fk_crujki_gogo_ibfk_2 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->dropForeignKey('crujki_gogo_ibfk_2','crujki_gogo');
//		$this->addForeignKey('crujki_gogo_ibfk_2','crujki_gogo','id_crujok','crujki','id');
	    $this->addColumn('crujki_gogo','id_so',$this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//	    $this->dropForeignKey('crujki_gogo_ibfk_2','crujki_gogo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_123608_fix_fk_crujki_gogo_ibfk_2 cannot be reverted.\n";

        return false;
    }
    */
}
