<?php

?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_so100">
    <table class="table-bordered table-hover" id="mytable">
        <tbody>

        <?php
    $i = 1;
    foreach($array as $q){
        if (strpos($q['in'],'1900-01-01') !== false){
            $t = '';
        }else{
            $t = Yii::$app->formatter->asDate(trim($q['in']));
        }
        if (!empty($q['out'])||!is_null($q['out'])){
            $t2 = Yii::$app->formatter->asDate(trim($q['out']));
            $classs = 'id="row_sotr_all"';
        }else{
            $t2 = '';
            $classs = '';
        }
        if (strpos($q['rozd'],'1900-01-01') !== false){
            $u = '';
        }else{
            $u = Yii::$app->formatter->asDate(trim($q['rozd']));
        }


        echo '<tr '.$classs.'>
                <td style="width: 3%">'.$i.'</td>
                <td style="width: 2%" id="tr_start_deti">'.trim($q['start']).'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: 8%">'.trim($q['grname']).'</td>
                <td style="width: 6%">'.trim($q['number_schet']).'</td>
                <td style="width: 7%">'.$t.'</td>
                <td style="width: 7%">'.$t2.'</td>
                <td style="width: 7%">'.$u.'</td>
                <td style="width: 3%">'.trim($q['pol']).'</td>
                <td style="width: 15%">'.trim($q['adress']).'</td>
                <td style="width: 12%">'.trim($q['polis']).'</td>
                <td style="width: 10%">'.trim($q['snils']).'</td>
            </tr>';
        $i++;
    }

    ?>

        </tbody>
    </table>
</div>
