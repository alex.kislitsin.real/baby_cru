<!--<h1>Show action</h1>-->
<?php
use yii\grid\GridView;
use yii\widgets\Pjax;
//use yii\bootstrap\ActiveForm;
//use yii\jui\DatePicker;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use timurmelnikov\widgets\LoadingOverlayPjax;
use rmrevin\yii\fontawesome\FAS;
use yii\bootstrap\Html;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$item = Yii::$app->controller->action->id;
$this->title = 'Движение';
//$this->params['breadcrumbs'][] = $this->title;
$percent = '9%';
//debug($model_dvig);
//debug($model_d_date);

$model_id->id = 0;

//$model_id->safeAttributes(Yii::$app->request->post());
Yii::$app->request->setBodyParams($model_id);

$array_disabled_dates = array();
foreach($model_d_date as $date){
    $date = date('d.m.Y',  strtotime($date));
    array_push($array_disabled_dates,$date);
}

?>



<div class="panel panel-info col-md-8" style="padding: 0">
    <!-- Default panel contents -->
    <div class="panel-heading clearfix">
        <h1 class="panel-title pull-left" style="padding-top: 10px;">Движение на выбранную дату</h1>
        <div class="pull-right">
            <?php $form = ActiveForm::begin([
                'id' => 'form2',
                'action' => ['my/show'],
                'method' => 'POST',
                'type' => ActiveForm::TYPE_INLINE,
                'enableAjaxValidation' => false,
//                'formConfig' => [
//                    'deviceSize' => ActiveForm::SIZE_SMALL,
//                    'labelSpan' => 3,
//                ],
//                'options' => [
//                    'class' => 'form-inline',
//                    'enctype' => 'multipart/form-data'
//                ]
            ])?>

            <?= $form->field($model_item_date,'item_date')->widget(DatePicker::className(),[
                'name' => 'dp_21',
                'size' => 'md',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'readonly' => true,
                'options' => [
                    'placeholder' => 'Выберите дату ...',
                    'id' => 'dp2'
                ],
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'todayBtn' => true,
//        'maxViewMode' => '4',
                    'daysOfWeekDisabled' => [0, 6],
                    'daysOfWeekHighlighted' => [0, 6],
                    'datesDisabled' => $array_disabled_dates,
                    'toggleActive'   => true,
                    'autoclose'=>true,
                    'format' => 'dd.mm.yyyy'
                ],
                'pluginEvents' => [
                 'changeDate' => 'function(e) {
                     var date1 = $("#dp2").val();
                     $("#id_form_datepicker_dvig").val("1");
                     if(date1 != ""){
//                         $("#pjax_grid_dvig").LoadingOverlay("show");
//                         $("#form2").submit();

                         var testform = $("#form2");
                         console.log(testform.serializeArray());
//                         return;

                                    $("#id_grid_dvig").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                                    $.ajax({
                                        type : testform.attr("method"),
                                        url : testform.attr("action"),
                                        data : testform.serializeArray()
                                    }).done(function(response) {
                                        console.log(response);
                                        $("#id_grid_dvig").html(response);
                                        $("*").LoadingOverlay("hide");

                                    }).fail(function() {
                                        $("*").LoadingOverlay("hide");
                                        console.log("not");
                                        alert("Ошибка");
                                    });

                     }
                 }',

                ]
            ])->label(''); ?>
            <?= $form->field($model_id,'id')->hiddenInput(['id' => 'id_form_datepicker_dvig'])->label(false);?>
            <?php $form = ActiveForm::end()?>
        </div>


    </div>
    <div id="id_grid_dvig">
    <?= $this->render('dvigenie_table',compact(
        'dataProvider',
        'model_dvig',
        'percent')) ?>
    </div>


</div>