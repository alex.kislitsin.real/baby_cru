<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\bootstrap\Modal;

?>

<h1>Index action</h1>

<?php
$this->title = 'Index';
$this->params['breadcrumbs'][] = $this->title;
//debug($array_gruppa);
//debug($array_test1);
?>

<?php if(Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <?php echo Yii::$app->session->getFlash('success'); ?>
        <?php echo debug($status) ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>

<?php if(Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <?php echo Yii::$app->session->getFlash('error'); ?>

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>



<?php /*$form = ActiveForm::begin(['id' => 'testForm','options' => ['class' => 'col-lg-5']])*/?>
<?php $form = ActiveForm::begin(['id' => 'testForm'])?>
<?php

$params = [
    'prompt' => 'выберите пункт'
];
?>
<?= $form->field($model,'name')->textInput(['placeholder' => "Введите имя"]) ?>
<?= $form->field($model,'email')->input('email',['placeholder' => "Введите почту"]) ?>
<?= $form->field($model,'data')->widget(DatePicker::class, [
    'language' => 'ru',
    'dateFormat' => 'dd.MM.yyyy',

    'options' => [
        'placeholder' => 'Select date',
        'class'=> 'form-control',

        'autocomplete'=>'off'
    ],
    'clientOptions' => [
        'changeMonth' => true,
        'changeYear' => true,
        'yearRange' => '2015:2050',
        //'showOn' => 'button',
        //'buttonText' => 'Выбрать дату',
        //'buttonImageOnly' => true,
        //'buttonImage' => 'images/calendar.gif'
    ]])->label('Дата') ?>
<?= $form->field($model,'text')->textarea(['placeholder' => "Введите текст сообщения"]) ?>
<?= $form->field($model,'status')->dropDownList($array_test1,$params) ?>
<?= Html::submitButton('Send',['class'=>'btn btn-success'])?>
<?php $form = ActiveForm::end()?>
