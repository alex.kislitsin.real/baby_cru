<?php
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\models\Category_id;
if (count($array)<1){
    $thead = 'cru_view_thead_empty_in_dialog';
}else{
    $thead = 'view_table_cru';
}
?>


<?php
Modal::begin([
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'id' => 'modal_add_edit_crujki',//D:\OSPanel\domains\test\web\css\main.css
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="#">Кружки</h4>',
    'size' => Modal::SIZE_LARGE,
    'footer' => '<!--<button type="submit" class="btn btn-light btn-md pull-left" id="#">Добавить новую категорию кружка</button>-->

    <div class="input-group" style="width: 50vh">
    <input style="margin-left: 10px;width: 35vh" type="text" id="id_input_add_new_category_cru" class="pull-left form-control" autofocus placeholder="Добавить новую категорию кружка">
    <span class="input-group-btn pull-left"><button type="button" id="id_button_input_add_new_category_cru" class="btn btn-light">добавить</button></span>
    </div>
    <button type="submit" class="btn btn-default btn-md pull-right" id="but_cru_close_dialog">Закрыть</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_hide_daysweek_cru',
]); ?>
<?= $form->field($model_cru,'pn')->hiddenInput([
    'id' => 'hidden_pole_cru_pn',
])->label(false); ?>
<?= $form->field($model_cru,'vt')->hiddenInput([
    'id' => 'hidden_pole_cru_vt',
])->label(false); ?>
<?= $form->field($model_cru,'sr')->hiddenInput([
    'id' => 'hidden_pole_cru_sr',
])->label(false); ?>
<?= $form->field($model_cru,'ch')->hiddenInput([
    'id' => 'hidden_pole_cru_ch',
])->label(false); ?>
<?= $form->field($model_cru,'pt')->hiddenInput([
    'id' => 'hidden_pole_cru_pt',
])->label(false); ?>
<?= $form->field($model_cru,'id')->hiddenInput([
    'id' => 'hidden_pole_cru_id',
])->label(false); ?>


<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form_cru_cru',
//    'layout' => 'horizontal',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'method' => 'POST',
    'action' => ['crujki/cru'],
    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
        'horizontalCssClasses' => [
            'label' => 'col-lg-5',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-7',
//             'error' => '',
//             'hint' => '',
        ],
    ],
]);

$model_category_cru = new Category_id();
$data = Yii::$app->db->createCommand("select * from category_cru")->queryAll();
$data = \yii\helpers\ArrayHelper::map($data, 'id', 'name');

?>

<div class="col-sm-5">
    <?/*= $form->field($model_category_cru, 'name')->widget(Select2::classname(), [
        'id' => 'name_cru_form',
        'data' => $data,
        'size' => Select2::MEDIUM,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите категорию'],
        'pluginOptions' => [
            'allowClear' => true,

        ],
    ])->label(false) */?>

    <?php echo $form->field($model_category_cru, 'name')->dropDownList($data, [
        'id' => 'drop_category_cru',
        'prompt' => 'Выберите категорию'
    ]) ?>

    <?= $form->field($model_cru, 'name')->textInput([
        'id' => 'name_cru_form',
        'placeholder' => 'Название кружка',
//        'style' => 'margin-bottom: 2px'
    ]) ?>
    <?/*= $form->field($model_cru, 'pedagog')->textInput([
        'id' => 'pedagog_cru_form',
        'placeholder' => 'Педагог'
    ])->label(false) */?>
    <?= $form->field($model_so,'name')->dropDownList($array_so,[
        'id' => 'drop_pedagog_cru_form',
        'prompt' => 'выбрать педагога',
    ]) ?>
    <?= $form->field($model_cru, 'price',[
        'addon' => [
            'prepend' => [
                'content' => Html::button('Цена', ['class'=>'btn btn-default btn-md','id' => '#','disabled' => 'disabled','style' => ['width' =>  '65px']]),
                'asButton' => true
            ],
            'append' => [
                'content' => Html::button('P', ['class'=>'btn btn-default btn-md','id' => 'id_show_button_one_price','style' => ['width' =>  '40px']]),
                'asButton' => true
            ],

        ]
    ])->textInput([
            'id' => 'price_cru_form',
            'type' => 'number',
//        'placeholder' => 'Стоимость'
        ]) ?>

    <p class="alert alert-info not_selected_text_on_block" style="cursor: pointer; text-align: center" id="cru_message_one_price" hidden="hidden">одинаковая цена для всех</p>

    <?= $form->field($model_cru, 'zp',[
        'addon' => [
            'prepend' => [
                'content' => Html::button('Плата', ['class'=>'btn btn-default btn-md','id' => '#','disabled' => 'disabled','style' => ['width' =>  '65px']]),
                'asButton' => true
            ],
            'append' => [
                'content' => Html::button('%', ['class'=>'btn btn-default btn-md','id' => 'id_show_button_one_percent','style' => ['width' =>  '40px']]),
                'asButton' => true
            ],

        ]
    ])->textInput([
            'id' => 'zp_cru_form',
            'type' => 'number',
//        'placeholder' => 'Стоимость'
        ]) ?>

    <p class="alert alert-info not_selected_text_on_block" style="cursor: pointer; text-align: center" id="cru_message_one_percent" hidden="hidden">одинаковый % для всех</p>


    <div class="btn-group btn-group-justified" data-toggle="buttons">
        <label class="btn btn-default" id="but_pn">
            <input type="checkbox" checked autocomplete="off">ПН
        </label>
        <label class="btn btn-default" id="but_vt">
            <input type="checkbox" checked autocomplete="off">ВТ
        </label>
        <label class="btn btn-default" id="but_sr">
            <input type="checkbox" checked autocomplete="off">СР
        </label>
        <label class="btn btn-default" id="but_ch">
            <input type="checkbox" checked autocomplete="off">ЧТ
        </label>
        <label class="btn btn-default" id="but_pt">
            <input type="checkbox" checked autocomplete="off">ПТ
        </label>
    </div>
    <br/>
    <div class="btn-group btn-group-justified" data-toggle="buttons">
        <label class="btn btn-warning" id="but_cru_reset">
            <input type="checkbox" checked autocomplete="off">очистить поля
        </label>
        <label class="btn btn-success" id="but_cru_save">
            <input type="checkbox" checked autocomplete="off">сохран / добав
        </label>

    </div>


</div>
<div class="col-sm-7" id="id_view_table_cru">

    <?= $this->render($thead,compact('array')) ?>

</div>

<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_cru',
])->label(false); ?>


<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<?php
$scr = <<< JS

$(function() {

    $('#id_button_input_add_new_category_cru').on('click',function(){
        var v = $('#id_input_add_new_category_cru').val();
//        console.log(v);
        $('#hidden_id_cru').val(14);
        var arr = $('#form_cru_cru');
        var form = $('#form_cru_cru').serializeArray();
        form.push({name: 'name',value: v});
        console.log(form);
//        return;
        $(this).LoadingOverlay("show");$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response!=400){

                var array_all = $.parseJSON(response);
                console.log(array_all);//return;
                $('#drop_category_cru').find('option').remove();
                $('#drop_category_cru').prepend($('<option value="">Выберите категорию</option>'));
                $.each(array_all, function(index, value) {
                    $('#drop_category_cru').append($('<option value="'+value.id+'">'+value.name+'</option>'));
                });

            $('#id_input_add_new_category_cru').val('');
            }

        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });


        return false;
    });

    $('#id_show_button_one_percent').on('click',function(){
        $('#cru_message_one_percent').toggle(200);
        return false;
    });

    $('#id_show_button_one_price').on('click',function(){
        $('#cru_message_one_price').toggle(200);
        return false;
    });

    $('#cru_message_one_percent').on('click',function(){
        var value1 = $('#zp_cru_form').val();
        if(value1 >=0 && value1.length > 0){

        $('#hidden_id_cru').val(11);
        var form = $('#form_cru_cru').serializeArray();
        var arr = $('#form_cru_cru');
        console.log(form);
//        return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_view_table_cru').html(response);
            $('#cru_message_one_percent').toggle(200);
            reset_pole();
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
        }
        return false;
    });

    $('#cru_message_one_price').on('click',function(){
        var value1 = $('#price_cru_form').val();
        if(value1 >=0 && value1.length > 0){

        $('#hidden_id_cru').val(12);
        var form = $('#form_cru_cru').serializeArray();
        var arr = $('#form_cru_cru');
        console.log(form);
//        return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_view_table_cru').html(response);
            $('#cru_message_one_price').toggle(200);
            reset_pole();
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
        }
        return false;
    });

    function reset_pole(){
        $('#form_cru_cru,#form_hide_daysweek_cru').trigger('reset');
        $('#but_pn,#but_vt,#but_sr,#but_ch,#but_pt,#but_cru_reset,#but_cru_save,#but_cru_add').removeClass('active');
        $('#name_cru_form,#price_cru_form,#zp_cru_form').val('');
//        $('#drop_pedagog_cru_form,').val(0);
        $('#hidden_pole_cru_id').val(0);
//        $('#but_cru_add').removeAttr('disabled');
    }

    $('#but_cru_save').on('click',function(){

        var s = $('#id_view_table_cru').scrollTop();
//        alert(s);return;

        var n = $('#name_cru_form').val();
        if(n.length<1){
            return false;
        }

        var ider = $('#hidden_pole_cru_id').val();
        if(ider > 0){
            $('#hidden_id_cru').val(2);
        }else{
            $('#hidden_id_cru').val(1);
        }

        if($('#but_pn').hasClass('active')){
            $('#hidden_pole_cru_pn').val(1);
        }else{
            $('#hidden_pole_cru_pn').val(0);
        }
        if($('#but_vt').hasClass('active')){
            $('#hidden_pole_cru_vt').val(1);
        }else{
            $('#hidden_pole_cru_vt').val(0);
        }
        if($('#but_sr').hasClass('active')){
            $('#hidden_pole_cru_sr').val(1);
        }else{
            $('#hidden_pole_cru_sr').val(0);
        }
        if($('#but_ch').hasClass('active')){
            $('#hidden_pole_cru_ch').val(1);
        }else{
            $('#hidden_pole_cru_ch').val(0);
        }
        if($('#but_pt').hasClass('active')){
            $('#hidden_pole_cru_pt').val(1);
        }else{
            $('#hidden_pole_cru_pt').val(0);
        }

        var form = $('#form_cru_panel_button,#form_hide_daysweek_cru,#form_cru_cru').serializeArray();

        var arr = $('#form_cru_cru');
        console.log(form);//return;
        $("#but_cru_save").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_view_table_cru').html(response).scrollTop(s);
//            var s = $("#id_view_table_cru").scrollTop();
//            alert(s);
//            return false;
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
        reset_pole();
        return false;
    });

    $('#but_cru_close_dialog,#but_cru_close_dialog2').on('click',function(){
        $('.modal').modal('hide');
        return false;
    });

    $('#but_cru_reset').on('click',function(){
        reset_pole();
        return false;
    });

    /*$('#but_cru_show_dialog').on('click',function(){
        $('#cru_message_one_percent,#cru_message_one_price').hide();
        reset_pole();

        $('#hidden_id_cru').val(0);
        var form = $('#form_cru_cru').serializeArray();
        console.log(form);//return;
        $(this).LoadingOverlay("show");
        var arr = $('form');
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_renderAjax_modal_cru').html(response);
//            $('#id_modal_search_add_child_cru').val('');
            $('#modal_add_edit_crujki').modal('show');
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

        return false;
    });*/

    $('#drop_category_cru').on('change',function(){
        var name = $("#drop_category_cru option:selected").text();
        console.log($(this).val());
        $('#name_cru_form').val(name);
        return false;
    });

});

JS;
$this->registerJs($scr, yii\web\View::POS_END);
?>