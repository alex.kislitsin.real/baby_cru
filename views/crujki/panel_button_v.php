<?php
use kartik\form\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\MaskedInput;
//use Yii;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//debug($model_m['name']);

?>

<div class="date_picker_so">
    <?php $form = ActiveForm::begin([
        'id' => 'form_cru_panel_button',
//        'action' => ['reports/rep'],
//        'method' => 'POST',
        'type' => ActiveForm::TYPE_INLINE,
    ])?>
    <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
        'id' => 'drop_cru_item_year',
    ])->label(false) ?>
    <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
        'id' => 'drop_cru_item_month',
    ])->label(false) ?>
    <?php $form = ActiveForm::end()?>
</div>

<div id="id_renderAjax_modal_cru">
    <?= $this->render('modal_edit_cru_view',compact(
        'array_so',
        'model_so',
        'array_drop',
        'model',
        'model_cru',
        'array',
        'model_id'
    )) ?>
</div>
















<?php Modal::begin([
    'id' => 'modal_cru_help',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Инструкция по кружкам</h4>',
//    'footer' => '<button type="submit" class="btn btn-info btn-md" id="#">Сохранить в excel</button>
//     <button type="submit" class="btn btn-success btn-md" id="#">Закрыть</button>',
//    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

//    'size' => Modal::SIZE_LARGE
]);?>
<?= $this->render('cru_view_help') ?>

<?php Modal::end();?>





<?php $form = ActiveForm::begin(['id' => 'id_form_add_id_ch']); ?>
<?= $form->field($model_cru_add,'id_child')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_id_child',
])->label(false); ?>
<?= $form->field($model_cru_add,'id_crujok_old')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_id_crujok_old',
])->label(false); ?>
<?= $form->field($model_cru_add,'in_cru_old')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_in_cru_old',
])->label(false); ?>

<?php ActiveForm::end(); ?>


<?php

Modal::begin([
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'id' => 'modal_add_child_crujki',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block pull-left" id="#">Привязка детей к кружкам</h4>',
    'size' => Modal::SIZE_LARGE,
    'footer' => '<button type="submit" class="btn btn-default btn-md" id="but_cru_close_dialog2">Закрыть</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_cru_cru_add_child',
//    'layout' => 'horizontal',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'method' => 'POST',
    'action' => ['crujki/cru'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-0',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-12',
        ],
    ],
]); ?>

<div class="col-md-5">
    <?= $form->field($model,'name',[
        'addon' => [
            'prepend' => [
                'content' => Html::button('', ['class'=>'btn btn-default btn-md','id' => 'cru_add_count','disabled' => 'disabled','style' => ['width' =>  '45px']]),
                'asButton' => true
            ]
        ]
    ])->dropDownList($array_drop,[
        'id' => 'drop_cru_item_add_child',
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'name')->textInput([
        'id' => 'id_name_cru_form_modal_add_child',
        'placeholder' => 'Фамилия Имя',
        'readonly'=> true
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'in_cru')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'id_modal_form_in_cru',
            'placeholder' => 'Дата зачисления',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'out_cru')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'id_modal_form_out_cru',
            'placeholder' => 'Дата отчисления',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>

    <div class="btn-group btn-group-justified" data-toggle="buttons">
        <label class="btn btn-warning" id="but_cru_reset_add_child">
            <input type="checkbox" checked autocomplete="off">очистить поля
        </label>
        <label class="btn btn-success" id="but_cru_save_add_child">
            <input type="checkbox" checked autocomplete="off">сохранить
        </label>

    </div>

    <p class="alert alert-danger" style="margin-top: 20px" id="cru_message_error" hidden="hidden">Произошла ошибка, попробуйте снова!</p>
    <p class="alert alert-success" style="margin-top: 20px" id="cru_message_success" hidden="hidden">Данные сохранены успешно.</p>

</div>
<div class="col-md-7">
    <?=$form->field($model_search, 'name',[
        'addon' => [
            'append' => [
                'content' => Html::button('стереть', ['class'=>'btn btn-primary btn-danger','id' => 'id_but_cru_modal_add_clear']),
                'asButton' => true
            ]
        ]
    ])->textInput([
            'autofocus' => true,
            'id' => 'id_modal_search_add_child_cru',
            'placeholder' => 'Введите данные для поиска',
        ])->label(false); ?>
    <div id="id_table_add_child">
        <?= $this->render('view_table_cru_add_child',compact('array_add_child')) ?>
    </div>
</div>




<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_cru_add_child',
])->label(false); ?>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>





















<?php Modal::begin([
    'id' => 'modal_cru_help',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Инструкция по кружкам</h4>',
//    'footer' => '<button type="submit" class="btn btn-info btn-md" id="#">Сохранить в excel</button>
//     <button type="submit" class="btn btn-success btn-md" id="#">Закрыть</button>',
//    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

//    'size' => Modal::SIZE_LARGE
]);?>
<?= $this->render('cru_view_help') ?>

<?php Modal::end();?>

<?php
$scr = <<< JS

$(function() {

//    history.pushState({}, null, "&nbsp");

    $('#drop_cru_item_year,#drop_cru_item_month').on('change', function() {
        var value = $(this).val();
        console.log(value);
        $("#id_item_cru_boss").val(8);
        var form = $('#form_boss_item_crujki,#form_cru_panel_button').serializeArray();
        var arr = $('#form_boss_item_crujki');
        console.log(form);
//        return false;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
//                $(".item_child_sp").text("");
                $("*").LoadingOverlay("hide");
                $('#renderAjax_cru_table_boss').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    $('#but_show_cru_help').on('click',function(){
        $('#modal_cru_help').modal('show');
        return false;
    });

});

JS;
$this->registerJs($scr, yii\web\View::POS_END);
?>




