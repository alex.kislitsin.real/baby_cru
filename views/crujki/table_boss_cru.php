<?php
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */


Modal::begin([
    'id' => 'modal_cru_messaga_not',
//    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Причина пропуска:</h3>',
]);
echo '<div class="not_selected_text_on_block">
                <div class="alert alert-danger" role="alert">Отметка невозможна</div>
                </div>';
Modal::end();
?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_cru_1">
    <table class="table-striped table-bordered">

    <?php

    for ($t=1;$t<=31;$t++){
        ${'sum'.$t} = 0;
    }
    $prishli = 0;
    $propuski = 0;

    $i = 1;
    foreach($array_boss as $q){

        for($t=1;$t<=31;$t++){
            switch($q[''.$t.'']){
                case 0:
                    ${'val'.$t} = '';
                    ${'sum'.$t}++;
                    break;
                case 1:
                    ${'val'.$t} = 'H';
                    break;
                case 5:
                    ${'val'.$t} = '<span style="color: #adadad">B</span>';
                    break;
            }
        }

        $prishli += $q['пришли'];
        $propuski += $q['пропуски'];

        echo '<tr>
                <td class="not_item" style="width: 2%">'.$i.'</td>
                <td class="not_item" id="id_n" style="width: 90px"><div>'.trim($q['name']).'</div></td>
                <td class="not_item" style="width: 5%">'.$q['number'].'</td>
                <td class="not_item" style="width: 5%">'.$q['stavka'].'</td>';

        for($o=1;$o<=31;$o++){
            echo '<td data-date="'.date('Y-m-d',strtotime(date($model_y->name.'-'.$model_m->name.'-'.$o))).'" data-id_child="'.$q['id_child'].'" class="nnn">'.${'val'.$o}.'</td>';
        }

        echo '
                <td class="not_item" style="width: 6%">'.$q['пришли'].'</td>
                <td class="not_item" style="width: 6%">'.$q['пропуски'].'</td>
            </tr>';
        $i++;
    }

    echo '<tr>
                <td class="not_item" style="width: 2%"></td>
                <td class="not_item" id="id_n" style="width: 90px"><div>Всего детей в кружке&nbsp&nbsp&nbsp'.count($array_boss).'</div></td>
                <td class="not_item" style="width: 5%"></td>
                <td class="not_item" style="width: 5%"></td>';
    for($o=1;$o<=31;$o++){
        echo '<td class="not_item">'.${'sum'.$o}.'</td>';
    }
    echo '
                <td class="not_item" style="width: 6%">'.$prishli.'</td>
                <td class="not_item" style="width: 6%">'.$propuski.'</td>
            </tr>';

    ?>

    </table>
</div>

<?php
$script = <<<JS

$(function(){

    /*$('#table_cru_1 td').each(function(){
        if($(this).text().indexOf('B') > -1){
            $(this).css('color','#adadad');
        }
    });*/

    /*if($('#table_cru_1 td').text().indexOf('B') > -1){
        $(this).css('color','#adadad');
    }*/

    $('#table_cru_1 td:not(.not_item)').on('click',function(){

//        $('#form_modal_journal').trigger('reset');

        var s = $("#renderAjax_cru_table_boss").scrollTop();

        var id_child = $(this).data('id_child');
        var date = $(this).data('date');
        var value = $(this).text().trim();
        console.log(id_child);
        console.log(date);
        console.log(value+'***');//return;
        if(value.indexOf('B') > -1 || value.indexOf('X') > -1 || (value >= 0 && value.length > 0)){
            console.log('return false');
            return false;
        }else{
            if(value.indexOf('H') > -1){
                if(confirm('Удалить отметку ?')){
                    $("#id_item_cru_boss").val(10);//delete
                }else{
                    return;
                }
            }else{
                $("#id_item_cru_boss").val(9);//insert
            }
        }
        var form = $('#form_boss_item_crujki,#drop_cru_item_month,#drop_cru_item_year').serializeArray();
        form.push({name: 'CruDate[date_cru]',value: date});
        form.push({name: 'CruDate[id_child]',value: id_child});
        var arr = $('#form_boss_item_crujki');
        console.log(form);
//        return;

        $(this).LoadingOverlay("show");$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                if(response == 505){
                    $('#modal_cru_messaga_not').modal('show');
                }else{
                    $('#renderAjax_cru_table_boss').html(response).scrollTop(s);
                }
//                console.log(response);
//                $('#renderAjax_cru_table_boss').html(response).scrollTop(s);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });


})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>