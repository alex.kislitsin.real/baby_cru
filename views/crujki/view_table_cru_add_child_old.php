<?php
$i = 0;
foreach($array_add_child as $e){
    if ($e['id_crujok']!=0)$i++;
}
?>
<div class="my_table my_table2 not_selected_text_on_block" id="table_crijki2"  data-count=<?= $i ?>><!--D:\OSPanel\domains\inet\web\css\reports\diseases\table.css-->
    <table class="table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 6%">№</th>
            <th style="width: 48%">Фамилия и Имя ребёнка</th>
            <th style="width: 23%">Дата зачисления</th>
            <th style="width: 23%">Дата отчисления</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $r = 0;
        foreach($array_add_child as $q){
            if (strlen($q['in_cru'])>0){
                $in_cru = Yii::$app->formatter->asDate(trim($q['in_cru']));
                $r++;
            }else{
                $in_cru = '';
                $r = '';
            }
            if (strlen($q['out_cru'])>0){
                $out_cru = Yii::$app->formatter->asDate(trim($q['out_cru']));
            }else{
                $out_cru = '';
            }

            echo '<tr class="cru_add_child_tr_background" data-id="'.$q['id'].'" data-name="'.trim($q['name']).'" data-id_crujok="'.$q['id_crujok'].'" data-in_cru="'.$in_cru.'" data-out_cru="'.$out_cru.'">
                <td style="width: 6%">'.$r.'</td>
                <td style="width: 48%" id="id_n">'.trim($q['name']).'</td>
                <td style="width: 23%">'.$in_cru.'</td>
                <td style="width: 23%">'.$out_cru.'</td>
            </tr>';
        }

        ?>
        </tbody>
    </table>
</div>

<?php
$scr = <<< JS

$(function() {

    $('#cru_add_count').text($('#table_crijki2').data('count'));

    function reset_pole_add_child(){
//        $('#form_cru_cru_add_child').trigger('reset');
        $('#but_cru_reset_add_child,#but_cru_save_add_child').removeClass('active');
        $('#id_name_cru_form_modal_add_child,#id_modal_form_in_cru,#id_modal_form_out_cru,#hidden_id_cru_add_child_id_child,#hidden_id_cru_add_child_in_cru_old').val('');
        $('#hidden_id_cru_add_child_id_crujok_old').val(0);
    }

    $('#table_crijki2 tr').on('click',function(){

        $('.cru_add_child_tr_background').css("background-color","#FFFFFF");
        $(this).css("background-color", "#fdffbd");
        $('#id_modal_form_in_cru').focus();

        reset_pole_add_child();

        var id = $(this).data('id');
        var name = $(this).data('name');
        var id_crujok = $(this).data('id_crujok');
        var in_cru = $(this).data('in_cru');
        var out_cru = $(this).data('out_cru');

//        console.log(id);
//        console.log(name);
//        console.log(id_crujok);
//        console.log(in_cru);
//        console.log(out_cru);

        $('#id_name_cru_form_modal_add_child').val(name);
        $('#id_modal_form_in_cru').val(in_cru);
        $('#hidden_id_cru_add_child_in_cru_old').val(in_cru);
        $('#id_modal_form_out_cru').val(out_cru);
        $('#hidden_id_cru_add_child_id_child').val(id);
        $('#hidden_id_cru_add_child_id_crujok_old').val(id_crujok);

        return false;
    });





});

JS;
$this->registerJs($scr, yii\web\View::POS_END);
?>