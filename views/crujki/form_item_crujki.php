<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);
if (count($array)<1){
    $off = 'disabled';
}else{
    $off = null;
}
?>

<div class="date_picker_so">
<?php $form = ActiveForm::begin([
    'id' => 'form_boss_item_crujki',
    'action' => ['crujki/cru'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_INLINE,
])?>

<?= $form->field($model,'name')->dropDownList($array_drop,[
    'id' => 'drop_cru',
    'disabled' => $off,
])->label(false) ?>

<?= $form->field($model_id,'id')->hiddenInput(['id' => 'id_item_cru_boss'])->label(false);?>
<?php $form = ActiveForm::end()?>
</div>




<?php
$scr = <<< JS

$(document).ready(function() {
    $('#drop_cru').on('change', function() {
        var value = $(this).val();
        $('#drop_cru_item_add_child').val(value);
        console.log(value);
        $("#id_item_cru_boss").val(8);
        var form = $('#form_boss_item_crujki,#form_cru_panel_button').serializeArray();
        var arr = $('#form_boss_item_crujki');
        console.log(form);
//        return false;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
//                $(".item_child_sp").text("");
                $("*").LoadingOverlay("hide");
                $('#renderAjax_cru_table_boss').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });
});


JS;
$this->registerJs($scr, yii\web\View::POS_END);
//?>




