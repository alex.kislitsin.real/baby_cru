﻿<?php
use kartik\form\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\MaskedInput;

use kartik\select2\Select2;
//use Yii;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//debug($model_m['name']);





?>

<div class="date_picker_so">
    <?php $form = ActiveForm::begin([
        'id' => 'form_cru_panel_button',
//        'action' => ['reports/rep'],
//        'method' => 'POST',
        'type' => ActiveForm::TYPE_INLINE,
    ])?>
    <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
        'id' => 'drop_cru_item_year',
    ])->label(false) ?>
    <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
        'id' => 'drop_cru_item_month',
    ])->label(false) ?>
    <?/*= $form->field($model_m,'name')->widget(Select2::classname(), [
        'data' => $_monthsList,
        'attribute' => 'state_2',
        'hideSearch' => true,
        'size' => Select2::MEDIUM,
        'theme' => Select2::THEME_DEFAULT,
        'language' => 'ru',


        'pluginOptions' => [
            'allowClear' => false,
            'width' => '200px',
            'height' => '40px'

        ],
    ])->label(false) */?>


    <?= Html::button('Добавить/изменить кружки', ['class'=>'btn btn-md btn-default','id' => 'but_cru_show_dialog']); ?>
    <?= Html::button('Привязка детей', ['class'=>'btn btn-md btn-default','id' => 'but_cru_show_dialog_add_child']); ?>
    <?= Html::button('Поиск <span class="glyphicon glyphicon-search" aria-hidden="true"></span>', [
        'class'=>'btn btn-md btn-default',
        'id' => 'but_cru_show_dialog_search_child',
        'href' => '#modal_search_child_crujki',
        'data-toggle' => 'modal',
    ]); ?>

    <?/*= Html::button('Настройки', [
        'class'=>'btn btn-default btn-md but_hot_line',
        'id'=>'but_settings_main',

    ]) */?>

    <div class="btn-group" id="dropdown_cru_excel">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Сохранить отчет в файл &nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li><?= Html::a('сохранить табель в excel', [
                    'reports/excel2','id' => 1,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_tabel']); ?></li>
            <li><?= Html::a('сохранить акт в word', [
                    'reports/excel2','id' => 2,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_akt']); ?></li>
            <li><?= Html::a('сохранить справку в word', [
                    'reports/excel2','id' => 3,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_spravka']); ?></li>
            <li><?= Html::a('сохранить договора в word', [
                    'reports/excel2','id' => 4,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_dogovora']); ?></li>
            <li><?= Html::a('сохранить акты к договорам', [
                    'reports/excel2','id' => 5,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_dogovora_akti']); ?></li>
            <li><?= Html::a('сохранить реестр договоров', [
                    'reports/excel2','id' => 6,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_reestr']); ?></li>
            <li><?= Html::a('сохранить табель все кружки', [
                    'reports/excel2','id' => 7,
                    'year' => $model_y->name,
                    'month' => $model_m->name,
                    'id_crujok' => $model->name,
                ], ['id' => 'but_save_excel_cru_all','style' => 'background:lightgray']); ?></li>




        </ul>
    </div>

    <?php $form = ActiveForm::end()?>
</div>

<div id="id_renderAjax_modal_cru">
    <?= $this->render('modal_edit_cru_view',compact(
        'array_so',
        'model_so',
        'array_drop',
        'model',
        'model_cru',
        'array',
        'model_id'
    )) ?>
</div>
















<?php Modal::begin([
    'id' => 'modal_cru_help',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Инструкция по кружкам</h4>',
//    'footer' => '<button type="submit" class="btn btn-info btn-md" id="#">Сохранить в excel</button>
//     <button type="submit" class="btn btn-success btn-md" id="#">Закрыть</button>',
//    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

//    'size' => Modal::SIZE_LARGE
]);?>
<?= $this->render('cru_view_help') ?>

<?php Modal::end();?>





<?php $form = ActiveForm::begin(['id' => 'id_form_add_id_ch']); ?>
<?= $form->field($model_cru_add,'id_child')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_id_child',
])->label(false); ?>
<?= $form->field($model_cru_add,'id_crujok_old')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_id_crujok_old',
])->label(false); ?>
<?= $form->field($model_cru_add,'in_cru_old')->hiddenInput([
    'id' => 'hidden_id_cru_add_child_in_cru_old',
])->label(false); ?>

<?php ActiveForm::end(); ?>


<?php

Modal::begin([
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'id' => 'modal_add_child_crujki',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block pull-left" id="#">Привязка детей к кружкам</h4>',
    'size' => Modal::SIZE_LARGE,
    'footer' => '<button type="submit" class="btn btn-default btn-md" id="but_cru_close_dialog2">Закрыть</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_cru_cru_add_child',
//    'layout' => 'horizontal',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'method' => 'POST',
    'action' => ['crujki/cru'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-0',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-12',
        ],
    ],
]); ?>

<div class="col-md-5">
    <?= $form->field($model,'name',[
        'addon' => [
            'prepend' => [
                'content' => Html::button('', ['class'=>'btn btn-default btn-md','id' => 'cru_add_count','disabled' => 'disabled','style' => ['width' =>  '45px']]),
                'asButton' => true
            ]
        ]
    ])->dropDownList($array_drop,[
        'id' => 'drop_cru_item_add_child',
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'name')->textInput([
        'id' => 'id_name_cru_form_modal_add_child',
        'placeholder' => 'Фамилия Имя',
        'readonly'=> true
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'in_cru')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'id_modal_form_in_cru',
            'placeholder' => 'Дата зачисления',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <?= $form->field($model_cru_add, 'out_cru')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'id_modal_form_out_cru',
            'placeholder' => 'Дата отчисления',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>

    <div class="btn-group btn-group-justified" data-toggle="buttons">
        <label class="btn btn-warning" id="but_cru_reset_add_child">
            <input type="checkbox" checked autocomplete="off">очистить поля
        </label>
        <label class="btn btn-success" id="but_cru_save_add_child">
            <input type="checkbox" checked autocomplete="off">сохранить
        </label>

    </div>

    <p class="alert alert-danger" style="margin-top: 20px" id="cru_message_error" hidden="hidden">Произошла ошибка, попробуйте снова!</p>
    <p class="alert alert-success" style="margin-top: 20px" id="cru_message_success" hidden="hidden">Данные сохранены успешно.</p>

</div>
<div class="col-md-7">
    <?=$form->field($model_search, 'name',[
        'addon' => [
            'append' => [
                'content' => Html::button('стереть', ['class'=>'btn btn-primary btn-danger','id' => 'id_but_cru_modal_add_clear']),
                'asButton' => true
            ]
        ]
    ])->textInput([
            'autofocus' => true,
            'id' => 'id_modal_search_add_child_cru',
            'placeholder' => 'Введите данные для поиска',
        ])->label(false); ?>
    <div id="id_table_add_child">
        <?= $this->render('view_table_cru_add_child',compact('array_add_child')) ?>
    </div>
</div>




<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_cru_add_child',
])->label(false); ?>

<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>





















<?php Modal::begin([
    'id' => 'modal_cru_help',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">Инструкция по кружкам</h4>',
//    'footer' => '<button type="submit" class="btn btn-info btn-md" id="#">Сохранить в excel</button>
//     <button type="submit" class="btn btn-success btn-md" id="#">Закрыть</button>',
//    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

//    'size' => Modal::SIZE_LARGE
]);?>
<?= $this->render('cru_view_help') ?>

<?php Modal::end();?>















<!--MODAL SEARCH-->


<?php Modal::begin([
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
    'id' => 'modal_search_child_crujki',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block pull-left">Поиск всех кружков у ребенка</h4>',
//    'size' => Modal::SIZE_LARGE,
//    'footer' => '<button type="submit" class="btn btn-default btn-md" id="but_cru_close_dialog2_search_child">Закрыть</button>',
]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form_cru_cru_search_child',
//    'layout' => 'horizontal',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'method' => 'POST',
    'action' => ['crujki/cru'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-lg-0',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-12',
        ],
    ],
]); ?>

<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_cru_search_child',
])->label(false); ?>

<?=$form->field($model_search, 'name',[
    'addon' => [
        'append' => [
            'content' => Html::button('Найти', ['class'=>'btn btn-light','id' => 'id_but_cru_modal_search_child']),
            'asButton' => true
        ]
    ]
])->textInput([
        'autofocus' => false,
        'id' => 'id_modal_search_search_child_cru',
        'placeholder' => 'Введите данные для поиска',
    ])->label(false); ?>

<div id="id_table_search_child">

</div>



<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>
<?php
$scr = <<< JS

$(function() {

    $('#id_but_cru_modal_search_child').on('click',function(){
        $('#hidden_id_cru_search_child').val(13);

        var form = $('#form_cru_cru_search_child').serializeArray();
//        console.log(form);return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader,#id_modal_search_search_child_cru').LoadingOverlay("show");
        var arr = $('#form_cru_cru_search_child');
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $('#form_cru_cru_search_child').trigger('reset');
            $("*").LoadingOverlay("hide");
            $('#id_table_search_child').hide().html(response).fadeIn();
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

        return false;
    });

    $('#drop_cru_item_year,#drop_cru_item_month,#drop_cru').on('change',function(){

        $('#but_save_excel_cru_tabel').attr("href","/index.php?r=reports%2Fexcel2&id=1&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());

        $('#but_save_excel_cru_akt').attr("href","/index.php?r=reports%2Fexcel2&id=2&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());

        $('#but_save_excel_cru_spravka').attr("href","/index.php?r=reports%2Fexcel2&id=3&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());

        $('#but_save_excel_cru_dogovora').attr("href","/index.php?r=reports%2Fexcel2&id=4&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());

        $('#but_save_excel_cru_dogovora_akti').attr("href","/index.php?r=reports%2Fexcel2&id=5&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());

        $('#but_save_excel_cru_reestr').attr("href","/index.php?r=reports%2Fexcel2&id=6&year="+$('#drop_cru_item_year').val()+
        "&month="+$('#drop_cru_item_month').val()+"&id_crujok="+$('#drop_cru').val());





        return false;
    });

    $('#form_cru_cru_add_child input').on('keyup', function(e){
        if(e.which == 13) $('#but_cru_save_add_child').click();
        return false;
    });



    $('#id_but_cru_modal_add_clear').on('click',function(){
        $('#id_modal_search_add_child_cru').val('').trigger('keyup');
        $('.cru_add_child_tr_background').css("background-color","#FFFFFF");
//        $('#hidden_id_cru_add_child_in_cru_old').val('');
        reset_pole_add_child();
        return false;
    });

    function reset_pole_add_child(){
//        $('#form_cru_cru_add_child').trigger('reset');
        $('#but_cru_reset_add_child,#but_cru_save_add_child').removeClass('active');
        $('#id_name_cru_form_modal_add_child,#id_modal_form_in_cru,#id_modal_form_out_cru,#hidden_id_cru_add_child_id_child,#hidden_id_cru_add_child_in_cru_old').val('');
        $('#hidden_id_cru_add_child_id_crujok_old').val(0);

    }

    /*$('#but_show_cru_help').on('click',function(){
        $('#modal_cru_help').modal('show');
        return false;
    });*/

    $('#but_cru_reset_add_child').on('click',function(){
        reset_pole_add_child();
        $('.cru_add_child_tr_background').css("background-color","#FFFFFF");
        return false;
    });

    $('#but_cru_show_dialog_add_child').on('click',function(){
//        $('#modal_add_child_crujki').modal('show');
//        return;

        $('#hidden_id_cru_add_child').val(3);
        var form = $('#form_cru_cru_add_child').serializeArray();
//        console.log(form);return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        var arr = $('#form_cru_cru');
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            reset_pole_add_child();
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_table_add_child').html(response);

            $('#modal_add_child_crujki').modal('show');
            $('#id_modal_search_add_child_cru').val('').focus();
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

        return false;

    });

    $('#but_cru_save_add_child').on('click',function(){

        $('.cru_add_child_tr_background').css("background-color","#FFFFFF");


        var old_check = $('#hidden_id_cru_add_child_id_crujok_old').val();
        console.log(old_check);
//        var s = $("#id_table_add_child").scrollTop();

        var n = $('#id_name_cru_form_modal_add_child').val();
        if(n.length<1){
            return false;
        }

        if(old_check > 0){
            var rrr = $('#id_modal_form_in_cru').val();
            if(rrr.length < 1){
                $('#hidden_id_cru_add_child').val(7);//delete
            }else{
                $('#hidden_id_cru_add_child').val(4);//update
            }
        }else{
            var rrr2 = $('#id_modal_form_in_cru').val();
            if(rrr2.length<1){
                return false;
            }
            $('#hidden_id_cru_add_child').val(6);//insert
        }

        var form = $('#form_cru_cru_add_child,#id_form_add_id_ch').serializeArray();

        var arr = $('#form_cru_cru');
        console.log(form);//return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            if(response==400){
                $('#cru_message_error').toggle(200).delay(2500).toggle(200);
            }else{
                reset_pole_add_child();
                $('#id_table_add_child').html(response);
                $('#id_modal_search_add_child_cru').val('').focus();
                $('#cru_message_success').toggle(200).delay(1000).toggle(200);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

        return false;
    });

    $('#drop_cru_item_add_child').on('change',function(){
        $('.cru_add_child_tr_background').css("background-color","#FFFFFF");
        $('#hidden_id_cru_add_child').val(5);
        $('#id_modal_search_add_child_cru').val('');
        reset_pole_add_child();
        var form = $('#form_cru_cru_add_child').serializeArray();
        var arr = $('#form_cru_cru');
        console.log(form);//return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_table_add_child').html(response);
            $('#id_modal_search_add_child_cru').val('').focus();
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
        return false;
    });

    $('#drop_cru_item_year,#drop_cru_item_month').on('change', function() {
        var value = $(this).val();
        console.log(value);
        $("#id_item_cru_boss").val(8);
        var form = $('#form_boss_item_crujki,#form_cru_panel_button').serializeArray();
        var arr = $('#form_boss_item_crujki');
        console.log(form);
//        return false;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
//                $(".item_child_sp").text("");
                $("*").LoadingOverlay("hide");
                $('#renderAjax_cru_table_boss').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });










    $('#but_cru_show_dialog').on('click',function(){
        $('#cru_message_one_percent,#cru_message_one_price').hide();
        reset_pole6();

        $('#hidden_id_cru').val(0);
        var form = $('#form_cru_cru').serializeArray();
        console.log(form);//return;
        $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        var arr = $('#form_cru_cru');
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
            $("*").LoadingOverlay("hide");
//            console.log(response);
            $('#id_renderAjax_modal_cru').html(response);
//            $('#id_modal_search_add_child_cru').val('');
            $('#modal_add_edit_crujki').modal('show');
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });

        return false;
    });

    function reset_pole6(){
        $('#form_cru_cru,#form_hide_daysweek_cru').trigger('reset');
        $('#but_pn,#but_vt,#but_sr,#but_ch,#but_pt,#but_cru_reset,#but_cru_save,#but_cru_add').removeClass('active');
        $('#name_cru_form,#price_cru_form,#zp_cru_form').val('');
//        $('#drop_pedagog_cru_form,').val(0);
        $('#hidden_pole_cru_id').val(0);
//        $('#but_cru_add').removeAttr('disabled');
    }

	/*$('#id_modal_search_add_child_cru').on('keyup',function(){
    $('#hidden_table_out_cru').hide();
    _this = this;
    $.each($('#table_crijki2 tbody tr'),function(){
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });*/

});

JS;
$this->registerJs($scr, yii\web\View::POS_END);
?>


