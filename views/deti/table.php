<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
use kartik\form\ActiveForm;
//use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$percent = '1%';
$model_so100->rozd = null;
$model_so100->in = null;

Modal::begin([
    'id' => 'modal_del_or_not',
    'header' => '<h3 style="text-align: center">Удалить ребёнка из базы данных ?</h3>',
    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_cancel_so">Отмена</button>
     <button type="submit" class="btn btn-success btn-md" id="but_deldel_so">Удалить навсегда</button>',
    'size' => Modal::SIZE_DEFAULT,
]);
echo '<div class="not_selected_text_on_block">
      <div class="alert alert-default" role="alert"><h3 style="text-align: center" id="name_so_for_del"></h3></div>
      </div>';
Modal::end();

Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>
<?php
 Modal::begin([
     'id' => 'modal_so100',
     'clientOptions' => [
         'backdrop' => 'static',
         'keyboard' => false,
     ],
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="id_header_modal_so100">Данные ребёнка</h4>',
     'size' => Modal::SIZE_LARGE,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_forever_so">Удалить ребёнка из базы</button>
     <button class="btn btn-default btn-md" id="but_message_save_change_deti" style="color: #ff0000;border: 0px" disabled="disabled">Не забудьте сохранить изменения !</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_so">Сохранить изменения</button>',
 ]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form100_hide',
]); ?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_so',
])->label(false); ?>
<?= $form->field($model_so100,'old_id_gruppa')->hiddenInput([
    'id' => 'hidden_old_id_gruppa',
])->label(false); ?>
<?/*= $form->field($model_so100,'id_gruppa')->hiddenInput([
    'id' => 'hidden_id_gruppa',
])->label(false); */?>


<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form1000',
//    'layout' => 'horizontal',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'method' => 'POST',
    'action' => ['deti/child'],
    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
         'horizontalCssClasses' => [
             'label' => 'col-lg-5',
             'offset' => 'col-lg-offset-3',
             'wrapper' => 'col-lg-7',
//             'error' => '',
//             'hint' => '',
         ],
     ],
]); ?>

<div class="col-sm-6">
    <?= $form->field($model_so100, 'name')->textInput([
//    'autofocus' => true,
//        'class' => 'fogot',
        'id' => 'nameso100',
        'placeholder' => 'Фамилия Имя'
    ]) ?>
    <?= $form->field($model_so100, 'otche')->textInput([
        'id' => 'otche100',
        'placeholder' => 'Отчество'
    ]) ?>
    <?= $form->field($model_so100,'id_gruppa')->dropDownList($array_gruppa,[
        'id' => 'id_gruppa',
//        'prompt' => 'Группа'
    ])->label('Группа') ?>

    <div id="id_deti_hidden_date_perevod_group_1" hidden="hidden">
        <?= $form->field($model_so100, 'perevod')->widget('yii\widgets\MaskedInput', [
            'options' => [
                'id' => 'rozdso1001_1',
                'placeholder' => 'Дата перевода в группу',
            ],
            'mask' => '99.99.9999',
        ]) ?>
    </div>

    <div id="id_deti_hidden_date_perevod_group_2" hidden="hidden">
        <?= $form->field($model_so100, 'perevod2',[
            'addon' => [
                'append' => [
                    'content' => Html::button('Отмена', [
                            'class'=>'btn btn-default btn-md',
                            'id' => 'id_cancel_perevod_group',
                            'style' => [
                                'background' => 'rgb(255, 219, 210)'
                            ],
                        ]),
                    'asButton' => true,
                ],
            ]
        ])->widget('yii\widgets\MaskedInput', [
            'options' => [
                'id' => 'rozdso1001_2',
                'placeholder' => 'Дата перевода',
            ],
            'mask' => '99.99.9999',
        ]) ?>
    </div>

    <?= $form->field($model_so100, 'rozd')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'rozdso100',
            'placeholder' => 'Дата рождения',
        ],
        'mask' => '99.99.9999',
    ]) ?>


    <?/*= $form->field($model_so100, 'tel')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'tel100',
            'placeholder' => 'Номер телефона',
        ],
        'mask' => '9(999)999-99-99',
    ]) */?>
    <?= $form->field($model_so100, 'snils')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'snils100',
            'placeholder' => 'СНИЛС',
        ],
        'mask' => '999-999-999 99',
    ]) ?>
    <?= $form->field($model_so100, 'adress')->textarea([
        'id' => 'address100',
        'rows' => 2,
        'placeholder' => 'Введите адрес'
    ]) ?>


    <?/*= $form->field($model_so100, 'pitanie', ['inline'=>true, 'enableLabel'=>true])->radioList([
        '1' => 'На питании',
        '2' => 'Нет',
    ],[
        'id' => 'blog_type',
        'class' => 'btn-group btn-group-justified',
        'data-toggle' => 'buttons',
        'unselect' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
            },
    ]); */?>

</div>
<div class="col-sm-6">

    <?/*= $form->field($model_so100, 'schet')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'schet100',
            'placeholder' => 'Номер лицевого счета',
        ],
        'mask' => '999999999999',
    ]) */?>

    <?=$form->field($model_so100, 'number_schet')
        ->textInput([
            'type' => 'number',
            'id' => 'schet100',
            'placeholder' => 'Номер лицевого счета',
        ]); ?>

    <?= $form->field($model_so100, 'in')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'ins100',
            'placeholder' => 'Дата зачисления',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?/*=$form->field($model_so100, 'prikaz_in')
        ->textInput([
            'type' => 'number',
            'id' => 'prikaz_in100',
            'placeholder' => 'Номер приказа зачисления',
        ]); */?>
<!--    <hr/>-->
    <?= $form->field($model_so100, 'out')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'outs100',
            'placeholder' => 'Дата отчисления',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?/*=$form->field($model_so100, 'prikaz_out')
        ->textInput([
            'type' => 'number',
            'id' => 'prikaz_out100',
            'placeholder' => 'Номер приказа увольнения',
        ]); */?>

    <?/*= $form->field($model_so100, 'polis')->textInput([
        'placeholder' => 'Полис',
        'id' => 'polis100',
    ]) */?>
    <?= $form->field($model_so100, 'polis')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'polis100',
            'placeholder' => 'Полис',
        ],
        'mask' => '9999 9999 9999 9999',
    ]) ?>
    <?= $form->field($model_so100, 'pol', [/*'inline'=>true,*/ 'enableLabel'=>true])->radioList([
        '1' => 'Мужской',
        '2' => 'Женский',
    ],[
        'id' => 'blog_type',
        'class' => 'btn-group btn-group-justified',
        'data-toggle' => 'buttons',
        'unselect' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
            },
    ]); ?>
    <?= $form->field($model_so100, 'alergia')->textarea([
        'id' => 'alergia100',
        'rows' => 2,
        'placeholder' => 'На что аллергия...'
    ]) ?>

<!--    --><?//= $form->field($model_so100, 'pol')->radio(['label' => 'Option 1', 'value' => 1, 'uncheck' => null]) ?>


</div>
<?= $form->field($model_so100,'id')->hiddenInput([
    'id' => 'hidden_pole_idso',
//    'disabled' => 'disabled'
])->label(false); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_so100">
    <table class="table-striped table-bordered table-hover">

    <!--<div class="my_table my_table2 not_selected_text_on_block">-->
    <?php
    $i = 1;
//    debug($model);
    foreach($model as $q){
        if (strpos($q['in'],'1900-01-01') !== false){
            $t = '';
        }else{
            $t = Yii::$app->formatter->asDate(trim($q['in']));
        }
        if (!empty($q['out'])||!is_null($q['out'])){
            $t2 = Yii::$app->formatter->asDate(trim($q['out']));
            $classs = 'id="row_sotr_all"';
        }else{
            $t2 = '';
            $classs = '';
        }
        if (strpos($q['rozd'],'1900-01-01') !== false){
            $u = '';
        }else{
            $u = Yii::$app->formatter->asDate(trim($q['rozd']));
        }

        $perevod = Yii::$app->formatter->asDate($q['perevod']);

        if (strlen(trim($q['alergia'])) > 0){
            $al = '&#127823; ';
        }else{
            $al = '';
        }

        echo '<tr '.$classs.' data-id="'.$q['id'].'" data-name="'.trim($q['name']).'" data-otche="'.trim($q['otche']).'" data-number_schet="'.trim($q['number_schet']).'" data-in="'.$t.'"  data-out="'.$t2.'" data-rozd="'.$u.'"
             data-id_gruppa="'.$q['id_gruppa'].'" data-pol="'.trim($q['pol']).'" data-otche="'.trim($q['otche']).'" data-adress="'.trim($q['adress']).'"
             data-snils="'.trim($q['snils']).'" data-polis="'.trim($q['polis']).'" data-old_id_gruppa="'.$q['old_id_gruppa'].'" data-perevod="'.$perevod.'" data-alergia="'.$q['alergia'].'">
                <td style="width: 2%">'.$i.'</td>
                <td style="width: 2%" id="tr_start_deti">'.trim($q['start']).'</td>
                <td id="id_n">'.$al.''.trim($q['name']).'</td>
                <td style="width: 6%">'.trim($q['number_schet']).'</td>
                <td style="width: 7%">'.$t.'</td>
                <td style="width: 7%">'.$u.'</td>
                <td style="width: 3%">'.trim($q['pol']).'</td>
                <td style="width: 15%">'.trim($q['adress']).'</td>
                <td style="width: 12%">'.trim($q['polis']).'</td>
                <td style="width: 10%">'.trim($q['snils']).'</td>
            </tr>';
        $i++;
    }

    ?>

    </table>
</div>

<?php
$script = <<<JS

$(function(){

    var idishka;
    if($('#b_del_so').hasClass('active')){
        $('#b_worked_so').removeClass('active');
//        $('#b_add_so').attr('disabled',false);
    }else{
        $('#b_worked_so').addClass('active');
    }

//    $('#b_del_so').hasClass('active') ? $('#b_worked_so').removeClass('active').$('#b_add_so').attr('disabled',false) : $('#b_worked_so').addClass('active');
    var check = 0;
    var id_gr = 0;
    var old_id_gruppa = 0;
    var perevod = null;

    $('table tr').on('click',function(){
        old_id_gruppa = 0;
        $('#form1000').trigger('reset');
        $('#id_data_check_deti').val(1);//ckeck hidden input on buttons panel up
        check = 0;
        $('#but_message_save_change_deti').hide();

        var id,form,name,schet,ins,outs,rozdso,id_gruppa,pitanie,pol,tel,address,polis,snils,opt,alergia,otche = null;
        $(this).addClass('#item_yellow_tr');
        id = $(this).data('id');
        name = $(this).data('name');
        schet = $(this).data('number_schet');
        ins = $(this).data('in');
        outs = $(this).data('out');
        rozdso = $(this).data('rozd');
        id_gruppa = $(this).data('id_gruppa');
        id_gr = $(this).data('id_gruppa');
        pol = $(this).data('pol');
        address = $(this).data('adress');
        polis = $(this).data('polis');
        snils = $(this).data('snils');
        old_id_gruppa = $(this).data('old_id_gruppa');
        console.log(old_id_gruppa+' old_id_gruppa');
        perevod = $(this).data('perevod');

        $('#id_deti_hidden_date_perevod_group_1').hide();
        if(old_id_gruppa > 0){
            $('#rozdso1001_2').val(perevod);
            $('#rozdso1001_1').val('');
            $('#id_deti_hidden_date_perevod_group_2').show();
        }else{
            $('#rozdso1001_2,#rozdso1001_1').val('');
            $('#id_deti_hidden_date_perevod_group_2').hide();
        }



//        console.log(perevod+' perevod');

        alergia = $(this).data('alergia');
        otche = $(this).data('otche');

        //установка переключателя "пол"
        if(pol.indexOf('М') > -1){
//        console.log('1');
            $('input:radio[name="Deti[pol]"][value=1]').click();
        }else if(pol.indexOf('Ж') > -1){
//        console.log('2');
            $('input:radio[name="Deti[pol]"][value=2]').click();
        }else if(pol.length < 1){
            console.log('3');
        }

        $('#id_gruppa').val(id_gruppa);

        /*opt = $('#id_gruppa option');
        opt.each(function(indx, element){
           if ( $(this).val() == id_gruppa ) {
              $(this).attr("selected", "selected")
           }
        });*/

        rozdso.length > 1 ? $('#rozdso100').val(rozdso) : $('#rozdso100').val('');
        address.length > 1 ? $('#address100').val(address) : $('#address100').val('');
        schet > 1 ? $('#schet100').val(schet) : $('#schet100').val('');
        ins.length > 1 ? $('#ins100').val(ins) : $('#ins100').val('');
        outs.length > 1 ? $('#outs100').val(outs) : $('#outs100').val('');
        snils.length > 1 ? $('#snils100').val(snils) : $('#snils100').val('');
        polis.length > 1 ? $('#polis100').val(polis) : $('#polis100').val('');

        $('#alergia100').val(alergia);
        $('#otche100').val(otche);
        $('#hidden_old_id_gruppa').val(old_id_gruppa);





        $('#but_save_so').text('Сохранить изменения');
        $('#hidden_pole_idso').val(id);
        $('#hidden_id_so').val(1);//update
        $('#nameso100').val(name);
        $('#but_del_forever_so').show();
        $('#modal_so100').modal('show');
        return false;
    });


    $('#but_save_so').click(function(){
        $('#b_del_so').hasClass('active') ? $('#hidden_id_so').val(4) : null;
        var iddd = $('#hidden_id_so').val();
        console.log(iddd+' iddd');//return;//////////////////////////*********************************************
        if (iddd == 1 || iddd == 5){//insert



            $('.modal').modal('hide');///////////*************************************************************************вернуть
            var form = $('#form100_hide,#form1000,#id_form_deti_drop_group').serializeArray();
            console.log(form);//return;//////////////////////////////////////////////////////
            var arr = $('#form1000');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
//                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }else if(iddd == 2){//update
            $('.modal').modal('hide');
//            dol = $("#dol100 option:selected").text();
            form = $('#form100_hide,#form1000,#id_form_deti_drop_group').serializeArray();
//            console.log(form)
//            form.splice(4,1);
//            form.splice(2,1);
//            form.push({name:'Sotrudniki[dol]',value:dol});
            console.log(form);//return;
            arr = $('#form1000');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
//                    $('.modal').modal('hide');
//                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#b_del_so').removeClass('active');
                        $('#b_worked_so').addClass('active');
                        $('#table_sotrudniki').html(response);
                    }
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('not');
//                    alert("Ошибка");
                });


        }else if(iddd == 4){
            $('#hidden_id_so').val(6);
            $('.modal').modal('hide');
//            var dol = $("#dol100 option:selected").text();
            form = $('#form100_hide,#form1000,#id_form_deti_drop_group').serializeArray();
//            form.splice(2,1);
//            form.splice(3,1);
//            form.push({name:'Sotrudniki[dol]',value:dol});
            console.log(form);//return;//////////////////////////////////////////////////////
            arr = $('#form1000');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }
        return false;
    });



    $('#but_del_forever_so').click(function(){
        $('#name_so_for_del').text($('#nameso100').val());
//        idishka = $('#hidden_pole_idso').val();
        $('.modal').modal('hide');
        $('#modal_del_or_not').modal('show');
        return false;
    });

    $('#but_del_cancel_so').click(function(){
        $('.modal').modal('hide');
        return false;
    });

    $('#but_deldel_so').click(function(){
        $('#hidden_id_so').val(3);
        var form = $('#form100_hide,#form1000,#id_form_deti_drop_group').serializeArray();
//        form.splice(2,1);
//        form.splice(3,1);
        if($('#b_del_so').hasClass('active')){
            var id7 = 0;
        }else{
            id7 = 1;
        }
        form.push({name:'id7',value:id7});
//        console.log(form);return;
//        return;
        $('.modal').modal('hide');
//        console.log(idishka);
        var arr = $('#form1000');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
            });
        return false;
    });


    $('#id_gruppa').on('change',function(){
    var current = $(this).val();
    console.log(current);
    console.log(id_gr);
    console.log(old_id_gruppa);

//    return;
    var tor = $('#id_data_check_deti').val();//твердая 1 при нажатии на ребенка в списке

//    console.log(tor+' tor');
//    console.log(check+' check');

    if(tor > 0){
        if(old_id_gruppa > 0){
            if(id_gr == current){
                $('#rozdso1001_2').val(perevod);
                $('#rozdso1001_1').val('');
                $('#id_deti_hidden_date_perevod_group_1').hide();
                $('#id_deti_hidden_date_perevod_group_2').fadeIn();
                $('#but_message_save_change_deti').hide();
                return;
            }else{
                $('#rozdso1001_1,#rozdso1001_2').val('');
                $('#id_deti_hidden_date_perevod_group_2').hide();
                $('#id_deti_hidden_date_perevod_group_1').fadeIn();
            }
        }else{

            var h = $('#hidden_id_so').val();
            console.log(h);
            console.log(check);
            if(h == 1 && check == 0){//if update
                check = 1;
                $('#rozdso1001_1,#rozdso1001_2').val('');
                $('#id_deti_hidden_date_perevod_group_1').fadeIn();
            }

            if(id_gr == current){
                console.log('ravno');
                check = 0;
                $('#rozdso1001_1,#rozdso1001_2').val('');
                $('#id_deti_hidden_date_perevod_group_1').fadeOut();
                $('#but_message_save_change_deti').hide();
                return;
            }
        }
    }
    $('#but_message_save_change_deti').fadeIn();
    return false;
    });





    $('#id_cancel_perevod_group').on('click',function(){
        var name_group = '';
        var opt = $('#id_gruppa option');
        opt.each(function(indx, element){
           if ( $(this).val() == old_id_gruppa ) {
              name_group = ($(this).text()).trim();
           }
        });
        if(confirm('Отменить перевод ? В случае отмены ребёнок вернется в группу: '+name_group+'.')){
            $('#id_deti_hidden_date_perevod_group_2').hide();
            $('#rozdso1001_1,#rozdso1001_2').val('');
            $('#id_gruppa').val(old_id_gruppa);
            $('#but_message_save_change_deti').fadeIn();
        }
        return false;
    });



})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

