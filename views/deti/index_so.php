<?php

use kartik\form\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;



?>

<?/*= $this->render('_search') */?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= Html::button('Добавить ребёнка', ['class'=>'btn btn-md btn-success btn-block','id' => 'b_add_so']); ?>
        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <?php $form = ActiveForm::begin([
                'id' => 'id_form_deti_drop_group',
                'method' => 'POST',
                'action' => ['deti/child'],
                'type' => ActiveForm::TYPE_INLINE,
                'enableAjaxValidation' => false,//
            ])?>
            <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
                'id' => 'drop_deti_group',
            ])->label(false) ?>
            <div class="btn-group btn-group" role="group">
                <div class="btn-group pull_right" role="group">
                    <?= Html::button('Отчисленные', ['class'=>'btn btn-md btn-default','id' => 'b_del_so']); ?>
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button('Зачисленные', ['class'=>'btn btn-md btn-default active','id' => 'b_worked_so']); ?>
                </div>
            </div>
            <?//= $form->field($model_id,'id')->hiddenInput()->label(false); ?>
<!--            --><?//= Html::button('Отчисленные', ['class'=>'btn btn-md btn-default','id' => 'b_del_so']); ?>
<!--            --><?//= Html::button('Зачисленные', ['class'=>'btn btn-md btn-default active','id' => 'b_worked_so']); ?>
            <?= $form->field($model_check,'id')->hiddenInput([
                'id' => 'id_data_check_deti',
            ])->label(false); ?>
            <?php $form = ActiveForm::end()?>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
        <table>
            <tr>
                <td style="width: 2%">№</td>
                <td style="width: 2%">!</td>
                <td>Фамилия Имя</td>
                <td style="width: 6%">№ л.счета</td>
                <td style="width: 7%">Дата зачис.</td>
                <td style="width: 7%">Дата рожд.</td>
<!--                <td style="width: 11%">Должность</td>-->
<!--                <td style="width: 7%">На питании</td>-->
                <td style="width: 3%">Пол</td>
<!--                <td style="width: 8%">Телефон</td>-->
                <td style="width: 15%">Адрес</td>
                <td style="width: 12%">Полис</td>
                <td  style="width: 10%">СНИЛС</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render('table',compact(
            'model_group',
            'array_gruppa',
            'model_so100',
            'model_id',
            'model')) ?>
    </div>
</div>
<?php
$script = <<<JS

$(function(){

    /*$(function(){
        if(prompt('пароль')!=112233){

            open('http://kru/index.php?r=crujki/cru');
            *//*console.log(500);
            $('#hidden_id_so').val(600);
            var form6 = $('#id_form_deti_drop_group,#form100_hide').serializeArray();
            var testform6 = $('#id_form_deti_drop_group');
            $.ajax({
                type : testform6.attr('method'),
                url : testform6.attr('action'),
                data : form6
            }).done(function(response) {
                console.log('yes');
            }).fail(function() {
                console.log('not');
            });
            return false;*//*
        }
    });*/

    $('#drop_deti_group').on('change', function() {
        var value = $(this).val();
        var show_del;
        $('#b_del_so').hasClass('active') ? show_del = 1 : show_del = 0;
//        console.log(value);//return;
        $('#hidden_id_so').val(555);
            var form = $('#id_form_deti_drop_group,#form100_hide').serializeArray();
            var testform = $('#id_form_deti_drop_group');
            form.push({name:'show_del',value:show_del});
//            form.splice(4,1);
//            form.splice(2,1);
            console.log(form);//return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {

                $("*").LoadingOverlay("hide");
//                console.log(response);return;
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    $('#b_del_so').click(function(){///button show deleted
        $(this).addClass('active');
        $('#b_worked_so').removeClass('active');

        $('#hidden_id_so').val(4);//show deleted empoyes
        var form = $('#id_form_deti_drop_group,#form100_hide').serializeArray();
        console.log(form);//return;
        var arr = $('#form1000');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        c
        return false;
    });

    $('#b_worked_so').click(function(){
        $(this).addClass('active');
        $('#b_del_so').removeClass('active');

        $('#hidden_id_so').val(5);
        var form = $('#id_form_deti_drop_group,#form100_hide').serializeArray();
        console.log(form);//return;
        var arr = $('#form1000');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#b_add_so').attr('disabled',false);
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    $('#b_add_so').click(function(){

    $('#id_data_check_deti').val(0);
//        var old_id_gruppa = 0;
        $('#hidden_old_id_gruppa').val(0);
        $('#form1000').trigger('reset');
        $('#but_del_forever_so,#but_message_save_change_deti').hide();
        $('#hidden_id_so').val(2);
        $('#address100').val('');
        $('#schet100').val('');
        $('#ins100').val('');
        $('#rozdso100').val('');
        $('#outs100').val('');
        $('#snils100').val('');
        $('#polis100').val('');
        $('#nameso100').val('');
        $('#id_gruppa').val($('#drop_deti_group').val());
        $('#id_deti_hidden_date_perevod_group_1,#id_deti_hidden_date_perevod_group_2').hide();
        $('input:radio[name="Deti[pol]"][value=1]').click();
        $('#but_save_so').text('Добавить ребёнка');
        $('#modal_so100').modal('show');
        return false;
    });


})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>