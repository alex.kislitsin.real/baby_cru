<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//debug($fff);
/*if (count($array)<1){
    $thead = 'cru_view_thead_empty';
}else{
    $thead = 'table_boss_cru';
}*/
$width = '2.5%';

Modal::begin([
    'id' => 'modal_error_five',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Выбранный период еще не наступил !</div>
</div>';
Modal::end();
?>




<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= Html::a('Сохранить в Excel', [
//                'reports/excel','id' => 1,
//                'year' => $model_y->name,
//                'month' => $model_m->name,
//                'id_group' => $model_group->name
            ], ['class'=>'btn btn-md btn-success btn-block','id' => 'but_five_excel1']); ?>
        </div>
        <div class="rep_boss_up_panel_1">
            <div class="date_picker_so">

                <?php $form = ActiveForm::begin([
                    'id' => 'form_five_panel_button',
                    'action' => ['five/five'],
                    'method' => 'POST',
                    'type' => ActiveForm::TYPE_INLINE,
                ])?>

                <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
                    'id' => 'drop_five_year',
                ])->label(false) ?>
                <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
                    'id' => 'drop_five_month',
                ])->label(false) ?>





                <?php $form = ActiveForm::end()?>
            </div>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block" id="id_table_five" style="overflow: hidden">
        <table>
            <tr>
                <td class="not_item" style="width: 2%;text-align: center">№</td>
                <td class="not_item">Название группы</td>
                <td class="not_item" style="width: 4%">Всего</td>

                <?php
                for($i=1;$i<=31;$i++){
                    echo '<td class="not_item" style="width: '.$width.' ">'.$i.'</td>';
                }
                ?>

                <td class="not_item" style="width: 5%">Детодни</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down" id="renderAjax_five_table_boss" style="overflow: hidden">
        <?= $this->render('five_table',compact(
            'date_proc',
            'model_d_antidate',
            'model_d_date',
            'array',
            'model_y',
            'model_m'
//            'array_boss'
        )) ?>
    </div>
</div>


