<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
$width = '2.5%';
$year = $model_y->name;
$month = $model_m->name;

?>
<!--<div style="background-color: #e83e8c"></div>-->
<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-striped table-bordered">

        <?php

        $kol_all = 0;
        $summ = 0;
        $nacopitel_summ = 0;

        for($t=1;$t<=31;$t++){
            ${'value'.$t} = '';
            ${'styles'.$t} = '"';
            ${'summ'.$t} = 0;
        }

        foreach($array as $q){

            $date = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
            $last_day = date('t',strtotime($date));
            $today = date('d',strtotime($date_proc));

        if (intval($q['id'])>0){
            $kol_all = $kol_all + $q['kolvse'];
            $summ = $summ + $q['summ'];
        }






            $red = 0;

            for($t=1;$t<=31;$t++){

                if ($t > $last_day){
                    ${'value'.$t} = 'X';
                    ${'styles'.$t} = '"';
                }else{

                    $iter_date = date('Y-m-d',strtotime(date($year.'-'.$month.'-'.$t)));

                    if($q['k'.$t] == null){
                        ${'value'.$t} = '<span style="color: #adadad">B</span>';
                        ${'styles'.$t} = '"';
                    }else{
                        $red++;
                        if ($red == 5){
                            ${'value'.$t} = $q['k'.$t] == 0 ? '' : $q['k'.$t];
                            $ch = 0;
                            $ch = $q['k'.$t] == 0 ? 0 : $q['k'.$t];
                            if (intval($q['id'])>0){
                                ${'summ'.$t} = ${'summ'.$t} + $ch;
                            }
                            ${'styles'.$t} = ';background-color: rgb(255, 219, 210)"';
                            $red = 0;
                        }else{
                            if ($iter_date > date('Y-m-d')){
                                ${'value'.$t} = '';
                                ${'styles'.$t} = ';background-color: transparent"';
                            }else{
                                ${'value'.$t} = $q['k'.$t] == 0 ? '' : $q['k'.$t];
                                $ch = 0;
                                $ch = $q['k'.$t] == 0 ? 0 : $q['k'.$t];
                                if (intval($q['id'])>0){
                                    ${'summ'.$t} = ${'summ'.$t} + $ch;
                                }
                                ${'styles'.$t} = ';background-color: rgb(216, 250, 217)"';//light - green
                            }
                        }
                    }
                }
            }

            if ($q['id'] == null){

                //итоговая строка
                echo '<tr>
                <td style="width: 2%"></td>
                <td id="id_n">Итого</td>
                <td style="width: 4%">'.$kol_all.'</td>';

                for($i=1;$i<=31;$i++){
                    if($q['k'.$i] == null){
                        $e = '<span style="color: #adadad">B</span>';
                    }else{
                        $e = ${'summ'.$i};
                        $e = $e == 0 ? '' : $e;
                    }

                    echo '<td style="width: 2.5%'.${'styles'.$i}.'>'.$e.'</td>';
                }

                echo '<td style="width: 5%">'.$summ.'</td>
                </tr>';

                //строка витамин
                echo '<tr style="background-color: #faffc5">
                <td style="width: 2%"></td>
                <td id="id_n">Витамин C</td>
                <td style="width: 4%"></td>';

                for($i=1;$i<=31;$i++){
                    $w = round(((${'summ'.$i}*50)/1000),1);
                    $w = $w == 0 ? '' : $w;
                    echo '<td style="width: 2.5%">'.$w.'</td>';
                    $nacopitel_summ = $nacopitel_summ + round(((${'summ'.$i}*50)/1000),1);
                }

                echo '<td style="width: 5%">'.$nacopitel_summ.'</td>
                </tr>';


                //сотрудники
                echo '<tr style="background-color: transparent;height: 10px">
                <td style="width: 2%;border: 0"></td>
                <td id="id_n" style="border: 0"></td>
                <td style="width: 4%;border: 0"></td>';

                for($i=1;$i<=31;$i++){
                    echo '<td style="width: 2.5%;border: 0"></td>';
                }

                echo '<td style="width: 5%;border: 0"></td>
                </tr>';

                echo '<tr>
                <td style="width: 2%">'.$q['id'].'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: 4%">'.$q['kolvse'].'</td>';

                for($i=1;$i<=31;$i++){
                    echo '<td style="width: 2.5%'.${'styles'.$i}.'>'.${'value'.$i}.'</td>';
                }

                echo '<td style="width: 5%">'.$q['summ'].'</td>
                </tr>';

                break;
            }else{
                echo '<tr>
                <td style="width: 2%">'.$q['id'].'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: 4%">'.$q['kolvse'].'</td>';

                for($i=1;$i<=31;$i++){
                    echo '<td style="width: 2.5%'.${'styles'.$i}.'>'.${'value'.$i}.'</td>';
                }

                echo '<td style="width: 5%">'.$q['summ'].'</td>
                </tr>';
            }


        }

        ?>

    </table>
</div>