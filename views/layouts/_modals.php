<?php
use app\models\Item_date;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

$model_item_date = new Item_date();
$model_item_date->item_date = date('d.m.Y');

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
$array_disabled_dates = array();
foreach($model_d_date as $date){
    $date = date('d.m.Y',  strtotime($date));
    array_push($array_disabled_dates,$date);
}

$model_dvig = array();
$array = array();


 Modal::begin([
    'id' => 'myModal555',
    'header' => '<h4 style="padding-left: 10px">Движение</h4>',
    /*'clientOptions' => [
    'backdrop' => 'static',
    'keyboard' => false,
    ],*/
    'size' => Modal::SIZE_LARGE,
    ]);?>

<?php $form = ActiveForm::begin([
    'id' => 'form_dvij_new',
    'action' => ['reports/dvij'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_INLINE,
    'enableAjaxValidation' => false,
])?>
<?= $form->field($model_item_date,'item_date')->widget(DatePicker::className(),[
    'removeButton' => false,
    'size' => 'md',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'readonly' => true,
    'options' => [
        'placeholder' => 'Выберите дату ...',
        'id' => 'dp_dvij_new',

    ],
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
//                        'daysOfWeekDisabled' => [0, 6],
        'daysOfWeekHighlighted' => [0, 6],
        'datesDisabled' => $array_disabled_dates,
        'toggleActive'   => true,
        'autoclose'=>true,
        'startDate' => '-1Y',
        'endDate' => '+1Y',
        'format' => 'dd.mm.yyyy'
    ],
    'pluginEvents' => [
        'changeDate' => 'function(e) {
                     var date1 = $("#dp_dvij_new").val();
                     if(date1 != ""){
                         var testform = $("#form_dvij_new");
                         $("#dp_dvij_new").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                         //$("#id_grid_dvig").LoadingOverlay("show");
                         $.ajax({
                             type : testform.attr("method"),
                             url : testform.attr("action"),
                             data : testform.serializeArray()
                         }).done(function(response) {
                             $("*").LoadingOverlay("hide");
                             $("#id_grid_dvig").html(response);
                         }).fail(function() {
                             $("*").LoadingOverlay("hide");
                             console.log("not");
                         });
                     }
                     }',
    ]
])->label(false); ?>

    <div class="rep_boss_down_dvij" id="id_grid_dvig" style="margin-top: 10px">
        <?= $this->render('dvigenie_table',compact('model_dvig','array')) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php Modal::end();?>