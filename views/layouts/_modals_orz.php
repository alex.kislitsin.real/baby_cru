<?php
use app\models\Item_date;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

$model_item_date = new Item_date();
$model_item_date->item_date = date('d.m.Y');

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
$array_disabled_dates = array();
foreach($model_d_date as $date){
    $date = date('d.m.Y',  strtotime($date));
    array_push($array_disabled_dates,$date);
}

$array = array(1,2,3);
//$data = '';

 Modal::begin([
    'id' => 'modal_hot_orz',
    'header' => '<h4 style="padding-left: 10px">ОРЗ</h4>',
    /*'clientOptions' => [
    'backdrop' => 'static',
    'keyboard' => false,
    ],*/
    'size' => Modal::SIZE_LARGE,
    ]);?>

<?php $form = ActiveForm::begin([
    'id' => 'hot_form_orz',
    'action' => ['reports/orz'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_INLINE,
    'enableAjaxValidation' => false,
])?>
<?= $form->field($model_item_date,'item_date')->widget(DatePicker::className(),[
    'removeButton' => false,
    'size' => 'md',
    'type' => DatePicker::TYPE_COMPONENT_APPEND,
    'readonly' => true,
    'options' => [
        'placeholder' => 'Выберите дату ...',
        'id' => 'dp_orz_new',
        'style' => 'height: 40px'

    ],
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
//                        'daysOfWeekDisabled' => [0, 6],
        'daysOfWeekHighlighted' => [0, 6],
        'datesDisabled' => $array_disabled_dates,
        'toggleActive'   => true,
        'autoclose'=>true,
        'format' => 'dd.mm.yyyy'
    ],
    'pluginEvents' => [
        'changeDate' => 'function(e) {
                     var date1 = $("#dp_orz_new").val();
                     if(date1 != ""){
                         var testform = $("#hot_form_orz");
                         var testform2 = $("#hot_form_orz").serializeArray();
//                         console.log(testform2);
                         $(this).LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                         $.ajax({
                             type : testform.attr("method"),
                             url : testform.attr("action"),
                             data : testform2
                         }).done(function(response) {
                             $("*").LoadingOverlay("hide");
//                             console.log(response);
                             $("#id_grid_orz").html(response);
                         }).fail(function() {
                             $("*").LoadingOverlay("hide");
                             console.log("not");
                         });
                     }
                     }',
    ]
])->label(false); ?>

    <div class="rep_boss_down_dvij" id="id_grid_orz" style="margin-top: 10px">
        <?= $this->render('dvigenie_orz',compact('array','data')) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php Modal::end();?>