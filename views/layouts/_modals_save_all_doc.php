<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$model = array();
$array = array();
$model_id = new \app\models\Id();
$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
if (count($array_gruppa)<1 || !isset($array_gruppa)){
    $query_gruppa = "select * from gruppa order by id";
    $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
    $array_gruppa = ArrayHelper::index($array_gruppa,'id');
    $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

    $json_array_group = json_encode($array_gruppa);
    $cookie = new \yii\web\Cookie([
        'name' => 'array_group',
        'value' => $json_array_group,
    ]);
    Yii::$app->response->cookies->add($cookie);
}
array_unshift($array_gruppa, "Все группы");

$model_group = new \app\models\Gruppa();
$model_group->name = 0;
$model_y = new \app\models\Year();
$model_y->name = date('Y');
$model_m = new \app\models\Month();
$model_m->name = date('n');

$model_period = new \app\models\Period_month();
$model_period->name1 = date('n');
$model_period->name2 = 0;
//$model_period->name2 = date('n');

$array_year = [
    2019 => '2019',
    2020 => '2020',
    2021 => '2021',
    2022 => '2022',
    2023 => '2023',
];
$array_years_deti = [
    0 => 'Возраст',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5',
    6 => '6',
    7 => '7',
    8 => '8',
];

$model_id->id = 0;

$_monthsList = array(
    1=>"Январь",2=>"Февраль",3=>"Март",
    4=>"Апрель",5=>"Май", 6=>"Июнь",
    7=>"Июль",8=>"Август",9=>"Сентябрь",
    10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");
?>


<?php Modal::begin([
    'id' => 'modal_hot_save_all_doc',
    'header' => '<h4 style="padding-left: 10px">Сохранить журналы в файл</h4>',
    'size' => Modal::SIZE_LARGE,
    'clientOptions' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ],
//    'footer' => '<button class="btn btn-default btn-md" id="but_message_save_settings" style="color: #008000;border: 0px;font-size: 16px;font-weight: bold" disabled="disabled">Данные успешно сохранены !</button>
//    <button type="submit" class="btn btn-success btn-md" id="but_save_settings">Сохранить изменения</button>',
]);?>

<div class="row" style="background-color: transparent;height: auto">
    <div class="col-md-4">
        <?php $form = ActiveForm::begin([
            'id' => 'hot_form_save_all_doc',
            'action' => ['reports/save_all_doc'],
            'method' => 'POST',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'enableAjaxValidation' => false,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-lg-6',
                    'offset' => 'col-lg-offset-0',
                    'wrapper' => 'col-lg-6',
                ],
            ],
        ])?>
        <?/*= $form->field($model_id,'id')->hiddenInput([
            'id' => 'id_save_all_doc_ajax',
        ])->label(false); */?>

        <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
            'id' => 'drop_group_save_all_doc',
        ])->label('Группа') ?>
        <?= $form->field($model_id,'id')->dropDownList($array_years_deti,[
            'id' => 'drop_vozrast_save_all_doc',
        ])->label('Возраст (лет)') ?>
        <?= $form->field($model_y,'name')->dropDownList($array_year,[
            'id' => 'drop_year_save_all_doc',
        ])->label('Год') ?>
        <?= $form->field($model_m,'name')->dropDownList($_monthsList,[
            'id' => 'drop_month_save_all_doc',
        ])->label('Месяц') ?>

        <?= $form->field($model_period,'name1')->dropDownList($_monthsList,[
            'id' => 'drop_month1_save_all_doc',
        ])->label('Заболев. месяц 1') ?>

        <?= $form->field($model_period,'name2')->dropDownList($_monthsList,[
            'id' => 'drop_month2_save_all_doc',
            'prompt' => '---'
        ])->label('Заболев. месяц 2') ?>




    </div>
    <div class="col-md-8">
        <div class="btn-group btn-group-justified" style="margin: 0 0 5px 0">
            <?= Html::a('Журнал учёта ОРЗ', [
                'reports/save_all_doc','id' => 1,
                'year' => $model_y->name,
                'month' => $model_m->name,
                'id_gruppa' => $model_group->name,
            ], ['class'=>'btn btn-md btn-default','style' => ['width' => '20px'],'id' => 'but_save_excel_journal_orz']); ?>
            <?= Html::a('обновить', ['reports/save_all_doc','id' => 101], ['class'=>'btn btn-md btn-success','id' => 'but_save_excel_journal_orz_update']); ?>
        </div>
        <div class="btn-group btn-group-justified" style="margin: 0 0 5px 0">
            <?= Html::a('Журнал учёта соматических заболеваний', [
                'reports/save_all_doc','id' => 2,
                'year' => $model_y->name,
                'month' => $model_m->name,
                'id_gruppa' => $model_group->name,
            ], ['class'=>'btn btn-md btn-default','style' => ['width' => '20px'],'id' => 'but_save_excel_journal_somat']); ?>
            <?= Html::a('обновить', ['reports/save_all_doc','id' => 201], ['class'=>'btn btn-md btn-success','id' => 'but_save_excel_journal_somat_update']); ?>
        </div>
        <div class="btn-group btn-group-justified" style="margin: 0 0 5px 0">
            <?= Html::a('Списки детей', [
                'reports/save_all_doc','id' => 3,
                'year' => $model_y->name,
                'month' => $model_m->name,
                'id_gruppa' => $model_group->name,
                'vozrast' => $model_id->id,
            ], ['class'=>'btn btn-md btn-default','style' => ['width' => '20px'],'id' => 'but_save_excel_journal_spiski_deti']); ?>
            <?= Html::a('обновить', ['reports/save_all_doc','id' => 301], ['class'=>'btn btn-md btn-success','id' => 'but_save_excel_journal_spiski_deti']); ?>
        </div>
        <div class="btn-group btn-group-justified" style="margin: 0 0 5px 0">
            <?= Html::a('Заболеваемость', [
                'reports/save_all_doc','id' => 4,
                'year' => $model_y->name,
                'month1' => $model_period->name1,
                'month2' => $model_period->name2,
            ], ['class'=>'btn btn-md btn-default','style' => ['width' => '20px'],'id' => 'but_save_excel_journal_zabol']); ?>
            <?= Html::a('обновить', ['reports/save_all_doc','id' => 401], ['class'=>'btn btn-md btn-success','id' => 'but_save_excel_journal_zabol']); ?>
        </div>
        <div class="btn-group btn-group-justified" style="margin: 0 0 5px 0">
            <?= Html::a('Журнал р.Манту', [
                'reports/save_all_doc','id' => 5,
                'id_gruppa' => $model_group->name,
            ], ['class'=>'btn btn-md btn-default','style' => ['width' => '20px'],'id' => 'but_save_excel_journal_mantu']); ?>
            <?= Html::a('обновить', ['reports/save_all_doc','id' => 401], ['class'=>'btn btn-md btn-success','id' => 'but_save_excel_journal_mantu']); ?>
        </div>






    </div>

</div>
<?php //debug($array_gruppa); ?>
<?php ActiveForm::end(); ?>
<?php Modal::end();?>

<?php
$scr = <<<JS
$(function(){

    $('#hot_form_save_all_doc').trigger('reset');

    $('#drop_year_save_all_doc,#drop_month_save_all_doc,#drop_group_save_all_doc,#drop_vozrast_save_all_doc,#drop_month1_save_all_doc,#drop_month2_save_all_doc').on('change',function(){

        $('#but_save_excel_journal_orz').attr("href","/index.php?r=reports%2Fsave_all_doc&id=1&year="+$('#drop_year_save_all_doc').val()+
        "&month="+$('#drop_month_save_all_doc').val()+"&id_gruppa="+$('#drop_group_save_all_doc').val());

        $('#but_save_excel_journal_somat').attr("href","/index.php?r=reports%2Fsave_all_doc&id=2&year="+$('#drop_year_save_all_doc').val()+
        "&month="+$('#drop_month_save_all_doc').val()+"&id_gruppa="+$('#drop_group_save_all_doc').val());

        $('#but_save_excel_journal_spiski_deti').attr("href","/index.php?r=reports%2Fsave_all_doc&id=3&year="+$('#drop_year_save_all_doc').val()+
        "&month="+$('#drop_month_save_all_doc').val()+"&id_gruppa="+$('#drop_group_save_all_doc').val()+"&vozrast="+$('#drop_vozrast_save_all_doc').val());

        //кнопка заболеваемость
        $('#but_save_excel_journal_zabol').attr("href","/index.php?r=reports%2Fsave_all_doc&id=4&year="+$('#drop_year_save_all_doc').val()+
        "&month1="+$('#drop_month1_save_all_doc').val()+"&month2="+$('#drop_month2_save_all_doc').val());

        $('#but_save_excel_journal_mantu').attr("href","/index.php?r=reports%2Fsave_all_doc&id=5&id_gruppa="+$('#drop_group_save_all_doc').val());

        /*$('#but_save_excel_cru_reestr').attr("href","/index.php?r=reports%2Fsave_all_doc&id=6&year="+$('#drop_year_save_all_doc').val()+
        "&month="+$('#drop_month_save_all_doc').val()+"&id_gruppa="+$('#drop_group_save_all_doc').val());*/





        return false;
    });
})
JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>