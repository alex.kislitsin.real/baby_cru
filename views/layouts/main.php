<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?//= Alert::widget()
/*$value = Yii::$app->db->createCommand("SELECT date_format(max(date_to), '%d.%m.%Y   %H:%i') AS `one` FROM send_five;")->queryOne();
if (empty($value['one'])){
    $messaga = '';
}else{
    $messaga = 'последний отчет детодни отправлен '.$value['one'];
}*/
$file = \Yii::$app->cache->get('access');
$styleButton = !$file ? 'btn btn-danger btn-md but_hot_line' : 'btn btn-success btn-md but_hot_line';
$gd = !$file ? 1 : 0;
$textButton = !$file ? 'режим педагог' : 'режим ст. воспитатель';
?>


<div class="wrap">

    <div class="block_shapka">

        <div class="block_shapka_left">
<!--            <div style="width: 250px;margin-right: 10px;min-height: 35px;background-color: transparent" id="id_a_access"></div>-->
            <?= Html::button($textButton,[
                'class' => $styleButton,
                'id' => 'id_a_access',
                'style' => [
                    'margin-right' => '1vh',
		    'width' => '250px'	
                ],
            ]) ?>
        </div>
        <div class="block_shapka_center">
            <div class="btn-group">
                <?/*= Html::button('Движение', [
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_dvij_main',

                ]) */?><!--
                <?/*= Html::button('ОРЗ', [
//                'href' => '#myModal555',
//                'data-toggle' => 'modal',
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_orz_main',

                ]) */?>
                <?/*= Html::button('&#127823; Алергики', [
//                'href' => '#modal_hot_alergia',
//                'data-toggle' => 'modal',
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_alergia_main',

                ]) */?>
                --><?/*= Html::a('На главную', ['start/system'],[
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_start_main',

                ]) */?>
                <?= Html::button('Настройки', [
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_settings_main',
                    'style' => [
                            'height' => '35px'
                    ]

                ]) ?>
                <?/*= Html::button('Доступ', [
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_rbac_main',

                ]) */?>
                <?/*= Html::button('Все журналы', [
                    'class'=>'btn btn-default btn-md but_hot_line',
                    'id'=>'but_all_save_doc_main',
//                    'href' => '#modal_hot_save_all_doc',
//                    'data-toggle' => 'modal',
//                    'onclick' => '$("#hot_form_save_all_doc").trigger("reset");',
                ]) */?>


            </div>

        </div>
        <div class="block_shapka_right_l_2">
<!--            <div id="id_message_five" style="height: 100%; width: 100%;text-align: right;padding-top: 2%;color: #000000">--><?//= $messaga ?><!--</div>-->
        </div>
        <div class="block_shapka_right_l">
            <div id="anim_loader" style="height: 100%; width: 40px"></div>
        </div>
        <div class="block_shapka_right_r">
            <?/*= Html::a('Выйти', ['site/logout'], [
//                'href' => 'site/logout',
                'onclick' => 'return confirm("Вы хотите выйти из системы ?");',
                'class'=>'btn btn-light btn-md',
                'id'=>'exit_off',
                'style' => [
                    'height' => '100%',
                    'color' => 'red'
//                    'border' => '1px solid rgb(255, 219, 210)'
                ]
            ]) */?>
        </div>

    </div>
    <div class="block2">
        <div class="block_sidebar">
            <?= $this->render('_sidebar') ?>
        </div>
        <div class="block_content">
            <?= $content ?>
        </div>
    </div>
<!--    <div class="block block_footer"></div>-->

</div>

<?php
$scr = <<<JS
    $('#id_a_access').on('click',function(e) {
      var url = 'cru/index.php?r=crujki%2Faccess&id={$gd}';
      e.preventDefault();
      if ({$gd} == 0){
      	location.href = url;
      } else {
      	var p = prompt('пароль для входа');
      if (parseInt(p) == 112233){
      	location.href = url;
      } 
      }      
      return false;
    })

JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>

<?//= $this->render('_modals') ?>
<?//= $this->render('_modals_orz') ?>
<?//= $this->render('_modals_alergia') ?>
<?= $this->render('_modals_settings') ?>
<?//= $this->render('_modals_rbac') ?>
<?//= $this->render('_modals_save_all_doc') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>




