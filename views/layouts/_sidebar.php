<?php
use kartik\sidenav\SideNav;
/*$array = json_decode(Yii::$app->request->cookies->getValue('array_checking'), true);

$pitanie_deti = $array['pitanie_deti'];
$pitanie_so = $array['pitanie_so'];
$delo_deti = $array['delo_deti'];
$delo_so = $array['delo_so'];
$antro = $array['antro'];
$tabel = $array['tabel'];
$journal = $array['journal'];
$search = $array['search'];
$five = $array['five'];
$correct = $array['correct'];
$history = $array['history'];
$injection = $array['injection'];
$crujki = $array['crujki'];
$zp = $array['zp'];*/

/*$summ = $pitanie_deti+$pitanie_so+$delo_deti+$delo_so+$antro+$tabel+$journal+$search+$five+$correct+$history+$injection+$crujki+$zp;
if($summ == 0){
    return $this->redirect(['site/notdostup']);
}*/

$item = Yii::$app->controller->action->id;


echo SideNav::widget([
    'type' => SideNav::TYPE_DEFAULT,
//    'heading' => 'Меню',
    'items' => [
        [
            'url' => 'index.php?r=sp/spview',
            'label' => 'Питание (дети)',
            'active'=>($item == 'spview'),
//            'visible' => $pitanie_deti == 1
            'visible' => false
//            'icon' => 'list'
        ],
        [
            'url' => 'index.php?r=spso/spsoview',
            'label' => 'Питание (сотрудники)',
            'active'=>($item == 'spsoview'),
//            'icon' => 'list',
            'visible' => false
//            'visible' => verification_user()==555
        ],
        [
            'url' => 'index.php?r=sotrudniki/so',
            'label' => 'Сотрудники',
            'active'=>($item == 'so'),
//            'icon' => 'user',
            'visible' => true
//            'visible' => verification_user()==555
        ],
        [
            'url' => 'index.php?r=deti/child',
            'label' => 'Дети',
            'active'=>($item == 'child'),
//            'icon' => 'user',
            'visible' => true,
//            'visible' => verification_user()==555
        ],
        [
            'url' => 'index.php?r=antro/antro',
            'label' => 'Антропометрия',
            'active'=>($item == 'antro'),
            'visible' => false
//            'visible' => verification_user()!=1425
//            'icon' => 'record'
        ],
        /*[
            'url' => 'index.php?r=my/test333',
            'label' => 'test333',
            'active'=>($item == 'test333'),
            'icon' => 'folder-open'
        ],*/
        [
            'url' => 'index.php?r=usernotsad/bosscalendar',
            'label' => 'Контакты',
//            'icon'=>'phone',
            'active'=>($item == 'bosscalendar'),
            'visible' => false
        ],
        [
            'label' => 'Табель',
            'url' => 'index.php?r=reports/rep',
            'active'=>($item == 'rep'),
//            'visible' => $tabel == 1
            'visible' => false
//            'icon' => 'option-horizontal'

        ],
        [
            'label' => 'Журналы',
            'url' => 'index.php?r=journal/journal',
//            'icon' => 'option-horizontal',
            'active' => ($item == 'journal'),
            'visible' => false
//            'visible' => verification_user()==555

        ],
        [
            'label' => 'Поиск ребенка',
            'url' => 'index.php?r=search/searchchild',
            'active'=>($item == 'searchchild'),
            'visible' => false
//            'visible' => verification_user()!=1425
//            'icon' => 'search'
        ],

        /*[
            'url' => 'index.php?r=my/show',
            'label' => 'Движение',
            'active'=>($item == 'show'),
            'icon' => 'sort'
        ],*/
        [
            'url' => 'index.php?r=five/five',
            'label' => '5 дневка',
            'active'=>($item == 'five'),
            'visible' => false
//            'visible' => verification_user()!=1425
//            'icon' => 'option-horizontal'
        ],
        [
            'url' => 'index.php?r=correct/corr',
            'label' => 'Корректировка причин',
            'active'=>($item == 'corr'),
//            'icon' => 'option-horizontal',
            'visible' => false
//            'visible' => verification_user()==555
        ],
        [
            'label' => 'История',
            'url' => 'index.php?r=history/find',
            'active'=>($item == 'find'),
            'visible' => false
//            'visible' => verification_user()!=1425
//            'icon' => 'option-horizontal'

        ],
        [
            'label' => 'Прививки',
            'url' => 'index.php?r=injection/inj',
            'active'=>($item == 'inj'),
//            'icon' => 'option-horizontal',
            'visible' => false
//            'visible' => verification_user()==555

        ],
        [
            'label' => 'Кружки',
            'url' => 'index.php?r=crujki/cru',
            'active'=>($item == 'cru'),
//            'visible' => $crujki == 1
//            'icon' => 'option-horizontal'

        ],
        [
            'label' => 'Калькулятор зп',
            'url' => 'index.php?r=zp/zp',
            'active'=>($item == 'zp'),
            'visible' => false
//            'visible' => verification_user()==555,
        ],



    ],
]);

?>

