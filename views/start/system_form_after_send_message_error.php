 <?php
 if (strlen($name) == 0){
     $name2 = '';
 }else{
     $name2 = $name.'.';
 }
 ?>


    <div style="margin: 0 25% 0 25%">
        <div class="alert alert-danger" style="text-align: center"><h4><?= $name2 ?><br/><br/>Произошла ошибка при вводе данных! <br/><br/>Внимательно вводите эл.почту и проверочный код!</h4></div>
        <div id="id_start_render_ajax_button_again" class="btn btn-default btn-lg btn-block">Попробовать еще раз</div>
    </div>

 <?php
 $script = <<<JS
    $(function(){



        $('#id_start_render_ajax_button_again').on('click',function(){
        $('#id_start_form_presentation').trigger('reset');
        var arr = $('#id_form_start_hidden');
        $('#id_hidden_start').val(0);
        var form = $('#id_form_start_hidden').serializeArray();
        console.log(form);//return;

            $.ajax({
                type: arr.attr('method'),
                url:  arr.attr('action'),
                data: form
            }).done(function(response){
                $('#id_start_render_content_ajax').html(response);
            }).fail(function(){
                alert('error');
            });

            return false;
        });

    })
JS;
 $this->registerJs($script,yii\web\View::POS_END);
 ?>