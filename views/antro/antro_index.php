<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//debug($fff);
/*if (count($array)<1){
    $thead = 'cru_view_thead_empty';
}else{
    $thead = 'table_boss_cru';
}*/
//$width = '2.5%';

Modal::begin([
    'id' => 'modal_error_antro',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Выбранный период еще не наступил !</div>
</div>';
Modal::end();
?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= Html::a('', [
//                'reports/excel','id' => 1,
//                'year' => $model_y->name,
//                'month' => $model_m->name,
//                'id_group' => $model_group->name
            ], [
                'class'=>'btn btn-default btn-block btn-md',
                'id' => 'but_antro_excel1',
                'disabled' => 'disabled'
            ]); ?>
        </div>
        <div class="rep_boss_up_panel_1">
            <?php $form = ActiveForm::begin([
                'id' => 'form_antro_panel_button',
                'action' => ['antro/antro'],
                'method' => 'POST',
                'type' => ActiveForm::TYPE_INLINE,
            ])?>
            <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
                'id' => 'drop_group_antro',
            ])->label(false) ?>
            <?= $form->field($model_y,'name')->dropDownList($array_year_tabel_deti,[
                'id' => 'drop_antro_year',
            ])->label(false) ?>
            <?= $form->field($model_m,'name')->dropDownList($array_vremya_goda,[
                'id' => 'drop_antro_month',
            ])->label(false) ?>
<!--            --><?//= Html::button('', ['class'=>'btn btn-md btn-primary','id' => 'antro_message','style' => 'display:none']); ?>
            <div class="btn-group" id="dropdown_antro_excel">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Отчеты Excel &nbsp;&nbsp;&nbsp;<span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Сводная таблица комплектов мебели</a></li>
                    <li><a href="#">отчет 2</a></li>
                    <li><a href="#">отчет 3</a></li>
                    <li><a href="#">отчет 4</a></li>
                </ul>
            </div>
            <?php $form = ActiveForm::end()?>
        </div>
    </div>

    <div id="renderAjax_antro_table_boss" style="height: 87vh">
        <?= $this->render('antro_all_table',compact(
            'array','array2','model_antro','model_id'
        )) ?>
    </div>
</div>

<?php
$scr = <<<JS
$(function(){

$('#drop_group_antro').on('change',function(){
//    $('#id_hidden_form_antro').val(3);
    var form = $('#form_antro_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 3});
    var arr = $('#form_antro_panel_button');
//    console.log(form);return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#antro_left_table').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});

$('#drop_antro_year,#drop_antro_month').on('change',function(){
//    $('#id_hidden_form_antro').val(0);
    var form = $('#form_antro_panel_button').serializeArray();
    form.push({name: 'Id[id]',value: 0});
    var arr = $('#form_antro_panel_button');
//    console.log(form);return;
    $(this).LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : form
    }).done(function(response) {
            $("*").LoadingOverlay("hide");
            if(response==400){
//                $('#modal_error').modal('show');
            }else{
                $('#renderAjax_antro_table_boss').html(response);
            }
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    return false;
});







});
JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>
