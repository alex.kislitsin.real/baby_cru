<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 05.01.20
 * Time: 12:24
 */
//debug($array);
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$width = '2.5%';
//$year = $model_y->name;
//$month = $model_m->name;
//$array = array(1,2,3,4,5,0,0,0,0,0,0,0,0,0,0);
if (count($array) > 14){
    $height = ';height: 82vh';
}else{
    $height = '';
}
?>
<div class="my_table not_selected_text_on_block" style="height: 5vh;overflow-y: scroll;background-color: #eeeeee">
    <table>
        <tr>
            <td class="not_item" style="width: 5%;text-align: center">№</td>
            <td class="not_item">Фамилия и имя ребёнка</td>
            <td class="not_item" style="width: 20%">Дата замера</td>
            <td class="not_item" style="width: 15%">Рост</td>
            <td class="not_item" style="width: 15%">Вес</td>
        </tr>
    </table>
</div>
<div class="my_table my_table2 not_selected_text_on_block" style="overflow-y: scroll<?= $height ?>" id="antro_table1">
    <table class="table-striped table-bordered">

        <?php
        $i = 1;
        foreach($array as $q){

            $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.$q['rozd'].'</span>';
            $q['rost'] < 1 ? $rost = '' : $rost = $q['rost'];
            $q['ves'] < 1 ? $ves = '' : $ves = $q['ves'];
            trim($q['dat'])=='01.01.1900' ? $dat = '' : $dat = trim($q['dat']);

                echo '<tr data-id_child="'.$q['id'].'" data-name="'.trim($q['name']).'" data-rost="'.$q['rost'].'" data-ves="'.$q['ves'].'" data-dat="'.trim($q['dat']).'">
                <td style="width: 5%">'.$i.'</td>
                <td id="id_n">'.trim($q['name']).$rozd.'</td>
                <td style="width: 20%">'.$dat.'</td>
                <td style="width: 15%">'.$rost.'</td>
                <td style="width: 15%">'.$ves.'</td>
            </tr>';
            $i++;
        }

        ?>

    </table>
</div>



<?php
$script = <<<JS
$(function(){
    $('#antro_table1 tr').on('click',function(){
    console.log(000);
    $('#form_modal_antro_mebel,#form_modal_antro_mebel2').trigger('reset');
    var id_child = $(this).data('id_child');
    var name = $(this).data('name');
    var dat = $(this).data('dat');
    var rost = $(this).data('rost');
    var ves = $(this).data('ves');
    if(rost < 1)rost = '';
    if(ves < 1)ves = '';
    $('#name_modal_antro').val(name);
    $('#rost_modal_antro').val(rost);
    $('#ves_modal_antro').val(ves);
    $('#dat_modal_antro').val(dat);
    $('#id_child_modal_antro').val(id_child);
    if(rost>0 || ves>0){
        $('#but_del_antro_mebel').show();
        $('#id_hidden_modal_antro').val(2);
    }else{
        $('#but_del_antro_mebel').hide();
        $('#id_hidden_modal_antro').val(1);
    }

    var year_header = $('#drop_antro_year').val();
    var period_header = $('#drop_antro_month').val();
    period_header == 1 ? period_header = 'ЯНВАРЬ - ИЮНЬ' : period_header = 'ИЮЛЬ - ДЕКАБРЬ';

    $('#header_modal_antro_mebel').text(name+' ('+year_header+' год, период '+period_header+')');

    $('#modal_antro_mebel').modal('show');
    return false;
});
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

