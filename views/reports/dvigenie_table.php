<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
$percent = '10%';
$color_stoyat = 'rgba(163, 212, 255, 0.55)';
$color_al = 'rgba(135, 216, 255, 0.25)';
$color_fakt = 'rgba(255, 255, 0, 0.14)';
$color_itogo = 'rgb(137, 134, 125)';
$color_c = 'rgba(248, 127, 127, 0.39)';
?>
<!--    <div style="background-color: rgba(135, 216, 255, 0.25); font-weight: bold"></div>-->
<!--    <div style="background-color: rgba(255, 255, 0, 0.14); font-weight: bold"></div>-->
<!--    <div style="background-color: rgb(137, 134, 125); font-weight: bold"></div>-->

    <div class="my_table my_table2 not_selected_text_on_block" id="id_grid_dvig">
        <table class="table-bordered">
            <thead>
            <tr>
                <th>Группа</th>
                <th style="width: <?= $percent ?>">Всего детей</th>
                <th style="width: <?= $percent ?>;background: <?= $color_stoyat ?>">Стоят</th>
                <th id="th_table_dvij_alerg_itogi" style="width: <?= $percent ?>;background: <?= $color_al ?>">&#127823;</th>
                <th style="width: <?= $percent ?>">Сняты</th>
                <th style="width: <?= $percent ?>;background: <?= $color_fakt ?>">Факт</th>
                <th style="width: <?= $percent ?>">Не пришли</th>
                <th style="width: <?= $percent ?>">Сюрприз</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $sum_vse = 0;
            $sum_stoyat = 0;
            $sum_snyati = 0;
            $sum_fakt = 0;
            $sum_no_sovsem = 0;
            $sum_surprise = 0;
            $sum_al = 0;

            foreach($model_dvig as $q){

                $iii = 0;

                /*if(trim($q['name']) == 'Итого'){
                    $style = 'style="background: '.$color_itogo.'; color: white; font-size: 25px; font-weight: bold"';
                    $iii = 1;
                }elseif(trim($q['name']) == 'С - витамин'){
                    $style = 'style="background: '.$color_c.'"';
                    $iii = 0;
                }else{
                    $style = '';
                    $iii = 0;
                }*/

                if ((int)trim($q['al']) > 0){
                    $al = $q['al'];
                }else{
                    $al = '';
                }

                if (intval($q['id'])>0){
                    $sum_vse += $q['kolvse'];
                    $sum_stoyat += $q['stoyat'];
                    $sum_snyati += $q['snyati'];
                    $sum_fakt += $q['fakt'];
                    $sum_no_sovsem += $q['no_sovsem'];
                    $sum_surprise += $q['surprise'];
                    $sum_al += $q['al'];

                }

                if ($q['id']==null){
                    $iii = 1;
                    $style = 'style="background: '.$color_itogo.'; color: white; font-size: 25px; font-weight: bold"';
                    echo '<tr '.$style.'>
                <td id="id_n">Итого</td>
                <td style="width: '.$percent.'">'.$sum_vse.'</td>
                <td style="width: '.$percent.'">'.$sum_stoyat.'</td>
                <td data-itogo="1" data-iii="100" data-idid="999"  id="id_td_table_column_alerg2" style="width: '.$percent.'">'.$sum_al.'</td>
                <td style="width: '.$percent.'">'.$sum_snyati.'</td>
                <td style="width: '.$percent.'">'.$sum_fakt.'</td>
                <td style="width: '.$percent.'">'.$sum_no_sovsem.'</td>
                <td style="width: '.$percent.'">'.$sum_surprise.'</td>
            </tr>';

                    $w = round((($sum_stoyat*50)/1000),1);
                    $w = $w == 0 ? '' : $w;

                    $style = 'style="background: '.$color_c.';"';
                    echo '<tr '.$style.'>
                <td id="id_n">С - витамин</td>
                <td style="width: '.$percent.'"></td>
                <td style="width: '.$percent.'">'.$w.'</td>
                <td data-itogo="0" data-iii="0" data-idid="0" id="id_td_table_column_alerg3" style="width: '.$percent.'"></td>
                <td style="width: '.$percent.'"></td>
                <td style="width: '.$percent.'"></td>
                <td style="width: '.$percent.'"></td>
                <td style="width: '.$percent.'"></td>
            </tr>';


                }


                $style = '';

                echo '<tr '.$style.'>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: '.$percent.'">'.$q['kolvse'].'</td>
                <td style="width: '.$percent.';background: '.$color_stoyat.'">'.$q['stoyat'].'</td>
                <td data-itogo="'.$iii.'" data-iii="'.(Int)$q['al'].'" data-idid="'.(Int)$q['id'].'" id="id_td_table_column_alerg" style="width: '.$percent.';background: '.$color_al.'">'.$al.'</td>
                <td style="width: '.$percent.'">'.$q['snyati'].'</td>
                <td style="width: '.$percent.';background: '.$color_fakt.'">'.$q['fakt'].'</td>
                <td style="width: '.$percent.'">'.$q['no_sovsem'].'</td>
                <td style="width: '.$percent.'">'.$q['surprise'].'</td>
            </tr>';
            }

            ?>
            </tbody>
        </table>
    </div>
<?//= debug($array); ?>
<div class="my_table my_table2 not_selected_text_on_block" hidden="hidden" id="id_show_dvij_alerg_table_down">
<!--    --><?//= debug($array); ?>
    <table class="table-bordered">
        <tbody>
        <?php
        foreach($array as $q){

            if($q['notnot'] == 1 || $q['opozd'] == 1){
                if($q['notnot'] == 1) $t=' &#128308';
                if($q['opozd'] == 1) $t=' &#127765';
            }else{
                $t='';
            }

            /*if ((Int)$q['today'] == 1){

            }*/


            echo '<tr data-id_gruppa="'.$q['id_gruppa'].'" data-today="'.(Int)$q['today'].'" data-alergia="'.trim($q['alergia']).'">
                <td style="width: 30.5%" id="id_n">'.trim($q['name']).$t.'</td>
                <td style="width: 20%" id="id_n">'.trim($q['namegr']).'</td>
                <td id="id_n">'.$q['alergia'].'</td>
                </tr>';
        }
        ?>
        </tbody>
    </table>
</div>
<!--<p style="background-color: #afafaf">  </p>-->
<?php
$scr = <<<JS
$(function(){
    $('td#id_td_table_column_alerg,#id_td_table_column_alerg2,#id_td_table_column_alerg3').click(function(){

        var bbb = Number($(this).data('iii'));
        var idid = Number($(this).data('idid'));
        var itogo = Number($(this).data('itogo'));

        if(bbb > 0){
            $('td#id_td_table_column_alerg').css({ "background-color": "rgba(135, 216, 255, 0.25)" });
            $(this).css({"background-color": "#afafaf"});

            console.log(itogo);

            if(itogo == 0){
                $.each($('#id_show_dvij_alerg_table_down tr'),function(){
                    var ggg = $(this).data('id_gruppa');
                    var today = $(this).data('today');
                    console.log(idid+'/'+ggg+'/'+today);
                    idid == ggg && today == 1 ? $(this).show() : $(this).hide();
                });
            }else{
                $.each($('#id_show_dvij_alerg_table_down tr'),function(){
                    var today = $(this).data('today');
                    today == 1 ? $(this).show() : $(this).hide();
                });
            }

            $('#id_show_dvij_alerg_table_down').fadeIn();
        }else{
            $('td#id_td_table_column_alerg').css({ "background-color": "rgba(135, 216, 255, 0.25)" });
            $('#id_show_dvij_alerg_table_down').hide();
        }
        return false;
    });

    /*$('th#th_table_dvij_alerg_itogi').on('click',function(){

        alert(222);
        return false;
    });*/
});
JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>