<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
$percent = '10%';
$color_stoyat = 'rgb(153, 200, 241)';
$color_fakt = 'rgba(255, 255, 0, 0.27)';
$color_itogo = 'rgb(121, 241, 109)';
$color_c = '#F1D205';

//debug($array);
//debug($data);
//debug(11111);
?>

<div class="my_table my_table2 not_selected_text_on_block" id="id_grid_dvig">
    <table class="table-bordered table-hover">
        <thead>
        <tr>
            <th style="width: 5%">№</th>
            <th>Группа</th>
            <th style="width: <?= $percent ?>">Всего детей</th>
            <th style="width: <?= $percent ?>;background: <?= $color_stoyat ?>">Стоят</th>
            <th style="width: <?= $percent ?>">Сняты</th>
            <th style="width: <?= $percent ?>;background: <?= $color_fakt ?>">ОРЗ</th>
            <th style="width: <?= $percent ?>">Заболевания</th>
            <th style="width: <?= $percent ?>">Прочее</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $sum_vse = 0;
        $sum_stoyat = 0;
        $sum_snyati = 0;
        $sum_orz = 0;
        $sum_zabol = 0;
        $sum_prochee = 0;

        foreach($array as $q){

            $sum_vse += $q['kolvse'];
            $sum_stoyat += $q['stoyat'];
            $sum_snyati += $q['snyati'];

            $sum_orz += $q['orz'];
            $sum_zabol += $q['bolezni'];
            $sum_prochee += $q['prochee'];


            echo '<tr '.$style.'>
                <td style="width: 5%">'.$q['id'].'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: '.$percent.'">'.$q['kolvse'].'</td>
                <td style="width: '.$percent.';background: '.$color_stoyat.'">'.$q['stoyat'].'</td>
                <td style="width: '.$percent.'">'.$q['snyati'].'</td>
                <td style="width: '.$percent.';background: '.$color_fakt.'">'.$q['orz'].'</td>
                <td style="width: '.$percent.'">'.$q['bolezni'].'</td>
                <td style="width: '.$percent.'">'.$q['prochee'].'</td>
            </tr>';

        }

        echo '<tr style="background: '.$color_itogo.'">
                <td style="width: 5%"></td>
                <td id="id_n">Итого</td>
                <td style="width: '.$percent.'">'.$sum_vse.'</td>
                <td style="width: '.$percent.'">'.$sum_stoyat.'</td>
                <td style="width: '.$percent.'">'.$sum_snyati.'</td>
                <td style="width: '.$percent.'">'.$sum_orz.'</td>
                <td style="width: '.$percent.'">'.$sum_zabol.'</td>
                <td style="width: '.$percent.'">'.$sum_prochee.'</td>
            </tr>';

        ?>
        </tbody>
    </table>
</div>
