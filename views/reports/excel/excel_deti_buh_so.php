<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 22.10.19
 * Time: 20:20
 */
$mon = $month - 1;
$number_sad = Yii::$app->request->cookies->getValue('number_sad');

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));

$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}

$iter_date = date($year.'-'.$monthwithzero.'-'.$count_days_all);
//$last_works_day = $iter_date;
$count_works_days = 0;
while($iter_date >= date($year.'-'.$monthwithzero.'-01')){
    if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
        $count_works_days++;
    }
    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
}

/*while(((date('N', strtotime($last_works_day)) > 5) || (in_array($last_works_day,$model_d_date))) && !in_array($last_works_day,$model_d_antidate)){
    $last_works_day = date('Y-m-d',strtotime($last_works_day. " - 1 day"));
}*/

$town = Yii::$app->request->cookies->getValue('cityr');//город в родительном падеже
$boss = Yii::$app->request->cookies->getValue('boss');//boss imenit padej
$medsestra = Yii::$app->request->cookies->getValue('medsestra');//$medsestra

switch($count_works_days){
    case 1:
    case 21:
    case 31:
        $count_works_days = $count_works_days.' день';
        break;
    case 2:
    case 3:
    case 4:
    case 22:
    case 23:
    case 24:
        $count_works_days = $count_works_days.' дня';
        break;
    default:
        $count_works_days = $count_works_days.' дней';
        break;
}


/*$query_years_of_deti = "select distinct(year(rozd))[year] from deti
where id_gruppa = {$id_group} and (({$month}>=month([in]) and {$year}>=year([in])) or {$year}>year([in]))
and ((({$month}<=month([out]) and {$year}<=year([out])) or {$year} < year([out])) or [out] is null)";
$array_years_of_deti = Yii::$app->db->createCommand($query_years_of_deti)->queryAll();
$array_years_of_deti = ArrayHelper::getColumn($array_years_of_deti,'year');

$first_v = date('Y') - ($array_years_of_deti[0]);
if (count($array_years_of_deti)>1){
    $last_v = date('Y') - (array_pop($array_years_of_deti));
    if ($last_v<5&&$first_v<5){
        $vozrast = $last_v.'-'.$first_v.' года';
    }else{
        $vozrast = $last_v.'-'.$first_v.' лет';
    }
}else{
    if ($first_v<5){
        $vozrast = $first_v.' года';
    }else{
        $vozrast = $first_v.' лет';
    }
}*/





$width_col = 2.89;//good
$height_row_up_3 = 12.5;//1-3 rows
$height_row_up = 11.5;//4-9 rows
$height_row_st = 22.48;

$height_row_shapka1 = 26.5;
$height_row_shapka2 = 26;
$height_row_shapka3 = 24;
$height_row_shapka_down = 12.2;

$style_border_bottom = array(
    'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
//    'fill' => array(
//        'type' => PHPExcel_Style_Fill::FILL_SOLID,
//        'startcolor' => array(
//            'argb' => 'FFFF0000',
//        ),
//    ),
);
$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);
$style_border_all_bold = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
        ),
    ),
);

$allign_center_all = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$allign_up_right = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_left = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_center = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(7.7);
$sheet->getDefaultStyle()->getFont()->setName('Arial');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');
//$sheet->setBreak('AO1',PHPExcel_Worksheet::BREAK_COLUMN);
//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.65);
$sheet->getPageMargins()->setBottom(0.65);
$sheet->getPageMargins()->setLeft(0.65);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
//$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(9,11);

$sheet->getColumnDimension('A')->setWidth(1)->setVisible(false);
$sheet->getColumnDimension('B')->setWidth(4.3);
$sheet->getColumnDimension('C')->setWidth(30);
$sheet->getColumnDimension('D')->setWidth(9);
$sheet->getColumnDimension('E')->setWidth($width_col);
$sheet->getColumnDimension('F')->setWidth($width_col);
$sheet->getColumnDimension('G')->setWidth($width_col);
$sheet->getColumnDimension('H')->setWidth($width_col);
$sheet->getColumnDimension('I')->setWidth($width_col);
$sheet->getColumnDimension('J')->setWidth($width_col);
$sheet->getColumnDimension('K')->setWidth($width_col);
$sheet->getColumnDimension('L')->setWidth($width_col);
$sheet->getColumnDimension('M')->setWidth($width_col);
$sheet->getColumnDimension('N')->setWidth($width_col);
$sheet->getColumnDimension('O')->setWidth($width_col);
$sheet->getColumnDimension('P')->setWidth($width_col);
$sheet->getColumnDimension('Q')->setWidth($width_col);
$sheet->getColumnDimension('R')->setWidth($width_col);
$sheet->getColumnDimension('S')->setWidth($width_col);
$sheet->getColumnDimension('T')->setWidth($width_col);
$sheet->getColumnDimension('U')->setWidth($width_col);
$sheet->getColumnDimension('V')->setWidth($width_col);
$sheet->getColumnDimension('W')->setWidth($width_col);
$sheet->getColumnDimension('X')->setWidth($width_col);
$sheet->getColumnDimension('Y')->setWidth($width_col);
$sheet->getColumnDimension('Z')->setWidth($width_col);
$sheet->getColumnDimension('AA')->setWidth($width_col);
$sheet->getColumnDimension('AB')->setWidth($width_col);
$sheet->getColumnDimension('AC')->setWidth($width_col);
$sheet->getColumnDimension('AD')->setWidth($width_col);
$sheet->getColumnDimension('AE')->setWidth($width_col);
$sheet->getColumnDimension('AF')->setWidth($width_col);
$sheet->getColumnDimension('AG')->setWidth($width_col);
$sheet->getColumnDimension('AH')->setWidth($width_col);
$sheet->getColumnDimension('AI')->setWidth($width_col);
$sheet->getColumnDimension('AJ')->setWidth(10);
$sheet->getColumnDimension('AK')->setWidth(10);
$sheet->getColumnDimension('AL')->setWidth(10);
$sheet->getColumnDimension('AM')->setWidth(10);
//$sheet->getColumnDimension('AN')->setWidth(7);
//$sheet->getColumnDimension('AO')->setWidth(12);

/*$i = 1;
while($i<=9){
    $sheet->getRowDimension($i)->setRowHeight($height_row_up);
    $i++;
}*/
//$sheet->getRowDimension("1")->setRowHeight($height_row_up);
//$sheet->getRowDimension("2")->setRowHeight($height_row_up);
//$sheet->getRowDimension("3")->setRowHeight($height_row_up);
//$sheet->getRowDimension("4")->setRowHeight($height_row_up);
//$sheet->getRowDimension("5")->setRowHeight($height_row_up);
//$sheet->getRowDimension("6")->setRowHeight($height_row_up);
//$sheet->getRowDimension("7")->setRowHeight($height_row_up);
//$sheet->getRowDimension("8")->setRowHeight($height_row_up);
//$sheet->getRowDimension("9")->setRowHeight($height_row_up);
//$sheet->getRowDimension("10")->setRowHeight($height_row_shapka1);
//$sheet->getRowDimension("11")->setRowHeight($height_row_shapka2);
//$sheet->getRowDimension("12")->setRowHeight($height_row_shapka3);


$line = 1;
$sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'Т А Б Е Л Ь');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);

$line++;//2
$sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'УЧЕТА ПОСЕЩАЕМОСТИ ДЕТЕЙ');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);
$sheet->setCellValue("AM{$line}", 'КОДЫ')->getStyle("AM{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//$line_4 = $line+4;
//$sheet->getStyle("AM{$line}:AM{$line_4}")->applyFromArray($style_border_all_bold);

$line++;//3
$sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'за '.$_monthsList[$month].' '.$year.' г.');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);
$sheet->setCellValue("AL{$line}", 'Форма по ОКУД  ');
$sheet->getStyle("AL{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AM{$line}", '504608')->getStyle("AM{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//4
$sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Учреждение');
$sheet->setCellValue("D{$line}", 'муниципальное казенное дошкольное образовательное учреждение "Детский сад № '.$number_sad.'" города '.$town);
$sheet->mergeCells("D{$line}:AJ{$line}");
$sheet->getStyle("D{$line}:AJ{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AL{$line}", 'Дата  ');
$sheet->getStyle("AL{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AM{$line}", date('d.m.Y',strtotime($last_work_day)))->getStyle("AM{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);;

$line++;//5
$sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Структурное подразделение');
$sheet->mergeCells("C{$line}:D{$line}");
$sheet->setCellValue("E{$line}", 'Сотрудники по питанию');
$sheet->mergeCells("E{$line}:AJ{$line}");
$sheet->getStyle("E{$line}:AJ{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AL{$line}", 'по ОКПО  ');
$sheet->getStyle("AL{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AM{$line}", '13691336')->getStyle("AM{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//6
$sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Вид расчета');
$sheet->setCellValue("E{$line}", $count_works_days);//кол-во раб. дней
$sheet->mergeCells("E{$line}:AJ{$line}");
$sheet->getStyle("E{$line}:AJ{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AM{$line}", '')->getStyle("AM{$line}")->applyFromArray($style_border_all);

$line++;//7
$sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Режим работы');
$sheet->setCellValue("E{$line}", '12 час');//кол-во раб. дней
$sheet->mergeCells("E{$line}:AJ{$line}");
$sheet->getStyle("E{$line}:AJ{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AM{$line}", '')->getStyle("AM{$line}")->applyFromArray($style_border_all);
$line++;
$sheet->setCellValue("AM{$line}", '')->getStyle("AM{$line}")->applyFromArray($style_border_all);


$line++;//10
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
$line3 = $line+2;
$sheet->setCellValue("B{$line}", "№№\nп/п");
$sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("B{$line}:B{$line3}");
$sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
$sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);

$sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
$sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("C{$line}:C{$line3}");

$sheet->setCellValue("D{$line}", "Номер\nсчета");
$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("D{$line}:D{$line3}");

$sheet->setCellValue("E{$line}", "Дни посещения");
$sheet->mergeCells("E{$line}:AI{$line}");

$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
$sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AJ{$line}:AJ{$line3}");

$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
$sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AK{$line}:AK{$line3}");

$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
//$sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AL{$line}:AM{$line3}");
//$line555 = $line+1;
//$sheet->mergeCells("AL{$line}:AL{$line555}");
//$sheet->mergeCells("AL{$line}:AL{$line3}");




$line++;//11
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
$line2=$line+1;//12
$sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
$sheet->setCellValue("E{$line}", "1");
$sheet->mergeCells("E{$line}:E{$line2}");

$sheet->setCellValue("F{$line}", "2");
$sheet->mergeCells("F{$line}:F{$line2}");

$sheet->setCellValue("G{$line}", "3");
$sheet->mergeCells("G{$line}:G{$line2}");

$sheet->setCellValue("H{$line}", "4");
$sheet->mergeCells("H{$line}:H{$line2}");

$sheet->setCellValue("I{$line}", "5");
$sheet->mergeCells("I{$line}:I{$line2}");

$sheet->setCellValue("J{$line}", "6");
$sheet->mergeCells("J{$line}:J{$line2}");

$sheet->setCellValue("K{$line}", "7");
$sheet->mergeCells("K{$line}:K{$line2}");

$sheet->setCellValue("L{$line}", "8");
$sheet->mergeCells("L{$line}:L{$line2}");

$sheet->setCellValue("M{$line}", "9");
$sheet->mergeCells("M{$line}:M{$line2}");

$sheet->setCellValue("N{$line}", "10");
$sheet->mergeCells("N{$line}:N{$line2}");

$sheet->setCellValue("O{$line}", "11");
$sheet->mergeCells("O{$line}:O{$line2}");

$sheet->setCellValue("P{$line}", "12");
$sheet->mergeCells("P{$line}:P{$line2}");

$sheet->setCellValue("Q{$line}", "13");
$sheet->mergeCells("Q{$line}:Q{$line2}");

$sheet->setCellValue("R{$line}", "14");
$sheet->mergeCells("R{$line}:R{$line2}");

$sheet->setCellValue("S{$line}", "15");
$sheet->mergeCells("S{$line}:S{$line2}");

$sheet->setCellValue("T{$line}", "16");
$sheet->mergeCells("T{$line}:T{$line2}");

$sheet->setCellValue("U{$line}", "17");
$sheet->mergeCells("U{$line}:U{$line2}");

$sheet->setCellValue("V{$line}", "18");
$sheet->mergeCells("V{$line}:V{$line2}");

$sheet->setCellValue("W{$line}", "19");
$sheet->mergeCells("W{$line}:W{$line2}");

$sheet->setCellValue("X{$line}", "20");
$sheet->mergeCells("X{$line}:X{$line2}");

$sheet->setCellValue("Y{$line}", "21");
$sheet->mergeCells("Y{$line}:Y{$line2}");

$sheet->setCellValue("Z{$line}", "22");
$sheet->mergeCells("Z{$line}:Z{$line2}");

$sheet->setCellValue("AA{$line}", "23");
$sheet->mergeCells("AA{$line}:AA{$line2}");

$sheet->setCellValue("AB{$line}", "24");
$sheet->mergeCells("AB{$line}:AB{$line2}");

$sheet->setCellValue("AC{$line}", "25");
$sheet->mergeCells("AC{$line}:AC{$line2}");

$sheet->setCellValue("AD{$line}", "26");
$sheet->mergeCells("AD{$line}:AD{$line2}");

$sheet->setCellValue("AE{$line}", "27");
$sheet->mergeCells("AE{$line}:AE{$line2}");

$sheet->setCellValue("AF{$line}", "28");
$sheet->mergeCells("AF{$line}:AF{$line2}");

$sheet->setCellValue("AG{$line}", "29");
$sheet->mergeCells("AG{$line}:AG{$line2}");

$sheet->setCellValue("AH{$line}", "30");
$sheet->mergeCells("AH{$line}:AH{$line2}");

$sheet->setCellValue("AI{$line}", "31");
$sheet->mergeCells("AI{$line}:AI{$line2}");

/*$sheet->setCellValue("AJ{$line}", "31");
$sheet->mergeCells("AJ{$line}:AJ{$line2}");

$sheet->setCellValue("AM{$line}", "всего");
$sheet->mergeCells("AM{$line}:AM{$line2}");

$sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
$sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AN{$line}:AN{$line2}");*/


/*****************************************************************/
//$arr_last_row = array_pop($model);//последняя итоговая строка вырезана из массива в переменную $arr_last_row
$count_sotrudniki = count($model);

/*foreach($model as $o){
    array_push($model,$o);
}

$model = array_slice($model, 0, 50);*/

if (count($model)>15){
    $m15 = array_slice($model, 0, 15);
}else{
    $m15 = $model;
    $m16 = array();
    $m37 = array();
}

if (count($model)>34){
    $m16 = array_slice($model, 15, 19);
    $m37 = array_slice($model, 34);
}else{
    $m16 = array_slice($model, 15);
    $m37 = array();
}



$t = 1;
while ($t <= 31){
    ${'sum'.$t} = 0;
    $t++;
}

$prishli = 0;

$line++;//12
$iter_number = 1;
foreach($m15 as $abc){

    $t = 1;
    while ($t <= 31){
        switch($abc[''.$t.'']){
            case 0:
            case 3:
                ${'val'.$t} = '';
                ${'sum'.$t}++;
                break;
            case 1:
                ${'val'.$t} = 'H';
                break;
            case 5:
                ${'val'.$t} = 'B';
                break;
            case 7:
                ${'val'.$t} = 'X';
                break;
            default:
                ${'val'.$t} = '';
                break;
        }
        $t++;
    }

    $prishli += $abc['пришли'];

    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);

    $sheet->setCellValue("B{$line}", $iter_number);
    $iter_number++;
    $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

    /*if (mb_strlen(trim($abc['name']))>24){
        $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
        $sheet->setCellValue("C{$line}", $rrr);
    }else{
        $sheet->setCellValue("C{$line}", trim($abc['name']));
    }*/

    $sheet->setCellValue("C{$line}", trim($abc['name']));

    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

    $sheet->setCellValue("D{$line}", trim($abc['schet']));
    $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

//    $sheet->setCellValue("E{$line}", '');
//    $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

    $sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($allign_up_right);



    $sheet->setCellValue("E{$line}", $val1);
    $sheet->setCellValue("F{$line}", $val2);
    $sheet->setCellValue("G{$line}", $val3);
    $sheet->setCellValue("H{$line}", $val4);
    $sheet->setCellValue("I{$line}", $val5);
    $sheet->setCellValue("J{$line}", $val6);
    $sheet->setCellValue("K{$line}", $val7);
    $sheet->setCellValue("L{$line}", $val8);
    $sheet->setCellValue("M{$line}", $val9);
    $sheet->setCellValue("N{$line}", $val10);
    $sheet->setCellValue("O{$line}", $val11);
    $sheet->setCellValue("P{$line}", $val12);
    $sheet->setCellValue("Q{$line}", $val13);
    $sheet->setCellValue("R{$line}", $val14);
    $sheet->setCellValue("S{$line}", $val15);
    $sheet->setCellValue("T{$line}", $val16);
    $sheet->setCellValue("U{$line}", $val17);
    $sheet->setCellValue("V{$line}", $val18);
    $sheet->setCellValue("W{$line}", $val19);
    $sheet->setCellValue("X{$line}", $val20);
    $sheet->setCellValue("Y{$line}", $val21);
    $sheet->setCellValue("Z{$line}", $val22);
    $sheet->setCellValue("AA{$line}", $val23);
    $sheet->setCellValue("AB{$line}", $val24);
    $sheet->setCellValue("AC{$line}", $val25);
    $sheet->setCellValue("AD{$line}", $val26);
    $sheet->setCellValue("AE{$line}", $val27);
    $sheet->setCellValue("AF{$line}", $val28);
    $sheet->setCellValue("AG{$line}", $val29);
    $sheet->setCellValue("AH{$line}", $val30);
    $sheet->setCellValue("AI{$line}", $val31);
    $sheet->setCellValue("AJ{$line}", trim($abc['пришли']));
    $sheet->setCellValue("AK{$line}", trim($abc['nextmonth']));
    $sheet->setCellValue("AL{$line}", trim($abc['podpis']));
    $sheet->mergeCells("AL{$line}:AM{$line}");
//    $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
//    $sheet->setCellValue("AN{$line}", trim($abc['popo']));
//    $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
}
    if (count($model)>15){
        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
        $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
        $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
        $sheet->mergeCells("AL{$line}:AM{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
        $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

        $line++;

        foreach($m16 as $abc){

        $t = 1;
        while ($t <= 31){
            switch($abc[''.$t.'']){
                case 0:
                case 3:
                    ${'val'.$t} = '';
                    ${'sum'.$t}++;
                    break;
                case 1:
                    ${'val'.$t} = 'H';
                    break;
                case 5:
                    ${'val'.$t} = 'B';
                    break;
                case 7:
                    ${'val'.$t} = 'X';
                    break;
                default:
                    ${'val'.$t} = '';
                    break;
            }
            $t++;
        }

        $prishli += $abc['пришли'];

        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);

        $sheet->setCellValue("B{$line}", $iter_number);
        $iter_number++;
        $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

        /*if (mb_strlen(trim($abc['name']))>24){
            $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
            $sheet->setCellValue("C{$line}", $rrr);
        }else{
            $sheet->setCellValue("C{$line}", trim($abc['name']));
        }*/

        $sheet->setCellValue("C{$line}", trim($abc['name']));

        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

        $sheet->setCellValue("D{$line}", trim($abc['schet']));
        $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

        $sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($allign_up_right);

        $sheet->setCellValue("E{$line}", $val1);
        $sheet->setCellValue("F{$line}", $val2);
        $sheet->setCellValue("G{$line}", $val3);
        $sheet->setCellValue("H{$line}", $val4);
        $sheet->setCellValue("I{$line}", $val5);
        $sheet->setCellValue("J{$line}", $val6);
        $sheet->setCellValue("K{$line}", $val7);
        $sheet->setCellValue("L{$line}", $val8);
        $sheet->setCellValue("M{$line}", $val9);
        $sheet->setCellValue("N{$line}", $val10);
        $sheet->setCellValue("O{$line}", $val11);
        $sheet->setCellValue("P{$line}", $val12);
        $sheet->setCellValue("Q{$line}", $val13);
        $sheet->setCellValue("R{$line}", $val14);
        $sheet->setCellValue("S{$line}", $val15);
        $sheet->setCellValue("T{$line}", $val16);
        $sheet->setCellValue("U{$line}", $val17);
        $sheet->setCellValue("V{$line}", $val18);
        $sheet->setCellValue("W{$line}", $val19);
        $sheet->setCellValue("X{$line}", $val20);
        $sheet->setCellValue("Y{$line}", $val21);
        $sheet->setCellValue("Z{$line}", $val22);
        $sheet->setCellValue("AA{$line}", $val23);
        $sheet->setCellValue("AB{$line}", $val24);
        $sheet->setCellValue("AC{$line}", $val25);
        $sheet->setCellValue("AD{$line}", $val26);
        $sheet->setCellValue("AE{$line}", $val27);
        $sheet->setCellValue("AF{$line}", $val28);
        $sheet->setCellValue("AG{$line}", $val29);
        $sheet->setCellValue("AH{$line}", $val30);
        $sheet->setCellValue("AI{$line}", $val31);
        $sheet->setCellValue("AJ{$line}", trim($abc['пришли']));
        $sheet->setCellValue("AK{$line}", trim($abc['nextmonth']));
        $sheet->setCellValue("AL{$line}", trim($abc['podpis']));
        $sheet->mergeCells("AL{$line}:AM{$line}");

    }

    if (count($m16)==19)$sheet->getPageMargins()->setBottom(0);


}

//следующие после 36

if (count($model)>34){
    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

    $line++;

    foreach($m37 as $abc){

    $t = 1;
    while ($t <= 31){
        switch($abc[''.$t.'']){
            case 0:
            case 3:
                ${'val'.$t} = '';
                ${'sum'.$t}++;
                break;
            case 1:
                ${'val'.$t} = 'H';
                break;
            case 5:
                ${'val'.$t} = 'B';
                break;
            case 7:
                ${'val'.$t} = 'X';
                break;
            default:
                ${'val'.$t} = '';
                break;
        }
        $t++;
    }

    $prishli += $abc['пришли'];

    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);

    $sheet->setCellValue("B{$line}", $iter_number);
    $iter_number++;
    $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

    /*if (mb_strlen(trim($abc['name']))>24){
        $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
        $sheet->setCellValue("C{$line}", $rrr);
    }else{
        $sheet->setCellValue("C{$line}", trim($abc['name']));
    }*/

    $sheet->setCellValue("C{$line}", trim($abc['name']));

    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

    $sheet->setCellValue("D{$line}", trim($abc['schet']));
    $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

    $sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($allign_up_right);

    $sheet->setCellValue("E{$line}", $val1);
    $sheet->setCellValue("F{$line}", $val2);
    $sheet->setCellValue("G{$line}", $val3);
    $sheet->setCellValue("H{$line}", $val4);
    $sheet->setCellValue("I{$line}", $val5);
    $sheet->setCellValue("J{$line}", $val6);
    $sheet->setCellValue("K{$line}", $val7);
    $sheet->setCellValue("L{$line}", $val8);
    $sheet->setCellValue("M{$line}", $val9);
    $sheet->setCellValue("N{$line}", $val10);
    $sheet->setCellValue("O{$line}", $val11);
    $sheet->setCellValue("P{$line}", $val12);
    $sheet->setCellValue("Q{$line}", $val13);
    $sheet->setCellValue("R{$line}", $val14);
    $sheet->setCellValue("S{$line}", $val15);
    $sheet->setCellValue("T{$line}", $val16);
    $sheet->setCellValue("U{$line}", $val17);
    $sheet->setCellValue("V{$line}", $val18);
    $sheet->setCellValue("W{$line}", $val19);
    $sheet->setCellValue("X{$line}", $val20);
    $sheet->setCellValue("Y{$line}", $val21);
    $sheet->setCellValue("Z{$line}", $val22);
    $sheet->setCellValue("AA{$line}", $val23);
    $sheet->setCellValue("AB{$line}", $val24);
    $sheet->setCellValue("AC{$line}", $val25);
    $sheet->setCellValue("AD{$line}", $val26);
    $sheet->setCellValue("AE{$line}", $val27);
    $sheet->setCellValue("AF{$line}", $val28);
    $sheet->setCellValue("AG{$line}", $val29);
    $sheet->setCellValue("AH{$line}", $val30);
    $sheet->setCellValue("AI{$line}", $val31);
    $sheet->setCellValue("AJ{$line}", trim($abc['пришли']));
    $sheet->setCellValue("AK{$line}", trim($abc['nextmonth']));
    $sheet->setCellValue("AL{$line}", trim($abc['podpis']));
    $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}

if ((count($m15)==15 && count($m16)==0) || count($model)==34){
    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

    $line++;
    for($i=1;$i<=5;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}

if ((count($m15)==14 && count($m16)==0) || count($model)==33){
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
    $sheet->mergeCells("AL{$line}:AM{$line}");

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);

    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

    $line++;
    for($i=1;$i<=4;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}

if ((count($m15)==13 && count($m16)==0) || count($model)==32){
    for($i=1;$i<=2;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

    $line++;
    for($i=1;$i<=3;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}

if ((count($m15)==12 && count($m16)==0) || count($model)==31){
    for($i=1;$i<=3;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");

    $line++;
    for($i=1;$i<=2;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}




if ((count($m15)==11 && count($m16)==0) || count($model)==30){
    for($i=1;$i<=4;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AM{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nсотрудника");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);$sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");$sheet->setCellValue("E{$line}", "Дни посещения");
    $sheet->mergeCells("E{$line}:AI{$line}");$sheet->setCellValue("AJ{$line}", "Кол-во\nобедов");
    $sheet->getStyle("AJ{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AJ{$line}:AJ{$line3}");$sheet->setCellValue("AK{$line}", "Кол-во\nобедов в\nслед.мес.");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");$sheet->setCellValue("AL{$line}", "Подпись сотрудника");
    $sheet->mergeCells("AL{$line}:AM{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("E{$line}", "1");$sheet->mergeCells("E{$line}:E{$line2}");
    $sheet->setCellValue("F{$line}", "2");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "3");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "4");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "5");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "6");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "7");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "8");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "9");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "10");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "11");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "12");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "13");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "14");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "15");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "16");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "17");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "18");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "19");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "20");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "21");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "22");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "23");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "24");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "25");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "26");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "27");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "28");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "29");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "30");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "31");$sheet->mergeCells("AI{$line}:AI{$line2}");
    $line++;
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
    $sheet->mergeCells("AL{$line}:AM{$line}");
}


if((count($m16)>0 && count($model)<=29) || (count($model)>34 && count($model)<49)){
    for($i=1;$i<=5;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
}

if(count($model)==49){
    for($i=1;$i<=5;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
        $sheet->mergeCells("AL{$line}:AM{$line}");
    }
//    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
}




/*switch(count($model)){

    case 36:
        $v = 5;
        break;
    case 37:
        $v = 4;
        break;
    case 38:
        $v = 3;
        break;
    case 39:
        $v = 2;
        break;
    case 40:
        $v = 1;
        break;


}

//$line++;
for($i=1;$i<=$v;$i++){
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AM{$line}")->applyFromArray($style_border_all);
}*/

/*set itogovaya row*/
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Всего присутствует сотрудников '.$count_sotrudniki);
//$sheet->mergeCells("B{$line}:D{$line}");
$sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_all);
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($allign_up_right);

$sheet->setCellValue("E{$line}", $sum1);
$sheet->setCellValue("F{$line}", $sum2);
$sheet->setCellValue("G{$line}", $sum3);
$sheet->setCellValue("H{$line}", $sum4);
$sheet->setCellValue("I{$line}", $sum5);
$sheet->setCellValue("J{$line}", $sum6);
$sheet->setCellValue("K{$line}", $sum7);
$sheet->setCellValue("L{$line}", $sum8);
$sheet->setCellValue("M{$line}", $sum9);
$sheet->setCellValue("N{$line}", $sum10);
$sheet->setCellValue("O{$line}", $sum11);
$sheet->setCellValue("P{$line}", $sum12);
$sheet->setCellValue("Q{$line}", $sum13);
$sheet->setCellValue("R{$line}", $sum14);
$sheet->setCellValue("S{$line}", $sum15);
$sheet->setCellValue("T{$line}", $sum16);
$sheet->setCellValue("U{$line}", $sum17);
$sheet->setCellValue("V{$line}", $sum18);
$sheet->setCellValue("W{$line}", $sum19);
$sheet->setCellValue("X{$line}", $sum20);
$sheet->setCellValue("Y{$line}", $sum21);
$sheet->setCellValue("Z{$line}", $sum22);
$sheet->setCellValue("AA{$line}", $sum23);
$sheet->setCellValue("AB{$line}", $sum24);
$sheet->setCellValue("AC{$line}", $sum25);
$sheet->setCellValue("AD{$line}", $sum26);
$sheet->setCellValue("AE{$line}", $sum27);
$sheet->setCellValue("AF{$line}", $sum28);
$sheet->setCellValue("AG{$line}", $sum29);
$sheet->setCellValue("AH{$line}", $sum30);
$sheet->setCellValue("AI{$line}", $sum31);
$sheet->setCellValue("AJ{$line}", $prishli);
$sheet->setCellValue("AK{$line}", trim($arr_last_row['nextmonth']));
$sheet->setCellValue("AL{$line}", trim($arr_last_row['podpis']));
$sheet->mergeCells("AL{$line}:AM{$line}");
//$sheet->setCellValue("AM{$line}", trim($arr_last_row['пропуски']));
//$sheet->setCellValue("AN{$line}", trim($arr_last_row['popo']));
//$sheet->setCellValue("AO{$line}", trim($arr_last_row['prichiny']));

if(count($model)==10 || (count($model)>26 && count($model)<=28) || (count($model)>=34 && count($model)<=48)){
    $sheet->getPageMargins()->setBottom(0);
//    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
}

if(count($model)==49){
    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
}

/*нижняя шапка*/
$line++;
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Руководитель учреждения');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->setCellValue("O{$line}", $boss)->getStyle("O{$line}")->applyFromArray($allign_up_center);
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Табель составил');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->setCellValue("O{$line}", $medsestra)->getStyle("O{$line}")->applyFromArray($allign_up_center);
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

$sheet->getStyle("AD{$line}")->applyFromArray($allign_center_all);
$sheet->setCellValue("AD{$line}", date('j',strtotime($last_work_day)).' '.$MonthNamesRus[$mon].' '.$year.' года');//*******************************************************
//$sheet->setCellValue("AD{$line}", rdate('j M Y').' года');
$sheet->mergeCells("AD{$line}:AK{$line}");
$sheet->getStyle("AD{$line}:AK{$line}")->applyFromArray($style_border_bottom);

$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);

/*$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Мед.сестра');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);*/

$sheet->getPageSetup()->setPrintArea("A1:AM{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);

$name_file = "Табель сотрудники (".$_monthsList[$month].' '.$year.') '.date("d.m.Y");

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;