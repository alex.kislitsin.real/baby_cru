<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */
//$date = date('d.m.Y');
//$year = date('Y');
$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));

$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;

$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));





$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');
$lico_cru = Yii::$app->request->cookies->getValue('lico_cru');//кружки родительный падеж
$lico_cru2 = Yii::$app->request->cookies->getValue('lico_cru2');//кружки именительный падеж


strlen($boss)<1 ? $boss = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;

foreach($array as $q){
    $sum += $q['summa'];
}

$sum = str_replace('.',',',$sum);
$sum = number_format($sum, 2, ',', '');

function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(12);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(27,5),
    'marginRight' => m2t(12.5),
    'marginTop' => m2t(10),
    'marginBottom' => m2t(1),
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);

$table = $section->addTable();
$table->addRow(m2t(10));
$table->addCell(m2t(100))->addText('');
$table->addCell(m2t(80))->addText('Утверждаю:<w:br/>Заведующий МКДОУ № '.$number_sad.'<w:br/>_____________/'.$boss,['size' => 12],['spaceAfter' => 1]);

$section->addText('Акт об оказании платных образовательных услуг от '.$last_work_day.' г.',['bold' => true],['align' => 'center','spaceAfter' => 1]);
$section->addText('Муниципальное казенное дошкольное образовательное учреждение "Детский сад № '.$number_sad.'" города '.$cityr.' в лице руководителя по дополнительным платным образовательным услугам '.$lico_cru.' подтверждает следующее: в '.$_monthsList_rod[$month].' месяце '.$year.' года фактически оказаны дополнительные платные образовательные услуги на общую сумму '.$sum.' ('.num2str($sum).'). А именно:',null,['align' => 'both','spaceAfter' => 1]);
$font_size_table = 11;
$table = $section->addTable();
$table->addRow(m2t(5));
$table->addCell(m2t(140),$cellStyle)->addText('Наименование кружка',['size' => 12],['align' => 'center','spaceAfter' => 1]);
$table->addCell(m2t(60),$cellStyle)->addText('Сумма, руб',['size' => 12],['align' => 'center','spaceAfter' => 1]);

$i = 1;
foreach($array as $q){

    $summa = str_replace('.',',',$q['summa']);

    $stil = ['size' => $font_size_table];
    $stil2 = array('align' => 'left','spaceAfter' => 1,'spaceBefore' => 1);
    $stil2_1 = array('align' => 'center','spaceAfter' => 1,'spaceBefore' => 1);
    $row_height = 5;
    $table->addRow(m2t($row_height));
    $table->addCell(m2t(140),$cellStyle)->addText(' '.trim($q['name']),$stil,$stil2);
    $table->addCell(m2t(60),$cellStyle)->addText($summa,$stil,$stil2_1);

    $i++;
}
$stil = array('bold' => true,'size' => $font_size_table);
$stil2 = array('align' => 'left','spaceAfter' => 100,'spaceBefore' => 100);
$stil2_1 = array('align' => 'center','spaceAfter' => 100,'spaceBefore' => 100);
$row_height = 8;
$table->addRow(m2t($row_height));
$table->addCell(m2t(140),$cellStyle)->addText(' Итого',$stil,$stil2);
$table->addCell(m2t(60),$cellStyle)->addText($sum,$stil,$stil2_1);

$section->addText('Руководитель по платным услугам __________________/ '.$lico_cru2,null,['align' => 'center','spaceBefore' => 100,'spaceAfter' => 1]);

header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Акт '.$_monthsList[$month].' '.$year.' года.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;