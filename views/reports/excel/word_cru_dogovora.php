<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */
$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;

$today = date('Y-m-d',strtotime(date($year.'-'.$month.'-01')));
if(((date('N', strtotime($today)) > 5) || (in_array($today,$model_d_date))) && !in_array($iter_date,$model_d_antidate)){
    $iter_date = date('Y-m-d',strtotime($today. " 1 day"));
    while(true){
        if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
            $first_work_day = date('d.m.Y',strtotime($iter_date));//получаем ближайший рабочий день после сегодня
            break;
        }else{
            $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
        }
    }
}else{
    $first_work_day = date('d.m.Y',strtotime($today));
}

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));
$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);
$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));

//echo $first_work_day.' - '.$last_work_day;return;

$_monthsList_rod = array(
    1=>"января",2=>"февраля",3=>"марта",
    4=>"апреля",5=>"мая", 6=>"июня",
    7=>"июля",8=>"августа",9=>"сентября",
    10=>"октября",11=>"ноября",12=>"декабря");



$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');
$town = Yii::$app->request->cookies->getValue('city');
$boss_rod = Yii::$app->request->cookies->getValue('boss_rod');

strlen($boss)<1 ? $boss = '______________' : null;
strlen($boss_rod)<1 ? $boss_rod = '______________' : null;
strlen($cityr)<1 ? $cityr = '______________' : null;
strlen($town)<1 ? $town = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;



function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(12);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(30),
    'marginRight' => m2t(15),
    'marginTop' => m2t(17.5),
    'marginBottom' => m2t(15),
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);

$PHPWord->addParagraphStyle(
    'redTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('left', m2t(20))))
);

$PHPWord->addParagraphStyle(
    'redTab44',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('left', m2t(10))))
);



$iteration = 1;
foreach($array as $item){

	$point_1_2 = '21.02.2014 г. № 0500';
	$napravlennost = 'социально-педагогической';

	switch ($item['category_id']){
		//занимательные шашки
		case 13:
			$point_1_2 = '07.10.2020 г. № 2252-п';
			$napravlennost = 'физкультурно-спортивной';
			$vozrast = 'от 4 до 7';
			break;
		//электроник
		case 11:
			$point_1_2 = '07.10.2020 г. № 2252-п';
			$napravlennost = 'технической';
			$vozrast = 'от 5 до 7';
			break;
		//электроник
		case 14:
			$point_1_2 = '07.10.2020 г. № 2252-п';
			$napravlennost = 'технической';
			$vozrast = 'от 4 до 7';
			break;
	}

    $summa = str_replace('.',',',$item['summa']);

    $iteration < 10 ? $number_document = '0'.$iteration : $number_document = $iteration;
    $date_vidacha = Yii::$app->formatter->asDate($item['date_vidacha']);
    $rozdso = Yii::$app->formatter->asDate($item['rozdso']);


    if (strpos($item['name'],'шахматис') !== false){
        $napravlennost = 'физкультурно-спортивной';
    }
    if(strpos($item['name'],'зоопарк') !== false){
        $napravlennost = 'оздоровительной';
    }

    if (strpos($item['name'],'Юный шахматист') !== false){
        $vozrast = 'от 5 до 7';
    }
    if (strpos($item['name'],'Послушный карандаш') !== false){
        $vozrast = 'от 6 до 7';
    }
    if (strpos($item['name'],'Глиняная сказка') !== false){
        $vozrast = 'от 4 до 7';
    }
    if (strpos($item['name'],'Занимательный английский') !== false){
        $vozrast = 'от 6 до 7';
    }
    if (strpos($item['name'],'РОСТок') !== false){
        $vozrast = 'от 4 до 7';
    }
    if (strpos($item['name'],'Весёлый зоопарк') !== false){
        $vozrast = 'от 3 до 7';
    }
    if (strpos($item['name'],'Ступеньки') !== false){
        $vozrast = 'от 3 до 7';
    }
    if (strpos($item['name'],'Цветик-семицветик') !== false){
        $vozrast = 'от 2 до 7';
    }
    if (strpos($item['name'],'Развитие познавательных способностей') !== false){
        $vozrast = 'от 5 до 7';
    }
    if (strpos($item['name'],'АБВГДейка') !== false){
        $vozrast = 'от 6 до 7';
    }



    (Int)$item['pn']==1 ? $pn = 'понедельник' : $pn = '';
    (Int)$item['vt']==1 ? $vt = ' вторник' : $vt = '';
    (Int)$item['sr']==1 ? $sr = ' среду' : $sr = '';
    (Int)$item['ch']==1 ? $ch = ' четверг' : $ch = '';
    (Int)$item['pt']==1 ? $pt = ' пятницу' : $pt = '';

    $ped = trim($item['nameso']);
    $m = explode(' ', $ped);
    $ped = substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.' . ' ' . $m[0];

    $section->addText('ДОГОВОР',null,['align' => 'center','spaceAfter' => 0]);
    $section->addText('возмездного оказания услуг № '.$number_document,['bold' => true],['align' => 'center','spaceAfter' => 150]);
//    $section->addTextBreak(1);
    $table = $section->addTable();
    $table->addRow();
    $table->addCell(m2t(81))->addText('г. '.$town,null,['align' => 'left','spaceAfter' => 0]);
    $table->addCell(m2t(81))->addText('«'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',null,['align' => 'right','spaceAfter' => 0]);
//    $section->addTextBreak(1);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0,'spaceBefore' => 150]);
    $textrun->addText("\tМуниципальное казенное дошкольное образовательное учреждение «Детский сад № ".$number_sad."» города ".$cityr." (далее – МКДОУ № ".$number_sad."), действующее от МО «Город ".$town."», в лице заведующего ".$boss_rod.", действующего на основании Устава, именуемое в дальнейшем «Заказчик», с одной стороны, и гражданка Российской Федерации (далее – РФ) ",null,'redTab');
    $textrun->addText(trim($item['nameso']),['bold' => true]);
    $textrun->addText(", действующая от своего имени, именуемая в дальнейшем «Исполнитель», паспорт серия ".$item['seria']." № ".$item['num_passport'].", выдан ".trim($item['kem_vidan']).", дата выдачи ".$date_vidacha." г., зарегистрированная по адресу: ".trim($item['address']).", с другой стороны, в соответствии с п. 5 ч.1 ст. 93 Федерального закона РФ № 44-ФЗ от 05 апреля 2013 г. «О контрактной системе в сфере закупок товаров, работ, услуг для обеспечения государственных и муниципальных нужд», заключили настоящий договор о нижеследующем:");

    $section->addText('1. Предмет договора.',['bold' => true],['align' => 'center','spaceAfter' => 0]);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t1.1. По настоящему договору Исполнитель обязуется по заданию Заказчика оказать дополнительные платные образовательные услуги в здании Заказчика, указанные в п.1.2. настоящего договора, а Заказчик обязуется оплатить эти услуги в порядке и на условиях, которые установлены настоящим договором.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t1.2. Исполнитель обязуется оказать следующие услуги: обучение детей в возрасте ".$vozrast." лет ".$napravlennost." направленности (кружок «".trim($item['name'])."»), на основании постановления администрации города Кирова от 30.19.2019 № 2691-п, лицензии на осуществление образовательной деятельности от ".$point_1_2.", выданной департаментом образования Кировской области.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\tОбъем оказываемых услуг определяется калькуляцией - Приложением № 1 к настоящему договору.",null,'redTab');

    $attr = array();
    $item['pn'] == 1 ? array_push($attr,1) : null;
    $item['vt'] == 1 ? array_push($attr,1) : null;
    $item['sr'] == 1 ? array_push($attr,1) : null;
    $item['ch'] == 1 ? array_push($attr,1) : null;
    $item['pt'] == 1 ? array_push($attr,1) : null;

    if (($item['sr'] == 1 || $item['pt'] == 1) && ($item['pn'] == 0 || $item['vt'] == 0 || $item['ch'] == 0)){
        count($attr) > 1 ? $kajdie = 'каждые' : $kajdie = 'каждую';
    }elseif($item['pn'] == 1 || $item['vt'] == 1 || $item['ch'] == 1){
        count($attr) > 1 ? $kajdie = 'каждые' : $kajdie = 'каждый';
    }else{
        count($attr) > 1 ? $kajdie = 'каждые' : $kajdie = 'каждый';
    }

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t1.3. Срок оказания услуг с ".$first_work_day." по ".$last_work_day.". Услуги оказываются: ".$kajdie." ".$pn.$vt.$sr.$ch.$pt." текущего месяца, за исключением нерабочих праздничных дней в РФ, установленных в соответствии со ст. 112 Трудового кодекса РФ. Исполнитель оказывает услуги лично.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t1.4. Идентификационный код закупки: ",null,'redTab');
    $textrun->addText("20343 45058288 434501001 0001 000 0000 244.",['size' => 11.5],null);

    $section->addText('2. Обязанности и права сторон.',['bold' => true],['align' => 'center','spaceAfter' => 0]);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.1. Заказчик обязан:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.1.1. Предоставить Исполнителю помещение и оборудование для оказания услуг в соответствии с дополнительной образовательной программой ".$napravlennost." направленности (кружок «".trim($item['name'])."») (далее – программа).",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.1.2. Оплатить стоимость оказанных Исполнителем услуг в размере, порядке и сроки, которые установлены настоящим договором.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.1.3. Осуществлять отчисления страховых взносов.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.2. Заказчик вправе:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.2.1. Отказаться от исполнения настоящего договора, предупредив Исполнителя за 14 календарных дней, при условии оплаты Исполнителю стоимости фактически оказанных услуг.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3. Исполнитель обязан:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.1. Предоставить заказчику до момента оказания услуг справку о наличии (отсутствии) судимости и (или) факта уголовного преследования по реабилитирующим основаниям, выданную в порядке и по форме, которые устанавливаются федеральным органом исполнительной власти, осуществляющим функции по выработке и реализации государственной политики и нормативно-правовому регулированию в сфере внутренних дел и медицинскую книжку с отметкой о прохождении периодического медицинского осмотра.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.2. Оказывать услуги по обучению детей в возрасте ".$vozrast." лет ".$napravlennost." направленности (кружок «".trim($item['name'])."») согласно программе.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.3. Следовать инструкции по охране жизни и здоровья детей.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.4. Защищать детей от всех форм физического и психического насилия.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.5. Обладать специальными знаниями, умениями и навыками, постоянно их совершенствовать.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.3.6. Нести ответственность за сохранность имущества и оборудования в предоставленном помещении при оказании услуг.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.4. Исполнитель вправе:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.4.1. Отказаться от исполнения настоящего договора, предупредив Заказчика за 14 календарных дней, при условии полного возмещения Заказчику убытков.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.5. По окончании каждого месяца не позднее 3 дней, с момента оказания услуг, стороны подписывают акт об оказании услуг, в котором содержатся сведения об объеме оказанных услуг и их стоимости.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t2.6. Стороны вправе при исполнении настоящего договора увеличить или уменьшить объем оказываемых услуг, определенный в соответствии с Приложением № 1 настоящего договора, но не более чем на 10 процентов, путем подписания дополнительного соглашения на основании пп.6 п.1 ч.1 статьи 95 Федерального закона от 05.04.2013 № 44-ФЗ «О контрактной системе в сфере закупок товаров, работ, услуг для обеспечения государственных и муниципальных нужд».",null,'redTab');

    $section->addText('3. Цена договора и порядок расчетов.',['bold' => true],['align' => 'center','spaceAfter' => 0]);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t3.1. Цена услуг Исполнителя составляет ".$summa." (".num2str($item['summa'])."), в том числе НДФЛ (13%).",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\tСумма, подлежащая уплате Заказчиком юридическому лицу или физическому лицу, в том числе зарегистрированному в качестве индивидуального предпринимателя, подлежит уменьшению на размер налогов, сборов и иных обязательных платежей в бюджеты бюджетной системы Российской Федерации, связанных с оплатой договора, если в соответствии с законодательством Российской Федерации о налогах и сборах такие налоги, сборы и иные обязательные платежи подлежат уплате в бюджеты бюджетной системы Российской Федерации Заказчиком.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t3.2. Расчёт цены услуг Исполнителя определяется по формуле:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("Цу= Ту х Кпдз х П, где Цу — цена услуги, Ту - утвержденный постановлением администрации города Кирова тариф на дополнительную платную образовательную услугу, Кпдз – количество посещений детьми занятий за период оказания услуги, согласно табеля учета посещаемости детей, П- процент от общей суммы денежных средств, полученных от оказания платных образовательных услуг, направленный в рамках утверждённой калькуляции на оплату труда, согласно положения об организации предоставления платных образовательных услуг муниципальным казенным дошкольным образовательным учреждением «Детский сад №35» города Кирова.");

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t3.3. Заказчик оплачивает услуги Исполнителя, указанные в п. 1.2 настоящего договора в течение 30 календарных дней с момента подписания акта об оказании услуг в соответствии с п. 2.5 настоящего договора.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t3.4. Оплата услуг производится Заказчиком безналичными денежными средствами через кредитные учреждения на указанный расчетный счет Исполнителя.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t3.5. При изменении объема услуг в соответствии с п. 2.6 договора изменяется цена договора пропорционально изменяемому объему услуг, но не более чем на десять процентов цены договора в соответствии с требованиями пп.б п.1 ч.1 статьи 95 Федерального закона от 05.04.2013 № 44-ФЗ «О контрактной системе в сфере закупок товаров, работ, услуг для обеспечения государственных и муниципальных нужд».",null,'redTab');

    $section->addText('4. Ответственность сторон.',['bold' => true],['align' => 'center','spaceAfter' => 0]);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.1. Заказчик и Исполнитель за неисполнение или ненадлежащее исполнение обязательств, предусмотренных договором, несут ответственность в виде пени и штрафов, определяемых в соответствии с Правилами определения размера штрафа, начисляемого в случае ненадлежащего исполнения Заказчиком, неисполнения или ненадлежащего исполнения Исполнителем обязательств, предусмотренных договором (за исключением просрочки исполнения обязательств Заказчиком, Исполнителем), и размера пени, начисляемой за каждый день просрочки исполнения Исполнителем обязательства, предусмотренного договором, утвержденными Постановлением Правительства Российской Федерации от 30.08.2017 № 1042 (далее – Правила).",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.2. Исполнитель уплачивает Заказчику пени (штрафы) в случаях:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.2.1. За каждый факт неисполнения или ненадлежащего исполнения Исполнителем обязательств, предусмотренных договором, за исключением просрочки исполнения обязательств (в том числе гарантийного обязательства), предусмотренных договором, размер штрафа устанавливается в виде следующем порядке:",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\tа) 10 процентов цены договора в случае, если цена договора не превышает 3 млн. рублей;",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.2.2. За каждый факт неисполнения или ненадлежащего исполнения Исполнителем обязательства, предусмотренного договором, которое не имеет стоимостного выражения, размер штрафа устанавливается (при наличии в договоре таких обязательств) в виде фиксированной суммы – 1000 (Одна тысяча) рублей.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.3. Пеня начисляется за каждый день просрочки исполнения Исполнителем обязательства, предусмотренного договором, в размере одной трехсотой действующей на дату уплаты пени ключевой ставки Центрального банка Российской Федерации от цены договора, уменьшенной на сумму, пропорциональную объему обязательств, предусмотренных договором и фактически исполненных Исполнителем.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.4. Общая сумма начисленной неустойки (штрафов, пени) за неисполнение или ненадлежащее исполнение Исполнителем обязательств, предусмотренных договором, не может превышать цену договора.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.5. За каждый факт неисполнения Заказчиком обязательств, предусмотренных договором, за исключением просрочки исполнения обязательств, предусмотренных договором, размер штрафа устанавливается в виде фиксированной суммы – 1000 (Одна тысяча) рублей.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.6. В случае просрочки исполнения Заказчиком обязательств, предусмотренных договором, Исполнитель вправе потребовать уплаты пеней. Пеня начисляется за каждый день просрочки исполнения обязательства, предусмотренного договором, начиная со дня, следующего после дня истечения, установленного договором срока исполнения обязательства. Такая пеня устанавливается в размере одной трехсотой действующей на дату уплаты пеней ключевой ставки Центрального банка Российской Федерации от не уплаченной в срок суммы.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.7. Общая сумма начисленной неустойки (штрафов, пени) за ненадлежащее исполнение Заказчиком обязательств, предусмотренных договором, не может превышать цену договора.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.8. Сторона освобождается от уплаты неустойки (штрафа, пени), если докажет, что неисполнение или ненадлежащее исполнение обязательства, предусмотренного договором, произошло вследствие непреодолимой силы или по вине другой стороны.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.9. Сторона, которая не исполняет своего обязательства вследствие действия непреодолимой силы, должна в 5-дневный срок с момента возникновения указанных обстоятельств письменно известить другую сторону о препятствии и его влиянии на исполнение обязательств по договору. Извещение должно содержать данные о характере обстоятельств, а также официальные документы, удостоверяющие наличие этих обстоятельств и дающие оценку их влияния на возможность исполнения Стороной своих обязательств по договору. Достаточным подтверждением возникновения обстоятельств непреодолимой силы будет являться справка, выданная компетентным органом государственной власти/управления Российской Федерации.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.10. Сторона, несвоевременно направившая извещение, предусмотренное в п. 4.9 договора, возмещает другой Стороне понесенные последней убытки.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.11. В случаях наступления обстоятельств, указанных в п. 4.8 договора, срок выполнения Стороной обязательств по договору отодвигается соразмерно времени, в течение которого действуют эти обстоятельства и их последствия.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.12. За ущерб, причиненный третьему лицу в процессе оказания услуг, отвечает Исполнитель, если не докажет, что ущерб был причинен вследствие обстоятельств, за которые отвечает Заказчик.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.13. Риск случайного повреждения (порчи) результата оказанных услуг лежит на Исполнителе до момента исполнения им своего обязательства по оказанию услуг.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t4.14. Ответственность за соблюдение правил и техники безопасности, требований охраны труда, требований пожарной безопасности при оказании услуг по предмету договора – односторонняя, возлагается на Исполнителя.",null,'redTab');

    $section->addText('5. Прочие условия.',['bold' => true],['align' => 'center','spaceAfter' => 0]);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t5.1. Настоящий договор вступает в силу с момента подписания и действует по полные исполнения Сторонами своих обязанностей по Договору. Окончание срока действия договора не влечет прекращения обязательств сторон по договору, возникших в период его действия до полного их исполнения.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t5.2. Все споры и разногласия по исполнению настоящего договора или в связи с ним разрешаются сторонами путем переговоров, а при недостижении согласия - в судебном порядке. В вопросах, не урегулированных настоящим договором, стороны руководствуются действующим законодательством РФ.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t5.3. Настоящий договор может быть изменен или расторгнут по соглашению сторон, в одностороннем порядке или по решению суда в соответствии с действующим законодательством. Все изменения и дополнения к настоящему договору считаются действительными, если они оформлены письменным соглашением сторон.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t5.4. При изменении адресов, реквизитов Сторон (наименование, адрес, обслуживающий банк и т.п.), а также в случае реорганизации или ликвидации, Стороны обязаны письменно уведомить друг друга об этом в 10 - дневный срок после их осуществления.",null,'redTab');

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\t5.5. Настоящий договор составлен в двух экземплярах, имеющих одинаковую юридическую силу, по одному экземпляру для каждой из сторон.",null,'redTab');

    $section->addPageBreak();

    $section->addText('6. Адреса, реквизиты и подписи сторон.',['bold' => true],['align' => 'center','spaceAfter' => 100]);

    $table = $section->addTable();
    $table->addRow();

    $probel = 0;

    $PHPWord->addParagraphStyle(
        'table_mable',
        array(
            'space' => array('line' => 10),
            'spaceAfter' => 1,
        )
    );
    $PHPWord->addParagraphStyle(
        'table_mable0',
        array(
            'space' => array('line' => 10),
            'spaceAfter' => 100,
        )
    );
    $PHPWord->addParagraphStyle(
        'table_mable_mp',
        array(
            'space' => array('line' => 10),
            'spaceAfter' => 100,
            'tabs' => array(new \PhpOffice\PhpWord\Style\Tab('left', m2t(10)))
        )
    );

    $table_run = $table->addCell(m2t(81));
    $table_run->addText("«Заказчик»:",['bold' => true],'table_mable0');
    $table_run->addText("МКДОУ № 35",null,'table_mable');
    $table_run->addText("Адрес: г. Киров, А.С. Большева 1.",null,'table_mable');
    $table_run->addText("ИНН: 4345058288 КПП434501001",null,'table_mable');
    $table_run->addText("Р/с: 40204810900000000002",null,'table_mable');
    $table_run->addText("в Отделение Киров г. Киров УФК по Кировской области (департамент финансов администрации города Кирова (МКДОУ №35)",null,'table_mable');
    $table_run->addText("л/с 02403025290, л/с 03909037022",null,'table_mable');
    $table_run->addText("ОКТМО 33701000 ОКПО 13691336",null,'table_mable');
    $table_run->addText("ОГРН 1034316574225 БИК 043304001",null,'table_mable');
    $table_run->addText("Телефон: 8332 23-02-22",null,'table_mable0');

    $table_run = $table->addCell(m2t(81));
    $table_run->addText("«Исполнитель»:",['bold' => true],'table_mable0');
    $table_run->addText(trim($item['nameso']),null,'table_mable');
    $table_run->addText('дата рождения '.$rozdso,null,'table_mable');
    $table_run->addText('серия '.$item['seria'].' № '.$item['num_passport'].',',null,'table_mable');
    $table_run->addText('выдан '.trim($item['kem_vidan']),null,'table_mable');
    $table_run->addText('дата выдачи '.$date_vidacha,null,'table_mable');
    $table_run->addText('адрес: '.trim($item['address']),null,'table_mable');
    $table_run->addText('ИНН: '.$item['inn'],null,'table_mable');
    $table_run->addText('СНИЛС: '.$item['snils'],null,'table_mable');

    $table->addRow();
    $table_run = $table->addCell(m2t(81));
    $table_run->addText("________________/".$boss."/",null,'table_mable0');
    $table_run->addText('«'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 9]);
    $table_run->addText("\tМ.П.",['size' => 8],'table_mable_mp');
    $table_run = $table->addCell(m2t(81));
    $table_run->addText('_______________/'.$ped.'/',null,'table_mable0');
    $table_run->addText('«'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 9]);


    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText('В целях заключения и исполнения настоящего договора на обработку персональных данных в соответствии с Федеральным законом от 27 июля 2006 г. N 152-ФЗ "О персональных данных" (с последующими изменениями и дополнениями.) согласна____________/'.$ped.'/ «'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 10]);

    $section->addPageBreak();

    $section->addText('Приложение № 1 к договору от «'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г. № '.$number_document,null,['align' => 'right','spaceAfter' => 0]);
    $section->addTextBreak(1);
    $width_col = 50;

    $table = $section->addTable();
    $table->addRow(m2t(10));
    $table->addCell(m2t($width_col),$cellStyle)->addText('Наименование услуг',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText('Количество посещений детьми занятий за период оказания услуги',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText('Тариф на дополнительную платную образовательную услугу, утвержденный Постановлением  №4104-п от 09.12.2016, рублей',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText('Процент от общей суммы денежных средств согласно Положения об организации предоставления платных образовательных услуг, направленный в рамках утверждённой калькуляции на оплату труда от 24.07.2019 г.',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText('Цена услуги, рублей, в том числе НДФЛ 13%',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);

    $table->addRow(m2t(10));
    $table->addCell(m2t($width_col),$cellStyle)->addText('услуги по обучению детей по ДОП '.$napravlennost.' направленности (кружок «'.trim($item['name']).'»)',['size' => 9],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText($item['detodays'],['size' => 11],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText($item['price'].',00',['size' => 11],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText($item['zp'].' %',['size' => 11],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);
    $table->addCell(m2t($width_col),$cellStyle)->addText($summa,['size' => 11],['align' => 'center','spaceAfter' => 0,'spaceBefore' => 0]);

    $section->addTextBreak(1);
    $table = $section->addTable();
    $table->addRow(m2t(0));

    $probel = 1;

    $table_run = $table->addCell(m2t(100));
    $table_run->addText("«Заказчик»",['size' => 12],['align' => 'left']);
    $table_run->addText("_________________/".$boss."/",['size' => 12],['align' => 'left']);
    $table_run->addText('«'.date('d',strtotime($last_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 12],['align' => 'left','spaceAfter' => 150]);
    $table_run->addText("\tМ.П.",['size' => 12],'table_mable_mp');

    $table_run = $table->addCell(m2t(100));
    $table_run->addText("«Исполнитель»",['size' => 12],['align' => 'left']);
    $table_run->addText('_________________/'.$ped.'/',['size' => 12],['align' => 'left']);
    $table_run->addText('«'.date('d',strtotime($last_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 12],['align' => 'left','spaceAfter' => 150]);

    $section->addTextBreak(5);
    $textrun = $section->createTextRun(['align' => 'left','spaceAfter' => 0,'spaceBefore' => 0]);
    $textrun->addText("1");
    $textrun->addText("\tПостановление администрации города Кирова «Об установлении тарифов на дополнительные платные образовательные услуги, оказываемые МКДОУ»",null,'redTab');

    if ($iteration < count($array)){
        $section->addPageBreak();
    }




    $iteration++;
}







header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Договора кружки '.$_monthsList[$month].' '.$year.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;