<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 06.02.20
 * Time: 22:00
 */
$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;

$today = date('Y-m-d',strtotime(date($year.'-'.$month.'-01')));
if(((date('N', strtotime($today)) > 5) || (in_array($today,$model_d_date))) && !in_array($iter_date,$model_d_antidate)){
    $iter_date = date('Y-m-d',strtotime($today. " 1 day"));
    while(true){
        if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
            $first_work_day = date('d.m.Y',strtotime($iter_date));//получаем ближайший рабочий день после сегодня
            break;
        }else{
            $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
        }
    }
}else{
    $first_work_day = date('d.m.Y',strtotime($today));
}

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));
$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);
$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));


$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');
$town = Yii::$app->request->cookies->getValue('town');
$boss_rod = Yii::$app->request->cookies->getValue('boss_rod');

strlen($boss)<1 ? $boss = '______________' : null;
strlen($boss_rod)<1 ? $boss_rod = '______________' : null;
strlen($cityr)<1 ? $cityr = '______________' : null;
strlen($town)<1 ? $town = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;

$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(12);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.8);
$sheet->getPageMargins()->setBottom(0.5);
$sheet->getPageMargins()->setLeft(0.7);
$sheet->getPageMargins()->setRight(0.5);

$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2);

$sheet->getColumnDimension('A')->setWidth(4);
$sheet->getColumnDimension('B')->setWidth(27);
$sheet->getColumnDimension('C')->setWidth(10);
$sheet->getColumnDimension('D')->setWidth(12);
$sheet->getColumnDimension('E')->setWidth(15);
$sheet->getColumnDimension('F')->setWidth(13);

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(40);
$sheet->setCellValue("A{$line}", 'Реестр договоров ГПХ по МКДОУ № '.$number_sad.' за '.$_monthsList[$month].' '.$year.' г.');
$sheet->mergeCells("A{$line}:F{$line}");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true);

$line++;//2
$sheet->getRowDimension($line)->setRowHeight(20);
$line++;//2
$sheet->getRowDimension($line)->setRowHeight(40);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", 'ФИО исполнителя')->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", 'Номер договора')->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", 'Дата договора')->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", 'Срок оказания услуг')->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", 'Сумма, руб.')->getStyle("F{$line}")->getAlignment()->setWrapText(true);

$sheet->getStyle("A{$line}:F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:F{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:F{$line}")->applyFromArray($style_border_all);

//$line++;//12
$iteration = 1;
foreach($array as $a){
    $line++;
    $iteration < 10 ? $number_document = '0'.$iteration : $number_document = $iteration;
    $sheet->getRowDimension($line)->setRowHeight(36);
    $sheet->getStyle("A{$line}:F{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $iteration)->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", ' '.trim($a['nameso']))->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(0.1);
    $sheet->setCellValue("C{$line}", $number_document)->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("D{$line}", $first_work_day)->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("E{$line}", $first_work_day." - \n".$last_work_day)->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("F{$line}", str_replace('.',',',$a['summa']))->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A{$line}:F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}

$sum = 0;
foreach($array as $num => $values) {
    $sum += $values['summa'];
//    $line++;
//    $sheet->setCellValue("B{$line}", number_format((float)$sum, 2, ',', ''));
}

//$sum += 1.22;

$line++;
$sheet->getRowDimension($line)->setRowHeight(30);
$sheet->getStyle("A{$line}:F{$line}")->applyFromArray($style_border_all);
$sheet->getStyle("A{$line}:F{$line}")->getFont()->setBold(true);
$sheet->setCellValue("B{$line}", 'Всего');
$sheet->getStyle("A{$line}:F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->setCellValue("F{$line}", number_format((float)$sum, 2, ',', ''));

$line++;
$sheet->getRowDimension($line)->setRowHeight(20);
$line++;
$sheet->getRowDimension($line)->setRowHeight(30);
$sheet->setCellValue("B{$line}", 'Заведующий __________________ '.$boss);





$sheet->setBreak("G{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:F{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);

$name_file = "Реестр договоров (".$_monthsList[$month].' '.$year.') '.date("d.m.Y");

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;