<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 06.02.20
 * Time: 22:00
 */


$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);



$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(10);
$sheet->getDefaultStyle()->getFont()->setName('Times New Roman');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');

//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.5);
$sheet->getPageMargins()->setBottom(0.5);
$sheet->getPageMargins()->setLeft(0.7);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);

$i = 1;
foreach($array as $a){
    if ((Int)$a['idd'] > 0){
        $i++;
    }
}
$i > 27 ? $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(2,2) : null;

$sheet->getColumnDimension('A')->setWidth(4);
$sheet->getColumnDimension('B')->setWidth(22);
$sheet->getColumnDimension('C')->setWidth(8);
$sheet->getColumnDimension('D')->setWidth(12);
$sheet->getColumnDimension('E')->setWidth(20);
$sheet->getColumnDimension('F')->setWidth(12);
$sheet->getColumnDimension('G')->setWidth(22);

$line = 1;
$sheet->getRowDimension($line)->setRowHeight(40);
$sheet->MergeCells("A{$line}:G{$line}");
$sheet->setCellValue("A{$line}", "Журнал заболеваемости ОРЗ ".$_monthsList[$month]." ".$year." г.");
$sheet->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}")->getFont()->setBold(true)->setSize(12);
$line++;
$sheet->getRowDimension($line)->setRowHeight(40);
$sheet->setCellValue("A{$line}", "№\nп/п")->getStyle("A{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("B{$line}", 'ФИО ребёнка')->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("C{$line}", '№ группы')->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("D{$line}", 'Ds. заболевания')->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("E{$line}", 'Период болезни')->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("F{$line}", 'Детодни по болезни')->getStyle("F{$line}")->getAlignment()->setWrapText(true);
$sheet->setCellValue("G{$line}", 'Примечание')->getStyle("G{$line}")->getAlignment()->setWrapText(true);

$sheet->getStyle("A{$line}:G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("A{$line}:G{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("A{$line}:G{$line}")->getFont()->setBold(true);
$sheet->getStyle("A{$line}:G{$line}")->applyFromArray($style_border_all);

//$line++;//12
$iteration = 1;
foreach($array as $a){
    $line++;
    $sheet->getRowDimension($line)->setRowHeight(25);
    $sheet->getStyle("A{$line}:G{$line}")->applyFromArray($style_border_all);
    $sheet->setCellValue("A{$line}", $a['idd'])->getStyle("A{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("B{$line}", trim($a['name']))->getStyle("B{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setWrapText(true)->setIndent(0.1);
    $sheet->setCellValue("C{$line}", $a['id_gruppa'])->getStyle("C{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("D{$line}", trim($a['diagnos']))->getStyle("D{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("E{$line}", $a['period'])->getStyle("E{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setWrapText(true);
    $sheet->setCellValue("F{$line}", $a['count_days'])->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->setCellValue("G{$line}", $a['coment'])->getStyle("G{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle("A{$line}:G{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    $iteration++;
}

$sheet->setBreak("H{$line}",PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->getPageSetup()->setPrintArea("A1:G{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);

$name_file = "Журнал заболеваемости ОРЗ ".$_monthsList[$month]." ".$year." г.";

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;