<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */


$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));

$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;

$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));







//$date = date('d.m.Y');
//$year = date('Y');
$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');
$lico_cru = Yii::$app->request->cookies->getValue('lico_cru');//кружки родительный падеж
$lico_cru2 = Yii::$app->request->cookies->getValue('lico_cru2');//кружки именительный падеж


strlen($boss)<1 ? $boss = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;

foreach($array as $q){
    $sum += $q['count_child'];//всего количество детей
    $detodays += $q['detodays'];//всего количество детей
}

function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(11);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(28),
    'marginRight' => m2t(10),
    'marginTop' => m2t(19),
    'marginBottom' => m2t(19),
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);

/*$table = $section->addTable();
$table->addRow(m2t(10));
$table->addCell(m2t(100))->addText('');
$table->addCell(m2t(80))->addText('Утверждаю:<w:br/>Заведующий МКДОУ № '.$number_sad.'<w:br/>_____________/'.$boss,['size' => 12],['spaceAfter' => 1]);*/

$section->addText('СПРАВКА МКДОУ № '.$number_sad.' г. '.$cityr.'',['size' => 12],['align' => 'center','spaceAfter' => 1]);
$section->addText('за '.$_monthsList[$month].' '.$year.' года',['size' => 12],['align' => 'center','spaceAfter' => 100]);

$y = 0;
foreach($array as $q){
    $last_char = (Int)(substr(trim($q['name']),-1));
    if($last_char == 1 || $last_char == 0){
        $y++;
    }
}

switch($y){
    case 1:
    case 21:
    $y = $y.' кружок';
    break;
    case 2:
    case 3:
    case 4:
    case 22:
    case 23:
    case 24:
    $y = $y.' кружка';
    break;
    default:
        $y = $y.' кружков';
        break;
}

$section->addText('Списочный состав: '.$y.' (по постановлению) '.$sum.' детей.',['size' => 12],['align' => 'center','spaceAfter' => 100]);
//$font_size_table = 11;
$table = $section->addTable();
$table->addRow(m2t(8));
$table->addCell(m2t(15),$cellStyle)->addText('',null,['align' => 'center','spaceAfter' => 100,'spaceBefore' => 100]);
$table->addCell(m2t(100),$cellStyle)->addText('Кружки по подгруппам',null,['align' => 'center','spaceAfter' => 200,'spaceBefore' => 200]);
$table->addCell(m2t(25),$cellStyle)->addText('Кол-во<w:br/> детей',null,['align' => 'center','spaceAfter' => 100,'spaceBefore' => 100]);
$table->addCell(m2t(25),$cellStyle)->addText('Кол-во<w:br/> детодней',null,['align' => 'center','spaceAfter' => 100,'spaceBefore' => 100]);



$i = 1;
foreach($array as $q){

    $last_char = (Int)(substr(trim($q['name']),-1));
    if($last_char == 1 || $last_char == 0){
        $ii = $i;
        $i++;
    }else{
        $ii = '';
    }

    $stil = null;
    $stil2 = array('align' => 'left','spaceAfter' => 5,'spaceBefore' => 5);
    $stil2_1 = array('align' => 'right','spaceAfter' => 5,'spaceBefore' => 5);
    $stil2_2 = array('align' => 'center','spaceAfter' => 5,'spaceBefore' => 5);
    $row_height = 5;

    $table->addRow(m2t($row_height));
    $table->addCell(m2t(15),$cellStyle)->addText($ii.' ',$stil,$stil2_1);
    $table->addCell(m2t(100),$cellStyle)->addText(' '.trim($q['name']),$stil,$stil2);
    $table->addCell(m2t(25),$cellStyle)->addText($q['count_child'],$stil,$stil2_2);
    $table->addCell(m2t(25),$cellStyle)->addText($q['detodays'],$stil,$stil2_2);


}
$stil = array('bold' => true);
$stil2 = array('align' => 'left','spaceAfter' => 100,'spaceBefore' => 100);
$stil2_1 = array('align' => 'right','spaceAfter' => 100,'spaceBefore' => 100);
$stil2_2 = array('align' => 'center','spaceAfter' => 100,'spaceBefore' => 100);
$row_height = 8;

$table->addRow(m2t($row_height));
$table->addCell(m2t(15),$cellStyle)->addText($ii,$stil,$stil2_1);
$table->addCell(m2t(100),$cellStyle)->addText(' Итого',$stil,$stil2);
$table->addCell(m2t(25),$cellStyle)->addText($sum,$stil,$stil2_2);
$table->addCell(m2t(25),$cellStyle)->addText($detodays,$stil,$stil2_2);
//$section->addTextBreak(1);

$section->addText('Заведующий МКДОУ № '.$number_sad.' ________________/ '.$boss,['size' => 12],['align' => 'left','spaceBefore' => 100,'spaceAfter' => 1]);
$section->addTextBreak(1);
$table = $section->addTable();
$table->addRow(m2t(8));
$table->addCell(m2t(15))->addText('МП.',null,['align' => 'left','spaceAfter' => 1,'spaceBefore' => 1]);
$table->addCell(m2t(100))->addText($last_work_day.' г.',null,['align' => 'right','spaceAfter' => 1,'spaceBefore' => 1]);
$table->addCell(m2t(50))->addText(' ',null,['align' => 'left','spaceAfter' => 1,'spaceBefore' => 1]);

header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Справка '.$_monthsList[$month].' '.$year.' года.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;