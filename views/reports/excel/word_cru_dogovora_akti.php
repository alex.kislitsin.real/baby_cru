<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */
$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;

$today = date('Y-m-d',strtotime(date($year.'-'.$month.'-01')));
if(((date('N', strtotime($today)) > 5) || (in_array($today,$model_d_date))) && !in_array($iter_date,$model_d_antidate)){
    $iter_date = date('Y-m-d',strtotime($today. " 1 day"));
    while(true){
        if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
            $first_work_day = date('d.m.Y',strtotime($iter_date));//получаем ближайший рабочий день после сегодня
            break;
        }else{
            $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
        }
    }
}else{
    $first_work_day = date('d.m.Y',strtotime($today));
}

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));
$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);
$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));

//echo $first_work_day.' - '.$last_work_day;return;

$_monthsList_rod = array(
    1=>"января",2=>"февраля",3=>"марта",
    4=>"апреля",5=>"мая", 6=>"июня",
    7=>"июля",8=>"августа",9=>"сентября",
    10=>"октября",11=>"ноября",12=>"декабря");



$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');
$town = Yii::$app->request->cookies->getValue('town');
$boss_rod = Yii::$app->request->cookies->getValue('boss_rod');

strlen($boss)<1 ? $boss = '______________' : null;
strlen($boss_rod)<1 ? $boss_rod = '______________' : null;
strlen($cityr)<1 ? $cityr = '______________' : null;
strlen($town)<1 ? $town = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;


function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(14);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(12.7),
    'marginRight' => m2t(12.7),
    'marginTop' => m2t(12.7),
    'marginBottom' => m2t(12.7),
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);

$PHPWord->addParagraphStyle(
    'redTab',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('left', m2t(20))))
);

$PHPWord->addParagraphStyle(
    'redTab44',
    array('tabs' => array(new \PhpOffice\PhpWord\Style\Tab('left', m2t(10))))
);



$iteration = 1;
foreach($array as $item){

    $summa = str_replace('.',',',$item['summa']);

    $iteration < 10 ? $number_document = '0'.$iteration : $number_document = $iteration;

    $ped = trim($item['nameso']);
    $m = explode(' ', $ped);
    $ped =  $m[0].' '.substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.';
    $ped2 =  substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.' . ' ' . $m[0];

    $section->addText('АКТ об оказании услуг',['bold' => true],['align' => 'center','spaceAfter' => 0]);
    $section->addText('№ '.$iteration.' к контракту № '.$number_document.' от «'.date('d',strtotime($first_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['bold' => true],['align' => 'center','spaceAfter' => 200]);
//    $section->addTextBreak(1);
    $table = $section->addTable();
    $table->addRow();
    $table->addCell(m2t(90))->addText('г. '.$town,['bold' => true],['align' => 'left','spaceAfter' => 0]);
    $table->addCell(m2t(90))->addText('«'.date('d',strtotime($last_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['bold' => true],['align' => 'right','spaceAfter' => 0]);
//    $section->addTextBreak(1);

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0,'spaceBefore' => 200]);
    $textrun->addText("\tМуниципальное казенное дошкольное образовательное учреждение «Детский сад № ".$number_sad."» города ".$cityr.", именуемое в дальнейшем «Заказчик», в лице заведующего ".$boss_rod.", действующей на основании Устава, с одной стороны, и гражданка РФ ",null,'redTab');
    $textrun->addText(trim($item['nameso']),['bold' => true]);
    $textrun->addText(", в дальнейшем именуется «Исполнитель», с другой стороны, составили настоящий акт о нижеследующем:");

    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\tВ соответствии с контрактом возмездного оказания услуг № ".$number_document." от ".$first_work_day." года гражданка РФ ".$ped." с «".date('d',strtotime($first_work_day))."» ".$_monthsList_rod[$month]." ".$year." года по «".date('d',strtotime($last_work_day))."» ".$_monthsList_rod[$month]." ".$year." года оказала услуги по ведению кружка ",null,'redTab');
    $textrun->addText("«".trim($item['name']).'».',['bold' => true]);
    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 0]);
    $textrun->addText("\tУслуги оказаны на сумму ",null,'redTab');
    $textrun->addText($summa,['bold' => true]);
    $textrun->addText(" (".num2str($item['summa'])."), в том числе НДФЛ (13%).");
    $textrun = $section->createTextRun(['align' => 'both','spaceAfter' => 1]);
    $textrun->addText("\tСтороны претензий по качеству, сумме и срокам оказания услуг не имеют.",null,'redTab');
    $section->addTextBreak(1);

    $table = $section->addTable();
    $table->addRow(m2t(0));

    $probel = 1;

    $table_run = $table->addCell(m2t(90));
    $table_run->addText("ЗАКАЗЧИК",null,['align' => 'center']);
    $table_run->addText("МКДОУ № ".$number_sad,['size' => 12],['align' => 'left']);
    $table_run->addText("________________________/".$boss."/",['size' => 12],['align' => 'left']);
    $table_run->addText('«'.date('d',strtotime($last_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 12],['align' => 'left','spaceAfter' => 150]);
    $table_run->addText("\tМ.П.",['size' => 12],'redTab44');




    $table_run = $table->addCell(m2t(90));
    $table_run->addText("ИСПОЛНИТЕЛЬ",null,['align' => 'center']);
    $table_run->addText(trim($item['nameso']),['size' => 12],['align' => 'left']);
    $table_run->addText('________________________/'.$ped2.'/',['size' => 12],['align' => 'left']);
    $table_run->addText('«'.date('d',strtotime($last_work_day)).'» '.$_monthsList_rod[$month].' '.$year.' г.',['size' => 12],['align' => 'left','spaceAfter' => 150]);


    if ($iteration < count($array)){
        $section->addPageBreak();
    }




    $iteration++;
}







header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Акты к Договорам кружки '.$_monthsList[$month].' '.$year.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;