<?php
use app\models\Queries;
use yii\bootstrap\Progress;
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 22.10.19
 * Time: 20:20
 */
$qu = new Queries();
$mon = $month - 1;
$number_sad = Yii::$app->request->cookies->getValue('number_sad');

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));

$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}

$iter_date = date($year.'-'.$monthwithzero.'-'.$count_days_all);
//$last_works_day = $iter_date;
$count_works_days = 0;
while($iter_date >= date($year.'-'.$monthwithzero.'-01')){
    if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
        $count_works_days++;
    }
    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
}

/*while(((date('N', strtotime($last_works_day)) > 5) || (in_array($last_works_day,$model_d_date))) && !in_array($last_works_day,$model_d_antidate)){
    $last_works_day = date('Y-m-d',strtotime($last_works_day. " - 1 day"));
}*/

switch($count_works_days){
    case 1:
    case 21:
    case 31:
        $count_works_days = $count_works_days.' день';
        break;
    case 2:
    case 3:
    case 4:
    case 22:
    case 23:
    case 24:
        $count_works_days = $count_works_days.' дня';
        break;
    default:
        $count_works_days = $count_works_days.' дней';
        break;
}




$town = Yii::$app->request->cookies->getValue('cityr');//город в родительном падеже
$boss = Yii::$app->request->cookies->getValue('boss');//boss imenit padej
$medsestra = Yii::$app->request->cookies->getValue('medsestra');//$medsestra

$array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
/*if (count($array_gruppa)>0){
    $name_group = $array_gruppa[$id_group];
}*/


$style_border_bottom = array(
    'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
//    'fill' => array(
//        'type' => PHPExcel_Style_Fill::FILL_SOLID,
//        'startcolor' => array(
//            'argb' => 'FFFF0000',
//        ),
//    ),
);
$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);
$allign_center_all = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$allign_up_right = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_left = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_center = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(7.7);
$sheet->getDefaultStyle()->getFont()->setName('Arial');
$sheet->getSheetView()->setZoomScale(85);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
//$sheet->getPageSetup()->setPrintArea('B1:AO29');
//$sheet->setBreak('AO1',PHPExcel_Worksheet::BREAK_COLUMN);
//$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0.65);
$sheet->getPageMargins()->setBottom(0.65);
$sheet->getPageMargins()->setLeft(0.65);
$sheet->getPageMargins()->setRight(0);

$sheet->getPageSetup()->setFitToWidth(1);
//$sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(10,12);

$width_col = 2.89;//good
$height_row_up_3 = 12.5;//1-3 rows
$height_row_up = 11.5;//4-9 rows
$height_row_st = 22.48;

$height_row_shapka1 = 26.5;
$height_row_shapka2 = 26;
$height_row_shapka3 = 24;
$height_row_shapka_down = 12.2;

$sheet->getColumnDimension('A')->setWidth(1)->setVisible(false);
$sheet->getColumnDimension('B')->setWidth(4.7);
$sheet->getColumnDimension('C')->setWidth(17.7);
$sheet->getColumnDimension('D')->setWidth(9.5);
$sheet->getColumnDimension('E')->setWidth(8);
$sheet->getColumnDimension('F')->setWidth($width_col);
$sheet->getColumnDimension('G')->setWidth($width_col);
$sheet->getColumnDimension('H')->setWidth($width_col);
$sheet->getColumnDimension('I')->setWidth($width_col);
$sheet->getColumnDimension('J')->setWidth($width_col);
$sheet->getColumnDimension('K')->setWidth($width_col);
$sheet->getColumnDimension('L')->setWidth($width_col);
$sheet->getColumnDimension('M')->setWidth($width_col);
$sheet->getColumnDimension('N')->setWidth($width_col);
$sheet->getColumnDimension('O')->setWidth($width_col);
$sheet->getColumnDimension('P')->setWidth($width_col);
$sheet->getColumnDimension('Q')->setWidth($width_col);
$sheet->getColumnDimension('R')->setWidth($width_col);
$sheet->getColumnDimension('S')->setWidth($width_col);
$sheet->getColumnDimension('T')->setWidth($width_col);
$sheet->getColumnDimension('U')->setWidth($width_col);
$sheet->getColumnDimension('V')->setWidth($width_col);
$sheet->getColumnDimension('W')->setWidth($width_col);
$sheet->getColumnDimension('X')->setWidth($width_col);
$sheet->getColumnDimension('Y')->setWidth($width_col);
$sheet->getColumnDimension('Z')->setWidth($width_col);
$sheet->getColumnDimension('AA')->setWidth($width_col);
$sheet->getColumnDimension('AB')->setWidth($width_col);
$sheet->getColumnDimension('AC')->setWidth($width_col);
$sheet->getColumnDimension('AD')->setWidth($width_col);
$sheet->getColumnDimension('AE')->setWidth($width_col);
$sheet->getColumnDimension('AF')->setWidth($width_col);
$sheet->getColumnDimension('AG')->setWidth($width_col);
$sheet->getColumnDimension('AH')->setWidth($width_col);
$sheet->getColumnDimension('AI')->setWidth($width_col);
$sheet->getColumnDimension('AJ')->setWidth($width_col);
$sheet->getColumnDimension('AK')->setWidth(8.5);
$sheet->getColumnDimension('AL')->setWidth(9);
$sheet->getColumnDimension('AM')->setWidth(6.8);
$sheet->getColumnDimension('AN')->setWidth(7.1);
$sheet->getColumnDimension('AO')->setWidth(12.5);

$line = 0;
/*$buh == 1 ? $que = 'Tabel_site_deti' : $que = 'Tabel_for_works_site';

if ($buh == 1){
    
}*/

for($y=1;$y<=count($array_gruppa);$y++){

    $ped = Yii::$app->db->createCommand("select rtrim(`name`) as `name` from sotrudniki where id_gruppa = :group and dol like '%Воспитатель%' and outS is null",['group' => $y])->queryOne();

    $query_years_of_deti = "select distinct(year(`rozd`)) as `year` from deti
where id_gruppa = {$y} and (({$month}>=month(`in`) and {$year}>=year(`in`)) or {$year}>year(`in`))
and ((({$month}<=month(`out`) and {$year}<=year(`out`)) or {$year} < year(`out`)) or `out` is null)";
    $array_years_of_deti = Yii::$app->db->createCommand($query_years_of_deti)->queryAll();
    $array_years_of_deti = ArrayHelper::getColumn($array_years_of_deti,'year');

    $first_v = date('Y') - ($array_years_of_deti[0]);
    if (count($array_years_of_deti)>1){
        $last_v = date('Y') - (array_pop($array_years_of_deti));
        if ($last_v<5&&$first_v<5){
            $vozrast = $last_v.'-'.$first_v.' года';
        }else{
            $vozrast = $last_v.'-'.$first_v.' лет';
        }
    }else{
        if ($first_v<5){
            $vozrast = $first_v.' года';
        }else{
            $vozrast = $first_v.' лет';
        }
    }

    $model = $qu->show_tabel_deti($year,$month,$y);

    /*$dataProvider = new \yii\data\SqlDataProvider([
        'sql' => 'SET NOCOUNT ON; EXEC '.$que.' @year=:year, @mon=:mon, @idgr=:idgr',//
        'params' => [
            ':year' => $year,
            ':mon'  => $month,
            ':idgr'  => $y
        ],
        'pagination' => false,
        'sort' => false,
    ]);
    $model = $dataProvider->getModels();*/






/*$i = 1;
while($i<=3){
    $sheet->getRowDimension($i)->setRowHeight($height_row_up_3);
    $i++;
}*/
/*while($i<=9){
    $sheet->getRowDimension($i)->setRowHeight($height_row_up);
    $i++;
}*/


$line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'Т А Б Е Л Ь');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);

$line++;//2
    $sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'УЧЕТА ПОСЕЩАЕМОСТИ ДЕТЕЙ');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);
$sheet->setCellValue("AO{$line}", 'КОДЫ')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//3
    $sheet->getRowDimension($line)->setRowHeight($height_row_up_3);
$sheet->setCellValue("F{$line}", 'за '.$_monthsList[$month].' '.$year.' г.');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(9.5);
$sheet->setCellValue("AL{$line}", 'Форма по ОКУД  ')->mergeCells("AL{$line}:AN{$line}");
$sheet->getStyle("AL{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", '504608')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//4
    $sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Учреждение');
$sheet->setCellValue("D{$line}", 'муниципальное казенное дошкольное образовательное учреждение "Детский сад № '.$number_sad.'" города '.$town);
$sheet->mergeCells("D{$line}:AM{$line}");
$sheet->getStyle("D{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AN{$line}", 'Дата  ');
$sheet->getStyle("AN{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", date('d.m.Y',strtotime($last_work_day)))->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);;

$line++;//5
    $sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Структурное подразделение');
$sheet->mergeCells("C{$line}:D{$line}");
$sheet->setCellValue("E{$line}", 'Группа '.$y.' возраст '.$vozrast);
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AN{$line}", 'по ОКПО  ');
$sheet->getStyle("AN{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", '13691336')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//6
    $sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Вид расчета');
$sheet->setCellValue("E{$line}", $count_works_days);//кол-во раб. дней
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AO{$line}", '')->getStyle("AO{$line}")->applyFromArray($style_border_all);

$line++;//7
    $sheet->getRowDimension($line)->setRowHeight($height_row_up);
$sheet->setCellValue("C{$line}", 'Режим работы');
$sheet->setCellValue("E{$line}", '12 час');//кол-во раб. дней
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AO{$line}", '')->getStyle("AO{$line}")->applyFromArray($style_border_all);
$line++;$sheet->getRowDimension($line)->setRowHeight($height_row_up);
$line++;$sheet->getRowDimension($line)->setRowHeight($height_row_up);

$line++;//10
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
$line3 = $line+2;
$sheet->setCellValue("B{$line}", "№№\nп/п");
$sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("B{$line}:B{$line3}");
$sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
$sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);

$sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
$sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("C{$line}:C{$line3}");

$sheet->setCellValue("D{$line}", "Номер\nсчета");
$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("D{$line}:D{$line3}");

$sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
$sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("E{$line}:E{$line3}");

$sheet->setCellValue("F{$line}", "Дни посещения");
$sheet->mergeCells("F{$line}:AJ{$line}");

$sheet->setCellValue("AK{$line}", "Номер\nсчета");
$sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AK{$line}:AK{$line3}");

$sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
$sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AL{$line}:AL{$line3}");

$sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AM{$line}:AN{$line}");

$sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
$sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AO{$line}:AO{$line3}");

$line++;//11
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
$line2=$line+1;//12
$sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
$sheet->setCellValue("F{$line}", "1");
$sheet->mergeCells("F{$line}:F{$line2}");

$sheet->setCellValue("G{$line}", "2");
$sheet->mergeCells("G{$line}:G{$line2}");

$sheet->setCellValue("H{$line}", "3");
$sheet->mergeCells("H{$line}:H{$line2}");

$sheet->setCellValue("I{$line}", "4");
$sheet->mergeCells("I{$line}:I{$line2}");

$sheet->setCellValue("J{$line}", "5");
$sheet->mergeCells("J{$line}:J{$line2}");

$sheet->setCellValue("K{$line}", "6");
$sheet->mergeCells("K{$line}:K{$line2}");

$sheet->setCellValue("L{$line}", "7");
$sheet->mergeCells("L{$line}:L{$line2}");

$sheet->setCellValue("M{$line}", "8");
$sheet->mergeCells("M{$line}:M{$line2}");

$sheet->setCellValue("N{$line}", "9");
$sheet->mergeCells("N{$line}:N{$line2}");

$sheet->setCellValue("O{$line}", "10");
$sheet->mergeCells("O{$line}:O{$line2}");

$sheet->setCellValue("P{$line}", "11");
$sheet->mergeCells("P{$line}:P{$line2}");

$sheet->setCellValue("Q{$line}", "12");
$sheet->mergeCells("Q{$line}:Q{$line2}");

$sheet->setCellValue("R{$line}", "13");
$sheet->mergeCells("R{$line}:R{$line2}");

$sheet->setCellValue("S{$line}", "14");
$sheet->mergeCells("S{$line}:S{$line2}");

$sheet->setCellValue("T{$line}", "15");
$sheet->mergeCells("T{$line}:T{$line2}");

$sheet->setCellValue("U{$line}", "16");
$sheet->mergeCells("U{$line}:U{$line2}");

$sheet->setCellValue("V{$line}", "17");
$sheet->mergeCells("V{$line}:V{$line2}");

$sheet->setCellValue("W{$line}", "18");
$sheet->mergeCells("W{$line}:W{$line2}");

$sheet->setCellValue("X{$line}", "19");
$sheet->mergeCells("X{$line}:X{$line2}");

$sheet->setCellValue("Y{$line}", "20");
$sheet->mergeCells("Y{$line}:Y{$line2}");

$sheet->setCellValue("Z{$line}", "21");
$sheet->mergeCells("Z{$line}:Z{$line2}");

$sheet->setCellValue("AA{$line}", "22");
$sheet->mergeCells("AA{$line}:AA{$line2}");

$sheet->setCellValue("AB{$line}", "23");
$sheet->mergeCells("AB{$line}:AB{$line2}");

$sheet->setCellValue("AC{$line}", "24");
$sheet->mergeCells("AC{$line}:AC{$line2}");

$sheet->setCellValue("AD{$line}", "25");
$sheet->mergeCells("AD{$line}:AD{$line2}");

$sheet->setCellValue("AE{$line}", "26");
$sheet->mergeCells("AE{$line}:AE{$line2}");

$sheet->setCellValue("AF{$line}", "27");
$sheet->mergeCells("AF{$line}:AF{$line2}");

$sheet->setCellValue("AG{$line}", "28");
$sheet->mergeCells("AG{$line}:AG{$line2}");

$sheet->setCellValue("AH{$line}", "29");
$sheet->mergeCells("AH{$line}:AH{$line2}");

$sheet->setCellValue("AI{$line}", "30");
$sheet->mergeCells("AI{$line}:AI{$line2}");

$sheet->setCellValue("AJ{$line}", "31");
$sheet->mergeCells("AJ{$line}:AJ{$line2}");

$sheet->setCellValue("AM{$line}", "всего");
$sheet->mergeCells("AM{$line}:AM{$line2}");

$sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
$sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AN{$line}:AN{$line2}");


/*****************************************************************/



//$arr_last_row = array_pop($model);//последняя итоговая строка вырезана из массива в переменную $arr_last_row

/*foreach($model as $o){
    array_push($model,$o);
}
foreach($model as $o){
    array_push($model,$o);
}*/


//$model = array_slice($model, 0, 49);
//$m15 = array_slice($model, 0, 15);
//$m16 = array_slice($model, 15, 20);
//$m37 = array_slice($model, 35);
//$m15 = array_slice($model, 0, 15);
//$m16 = array();

if (count($model)>15){
    $m15 = array_slice($model, 0, 15);
}else{
    $m15 = $model;
    $m16 = array();
    $m37 = array();
}

if (count($model)>35){
    $m16 = array_slice($model, 15, 20);
    $m37 = array_slice($model, 35);
}else{
    $m16 = array_slice($model, 15);
    $m37 = array();
}

    $t = 1;
    while ($t <= 31){
        ${'sum'.$t} = 0;
        $t++;
    }

    $prishli = 0;
    $propuski = 0;
    $prochee = 0;
    $bolez = 0;

    $count_children = count($model);
    $line++;//12
    $iter_number = 1;
    foreach($m15 as $abc){

        if ($buh == 1){
            $t = 1;
            while ($t <= 31){
                switch($abc[''.$t.'']){
                    case 0:
                    case 3:
                        ${'val'.$t} = '';
                        ${'sum'.$t}++;
                        break;
                    case 1:
                    case 2:
                        ${'val'.$t} = 'H';
                        break;
                    case 5:
                        ${'val'.$t} = 'B';
                        break;
                    case 7:
                        ${'val'.$t} = 'X';
                        break;
                    default:
                        ${'val'.$t} = '';
                        break;
                }
                $t++;
            }
        }else{
            $t = 1;
            while ($t <= 31){
                switch($abc[''.$t.'']){
                    case 0:
                        ${'val'.$t} = '';
                        ${'sum'.$t}++;
                        break;
                    case 1:
                        ${'val'.$t} = 'H';
                        break;
                    case 2:
                        ${'val'.$t} = 'Б';
                        break;
                    case 3:
                        ${'val'.$t} = '–';
                        ${'sum'.$t}++;
                        break;
                    case 5:
                        ${'val'.$t} = 'B';
                        break;
                    case 7:
                        ${'val'.$t} = 'X';
                        break;
                    default:
                        ${'val'.$t} = '';
                        break;
                }
                $t++;
            }
        }

        

        $prishli += $abc['пришли'];
        $propuski += $abc['пропуски'];
        $prochee += $abc['prochee'];
        $bolez += $abc['bolez'];
        
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

        $sheet->setCellValue("B{$line}", $iter_number);
        $iter_number++;
//    $sheet->setCellValue("B{$line}", trim($abc['id5']));
        $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

        if (mb_strlen(trim($abc['name']))>24){
            $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
            $sheet->setCellValue("C{$line}", $rrr);
        }else{
            $sheet->setCellValue("C{$line}", trim($abc['name']));
        }

        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

        $sheet->setCellValue("D{$line}", trim($abc['number_schet']));
        $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

        $sheet->setCellValue("E{$line}", '');
        $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

        $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

        $sheet->setCellValue("F{$line}", $val1);
        $sheet->setCellValue("G{$line}", $val2);
        $sheet->setCellValue("H{$line}", $val3);
        $sheet->setCellValue("I{$line}", $val4);
        $sheet->setCellValue("J{$line}", $val5);
        $sheet->setCellValue("K{$line}", $val6);
        $sheet->setCellValue("L{$line}", $val7);
        $sheet->setCellValue("M{$line}", $val8);
        $sheet->setCellValue("N{$line}", $val9);
        $sheet->setCellValue("O{$line}", $val10);
        $sheet->setCellValue("P{$line}", $val11);
        $sheet->setCellValue("Q{$line}", $val12);
        $sheet->setCellValue("R{$line}", $val13);
        $sheet->setCellValue("S{$line}", $val14);
        $sheet->setCellValue("T{$line}", $val15);
        $sheet->setCellValue("U{$line}", $val16);
        $sheet->setCellValue("V{$line}", $val17);
        $sheet->setCellValue("W{$line}", $val18);
        $sheet->setCellValue("X{$line}", $val19);
        $sheet->setCellValue("Y{$line}", $val20);
        $sheet->setCellValue("Z{$line}", $val21);
        $sheet->setCellValue("AA{$line}", $val22);
        $sheet->setCellValue("AB{$line}", $val23);
        $sheet->setCellValue("AC{$line}", $val24);
        $sheet->setCellValue("AD{$line}", $val25);
        $sheet->setCellValue("AE{$line}", $val26);
        $sheet->setCellValue("AF{$line}", $val27);
        $sheet->setCellValue("AG{$line}", $val28);
        $sheet->setCellValue("AH{$line}", $val29);
        $sheet->setCellValue("AI{$line}", $val30);
        $sheet->setCellValue("AJ{$line}", $val31);
        $sheet->setCellValue("AK{$line}", trim($abc['number_schet']));
        $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
        $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
        $sheet->setCellValue("AN{$line}", '');
        $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
    }

    if (count($model)>15){

        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");
        $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");
        $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
        $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$line}:E{$line3}");
        $sheet->setCellValue("F{$line}", "Дни посещения");
        $sheet->mergeCells("F{$line}:AJ{$line}");
        $sheet->setCellValue("AK{$line}", "Номер\nсчета");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");
        $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
        $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AL{$line}:AL{$line3}");
        $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AM{$line}:AN{$line}");
        $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
        $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AO{$line}:AO{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
        $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
        $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
        $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
        $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AN{$line}:AN{$line2}");

        $line++;
//    $iter_number = 1;
        foreach($m16 as $abc){

            if ($buh == 1){
                $t = 1;
                while ($t <= 31){
                    switch($abc[''.$t.'']){
                        case 0:
                        case 3:
                            ${'val'.$t} = '';
                            ${'sum'.$t}++;
                            break;
                        case 1:
                        case 2:
                            ${'val'.$t} = 'H';
                            break;
                        case 5:
                            ${'val'.$t} = 'B';
                            break;
                        case 7:
                            ${'val'.$t} = 'X';
                            break;
                        default:
                            ${'val'.$t} = '';
                            break;
                    }
                    $t++;
                }
            }else{
                $t = 1;
                while ($t <= 31){
                    switch($abc[''.$t.'']){
                        case 0:
                            ${'val'.$t} = '';
                            ${'sum'.$t}++;
                            break;
                        case 1:
                            ${'val'.$t} = 'H';
                            break;
                        case 2:
                            ${'val'.$t} = 'Б';
                            break;
                        case 3:
                            ${'val'.$t} = '–';
                            ${'sum'.$t}++;
                            break;
                        case 5:
                            ${'val'.$t} = 'B';
                            break;
                        case 7:
                            ${'val'.$t} = 'X';
                            break;
                        default:
                            ${'val'.$t} = '';
                            break;
                    }
                    $t++;
                }
            }

            $prishli += $abc['пришли'];
            $propuski += $abc['пропуски'];
            $prochee += $abc['prochee'];
            $bolez += $abc['bolez'];
            
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

            $sheet->setCellValue("B{$line}", $iter_number);
            $iter_number++;
//    $sheet->setCellValue("B{$line}", trim($abc['id5']));
            $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

            if (mb_strlen(trim($abc['name']))>24){
                $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
                $sheet->setCellValue("C{$line}", $rrr);
            }else{
                $sheet->setCellValue("C{$line}", trim($abc['name']));
            }

            $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
            $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

            $sheet->setCellValue("D{$line}", trim($abc['number_schet']));
            $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

            $sheet->setCellValue("E{$line}", '');
            $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

            $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

            $sheet->setCellValue("F{$line}", $val1);
            $sheet->setCellValue("G{$line}", $val2);
            $sheet->setCellValue("H{$line}", $val3);
            $sheet->setCellValue("I{$line}", $val4);
            $sheet->setCellValue("J{$line}", $val5);
            $sheet->setCellValue("K{$line}", $val6);
            $sheet->setCellValue("L{$line}", $val7);
            $sheet->setCellValue("M{$line}", $val8);
            $sheet->setCellValue("N{$line}", $val9);
            $sheet->setCellValue("O{$line}", $val10);
            $sheet->setCellValue("P{$line}", $val11);
            $sheet->setCellValue("Q{$line}", $val12);
            $sheet->setCellValue("R{$line}", $val13);
            $sheet->setCellValue("S{$line}", $val14);
            $sheet->setCellValue("T{$line}", $val15);
            $sheet->setCellValue("U{$line}", $val16);
            $sheet->setCellValue("V{$line}", $val17);
            $sheet->setCellValue("W{$line}", $val18);
            $sheet->setCellValue("X{$line}", $val19);
            $sheet->setCellValue("Y{$line}", $val20);
            $sheet->setCellValue("Z{$line}", $val21);
            $sheet->setCellValue("AA{$line}", $val22);
            $sheet->setCellValue("AB{$line}", $val23);
            $sheet->setCellValue("AC{$line}", $val24);
            $sheet->setCellValue("AD{$line}", $val25);
            $sheet->setCellValue("AE{$line}", $val26);
            $sheet->setCellValue("AF{$line}", $val27);
            $sheet->setCellValue("AG{$line}", $val28);
            $sheet->setCellValue("AH{$line}", $val29);
            $sheet->setCellValue("AI{$line}", $val30);
            $sheet->setCellValue("AJ{$line}", $val31);
            $sheet->setCellValue("AK{$line}", trim($abc['number_schet']));
            $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
            $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
            $sheet->setCellValue("AN{$line}", '');
            $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
        }

        if (count($m16)==20)$sheet->getPageMargins()->setBottom(0);

    }

//следующие после 36

    if (count($model)>35){

        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");
        $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");
        $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
        $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$line}:E{$line3}");
        $sheet->setCellValue("F{$line}", "Дни посещения");
        $sheet->mergeCells("F{$line}:AJ{$line}");
        $sheet->setCellValue("AK{$line}", "Номер\nсчета");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");
        $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
        $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AL{$line}:AL{$line3}");
        $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AM{$line}:AN{$line}");
        $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
        $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AO{$line}:AO{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
        $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
        $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
        $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
        $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AN{$line}:AN{$line2}");

        $line++;
//    $iter_number = 1;
        foreach($m37 as $abc){

            if ($buh == 1){
                $t = 1;
                while ($t <= 31){
                    switch($abc[''.$t.'']){
                        case 0:
                        case 3:
                            ${'val'.$t} = '';
                            ${'sum'.$t}++;
                            break;
                        case 1:
                        case 2:
                            ${'val'.$t} = 'H';
                            break;
                        case 5:
                            ${'val'.$t} = 'B';
                            break;
                        case 7:
                            ${'val'.$t} = 'X';
                            break;
                        default:
                            ${'val'.$t} = '';
                            break;
                    }
                    $t++;
                }
            }else{
                $t = 1;
                while ($t <= 31){
                    switch($abc[''.$t.'']){
                        case 0:
                            ${'val'.$t} = '';
                            ${'sum'.$t}++;
                            break;
                        case 1:
                            ${'val'.$t} = 'H';
                            break;
                        case 2:
                            ${'val'.$t} = 'Б';
                            break;
                        case 3:
                            ${'val'.$t} = '–';
                            ${'sum'.$t}++;
                            break;
                        case 5:
                            ${'val'.$t} = 'B';
                            break;
                        case 7:
                            ${'val'.$t} = 'X';
                            break;
                        default:
                            ${'val'.$t} = '';
                            break;
                    }
                    $t++;
                }
            }



            $prishli += $abc['пришли'];
            $propuski += $abc['пропуски'];
            $prochee += $abc['prochee'];
            $bolez += $abc['bolez'];
            
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

            $sheet->setCellValue("B{$line}", $iter_number);
            $iter_number++;
//    $sheet->setCellValue("B{$line}", trim($abc['id5']));
            $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

            if (mb_strlen(trim($abc['name']))>24){
                $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
                $sheet->setCellValue("C{$line}", $rrr);
            }else{
                $sheet->setCellValue("C{$line}", trim($abc['name']));
            }

            $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
            $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

            $sheet->setCellValue("D{$line}", trim($abc['number_schet']));
            $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

            $sheet->setCellValue("E{$line}", '');
            $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

            $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

            $sheet->setCellValue("F{$line}", $val1);
            $sheet->setCellValue("G{$line}", $val2);
            $sheet->setCellValue("H{$line}", $val3);
            $sheet->setCellValue("I{$line}", $val4);
            $sheet->setCellValue("J{$line}", $val5);
            $sheet->setCellValue("K{$line}", $val6);
            $sheet->setCellValue("L{$line}", $val7);
            $sheet->setCellValue("M{$line}", $val8);
            $sheet->setCellValue("N{$line}", $val9);
            $sheet->setCellValue("O{$line}", $val10);
            $sheet->setCellValue("P{$line}", $val11);
            $sheet->setCellValue("Q{$line}", $val12);
            $sheet->setCellValue("R{$line}", $val13);
            $sheet->setCellValue("S{$line}", $val14);
            $sheet->setCellValue("T{$line}", $val15);
            $sheet->setCellValue("U{$line}", $val16);
            $sheet->setCellValue("V{$line}", $val17);
            $sheet->setCellValue("W{$line}", $val18);
            $sheet->setCellValue("X{$line}", $val19);
            $sheet->setCellValue("Y{$line}", $val20);
            $sheet->setCellValue("Z{$line}", $val21);
            $sheet->setCellValue("AA{$line}", $val22);
            $sheet->setCellValue("AB{$line}", $val23);
            $sheet->setCellValue("AC{$line}", $val24);
            $sheet->setCellValue("AD{$line}", $val25);
            $sheet->setCellValue("AE{$line}", $val26);
            $sheet->setCellValue("AF{$line}", $val27);
            $sheet->setCellValue("AG{$line}", $val28);
            $sheet->setCellValue("AH{$line}", $val29);
            $sheet->setCellValue("AI{$line}", $val30);
            $sheet->setCellValue("AJ{$line}", $val31);
            $sheet->setCellValue("AK{$line}", trim($abc['number_schet']));
            $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
            $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
            $sheet->setCellValue("AN{$line}", '');
            $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
        }
    }

    if ((count($m15)==15 && count($m16)==0) || count($model)==35){
        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");
        $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");
        $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
        $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$line}:E{$line3}");
        $sheet->setCellValue("F{$line}", "Дни посещения");
        $sheet->mergeCells("F{$line}:AJ{$line}");
        $sheet->setCellValue("AK{$line}", "Номер\nсчета");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");
        $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
        $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AL{$line}:AL{$line3}");
        $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AM{$line}:AN{$line}");
        $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
        $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AO{$line}:AO{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
        $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
        $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
        $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
        $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AN{$line}:AN{$line2}");
        $line++;
        for($i=1;$i<=5;$i++){
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
        }
    }

    
if ((count($m15)==14 && count($m16)==0) || count($model)==34){
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");
    $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");
    $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
    $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("E{$line}:E{$line3}");
    $sheet->setCellValue("F{$line}", "Дни посещения");
    $sheet->mergeCells("F{$line}:AJ{$line}");
    $sheet->setCellValue("AK{$line}", "Номер\nсчета");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");
    $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
    $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AL{$line}:AL{$line3}");
    $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AM{$line}:AN{$line}");
    $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
    $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AO{$line}:AO{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
    $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
    $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
    $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
    $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AN{$line}:AN{$line2}");
    $line++;
    for($i=1;$i<=4;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
    }
}

    
if ((count($m15)==13 && count($m16)==0) || count($model)==33){
    for($i=1;$i<=2;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
    }

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    $line++;//10
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
    $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("C{$line}:C{$line3}");
    $sheet->setCellValue("D{$line}", "Номер\nсчета");
    $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");
    $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
    $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("E{$line}:E{$line3}");
    $sheet->setCellValue("F{$line}", "Дни посещения");
    $sheet->mergeCells("F{$line}:AJ{$line}");
    $sheet->setCellValue("AK{$line}", "Номер\nсчета");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");
    $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
    $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AL{$line}:AL{$line3}");
    $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AM{$line}:AN{$line}");
    $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
    $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AO{$line}:AO{$line3}");
    $line++;//11
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
    $line2=$line+1;//12
    $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
    $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
    $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
    $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
    $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
    $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AN{$line}:AN{$line2}");
    $line++;
    for($i=1;$i<=3;$i++){
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
    }
}

    if ((count($m15)==12 && count($m16)==0) || count($model)==32){
        for($i=1;$i<=3;$i++){
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
        }

        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");
        $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");
        $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
        $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$line}:E{$line3}");
        $sheet->setCellValue("F{$line}", "Дни посещения");
        $sheet->mergeCells("F{$line}:AJ{$line}");
        $sheet->setCellValue("AK{$line}", "Номер\nсчета");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");
        $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
        $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AL{$line}:AL{$line3}");
        $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AM{$line}:AN{$line}");
        $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
        $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AO{$line}:AO{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
        $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
        $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
        $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
        $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AN{$line}:AN{$line2}");
        $line++;

        for($i=1;$i<=2;$i++){
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
        }
    }

    if ((count($m15)==11 && count($m16)==0) || count($model)==31){
        for($i=1;$i<=4;$i++){
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
        }

        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
        $line++;//10
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
        $line3 = $line+2;
        $sheet->setCellValue("B{$line}", "№№\nп/п");
        $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$line}:B{$line3}");
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
        $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);
        $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$line}:C{$line3}");
        $sheet->setCellValue("D{$line}", "Номер\nсчета");
        $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("D{$line}:D{$line3}");
        $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
        $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$line}:E{$line3}");
        $sheet->setCellValue("F{$line}", "Дни посещения");
        $sheet->mergeCells("F{$line}:AJ{$line}");
        $sheet->setCellValue("AK{$line}", "Номер\nсчета");
        $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AK{$line}:AK{$line3}");
        $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
        $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AL{$line}:AL{$line3}");
        $sheet->setCellValue("AM{$line}", "Пропущено\nдней")->getStyle("AM{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AM{$line}:AN{$line}");
        $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
        $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AO{$line}:AO{$line3}");
        $line++;//11
        $sheet->getRowDimension($line)->setRowHeight($height_row_shapka2);
        $line2=$line+1;//12
        $sheet->getRowDimension($line2)->setRowHeight($height_row_shapka3);
        $sheet->setCellValue("F{$line}", "1");$sheet->mergeCells("F{$line}:F{$line2}");
        $sheet->setCellValue("G{$line}", "2");$sheet->mergeCells("G{$line}:G{$line2}");
        $sheet->setCellValue("H{$line}", "3");$sheet->mergeCells("H{$line}:H{$line2}");
        $sheet->setCellValue("I{$line}", "4");$sheet->mergeCells("I{$line}:I{$line2}");
        $sheet->setCellValue("J{$line}", "5");$sheet->mergeCells("J{$line}:J{$line2}");
        $sheet->setCellValue("K{$line}", "6");$sheet->mergeCells("K{$line}:K{$line2}");
        $sheet->setCellValue("L{$line}", "7");$sheet->mergeCells("L{$line}:L{$line2}");
        $sheet->setCellValue("M{$line}", "8");$sheet->mergeCells("M{$line}:M{$line2}");
        $sheet->setCellValue("N{$line}", "9");$sheet->mergeCells("N{$line}:N{$line2}");
        $sheet->setCellValue("O{$line}", "10");$sheet->mergeCells("O{$line}:O{$line2}");
        $sheet->setCellValue("P{$line}", "11");$sheet->mergeCells("P{$line}:P{$line2}");
        $sheet->setCellValue("Q{$line}", "12");$sheet->mergeCells("Q{$line}:Q{$line2}");
        $sheet->setCellValue("R{$line}", "13");$sheet->mergeCells("R{$line}:R{$line2}");
        $sheet->setCellValue("S{$line}", "14");$sheet->mergeCells("S{$line}:S{$line2}");
        $sheet->setCellValue("T{$line}", "15");$sheet->mergeCells("T{$line}:T{$line2}");
        $sheet->setCellValue("U{$line}", "16");$sheet->mergeCells("U{$line}:U{$line2}");
        $sheet->setCellValue("V{$line}", "17");$sheet->mergeCells("V{$line}:V{$line2}");
        $sheet->setCellValue("W{$line}", "18");$sheet->mergeCells("W{$line}:W{$line2}");
        $sheet->setCellValue("X{$line}", "19");$sheet->mergeCells("X{$line}:X{$line2}");
        $sheet->setCellValue("Y{$line}", "20");$sheet->mergeCells("Y{$line}:Y{$line2}");
        $sheet->setCellValue("Z{$line}", "21");$sheet->mergeCells("Z{$line}:Z{$line2}");
        $sheet->setCellValue("AA{$line}", "22");$sheet->mergeCells("AA{$line}:AA{$line2}");
        $sheet->setCellValue("AB{$line}", "23");$sheet->mergeCells("AB{$line}:AB{$line2}");
        $sheet->setCellValue("AC{$line}", "24");$sheet->mergeCells("AC{$line}:AC{$line2}");
        $sheet->setCellValue("AD{$line}", "25");$sheet->mergeCells("AD{$line}:AD{$line2}");
        $sheet->setCellValue("AE{$line}", "26");$sheet->mergeCells("AE{$line}:AE{$line2}");
        $sheet->setCellValue("AF{$line}", "27");$sheet->mergeCells("AF{$line}:AF{$line2}");
        $sheet->setCellValue("AG{$line}", "28");$sheet->mergeCells("AG{$line}:AG{$line2}");
        $sheet->setCellValue("AH{$line}", "29");$sheet->mergeCells("AH{$line}:AH{$line2}");
        $sheet->setCellValue("AI{$line}", "30");$sheet->mergeCells("AI{$line}:AI{$line2}");
        $sheet->setCellValue("AJ{$line}", "31");$sheet->mergeCells("AJ{$line}:AJ{$line2}");
        $sheet->setCellValue("AM{$line}", "всего");$sheet->mergeCells("AM{$line}:AM{$line2}");
        $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
        $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
        $sheet->mergeCells("AN{$line}:AN{$line2}");
        $line++;
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
    }

    if((count($m16)>0 && count($model)<=31) || count($model)>35){
        for($i=1;$i<=5;$i++){
            $line++;
            $sheet->getRowDimension($line)->setRowHeight($height_row_st);
            $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);
        }
    }

/*set itogovaya row*/
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("C{$line}", 'Всего присутствует детей '.$count_children);
//$sheet->mergeCells("B{$line}:D{$line}");
    $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);
    $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($style_border_all);
    $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

    $sheet->setCellValue("F{$line}", $sum1);
    $sheet->setCellValue("G{$line}", $sum2);
    $sheet->setCellValue("H{$line}", $sum3);
    $sheet->setCellValue("I{$line}", $sum4);
    $sheet->setCellValue("J{$line}", $sum5);
    $sheet->setCellValue("K{$line}", $sum6);
    $sheet->setCellValue("L{$line}", $sum7);
    $sheet->setCellValue("M{$line}", $sum8);
    $sheet->setCellValue("N{$line}", $sum9);
    $sheet->setCellValue("O{$line}", $sum10);
    $sheet->setCellValue("P{$line}", $sum11);
    $sheet->setCellValue("Q{$line}", $sum12);
    $sheet->setCellValue("R{$line}", $sum13);
    $sheet->setCellValue("S{$line}", $sum14);
    $sheet->setCellValue("T{$line}", $sum15);
    $sheet->setCellValue("U{$line}", $sum16);
    $sheet->setCellValue("V{$line}", $sum17);
    $sheet->setCellValue("W{$line}", $sum18);
    $sheet->setCellValue("X{$line}", $sum19);
    $sheet->setCellValue("Y{$line}", $sum20);
    $sheet->setCellValue("Z{$line}", $sum21);
    $sheet->setCellValue("AA{$line}", $sum22);
    $sheet->setCellValue("AB{$line}", $sum23);
    $sheet->setCellValue("AC{$line}", $sum24);
    $sheet->setCellValue("AD{$line}", $sum25);
    $sheet->setCellValue("AE{$line}", $sum26);
    $sheet->setCellValue("AF{$line}", $sum27);
    $sheet->setCellValue("AG{$line}", $sum28);
    $sheet->setCellValue("AH{$line}", $sum29);
    $sheet->setCellValue("AI{$line}", $sum30);
    $sheet->setCellValue("AJ{$line}", $sum31);
    $sheet->setCellValue("AK{$line}", '');
    $sheet->setCellValue("AL{$line}", $prishli);
    $sheet->setCellValue("AM{$line}", $propuski);
    $sheet->setCellValue("AN{$line}", '');
    $sheet->setCellValue("AO{$line}", $prochee.' пр / '.$bolez.' б');

    if(count($model)==10 || (count($model)>26 && count($model)<=31) || (count($model)>=35 && count($model)<=49)){
        $sheet->getPageMargins()->setBottom(0);
        $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
    }

/*нижняя шапка*/
    $line++;
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("C{$line}", 'Руководитель учреждения');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
    $sheet->setCellValue("O{$line}", $boss)->getStyle("O{$line}")->applyFromArray($allign_up_center);
    $sheet->mergeCells("O{$line}:Y{$line}");
    $sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("E{$line}", '(подпись)');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
    $sheet->setCellValue("O{$line}", '(расшифровка подписи)');
    $sheet->mergeCells("O{$line}:Y{$line}");
    $sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("C{$line}", 'Воспитатель');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
    $sheet->mergeCells("O{$line}:Y{$line}");

    $ped = $ped['name'];
    if (strlen($ped)>0){
        $m = explode(' ', $ped);
        $ped = substr($m[1],0,2) . '.' . substr($m[2],0,2) . '.' . ' ' . $m[0];
    }else{
        $ped = '';
    }

    $sheet->setCellValue("O{$line}", $ped)->getStyle("O{$line}")->applyFromArray($allign_up_center);
    $sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

    $sheet->getStyle("AD{$line}")->applyFromArray($allign_center_all);
    $sheet->setCellValue("AD{$line}", date('j',strtotime($last_work_day)).' '.$MonthNamesRus[$mon].' '.$year.' года');//*******************************************************
//$sheet->setCellValue("AD{$line}", rdate('j M Y').' года');
    $sheet->mergeCells("AD{$line}:AK{$line}");
    $sheet->getStyle("AD{$line}:AK{$line}")->applyFromArray($style_border_bottom);

    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("E{$line}", '(подпись)');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
    $sheet->setCellValue("O{$line}", '(расшифровка подписи)');
    $sheet->mergeCells("O{$line}:Y{$line}");
    $sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);

    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("C{$line}", 'Мед.сестра');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
    $sheet->mergeCells("O{$line}:Y{$line}");
    $sheet->setCellValue("O{$line}", $medsestra)->getStyle("O{$line}")->applyFromArray($allign_up_center);
    $sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
    $sheet->setCellValue("E{$line}", '(подпись)');
    $sheet->mergeCells("E{$line}:K{$line}");
    $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
    $sheet->setCellValue("O{$line}", '(расшифровка подписи)');
    $sheet->mergeCells("O{$line}:Y{$line}");
    $sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);

    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
}

if(count($model)==10 || (count($model)==26 && count($model)<=31) || (count($model)>=35 && count($model)<=49)){
    $sheet->getPageMargins()->setBottom(0);
//    $sheet->setBreak("A{$line}",PHPExcel_Worksheet::BREAK_ROW);
}

$sheet->getPageSetup()->setPrintArea("A1:AO{$line}");
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->getPageSetup()->setFitToPage(false)->setScale(100);



if ($buh==1){
    $name_file = "Табель ВСЕ ГРУППЫ (".$_monthsList[$month].' '.$year.') '.date("d.m.Y");
}else{
    $name_file = "Табель ВСЕ ГРУППЫ (".$_monthsList[$month].' '.$year.') '.date("d.m.Y")." для своих нужд";
}


//$sheet->setBreak("AP5",PHPExcel_Worksheet::BREAK_COLUMN);

header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=".$name_file.".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
ob_clean();
$objWriter->save('php://output');


exit;


