<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 26.01.20
 * Time: 15:57
 */


//$year = 2020;
//$month = 1;
//$mon = $month;

$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));
$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);
$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
count($model_d_date)==0 ? $model_d_date = array() : null;
count($model_d_antidate)==0 ? $model_d_antidate = array() : null;
$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний календарный день, затем рабочий
while(((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))) && !in_array($last_work_day,$model_d_antidate)){
    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
}
$last_work_day = date('d.m.Y',strtotime($last_work_day));

$_monthsList = array(
    1=>"январь",2=>"февраль",3=>"март",
    4=>"апрель",5=>"май", 6=>"июнь",
    7=>"июль",8=>"август",9=>"сентябрь",
    10=>"октябрь",11=>"ноябрь",12=>"декабрь");

/*$query = "SET NOCOUNT ON; EXEC Spravka27012020 @mon=:mon,@year=:year";
$array = Yii::$app->db->createCommand($query,[
    'year' => $year,
    'mon' => $month,
])->queryAll();*/

$boss = Yii::$app->request->cookies->getValue('boss');
$number_sad = Yii::$app->request->cookies->getValue('number_sad');
$cityr = Yii::$app->request->cookies->getValue('cityr');

strlen($boss)<1 ? $boss = '______________' : null;
strlen($number_sad)<1 ? $number_sad = '___' : null;

$all_propuski = 0;
$propuski_prochee = 0;
$propuski_bolezni = 0;
$detodni = 0;
$all_deti = 0;
$work_days = 0;
$count_group = 0;

if (count($array)<1){
    $array = array();
    $all_propuski = 0;
    $propuski_prochee = 0;
    $propuski_bolezni = 0;
    $detodni = 0;
    $all_deti = 0;
    $work_days = 0;
    $count_group = 0;
}
foreach($array as $q){
    $q['id']==1 ? $all_propuski = $q['znachenie'] : null;
    $q['id']==2 ? $propuski_prochee = $q['znachenie'] : null;
    $q['id']==3 ? $propuski_bolezni = $q['znachenie'] : null;
    $q['id']==4 ? $detodni = $q['znachenie'] : null;
    $q['id']==5 ? $all_deti = $q['znachenie'] : null;
    $q['id']==6 ? $work_days = $q['znachenie'] : null;
    $q['id']==7 ? $count_group = $q['znachenie'] : null;
}

//debug($array);return;

function m2t($millimeters){
    return floor($millimeters*56.7); //1 твип равен 1/567 сантиметра
}

$PHPWord = new \PhpOffice\PhpWord\PhpWord();

$PHPWord->setDefaultFontName('Times New Roman');
$PHPWord->setDefaultFontSize(12);

$sectionStyle = array('orientation' => null,
    'marginLeft' => m2t(25),
    'marginRight' => m2t(0),
    'marginTop' => m2t(19),
    'marginBottom' => m2t(19),
);
$section = $PHPWord->addSection($sectionStyle);

$cellStyle = array(
    'borderTopSize' => 1,
    'borderRightSize' => 1,
    'borderBottomSize' => 1,
    'borderLeftSize' => 1,
    'borderTopColor' => '#000000',
    'borderRightColor' => '#000000',
    'borderBottomColor' => '#000000',
    'borderLeftColor' => '#000000',
);

$textrun = $section->createTextRun(['align' => 'center','spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('СПРАВКА МКДОУ № ');
$textrun->addText($number_sad, ['underline' => 'single']);
$textrun->addText(' г. '.$cityr.'');

$textrun = $section->createTextRun(['align' => 'center','spaceAfter' => 200,'spaceBefore' => 1]);
$textrun->addText('за '.$_monthsList[(Int)$month], ['underline' => 'single']);
$textrun->addText(' месяц '.$year.' года');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Списочный состав:  ', ['bold' => true]);
$textrun->addText($count_group, ['bold' => true,'underline' => 'single']);
$textrun->addText('  групп  ');
$textrun->addText($all_deti, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детей');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('В том числе:');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Ясли: ');
$textrun->addText(' 0 ', ['bold' => true,'underline' => 'single']);
$textrun->addText(' групп ');
$textrun->addText(' 0 ', ['bold' => true,'underline' => 'single']);
$textrun->addText(' детей ');
$textrun->addText(' 0 ', ['bold' => true,'underline' => 'single']);
$textrun->addText(' детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Сад: обычные группы:  ');
$textrun->addText($count_group, ['bold' => true,'underline' => 'single']);
$textrun->addText('  групп  ');
$textrun->addText($all_deti, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детей  ');
$textrun->addText($detodni, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('         Обычные  час  групп  детей  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('         Спец  группы  групп  детей  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Кратковременные (КП-группы)___  групп___  детей___  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Итого по детскому саду  ');
$textrun->addText($count_group, ['bold' => true,'underline' => 'single']);
$textrun->addText('  групп  ');
$textrun->addText($all_deti, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детей  ');
$textrun->addText($detodni, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Пропущено детьми всего  ');
$textrun->addText($all_propuski, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детодней  ');
$textrun->addText(' в т.ч. по болезни  ');
$textrun->addText($propuski_bolezni, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('                                                                                  по другим причинам  ');
$textrun->addText($propuski_prochee, ['bold' => true,'underline' => 'single']);
$textrun->addText('  детодней');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Количество рабочих дней  ');
$textrun->addText($work_days, ['bold' => true,'underline' => 'single']);

$textrun = $section->createTextRun(['spaceAfter' => 200,'spaceBefore' => 1]);
$textrun->addText('Количество дней работы с детьми в д/с  ');
$textrun->addText($work_days, ['bold' => true,'underline' => 'single']);

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Закрытие детского сада или групп (полностью) по причине аварийной ситуации или карантина');

$width_col = 32;

$table = $section->addTable();
$table->addRow(m2t(10));
$table->addCell(m2t($width_col),$cellStyle)->addText('Дней <w:br/>закрытия',null,['align' => 'center','spaceAfter' => 200,'spaceBefore' => 200]);
$table->addCell(m2t($width_col),$cellStyle)->addText('Количество <w:br/>групп',null,['align' => 'center','spaceAfter' => 200,'spaceBefore' => 200]);
$table->addCell(m2t($width_col),$cellStyle)->addText('Количество <w:br/>детей',null,['align' => 'center','spaceAfter' => 200,'spaceBefore' => 200]);
$table->addCell(m2t($width_col),$cellStyle)->addText('Ясли/сад',null,['align' => 'center','spaceAfter' => 250,'spaceBefore' => 250]);
$table->addCell(m2t(45),$cellStyle)->addText('Примечание <w:br/>(дата, № приказа <w:br/>по УО)',null,['align' => 'center','spaceAfter' => 100,'spaceBefore' => 100]);

$table->addRow(m2t(1));
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t(45),$cellStyle)->addText('');
$table->addRow(m2t(1));
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t($width_col),$cellStyle)->addText('');
$table->addCell(m2t(45),$cellStyle)->addText('');

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('Заведующий д/с _________________ /'.$boss);

$textrun = $section->createTextRun(['spaceAfter' => 1,'spaceBefore' => 1]);
$textrun->addText('М.П.                                                                                     дата сдачи справки ');
$textrun->addText($last_work_day, ['underline' => 'single','align' => 'center']);





header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="Справка дети '.$_monthsList[$month].' '.$year.' года.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');

$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($PHPWord, 'Word2007');
ob_clean();
$objWriter->save("php://output");
exit;