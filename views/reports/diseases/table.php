<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
$vstavish = '';

if ($number_reports == 1){
    $vstavish = 'детей в группе';
}else{
    $vstavish = 'сотрудников';
}
?>

<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-striped table-bordered">

    <?php
    $i = 1;

    $t = 1;
    while ($t <= 31){
        ${'sum'.$t} = 0;
        $t++;
    }

    $prishli = 0;
    $propuski = 0;
    $prochee = 0;
    $bolez = 0;

    foreach($model2 as $q){

        $t = 1;
        while ($t <= 31){
            switch($q[''.$t.'']){
                case 0:
                    ${'val'.$t} = '';
                    ${'sum'.$t}++;
                    break;
                case 1:
                    ${'val'.$t} = 'H';
                    break;
                case 2:
                    ${'val'.$t} = '<span style="color: #ff0000">Б</span>';
                    break;
                case 3:
                    ${'val'.$t} = '–';
                    ${'sum'.$t}++;
                    break;
                case 5:
                    ${'val'.$t} = '<span style="color: #adadad">B</span>';
                    break;
                case 7:
                    ${'val'.$t} = '<span style="color: #adadad">X</span>';
                    break;
                default:
                    ${'val'.$t} = '';
                    break;
            }
            $t++;
        }

        $prishli += $q['пришли'];
        $propuski += $q['пропуски'];
        $prochee += $q['prochee'];
        $bolez += $q['bolez'];

        echo '<tr>
                <td style="width: 2%">'.$i.'</td>
                <td id="id_n" style="width: 90px"><div>'.$q['name'].'</div></td>
                <td style="width: 5%">'.$q['number_schet'].'</td>';
        $t = 1;
        while ($t <= 31){
            echo '
                <td class="nnn">'.${'val'.$t}.'</td>';
            $t++;
        }

        if ($number_reports == 1){
            echo ' <td style="width: 5%">'.$q['пришли'].'</td>
                <td style="width: 5%">'.$q['пропуски'].'</td>
                <td style="width: 8%">'.$q['prichiny'].'</td>
            </tr>';
        }else{
            echo ' <td style="width: 5%">'.$q['пришли'].'</td>
                <td style="width: 5%">'.$q['пропуски'].'</td>
                <td style="width: 8%"></td>
            </tr>';
        }

        $i++;
    }

    echo '<tr>
                <td style="width: 2%"></td>
                <td id="id_n" style="width: 90px"><div>Всего '.$vstavish.' '.count($model2).'</div></td>
                <td style="width: 5%"></td>';
    $t = 1;
    while ($t <= 31){
        echo '
                <td class="nnn">'.${'sum'.$t}.'</td>';
        $t++;
    }

    if ($number_reports == 1){
        echo ' <td style="width: 5%">'.$prishli.'</td>
                <td style="width: 5%">'.$propuski.'</td>
                <td style="width: 8%">'.$prochee.' пр / '.$bolez.' б</td>
            </tr>';
    }else{
        echo ' <td style="width: 5%">'.$prishli.'</td>
                <td style="width: 5%">'.$propuski.'</td>
                <td style="width: 8%"></td>
            </tr>';
    }

    ?>

    </table>
</div>
