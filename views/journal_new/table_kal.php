<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);


Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>

<?php

switch($status){
    case 0:

        break;
    case 1:

        $model5 = array();
        foreach($model as $key  => $value){
            if(strpos($value['currentyear1'],'1900-01-01') !== false){
                array_push($model5,$value);
            }
        }
        $model = $model5;
        break;
}

?>


<?php
 Modal::begin([
     'id' => 'modal_journal',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="header_modal_journal"></h4>',
     'size' => Modal::SIZE_DEFAULT,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_mantu">Удалить отметку</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_mantu">Сохранить изменения</button>',
 ]);
?>

<?php $form = ActiveForm::begin(['id' => '#']); ?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_pole_id_j',
])->label(false); ?>
<?= $form->field($model_kal,'id_child')->hiddenInput([
    'id' => 'hidden_id_child',
])->label(false); ?>
<?= $form->field($model_kal,'year')->hiddenInput([
    'id' => 'hidden_year',
])->label(false); ?>
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form_modal_journal',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['journal/journal'],
    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
        'horizontalCssClasses' => [
            'label' => 'col-lg-4',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-7',
//             'error' => '',
//             'hint' => '',
        ],
    ],
]); ?>
<?= $form->field($model_kal, 'name')->textInput([
    'id' => 'name_modal_j',
    'placeholder' => 'Фамилия Имя',
    'readonly'=> true
]) ?>
<?= $form->field($model_kal, 'dat')->widget('yii\widgets\MaskedInput', [
    'options' => [
        'id' => 'dat_modal_j',
        'placeholder' => 'Дата проведения',
    ],
    'mask' => '99.99.9999',
]) ?>
<?= $form->field($model_kal, 'coment')->textarea([
    'id' => 'coment_modal_j',
    'rows' => 3,
    'placeholder' => 'Примечание'
]) ?>
<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>


<div class="my_table my_table2 not_selected_text_on_block" id="table_journal">
    <table class="table-striped table-bordered" id="t_mantu" data-all="<?= $all_deti ?>" data-notall="<?= $not_all_deti ?>">
    <?php
    $i = 1;
//    debug($model);

    foreach($model as $q){

        if (strpos($q['currentyear1'],'1900-01-01') !== false){
            $dat = '';
        }else{
            $dat = Yii::$app->formatter->asDate(trim($q['currentyear1']));
        }
        if (strpos($q['year1'],'1900-01-01') !== false){
            $dat1 = '';
        }else{
            $dat1 = Yii::$app->formatter->asDate(trim($q['year1']));
        }

        strpos($q['rozd'],'1900-01-01') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'</span>';

        echo '<tr '.$classs.' >
                <td class="not_hover_td" style="width: 2%">'.$i.'</td>
                <td class="idstart" id="id_n">'.trim($q['name']).$rozd.'</td>
                <td style="width: 15%" data-y="'.(date('Y')-1).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat1.'" data-coment="'.$q['coment1'].'">'.$dat1.'</td>
                <td style="width: 20%" data-y="'.(date('Y')).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat.'" data-coment="'.$q['coment'].'">'.$dat.'</td>
                <td style="width: 20%" data-y="'.(date('Y')).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat.'" data-coment="'.$q['coment'].'">'.$q['coment'].'</td>
            </tr>';
        $i++;
    }

    ?>

    </table>
</div>

<?php
$script = <<<JS

$(function(){

    $('table td:not(.not_hover_td,.idstart)').on('click',function(){

//        $('#form_modal_journal').trigger('reset');

        var id_child = $(this).data('id_child');
        var y = $(this).data('y');
        var name = $(this).data('name');
        var dat = $(this).data('dat');
        var coment = $(this).data('coment');
        $('#name_modal_j').val(name.trim());
        $('#dat_modal_j').val(dat);
        $('#coment_modal_j').val(coment.trim());
        $('#hidden_id_child').val(id_child);
        $('#hidden_year').val(y);

        $('#header_modal_journal').text('Отметка Гельминты '+y+' г.');

        if(dat.length > 0){
            $('#hidden_pole_id_j').val(8);
            $('#but_del_mantu').show();
            $('#but_save_mantu').text('Сохранить изменения');
        }else{
            $('#hidden_pole_id_j').val(9);
            $('#but_del_mantu').hide();
            $('#but_save_mantu').text('Добавить отметку');
        }

        $('#modal_journal').modal('show');

        return false;
    });


    $('#but_save_mantu').click(function(){

        var iddd = $('#hidden_pole_id_j').val();

        console.log(iddd+' iddd');//return;/////////////////////////*//*********************************************
        if (iddd == 8){//update kal
            $('.modal').modal('hide');
            var form = $('form').serializeArray();
//            form.splice(4,1);
//            form.splice(2,1);
//            form.splice(5,1);
//            form.push({name:'Mantu[ye]',value:infiltrat});
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            console.log(form);//return;//////////////////////////////////////////////////////
            var arr = $('#form_rep_item_journal');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }else if(iddd == 9){//insert kal
            $('.modal').modal('hide');
            form = $('form').serializeArray();
//            console.log(form)
//            form.splice(4,1);
//            form.splice(2,1);
//            form.splice(5,1);
//            form.push({name:'Mantu[ye]',value:infiltrat});
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            console.log(form);//return;
            arr = $('#form_rep_item_journal');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('not');
//                    alert("Ошибка");
                });


        }
        return false;
    });

    $('#but_del_mantu').click(function(){
        $('.modal').modal('hide');

        $('#hidden_pole_id_j').val(10);
        var form = $('form').serializeArray();
//        form.splice(4,1);
//        form.splice(2,1);
        if($('#but_all_deti_mantu').hasClass('active')){
            var id7 = 0;
        }else{
            id7 = 1;
        }
        form.push({name:'id7',value:id7});
        console.log(form);//return;
//        return;

//        console.log(idishka);
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
            });
        return false;
    });


})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

