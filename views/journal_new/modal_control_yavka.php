<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-striped table-bordered">
        <thead>
        <tr>
            <th style="width: 5%">п/п</th>
            <th style="width: 50%">Фамилия Имя</th>
            <th style="width: 15%">№ группы</th>
            <th style="width: 40%">Дата контрольной явки</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $i = 1;
        foreach($array_control_yavka as $q){
            strpos($q['rozd'],'1900-01-01') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'</span>';
            strpos($q['date_to'],'1900-01-01') !== false ? $date_to = '' : $date_to = Yii::$app->formatter->asDate($q['date_to']);
            echo '<tr>
                     <td class="not_hover_td" style="width: 5%">'.$i.'</td>
                     <td style="width: 50%" id="id_n">'.trim($q['name']).$rozd.'</td>
                     <td style="width: 15%">'.$q['id_gruppa'].'</td>
                     <td style="width: 40%">'.$date_to.'</td>
                  </tr>';
            $i++;
        }
        ?>

        </tbody>
</table>
</div>

<?php
$script = <<<JS

$(function(){

    $('#id_but_close_modal_control_yavka').on('click',function(){
        $('.modal').modal('hide');
        return false;
    });
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>