<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);


Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>





<?php

switch($status){
    case 0:
        $status = 0;
        break;
    case 1:
        $status = 1;
        $model5 = array();
        foreach($model as $key  => $value){
            if(strpos($value['dat'],'1900') !== false){
                array_push($model5,$value);
            }
        }
        $model = $model5;
        break;
}


?>
<div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
    <table id="rrr555">
        <tr>
            <td class="not_hover_td" style="width: 2%">№</td>
            <td class="not_hover_td">Фамилия Имя</td>
            <td class="not_hover_td" style="width: 10%"><?= date('Y')-4 ?></td>
            <td class="not_hover_td" style="width: 10%"><?= date('Y')-3 ?></td>
            <td class="not_hover_td" style="width: 10%"><?= date('Y')-2 ?></td>
            <td class="not_hover_td" style="width: 10%"><?= date('Y')-1 ?></td>
            <td class="not_hover_td" style="width: 20%"><?= date('Y') ?></td>
        </tr>
    </table>
</div>

<div class="my_table my_table2 not_selected_text_on_block" id="table_journal" style="overflow-y: scroll;height: 100%">
    <table class="table-striped table-bordered" id="t_mantu" data-all="<?= $all_deti ?>" data-notall="<?= $not_all_deti ?>">
        <?php
        $i = 1;
        foreach($model as $q){

            if (strlen(trim($q['coment4'])) > 0 && strlen(trim($q['year4'])) < 1){
                $content4 = '?';
            }else{
                if (strlen(trim($q['year4_2'])) > 0 && strlen(trim($q['year4_2'])) <= 3){
                    $content4 = $q['year4']."<br/>".$q['year4_2'].' мм'."<br/>".$q['progress4'];
                }else{
                    $content4 = $q['year4'].''.$q['year4_2'];
                }
            }

            if (strlen(trim($q['coment3'])) > 0 && strlen(trim($q['year3'])) < 1){
                $content3 = '?';
            }else{
                if (strlen(trim($q['year3_2'])) > 0 && strlen(trim($q['year3_2'])) <= 3){
                    $content3 = $q['year3']."<br/>".$q['year3_2'].' мм'."<br/>".$q['progress3'];
                }else{
                    $content3 = $q['year3'].''.$q['year3_2'];
                }
            }

            if (strlen(trim($q['coment2'])) > 0 && strlen(trim($q['year2'])) < 1){
                $content2 = '?';
            }else{
                if (strlen(trim($q['year2_2'])) > 0 && strlen(trim($q['year2_2'])) <= 3){
                    $content2 = $q['year2']."<br/>".$q['year2_2'].' мм'."<br/>".$q['progress2'];
                }else{
                    $content2 = $q['year2'].''.$q['year2_2'];
                }
            }

            if (strlen(trim($q['coment1'])) > 0 && strlen(trim($q['year1'])) < 1){
                $content1 = '?';
            }else{
                if (strlen(trim($q['year1_2'])) > 0 && strlen(trim($q['year1_2'])) <= 3){
                    $content1 = $q['year1']."<br/>".$q['year1_2'].' мм'."<br/>".$q['progress1'];
                }else{
                    $content1 = $q['year1'].''.$q['year1_2'];
                }
            }

            if (strlen(trim($q['coment'])) > 0 && strlen(trim($q['currentyear1'])) < 1){
                $content = '?';
            }else{
                if (strlen(trim($q['currentyear1_2'])) > 0 && strlen(trim($q['currentyear1_2'])) <= 3){
                    $content = $q['currentyear1']."<br/>".$q['currentyear1_2'].' мм'."<br/>".$q['progress'];
                }else{
                    $content = $q['currentyear1'].''.$q['currentyear1_2'];
                }
            }

            if (strpos($q['dat4'],'1900-01-01') !== false){
                $dat4 = '';
            }else{
                $dat4 = Yii::$app->formatter->asDate(trim($q['dat4']));
            }
            if (strpos($q['dat3'],'1900-01-01') !== false){
                $dat3 = '';
            }else{
                $dat3 = Yii::$app->formatter->asDate(trim($q['dat3']));
            }
            if (strpos($q['dat2'],'1900-01-01') !== false){
                $dat2 = '';
            }else{
                $dat2 = Yii::$app->formatter->asDate(trim($q['dat2']));
            }
            if (strpos($q['dat1'],'1900-01-01') !== false){
                $dat1 = '';
            }else{
                $dat1 = Yii::$app->formatter->asDate(trim($q['dat1']));
            }
            if (strpos($q['dat'],'1900-01-01') !== false){
                $dat = '';
            }else{
                $dat = Yii::$app->formatter->asDate(trim($q['dat']));
            }

            strpos($q['rozd'],'01.01.00') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.$q['rozd'].'</span>';

            echo '<tr>
                <td class="not_hover_td" style="width: 2%">'.$i.'</td>
                <td class="idstart" id="id_n">'.trim($q['name']).$rozd.'</td>
                <td style="width: 10%" data-y="'.(date('Y')-4).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat4.'" data-ye="'.$q['year4'].'" data-yea="'.$q['year4_2'].'" data-coment="'.$q['coment4'].'">'.$content4.'</td>
                <td style="width: 10%" data-y="'.(date('Y')-3).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat3.'" data-ye="'.$q['year3'].'" data-yea="'.$q['year3_2'].'" data-coment="'.$q['coment3'].'">'.$content3.'</td>
                <td style="width: 10%" data-y="'.(date('Y')-2).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat2.'" data-ye="'.$q['year2'].'" data-yea="'.$q['year2_2'].'" data-coment="'.$q['coment2'].'">'.$content2.'</td>
                <td style="width: 10%" data-y="'.(date('Y')-1).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat1.'" data-ye="'.$q['year1'].'" data-yea="'.$q['year1_2'].'" data-coment="'.$q['coment1'].'">'.$content1.'</td>
                <td style="width: 20%" data-y="'.(date('Y')).'" data-id_child="'.trim($q['id_child']).'" data-name="'.trim($q['name']).'" data-dat="'.$dat.'" data-ye="'.$q['currentyear1'].'" data-yea="'.$q['currentyear1_2'].'" data-coment="'.$q['coment'].'">'.$content.'<span style="color: #ff0000"> '.$q['viraj'].'</span></td>
            </tr>';
            $i++;
        }

        ?>

    </table>
</div>











<?php $form = ActiveForm::begin([
    'id' => 'id_form_hidden_j'
]); ?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_pole_id_j',
])->label(false); ?>
<?= $form->field($model_mantu,'id_child')->hiddenInput([
    'id' => 'hidden_id_child',
])->label(false); ?>
<?= $form->field($model_mantu,'year')->hiddenInput([
    'id' => 'hidden_year',
])->label(false); ?>
<?php ActiveForm::end(); ?>

<?php
 Modal::begin([
     'id' => 'modal_journal',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="header_modal_journal"></h4>',
     'size' => Modal::SIZE_DEFAULT,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_mantu">Удалить отметку</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_mantu">Сохранить изменения</button>',
 ]);
?>



<?php $form = ActiveForm::begin([
    'id' => 'form_modal_journal',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['journal/journal'],
    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
        'horizontalCssClasses' => [
            'label' => 'col-lg-4',
            'offset' => 'col-lg-offset-0',
            'wrapper' => 'col-lg-7',
//             'error' => '',
//             'hint' => '',
        ],
    ],
]); ?>

<?= $form->field($model_mantu, 'name')->textInput([
    'id' => 'name_modal_j',
    'placeholder' => 'Фамилия Имя',
    'readonly'=> true
]) ?>
<?= $form->field($model_mantu, 'dat')->widget('yii\widgets\MaskedInput', [
    'options' => [
        'id' => 'dat_modal_j',
        'placeholder' => 'Дата проведения',
    ],
    'mask' => '99.99.9999',
]) ?>
<?= $form->field($model_mantu,'ye')->dropDownList($array2,[
    'id' => 'ye_modal_j',
    'prompt' => 'Инфильтрат'
]) ?>
<?= $form->field($model_mantu, 'yea')->textInput([
    'id' => 'yea_modal_j',
    'placeholder' => 'Размер, мм'
]) ?>
<?= $form->field($model_mantu, 'coment')->textarea([
    'id' => 'coment_modal_j',
    'rows' => 3,
    'placeholder' => 'Примечание'
]) ?>
<?php ActiveForm::end(); ?>
<?php Modal::end(); ?>




<?php
$script = <<<JS

$(function(){



    $('table td:not(.not_hover_td,.idstart)').on('click',function(){

        $('#form_modal_journal').trigger('reset');

        var id_child = $(this).data('id_child');
        var y = $(this).data('y');
        var name = $(this).data('name');
        var dat = $(this).data('dat');
        var ye = $(this).data('ye');
        var yea = $(this).data('yea');
        var coment = $(this).data('coment');
        $('#yea_modal_j').val(yea.trim());
        $('#name_modal_j').val(name.trim());
        $('#dat_modal_j').val(dat.trim());
        $('#coment_modal_j').val(coment.trim());
        $('#hidden_id_child').val(id_child);
        $('#hidden_year').val(y);

        $('#header_modal_journal').text('Отметка р.Манту '+y+' г.');

        if((ye.trim()).length > 0){
            $('#ye_modal_j option:selected').text(ye.trim());
        }else{
            $('#ye_modal_j option:selected').text('Инфильтрат');
        }
        if((dat.trim()).length > 0){
            $('#hidden_pole_id_j').val(1);
            $('#but_del_mantu').show();
            $('#but_save_mantu').text('Сохранить изменения');
        }else{
            $('#hidden_pole_id_j').val(2);
            $('#but_del_mantu').hide();
            $('#but_save_mantu').text('Добавить отметку');
        }

        $('#modal_journal').modal('show');

        return false;
    });


    $('#but_save_mantu').click(function(){

        var infiltrat = $("#ye_modal_j :selected").text();
//        $('#b_del_so').hasClass('active') ? $('#hidden_id_so').val(4) : null;
        var iddd = $('#hidden_pole_id_j').val();

        console.log(iddd+' iddd');//return;/////////////////////////*//*********************************************
        if (iddd == 1 || iddd == 5){
//            $('#hidden_pole_id_j').val(1);
            $('.modal').modal('hide');
//            var dol = $("#dol100 option:selected").text();
            var form = $('form').serializeArray();
            form.splice(4,1);
            form.splice(2,1);
            form.splice(5,1);
            form.push({name:'Mantu[ye]',value:infiltrat});
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            console.log(infiltrat+'*');
            console.log(form);//return;//////////////////////////////////////////////////////
            var arr = $('#form_rep_item_journal');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }else if(iddd == 2){
            $('.modal').modal('hide');
//            dol = $("#dol100 option:selected").text();
            form = $('form').serializeArray();
//            console.log(form)
            form.splice(4,1);
            form.splice(2,1);
            form.splice(5,1);
            form.push({name:'Mantu[ye]',value:infiltrat});
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            console.log(form);//return;
            arr = $('#form_rep_item_journal');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        $('#b_del_so').removeClass('active');
//                        $('#b_worked_so').addClass('active');
                        $('#table_sotrudniki').html(response);
//                        console.log($('#t_mantu').data('all'));
//                        console.log($('#t_mantu').data('notall'));
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('not');
//                    alert("Ошибка");
                });


        }else if(iddd == 4){return;
            $('#hidden_pole_id_j').val(6);
            $('.modal').modal('hide');
//            var dol = $("#dol100 option:selected").text();
            form = $('form').serializeArray();
            form.splice(2,1);
            form.splice(3,1);
//            form.push({name:'Sotrudniki[dol]',value:dol});
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            console.log(form);return;//////////////////////////////////////////////////////
            arr = $('#form_rep_item_journal');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }
        return false;
    });

    $('#but_del_mantu').click(function(){
        $('#hidden_pole_id_j').val(3);
        var form = $('form').serializeArray();
        form.splice(4,1);
        form.splice(2,1);
        if($('#but_all_deti_mantu').hasClass('active')){
            var id7 = 0;
        }else{
            id7 = 1;
        }
        form.push({name:'id7',value:id7});
        console.log(form);//return;
//        return;
        $('.modal').modal('hide');
//        console.log(idishka);
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
            });
        return false;
    });


})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

