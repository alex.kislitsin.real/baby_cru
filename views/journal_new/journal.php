<?php

use kartik\form\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
?>
<?php
switch($model_rep->name){
    case 1:
        $table = 'table';
        break;
    case 2:
        $table = 'table_kal';
        break;
}

?>

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <div class="date_picker_so">
            <?php $form = ActiveForm::begin([
                'id' => 'form_rep_item_journal',
                'action' => ['journal/journal'],
                'method' => 'POST',
                'type' => ActiveForm::TYPE_VERTICAL,
                'enableAjaxValidation' => false,//
            ])?>

            <?= $form->field($model_rep,'name')->dropDownList($array,[
                'id' => 'drop_journals',
            ])->label(false) ?>
            <?= $form->field($model_id,'id')->hiddenInput(['id' => 'model_id'])->label(false); ?>
            <?php $form = ActiveForm::end()?>
            </div>
        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_INLINE,
                'action' => ['journal/journal'],
                'method' => 'POST',
                'id' => 'form_item_group_all_journal',
                'enableAjaxValidation' => false,//
            ])?>
            <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
                'id' => 'drop_group_mantu',
            ])->label(false) ?>
            <div class="btn-group btn-group" role="group">
                <div class="btn-group pull_right" role="group">
                    <?= Html::button($all_deti, ['class'=>'btn btn-md btn-default active','id' => 'but_all_deti_mantu']); ?>
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button($not_all_deti, ['class'=>'btn btn-md btn-default','id' => 'but_deti_mantu_not_all_otmetki']); ?>
                </div>
            </div>

            <?= Html::button('Показатели по садику', ['class'=>'btn btn-md btn-info','id' => 'but_id_show_all_sad']); ?>
            <?= Html::button('Направить в ОПТД', ['class'=>'btn btn-md btn-success','id' => 'but_id_go_to_optd','style'=>"display: none;"]); ?>
            <?= Html::button('Контрольная явка в ПТД на текущий месяц', ['class'=>'btn btn-md btn-default','id' => 'but_id_go_to_optd_yavka','style'=>"display: none;"]); ?>
            <?php $form = ActiveForm::end()?>
        </div>
    </div>

    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render($table,compact(
            'array_control_yavka',
            'model_kal',
            'model_rep',
            'array2',
            'model_mantu',
            'model_id',
            'model',
            'all_deti',
            'not_all_deti')) ?>
    </div>


</div>
<?php
Modal::begin([
    'id' => 'modal_mantu_itogo',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block" id="#">Показатели р.Манту по садику за '.date("Y").' год</h3>',
    'size' => Modal::SIZE_DEFAULT,
    'footer' => '<button type="submit" class="btn btn-success btn-md" id="but_cancel_mantu_itogo">Закрыть</button>',
]);?>
<div id="id_modal_mantu_itogo_render">
    <?= $this->render('modal_mantu_itogo',compact('array_itogo')) ?>
</div>
<?php Modal::end(); ?>

<?php
$script = <<<JS

$(function(){



    $('#drop_group_mantu').on('change', function() {

    var value_drop_journals = $('#drop_journals').val();
    console.log(value_drop_journals+' value_drop_journals');//return;
    var form = '';

    if(value_drop_journals==1){

            $('#hidden_pole_id_j').val(4);
            form = $('#form_item_group_all_journal,#id_form_hidden_j').serializeArray();
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            var testform = $('#form_rep_item_journal');
//            form.splice(4,1);
//            form.splice(2,1);
            console.log(form);//return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
            return false;

    }

    if(value_drop_journals==2){

            $('#hidden_pole_id_j').val(7);
            form = $('form').serializeArray();
            if($('#but_all_deti_mantu').hasClass('active')){
                var id7 = 0;
            }else{
                id7 = 1;
            }
            form.push({name:'id7',value:id7});
            var testform = $('#form_rep_item_journal');
//            form.splice(4,1);
//            form.splice(2,1);
            console.log(form+' form kal');//return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
            return false;

    }


    });

    /*$('#but_deti_mantu_not_all_otmetki').click(function(){
        $(this).addClass('active');
        $('#but_all_deti_mantu').removeClass('active');
//        $('.modal').modal('hide');
        return false;
    });*/

    $('#but_all_deti_mantu').click(function(){
    var value_drop_journals = $('#drop_journals').val();

    if(value_drop_journals==1){

        $(this).addClass('active');
        $('#but_deti_mantu_not_all_otmetki').removeClass('active');
        $('#hidden_pole_id_j').val(4);
        var form = $('form').serializeArray();
        form.push({name:'id7',value:0});
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        $('#b_add_so').attr('disabled',true);
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;

    }else if(value_drop_journals==2){

        $(this).addClass('active');
        $('#but_deti_mantu_not_all_otmetki').removeClass('active');
        $('#hidden_pole_id_j').val(7);
        var form = $('form').serializeArray();
        form.push({name:'id7',value:0});
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        $('#b_add_so').attr('disabled',true);
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;

    }


    });

    $('#but_deti_mantu_not_all_otmetki').click(function(){///button show not otmetki

    var value_drop_journals = $('#drop_journals').val();

    if(value_drop_journals==1){

        $(this).addClass('active');
        $('#but_all_deti_mantu').removeClass('active');
        $('#hidden_pole_id_j').val(4);
        var form = $('form').serializeArray();
        form.push({name:'id7',value:1});
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        $('#b_add_so').attr('disabled',true);
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;

    }else if(value_drop_journals==2){

        $(this).addClass('active');
        $('#but_all_deti_mantu').removeClass('active');
        $('#hidden_pole_id_j').val(7);
        var form = $('form').serializeArray();
        form.push({name:'id7',value:1});
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        $('#b_add_so').attr('disabled',true);
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;

    }




    });


    $('#but_cancel_mantu_itogo').click(function(){
        $('.modal').modal('hide');
        return false;
    });

    $('#but_id_show_all_sad').click(function(){

//        return;
//        $(this).addClass('active');
//        $('#b_del_so').removeClass('active');

        $('#hidden_pole_id_j').val(12);
        var form = $('form').serializeArray();
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $("#but_id_show_all_sad").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{

//                        $('#b_add_so').attr('disabled',false);
                        $('#id_modal_mantu_itogo_render').html(response);
                        $('#modal_mantu_itogo').modal('show');
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    $('#but_id_go_to_optd_yavka').on('click',function(){
//        $('#hidden_pole_id_j').val(19);
        var form = $('#form_item_group_all_journal').serializeArray();
        form.push({name:'Id[id]',value:19});
        console.log(form);//return;
        var arr = $('#form_rep_item_journal');
        $("#but_id_go_to_optd_yavka").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);return;
                        $('#id_div_table_plan_control_yavka_optd').html(response);
                        $('#modal_control_yavka').modal('show');
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    /*$('#b_add_so').click(function(){
        $('form').trigger('reset');
        $('#but_del_forever_so').hide();
        $('#hidden_id_so').val(2);
        $('#address100').val('');
        $('#schet100').val('');
        $('#ins100').val('');
        $('#rozdso100').val('');
        $('#outs100').val('');
        $('#snils100').val('');
        $('#polis100').val('');
        $('#nameso100').val('');
        $('input:radio[name="Deti[pol]"][value=1]').click();
        $('#but_save_so').text('Добавить ребёнка');
        $('#modal_so100').modal('show');
        return false;
    });*/



    //////form item journal

    $('#drop_journals').on('change', function() {
        var value = $(this).val();
        if(value==1){
            $('#but_id_show_all_sad,#but_deti_mantu_not_all_otmetki,#but_all_deti_mantu,#drop_group_mantu').fadeIn().show();
            $('#but_id_go_to_optd,#but_id_go_to_optd_yavka').fadeOut().hide();
            console.log(value);

            var rrr = $('#drop_group_mantu').val();
            $('#drop_group_optd').val(rrr);

//            $('#hidden_pole_id_j').val(5);
//            var form = $('form').serializeArray();
            var form = $('#form_item_group_all_journal,#form_rep_item_journal,#id_form_hidden_j').serializeArray();
            form.push({name:'Id[id]',value:5});

            var testform = $('#form_rep_item_journal');
            console.log(form);return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
    //            $("*").LoadingOverlay("hide");
                $('#rrr555').html(response);
            }).fail(function() {
    //            $("*").LoadingOverlay("hide");
                console.log('not');
            });


            $('#hidden_pole_id_j').val(4);
//            form = $('form').serializeArray();
            form = $('#form_item_group_all_journal,#form_rep_item_journal,#id_form_hidden_j').serializeArray();
            form.push({name:'Id[id]',value:4});
//            form = $('form').not('#form_optd_new_child').serializeArray();
            if($('#but_all_deti_mantu').hasClass('active')){
                    var id7 = 0;
                }else{
                    id7 = 1;
                }
            form.push({name:'id7',value:id7});console.log(form);//return;
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");

                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
            return false;
        }

        if(value==2){
            var rrr = $('#drop_group_mantu').val();
            $('#drop_group_optd').val(rrr);

            $('#but_deti_mantu_not_all_otmetki,#but_all_deti_mantu,#drop_group_mantu').fadeIn().show();
            $('#but_id_show_all_sad,#but_id_go_to_optd,#but_id_go_to_optd_yavka').hide();
            console.log(value);
            $('#model_id').val(7);
            var form = $('#form_item_group_all_journal,#form_rep_item_journal').serializeArray();
            var testform = $('#form_rep_item_journal');
            if($('#but_all_deti_mantu').hasClass('active')){
                    var id7 = 0;
                }else{
                    id7 = 1;
                }
            form.push({name:'id7',value:id7});
            console.log(form);//return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);
                        $('#table_sotrudniki').html(response);
                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
            return false;
        }

        if(value==3){
            $('#but_id_go_to_optd,#but_id_go_to_optd_yavka').fadeIn();
            $('#but_id_show_all_sad,#but_deti_mantu_not_all_otmetki,#but_all_deti_mantu,#drop_group_mantu').hide();
            console.log(value);
            $('#hidden_pole_id_j').val(13);
            var form = $('#form_item_group_all_journal,#form_rep_item_journal,#id_form_hidden_j').serializeArray();
            var testform = $('#form_rep_item_journal');
            console.log(form);
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
    //            $("*").LoadingOverlay("hide");
                $('#rrr555').html(response);


            }).fail(function() {
    //            $("*").LoadingOverlay("hide");
                console.log('not');
            });



            $('#hidden_pole_id_j').val(14);
            form = $('#form_item_group_all_journal,#form_rep_item_journal,#id_form_hidden_j').serializeArray();
            /*if($('#but_all_deti_mantu').hasClass('active')){
                    var id7 = 0;
                }else{
                    id7 = 1;
                }*/
            form.push({name:'id7',value:0});console.log(form);//return;
//            form.push({name:'id7',value:id7});console.log(form);//return;
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");

                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
//                        console.log(response);
                        $('#table_sotrudniki').html(response);
//                        $('#but_all_deti_mantu').text($('#t_mantu').data('all'));
//                        $('#but_deti_mantu_not_all_otmetki').text($('#t_mantu').data('notall'));
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });

            return false;
        }


    });


})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>