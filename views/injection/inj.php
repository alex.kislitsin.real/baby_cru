<?php

use kartik\form\ActiveForm;
//use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
$width1 = '7%';
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
?>

<?/*= $this->render('_search') */?>

    <div class="rep_boss">
        <div class="rep_boss_up_panel">
            <div class="rep_boss_up_panel_item_rep">
                <?= Html::button('Сохранить в Excel', [
                    'class'=>'btn btn-md btn-success btn-block',
                    'id' => 'id_green_button_inj',
//                    'disabled' => 'disabled'
                ]); ?>
            </div>
            <div class="rep_boss_up_panel_1" data-id7="0">
                <?php $form = ActiveForm::begin([
                    'type' => ActiveForm::TYPE_INLINE,
                    'id' => 'form_inj_item_group',
//                    'layout' => 'horizontal',
                    'method' => 'POST',
                    'action' => ['injection/inj'],
                    'enableAjaxValidation' => false,//
                ])?>
                <?= $form->field($model_group,'name')->dropDownList($array_gruppa,[
                    'id' => 'drop_deti_group_inj',
                ])->label(false) ?>

                <!--            --><?//= Html::button('Отчисленные', ['class'=>'btn btn-md btn-default','id' => 'b_del_so']); ?>
                <?= Html::button('План прививок по всем группам', ['class'=>'btn btn-md btn-default','id' => 'id_but_show_plan_injection']); ?>
                <?php $form = ActiveForm::end()?>
            </div>
        </div>
        <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
            <table>
                <tr>
                    <td class="not_hover_td" style="width: 2%">№</td>
                    <!--<td style="width: 2%">!</td>-->
                    <td class="not_hover_td">Фамилия Имя</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">БЦЖ</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">ОПВ</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">АКДС</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Гепатит B</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Корь +<br/>Паротит</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Краснуха</td>
<!--                    <td class="not_hover_td" style="width: --><?//= $width1 ?><!--">Паротит</td>-->
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Гемофильная</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Превенар</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Грипп</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">Кл.энцеф.</td>
                    <td class="not_hover_td" style="width: <?= $width1 ?>">ВО</td>
                </tr>
            </table>
        </div>
        <div class="rep_boss_down" id="table_injection">
            <?= $this->render('table',compact(
                'model_vr',
                'array',
                'model_id',
                'model')) ?>
        </div>
    </div>

<?php
Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();
?>
<?php
$script = <<<JS

$(function(){

    $('#drop_deti_group_inj').on('change', function() {
        var value = $(this).val();

        $('#hidden_id').val(3);
            var form = $('#form_inj_item_group,#form100_hide').serializeArray();
            var testform = $('#form_inj_table');
            console.log(form);//return;
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
//                console.log(response);//return;
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_injection').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;
    });

    $('#id_but_show_plan_injection').on('click',function(){
        $('#hidden_id').val(4);
        var form = $('#form100_hide').serializeArray();
            var testform = $('#form_inj_table');
            console.log(form);//return;
            $("#id_but_show_plan_injection").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
//                console.log(response);//return;
                if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#id_div_table_plan_injection').html(response);
                        $('#modal_show_plan_injection').modal('show');
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
            });
        return false;

    });



})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>