<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$percent = '1%';
//$model_so100->rozd = null;
//$model->doza_v1 = '5555555';
$color = '#adadad';
$color_otkaz = '#ff1800';
$font = '10px';
?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_inj">
    <table class="table-striped table-bordered table-hover">

    <?php
//    debug($array);
    $i = 1;
    foreach($array as $q){

        if ($q['otkaz_bcg']==1){
            $bcg = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_bcg = '';$r_bcg = '';$napravlen_bcg = '';
        }else{
            strpos($q['bcg'],'1900-01-01') !== false ? $bcg = '' : $bcg = Yii::$app->formatter->asDate(trim($q['bcg']));
            $q['v_bcg']!=0 ? $v_bcg = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_bcg'].'</span>'.'&nbsp;' : $v_bcg = '';
            $q['r_bcg']!=0 ? $r_bcg = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_bcg'].'</span>'.'&nbsp;' : $r_bcg = '';
            strpos($q['napravlen_bcg'],'1900-01-01') !== false ? $napravlen_bcg = '' : $napravlen_bcg = '<br/>направлен';
        }

        if ($q['otkaz_opv']==1){
            $opv = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_opv = '';$r_opv = '';$napravlen_opv = '';
        }else{
            strpos($q['opv'],'1900-01-01') !== false ? $opv = '' : $opv = Yii::$app->formatter->asDate(trim($q['opv']));
            $q['v_opv']!=0 ? $v_opv = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_opv'].'</span>'.'&nbsp;' : $v_opv = '';
            $q['r_opv']!=0 ? $r_opv = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_opv'].'</span>'.'&nbsp;' : $r_opv = '';
            strpos($q['napravlen_opv'],'1900-01-01') !== false ? $napravlen_opv = '' : $napravlen_opv = '<br/>направлен';
        }

        if ($q['otkaz_akds']==1){
            $akds = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_akds = '';$r_akds = '';$napravlen_akds = '';
        }else{
            strpos($q['akds'],'1900-01-01') !== false ? $akds = '' : $akds = Yii::$app->formatter->asDate(trim($q['akds']));
            $q['v_akds']!=0 ? $v_akds = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_akds'].'</span>'.'&nbsp;' : $v_akds = '';
            $q['r_akds']!=0 ? $r_akds = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_akds'].'</span>'.'&nbsp;' : $r_akds = '';
            strpos($q['napravlen_akds'],'1900-01-01') !== false ? $napravlen_akds = '' : $napravlen_akds = '<br/>направлен';
        }

        if ($q['otkaz_gepatit_b']==1){
            $gepatit_b = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_gepatit_b = '';$r_gepatit_b = '';$napravlen_gepatit_b = '';
        }else{
            strpos($q['gepatit_b'],'1900-01-01') !== false ? $gepatit_b = '' : $gepatit_b = Yii::$app->formatter->asDate(trim($q['gepatit_b']));
            $q['v_gepatit_b']!=0 ? $v_gepatit_b = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_gepatit_b'].'</span>'.'&nbsp;' : $v_gepatit_b = '';
            $q['r_gepatit_b']!=0 ? $r_gepatit_b = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_gepatit_b'].'</span>'.'&nbsp;' : $r_gepatit_b = '';
            strpos($q['napravlen_gepatit_b'],'1900-01-01') !== false ? $napravlen_gepatit_b = '' : $napravlen_gepatit_b = '<br/>направлен';
        }

        if ($q['otkaz_kor']==1){
            $kor = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_kor = '';$r_kor = '';$napravlen_kor = '';
        }else{
            strpos($q['kor'],'1900-01-01') !== false ? $kor = '' : $kor = Yii::$app->formatter->asDate(trim($q['kor']));
            $q['v_kor']!=0 ? $v_kor = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_kor'].'</span>'.'&nbsp;' : $v_kor = '';
            $q['r_kor']!=0 ? $r_kor = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_kor'].'</span>'.'&nbsp;' : $r_kor = '';
            strpos($q['napravlen_kor'],'1900-01-01') !== false ? $napravlen_kor = '' : $napravlen_kor = '<br/>направлен';
        }

        if ($q['otkaz_red']==1){
            $red = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_red = '';$r_red = '';$napravlen_red = '';
        }else{
            strpos($q['red'],'1900-01-01') !== false ? $red = '' : $red = Yii::$app->formatter->asDate(trim($q['red']));
            $q['v_red']!=0 ? $v_red = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_red'].'</span>'.'&nbsp;' : $v_red = '';
            $q['r_red']!=0 ? $r_red = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_red'].'</span>'.'&nbsp;' : $r_red = '';
            strpos($q['napravlen_red'],'1900-01-01') !== false ? $napravlen_red = '' : $napravlen_red = '<br/>направлен';
        }

        if ($q['otkaz_parotit']==1){
            $parotit = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_parotit = '';$r_parotit = '';$napravlen_parotit = '';
        }else{
            strpos($q['parotit'],'1900-01-01') !== false ? $parotit = '' : $parotit = Yii::$app->formatter->asDate(trim($q['parotit']));
            $q['v_parotit']!=0 ? $v_parotit = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_parotit'].'</span>'.'&nbsp;' : $v_parotit = '';
            $q['r_parotit']!=0 ? $r_parotit = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_parotit'].'</span>'.'&nbsp;' : $r_parotit = '';
            strpos($q['napravlen_parotit'],'1900-01-01') !== false ? $napravlen_parotit = '' : $napravlen_parotit = '<br/>направлен';
        }

        if ($q['otkaz_gomofil']==1){
            $gomofil = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_gomofil = '';$r_gomofil = '';$napravlen_gomofil = '';
        }else{
            strpos($q['gomofil'],'1900-01-01') !== false ? $gomofil = '' : $gomofil = Yii::$app->formatter->asDate(trim($q['gomofil']));
            $q['v_gomofil']!=0 ? $v_gomofil = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_gomofil'].'</span>'.'&nbsp;' : $v_gomofil = '';
            $q['r_gomofil']!=0 ? $r_gomofil = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_gomofil'].'</span>'.'&nbsp;' : $r_gomofil = '';
            strpos($q['napravlen_gomofil'],'1900-01-01') !== false ? $napravlen_gomofil = '' : $napravlen_gomofil = '<br/>направлен';
        }

        if ($q['otkaz_prevenar']==1){
            $prevenar = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';$v_prevenar = '';$r_prevenar = '';$napravlen_prevenar = '';
        }else{
            strpos($q['prevenar'],'1900-01-01') !== false ? $prevenar = '' : $prevenar = Yii::$app->formatter->asDate(trim($q['prevenar']));
            $q['v_prevenar']!=0 ? $v_prevenar = '<span style="color: '.$color.';font-size:'.$font.'">V'.$q['v_prevenar'].'</span>'.'&nbsp;' : $v_prevenar = '';
            $q['r_prevenar']!=0 ? $r_prevenar = '<span style="color: '.$color.';font-size:'.$font.'">R'.$q['r_prevenar'].'</span>'.'&nbsp;' : $r_prevenar = '';
            strpos($q['napravlen_prevenar'],'1900-01-01') !== false ? $napravlen_prevenar = '' : $napravlen_prevenar = '<br/>направлен';
        }

        if ($q['otkaz_gripp']==1){
            $gripp = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';
        }else{
            strpos($q['gripp'],'1900-01-01') !== false ? $gripp = '' : $gripp = Yii::$app->formatter->asDate(trim($q['gripp']));
        }

        if ($q['otkaz_encefalit']==1){
            $encefalit = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';
        }else{
            strpos($q['encefalit'],'1900-01-01') !== false ? $encefalit = '' : $encefalit = Yii::$app->formatter->asDate(trim($q['encefalit']));
        }

        if ($q['otkaz_vo']==1){
            $vo = '<span style="color: '.$color_otkaz.'">ОТКАЗ</span>';
        }else{
            strpos($q['vo'],'1900-01-01') !== false ? $vo = '' : $vo = Yii::$app->formatter->asDate(trim($q['vo']));
        }


        /*strpos($q['napravlen'],'1900-01-01') !== false ? $napravlen = '' : $napravlen = '<br/>направлен';*/

        strpos($q['rozd'],'1900-01-01') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'</span>';

        echo '<tr>
                <td class="idstart not_hover_td" style="width: 2%">'.$i.'</td>
                <td class="idstart" id="id_n">'.trim($q['name']).$rozd.'</td>
                <td data-id_inj="1" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_bcg']).'" style="width: 7%">'.$r_bcg.$v_bcg.$bcg.$napravlen_bcg.'</td>
                <td data-id_inj="2" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_opv']).'" style="width: 7%">'.$r_opv.$v_opv.$opv.$napravlen_opv.'</td>
                <td data-id_inj="3" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_akds']).'" style="width: 7%">'.$r_akds.$v_akds.$akds.$napravlen_akds.'</td>
                <td data-id_inj="4" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_gepatit_b']).'" style="width: 7%">'.$r_gepatit_b.$v_gepatit_b.$gepatit_b.$napravlen_gepatit_b.'</td>
                <td data-id_inj="5" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_kor']).'" style="width: 7%">'.$r_kor.$v_kor.$kor.$napravlen_kor.'</td>
                <td data-id_inj="6" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_red']).'" style="width: 7%">'.$r_red.$v_red.$red.$napravlen_red.'</td>
                <td data-id_inj="8" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_gomofil']).'" style="width: 7%">'.$r_gomofil.$v_gomofil.$gomofil.$napravlen_gomofil.'</td>
                <td data-id_inj="9" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_prevenar']).'" style="width: 7%">'.$r_prevenar.$v_prevenar.$prevenar.$napravlen_prevenar.'</td>
                <td data-id_inj="10" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_gripp']).'" style="width: 7%">'.$gripp.'</td>
                <td data-id_inj="11" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_encefalit']).'" style="width: 7%">'.$encefalit.'</td>
                <td data-id_inj="12" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" data-otkaz="'.trim($q['otkaz_vo']).'" style="width: 7%">'.$vo.'</td>

            </tr>';
        $i++;
    }
    ?>
    </table>
</div>



<!--                <td data-id_inj="7" data-id_child="'.$q['id_child'].'" data-name="'.trim($q['name']).'" style="width: 7%">'.$r_parotit.$v_parotit.$parotit.$napravlen_parotit.'</td>
-->








<?php
Modal::begin([
    'id' => 'modal_double_dates',
    'header' => '<h3 style="text-align: center">Ошибка ввода данных !!!</h3>',
//    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_cancel_so">Отмена</button>
//     <button type="submit" class="btn btn-success bt<!--n--->md" id//="but_deldel_so">Удалить навсегда</button>',
    'size' => Modal::SIZE_DEFAULT,
]);
echo '<div class="not_selected_text_on_block">
      <div class="alert alert-danger" role="alert"><h3 style="text-align: center">Одинаковых дат в одной прививке быть не должно !!!</h3></div>
      </div>';
Modal::end();

Modal::begin([
    'id' => 'modal_error',
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>
<?php
 Modal::begin([
     'id' => 'modal_inj',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="header_modal_inj"></h4>',
     'size' => Modal::SIZE_LARGE,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_cancel">Отмена</button>
     <input type="checkbox" name="checkbox0" value="300" id="check_otkaz">&nbsp;Отказ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <button type="submit" class="btn btn-default btn-md" id="id_div_hidden_napravlen_date" style="display: none;"></button>&nbsp;
     <input type="checkbox" name="checkbox1" value="200" id="check">&nbsp;Направлен&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <button type="submit" class="btn btn-success btn-md" id="but_save">Сохранить изменения</button>',
 ]);
?>

<?php $form = ActiveForm::begin([
    'id' => 'form100_hide',
]); ?>
<?= $form->field($model_id,'id')->hiddenInput(['id' => 'hidden_id',])->label(false); ?>
<?= $form->field($model,'id_inj')->hiddenInput(['id' => 'id_inj',])->label(false); ?>

<?= $form->field($model,'vv1')->hiddenInput(['id' => 'vv1',])->label(false); ?>
<?= $form->field($model,'vv2')->hiddenInput(['id' => 'vv2',])->label(false); ?>
<?= $form->field($model,'vv3')->hiddenInput(['id' => 'vv3',])->label(false); ?>

<?= $form->field($model,'rr1')->hiddenInput(['id' => 'rr1',])->label(false); ?>
<?= $form->field($model,'rr2')->hiddenInput(['id' => 'rr2',])->label(false); ?>
<?= $form->field($model,'rr3')->hiddenInput(['id' => 'rr3',])->label(false); ?>
<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form_inj_table',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['injection/inj'],

    /*'fieldConfig' => [
        'template' => "<div> {label} </div> <div>{input} {error}</div>\n",
//        'labelOptions'=>['class' => 'control-label'],
    ],*/

    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'template' => "<div> {label} </div> <div>{input}</div>\n",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
         'horizontalCssClasses' => [
             'label' => 'col-lg-1',
             'offset' => 'col-lg-offset-0',
             'wrapper' => 'col-lg-12',
             'error' => '',
             'hint' => '',
         ],
     ],
]); ?>




<div class="col-sm-1" style="width: 45px">

    <?= $form->field($model_vr, 'v1')->textInput([
        'id' => 'v1',
        'value' => 'V1',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label('&nbsp;') ?>
    <?= $form->field($model_vr, 'v2')->textInput([
        'id' => 'v2',
        'value' => 'V2',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label(false) ?>
    <?= $form->field($model_vr, 'v3')->textInput([
        'id' => 'v3',
        'value' => 'V3',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model_vr, 'r1')->textInput([
        'id' => 'r1',
        'value' => 'R1',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label(false) ?>
    <?= $form->field($model_vr, 'r2')->textInput([
        'id' => 'r2',
        'value' => 'R2',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label(false) ?>
    <?= $form->field($model_vr, 'r3')->textInput([
        'id' => 'r3',
        'value' => 'R3',
        'readonly'=> true,
        'style' => 'color:black;border-radius:3px 0 0 3px'
    ])->label(false) ?>

</div>

<div class="col-sm-2" style="width: 100px">

    <?= $form->field($model, 'date_inj_v1')->widget('yii\widgets\MaskedInput', [

        'options' => [
//            'autofocus' => true,
            'id' => 'date_inj_v1',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?= $form->field($model, 'date_inj_v2')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_inj_v2',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <?= $form->field($model, 'date_inj_v3')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_inj_v3',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'date_inj_r1')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_inj_r1',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <?= $form->field($model, 'date_inj_r2')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_inj_r2',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
    <?= $form->field($model, 'date_inj_r3')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_inj_r3',
            'placeholder' => '',
            'style' => 'border-radius:0',
        ],
        'mask' => '99.99.9999',
    ])->label(false) ?>
</div>
<div class="col-sm-2" style="width: 105px">

    <?= $form->field($model, 'doza_v1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_v1',
    ]) ?>
    <?= $form->field($model, 'doza_v2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_v2',
    ])->label(false) ?>
    <?= $form->field($model, 'doza_v3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_v3',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'doza_r1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_r1',
    ])->label(false) ?>
    <?= $form->field($model, 'doza_r2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_r2',
    ])->label(false) ?>
    <?= $form->field($model, 'doza_r3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'doza_r3',
    ])->label(false) ?>
</div>

<div class="col-sm-2">

    <?= $form->field($model, 'seriya_v1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_v1',
    ]) ?>
    <?= $form->field($model, 'seriya_v2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_v2',
    ])->label(false) ?>
    <?= $form->field($model, 'seriya_v3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_v3',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'seriya_r1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_r1',
    ])->label(false) ?>
    <?= $form->field($model, 'seriya_r2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_r2',
    ])->label(false) ?>
    <?= $form->field($model, 'seriya_r3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'seriya_r3',
    ])->label(false) ?>
</div>

<div class="col-sm-2">

    <?= $form->field($model, 'name_preparat_v1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_v1',
    ]) ?>
    <?= $form->field($model, 'name_preparat_v2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_v2',
    ])->label(false) ?>
    <?= $form->field($model, 'name_preparat_v3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_v3',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'name_preparat_r1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_r1',
    ])->label(false) ?>
    <?= $form->field($model, 'name_preparat_r2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_r2',
    ])->label(false) ?>
    <?= $form->field($model, 'name_preparat_r3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'name_preparat_r3',
    ])->label(false) ?>
</div>

<div class="col-sm-2">

    <?= $form->field($model, 'reaction_v1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_v1',
    ]) ?>
    <?= $form->field($model, 'reaction_v2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_v2',
    ])->label(false) ?>
    <?= $form->field($model, 'reaction_v3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_v3',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'reaction_r1')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_r1',
    ])->label(false) ?>
    <?= $form->field($model, 'reaction_r2')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_r2',
    ])->label(false) ?>
    <?= $form->field($model, 'reaction_r3')->textInput([
        'style' => 'border-radius:0',
        'id' => 'reaction_r3',
    ])->label(false) ?>
</div>

<div class="col-sm-2" style="width: 180px">

    <?= $form->field($model, 'medotvod_v1')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_v1',
    ]) ?>
    <?= $form->field($model, 'medotvod_v2')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_v2',
    ])->label(false) ?>
    <?= $form->field($model, 'medotvod_v3')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_v3',
    ])->label(false) ?>
    <hr/>
    <?= $form->field($model, 'medotvod_r1')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_r1',
    ])->label(false) ?>
    <?= $form->field($model, 'medotvod_r2')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_r2',
    ])->label(false) ?>
    <?= $form->field($model, 'medotvod_r3')->textInput([
        'style' => 'border-radius:0 3px 3px 0',
        'id' => 'medotvod_r3',
    ])->label(false) ?>
</div>














<?= $form->field($model,'id_child')->hiddenInput(['id' => 'id_child',])->label(false); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>


<?php Modal::begin([
    'id' => 'modal_show_plan_injection',
    'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block">План прививок на текущий месяц</h4>',
//    'footer' => '<button type="submit" class="btn btn-info btn-md" id="#">Сохранить в excel</button>
//     <button type="submit" class="btn btn-success btn-md" id="#">Закрыть</button>',
    'footer' => Html::a('<span class="glyphicon glyphicon-save"></span> Сохранить отчёт в Excel', ['sp/exceltest','id' => 2], ['class'=>'btn btn-md btn-success']),

    'size' => Modal::SIZE_LARGE
]);?>
<div id="id_div_table_plan_injection"><?= $this->render('modal_plan',compact('array')) ?></div>

<?php Modal::end();?>



<?php
$script = <<<JS

$(function(){

var old_otkaz;

$('#form_inj_table input').on('keyup', function(e){
    if(e.which == 13) $('#but_save').click();
    return false;
});

    //var v1,v2,v3,r1,r2,r3;

$('#but_save').on('click',function(){
    var s = $("#table_injection").scrollTop();

    $('.modal').modal('hide');
//    console.log('34354');
    var arr = $('#form_inj_table');
    $('#hidden_id').val(2);
    var test = $('#form100_hide,#form_inj_table').serializeArray();



    if($('#check_otkaz').is(':checked')){
        test.push({name: 'otkaz', value: 1});
        test.push({name: 'old_otkaz', value: old_otkaz});
    }else{
        test.push({name: 'otkaz', value: 0});
        test.push({name: 'old_otkaz', value: old_otkaz});
    }

    console.log(test);//return;
    $(".rep_boss_down").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : test
    }).done(function(response) {
    console.log(response);//return;
    $('.modal').modal('hide');
            $("*").LoadingOverlay("hide");
            if(response==400){
                $('#modal_error').modal('show');
            }else{
                if(response==888){
                    $('#modal_double_dates').modal('show');
                }else{
                    $('#table_injection').html(response).scrollTop(s);
                }
            }
//            console.log(response);//return;


        }).fail(function() {
        $('.modal').modal('hide');
            console.log('not1');
            $("*").LoadingOverlay("hide");
        });

    $('.modal').modal('hide');

    return false;
});

$('#but_cancel').on('click',function(){
    $('.modal').modal('hide');
    return false;
});

$('#check').change(function() {
    var options = {
        year: 'numeric',
        month: 'numeric',
        day: 'numeric'
    };
    if($(this).is(':checked')){
        console.log('checked');
        $('#id_div_hidden_napravlen_date').text(new Date().toLocaleString("ru", options)).fadeIn(300);
    }else{
        console.log('unchecked');
        $('#id_div_hidden_napravlen_date').fadeOut(300);
    }
    /*if($(this).prop('checked',false)){
        console.log('unchecked');
    }*/
    return false;

});

$('#table_inj td:not(.not_hover_td,.idstart)').on('click',function(){
//    console.log($(this).data('id_child'));
    console.log('1');

    var array_inj = new Map([
    [1,'БЦЖ'],
    [2,'ОПВ'],
    [3,'АКДС'],
    [4,'Гепатит B'],
    [5,'Корь'],
    [6,'Краснуха'],
    [7,'Паротит'],
    [8,'Гемофильная'],
    [9,'Превенар'],
    [10,'Грипп'],
    [11,'Клещевой энцефалит'],
    [12,'Ветряная Оспа']
    ]);

    $('#form_inj_table').trigger('reset');
    $(this).LoadingOverlay("show");$('#anim_loader').LoadingOverlay("show");
    var arr = $('#form_inj_table');

    $('input[name^="Injection"]').val('');

    $('#hidden_id').val(1);
    $('#vv1,#vv2,#vv3,#rr1,#rr2,#rr3').val(0);

//    $('#id_child').val('fgdf171sdfd');
    $('#id_child').val($(this).data('id_child'));
    $('#id_inj').val($(this).data('id_inj'));

    $('#header_modal_inj').text($(this).data('name')+' ('+array_inj.get($(this).data('id_inj'))+')');

    var test = $('#form100_hide,#form_inj_table').serializeArray();

    console.log(test);//return;
    var options = {
        day: 'numeric',
        month: 'numeric',
        year: 'numeric'
    };

    old_otkaz = $(this).data('otkaz');

    $.ajax({
        type : arr.attr('method'),
        url : arr.attr('action'),
        data : test
    }).done(function(response) {
//            console.log(response);
            array_all = $.parseJSON(response);
            console.log(array_all);//return;
            if(array_all.length>0){
                $.each(array_all,function(index,value){
                    $("*").LoadingOverlay("hide");
                    value.otkaz!=0 ? $('#check_otkaz').prop('checked',true) : $('#check_otkaz').prop('checked',false);
//                    $('#date_inj_v1').focus();
                    if(value.v==1){
//                        v1=1;
                        $('#vv1').val(1);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
                        //console.log(d);return;
                        $('#date_inj_v1').val(d);
                        $('#doza_v1').val((value.doza));
                        $('#seriya_v1').val((value.seriya));
                        $('#name_preparat_v1').val((value.name_preparat));
                        $('#reaction_v1').val((value.reaction));
                        $('#medotvod_v1').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }

                        $('#modal_inj').modal('show');
                    }
                    if(value.v==2){
//                        v2=2;
                        $('#vv2').val(2);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
                        $('#date_inj_v2').val(d);
                        $('#doza_v2').val((value.doza));
                        $('#seriya_v2').val((value.seriya));
                        $('#name_preparat_v2').val((value.name_preparat));
                        $('#reaction_v2').val((value.reaction));
                        $('#medotvod_v2').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }
                    if(value.v==3){
//                        v3=3;
                        $('#vv3').val(3);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
                        $('#date_inj_v3').val(d);
                        $('#doza_v3').val((value.doza));
                        $('#seriya_v3').val((value.seriya));
                        $('#name_preparat_v3').val((value.name_preparat));
                        $('#reaction_v3').val((value.reaction));
                        $('#medotvod_v3').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }

                    if(value.r==1){
//                        r1=1;
                        $('#rr1').val(1);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
                        $('#date_inj_r1').val(d);
                        $('#doza_r1').val((value.doza));
                        $('#seriya_r1').val((value.seriya));
                        $('#name_preparat_r1').val((value.name_preparat));
                        $('#reaction_r1').val((value.reaction));
                        $('#medotvod_r1').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }
                    if(value.r==2){
//                        r2=2;
                        $('#rr2').val(2);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
                        $('#date_inj_r2').val(d);
                        $('#doza_r2').val((value.doza));
                        $('#seriya_r2').val((value.seriya));
                        $('#name_preparat_r2').val((value.name_preparat));
                        $('#reaction_r2').val((value.reaction));
                        $('#medotvod_r2').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }
                    if(value.r==3){
//                        r3=3;
                        $('#rr3').val(3);
                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
                        $('#date_inj_r3').val(d);
                        $('#doza_r3').val((value.doza));
                        $('#seriya_r3').val((value.seriya));
                        $('#name_preparat_r3').val((value.name_preparat));
                        $('#reaction_r3').val((value.reaction));
                        $('#medotvod_r3').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }

                    if(value.r==0 && value.v==0){
//                        r3=3;
//                        $('#rr3').val(3);
//                        var d = (new Date(value.date_inj)).toLocaleString("ru", options);
//                console.log(d);return;
//                        $('#date_inj_r3').val(d);
//                        $('#doza_r3').val((value.doza));
//                        $('#seriya_r3').val((value.seriya));
//                        $('#name_preparat_r3').val((value.name_preparat));
//                        $('#reaction_r3').val((value.reaction));
//                        $('#medotvod_r3').val((value.medotvod));
                        if((value.napravlen).indexOf('1900')== -1){
                            $('#check').prop('checked',true);
                            var d_napr = (new Date(value.napravlen)).toLocaleString("ru", options);
                            $('#id_div_hidden_napravlen_date').text(d_napr).fadeIn(300);
                        }else{
                            $('#check').prop('checked',false);
                            $('#id_div_hidden_napravlen_date').hide();
                        }
                        $('#modal_inj').modal('show');
                    }
                });
            }else{
                console.log('empty array');
                $("*").LoadingOverlay("hide");
                $('#check').prop('checked',false);
                $('#check_otkaz').prop('checked',false);
                $('#id_div_hidden_napravlen_date').hide();
                $('#modal_inj').modal('show');
            }

        }).fail(function() {
            console.log('not1_inj');
            $("*").LoadingOverlay("hide");
        });
    return false;
});

})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>



