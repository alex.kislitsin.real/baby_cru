<?php
use yii\bootstrap\Modal;
?>


<div class="my_table my_table2 not_selected_text_on_block">
    <table class="table-striped table-bordered">
        <thead>
        <tr>
            <th style="width: 4%">п/п</th>
            <th style="width: 22%">Фамилия Имя</th>
            <th style="width: 10%">Возраст</th>
            <th style="width: 10%">№ группы</th>
            <th style="width: 12%">Прививка по плану</th>
            <th style="width: 12%">Дата по плану</th>
            <th style="width: 30%">Примечание</th>
        </tr>
        </thead>
        <tbody>
<?php
$i = 1;
$id_child_save = null;
foreach($array as $q){
    strpos($q['rozd'],'1900-01-01') !== false ? $rozd = '' : $rozd = '<br/>'.'<span style="color: #0000ff;font-size:12px">'.(Yii::$app->formatter->asDate(trim($q['rozd']))).'</span>';
    $po_planu = trim($q['RV']).' '.trim($q['name_code']);
    strpos($q['next_date'],'1900-01-01') !== false ? $next_date = '' : $next_date = Yii::$app->formatter->asDate($q['next_date']);
    if (strlen(trim($q['medotvod_date']))>0){
        $med_date = Yii::$app->formatter->asDate(trim($q['medotvod_date']));
    }else{
        $med_date = '';
    }
    if (strlen($med_date)>0){
        $text_med = 'медотвод до ';
    }else{
        $text_med = '';
    }
    $coment = $text_med.' '.$med_date.' '.trim($q['coment']).' '.trim($q['medotvod']);

    if ($id_child_save != null){
        if ($id_child_save == $q['id_child']){
            $name = '';
            $rozd = '';
            $ii = '';
            $group = '';
            $vozrast_year = '';
        }else{
            $id_child_save = $q['id_child'];
            $name = trim($q['name']);
            $ii = $i;
            $group = $q['id_gruppa'];
            $vozrast_year = $q['vozrast_year'];
        }
    }else{
        $id_child_save = $q['id_child'];
        $name = trim($q['name']);
        $ii = $i;
        $group = $q['id_gruppa'];
        $vozrast_year = $q['vozrast_year'];
    }

    echo '<tr>
             <td class="not_hover_td" style="width: 4%">'.$ii.'</td>
             <td style="width: 22%" id="id_n">'.$name.$rozd.'</td>
             <td style="width: 10%">'.$vozrast_year.'</td>
             <td style="width: 10%">'.$group.'</td>
             <td style="width: 12%">'.$po_planu.'</td>
             <td style="width: 12%">'.$next_date.'</td>
             <td style="width: 30%">'.$coment.'</td>
          </tr>';
//    $i++;
    if (strlen($name)>0)$i++;
}
?>

        </tbody>
</table>
</div>






<?php
$script = <<<JS
$(function(){
    $('.rrrrr0 li').click(function(){
//    $('#meesage_success_change_correct').attr('hidden','hidden');
    console.log('rrrrr0');
    $('#id_hidden_corr').val(2);
    var testform = $('#form_inj_table');
    var form = $('form').serializeArray();
    console.log(form);
//    return;
    var time1 = $.now();
    var begin = $('#datbegin').val();
    var end = $('#datend').val();
        console.log(time1);
        $.ajax({
            type : testform.attr('method'),
            url : testform.attr('action'),
            data : form
        }).done(function(response) {

            if(response==400){
                $('#modal_error').modal('show');
            }else if(response==300){
//                $('#name_corr_not_otmetka').text(name+' ('+begin+' - '+end+')');
//                $('#message_not_otmetka_corr').text('За указанный период отметок не найдено !');
                /*$('#name_corr_not_otmetka').text(name);
                $('#message_not_otmetka_corr').text('За период c '+begin+' по '+end+' отметок не найдено');*/
//                $('#modal_info_corr').modal('show');
            }else{
                var time2 = $.now();
                console.log(time2-time1);
                /*if(time2-time1 > 400){
                    $("#table_corr").LoadingOverlay("show");
                }*/
                $('#id_modal_corr').html(response);
//                $('#name_corr_not_otmetka1').text(name+' ('+begin+' - '+end+')');
                $('#modal_get_reason_corr').modal('show');
            }
                $("*").LoadingOverlay("hide");
        }).fail(function() {
            $("*").LoadingOverlay("hide");
            console.log('not');
        });
    console.log('3333');
    return false;
    });



    $('#but_cancel_corr').on('click',function(){
    $('.modal').modal('hide');
    return false;
    });

})
JS;
//$this->registerJs($script,yii\web\View::POS_END);
?>