<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 15.10.19
 * Time: 17:13
 */
//use yii\grid\GridView;
//use kartik\form\ActiveForm;
use yii\bootstrap\ActiveForm;
use kartik\grid\GridView;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$percent = '1%';
$model_so100->rozdso = null;
$model_so100->ins = null;
//debug($model2);
//foreach($model2 as $f){
//    if($f['id5']==29){
//        $f['id5'] = '';
//    }
//}
//debug($model2);
//$number_reports = Yii::$app->request->cookies->getValue('save_last_item_reports');
/*switch($number_reports){
    case 1:
        $eee = 'id5';
        $eee1 = 'пропуски';
        $eee2 = 'prichiny';
        break;
    case 2:
        $eee = 'idd';
        $eee1 = 'nextmonth';
        $eee2 = 'podpis';
        break;

    default:
        echo '<div class="alert alert-danger" role="alert">Ошибка</div>';
        break;
}*/
Modal::begin([
    'id' => 'modal_del_or_not',
//    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
    'header' => '<h3 style="text-align: center">Удалить сотрудника из базы данных ?</h3>',
    'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_cancel_so">Отмена</button>
     <button type="submit" class="btn btn-success btn-md" id="but_deldel_so">Удалить навсегда</button>',
    'size' => Modal::SIZE_DEFAULT,
]);
echo '<div class="not_selected_text_on_block">
      <div class="alert alert-default" role="alert"><h3 style="text-align: center" id="name_so_for_del"></h3></div>
      </div>';
Modal::end();

Modal::begin([
    'id' => 'modal_error',
//    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Произошла ошибка, попробуйте снова или обратитесь в службу поддержки.</div>
</div>';
Modal::end();

?>
<?php
 Modal::begin([
     'clientOptions' => [
         'backdrop' => 'static',
         'keyboard' => false,
     ],
     'id' => 'modal_so100',
     'header' => '<h4 style="padding-left: 10px" class="not_selected_text_on_block" id="id_header_modal_so100">Данные сотрудника</h4>',
     'size' => Modal::SIZE_LARGE,
     'footer' => '<button type="submit" class="btn btn-danger btn-md pull-left" id="but_del_forever_so">Удалить сотрудника из базы</button>
     <button type="submit" class="btn btn-success btn-md" id="but_save_so">Сохранить изменения</button>',
 ]);
?>
<?php //$model_so100->pol = 0; ?>

<?php $form = ActiveForm::begin([
    'id' => 'form100_hide',
]); ?>
<?= $form->field($model_id,'id')->hiddenInput([
    'id' => 'hidden_id_so',
])->label(false); ?>

<?php ActiveForm::end(); ?>

<?php $form = ActiveForm::begin([
    'id' => 'form100',
    'layout' => 'horizontal',
    'method' => 'POST',
    'action' => ['sotrudniki/so'],
    'fieldConfig' => [
//         'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
//         'template' => "{label}\n{beginWrapper}\n{input}\n{endWrapper}",
//         'labelOptions'=>['class' => 'control-label'],
         'horizontalCssClasses' => [
             'label' => 'col-lg-5',
             'offset' => 'col-lg-offset-3',
             'wrapper' => 'col-lg-7',
//             'error' => '',
//             'hint' => '',
         ],
     ],
]); ?>

<div class="col-sm-6">
    <?= $form->field($model_so100, 'name')->textInput([
//    'autofocus' => true,
//        'class' => 'fogot',
        'id' => 'name100',
        'placeholder' => 'ФИО'
    ]) ?>
    <?= $form->field($model_so100,'dol')->dropDownList($dol,[
        'id' => 'dol100',
        'prompt' => 'Должность'
    ]) ?>
    <?= $form->field($model_so100, 'rozdso')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'rozdso100',
            'placeholder' => 'Дата рождения',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?= $form->field($model_so100, 'tel')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'tel100',
            'placeholder' => 'Номер телефона',
        ],
        'mask' => '9(999)999-99-99',
    ]) ?>
    <?= $form->field($model_so100, 'snils')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'snils100',
            'placeholder' => 'СНИЛС',
        ],
        'mask' => '999-999-999 99',
    ]) ?>
    <?= $form->field($model_so100, 'polis')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'polis100',
            'placeholder' => 'Полис',
        ],
        'mask' => '9999 9999 9999 9999',
    ]) ?>
    <?= $form->field($model_so100, 'address')->textarea([
        'id' => 'address100',
        'rows' => 2,
        'placeholder' => 'Введите адрес'
    ]) ?>
    <?= $form->field($model_so100, 'pitanie', ['inline'=>true, 'enableLabel'=>true])->radioList([
        '1' => 'На питании',
        '2' => 'Нет',
    ],[
        'id' => 'blog_type',
        'class' => 'btn-group btn-group-justified',
        'data-toggle' => 'buttons',
        'unselect' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
            },
    ]); ?>

</div>
<div class="col-sm-6">

    <?/*= $form->field($model_so100, 'schet')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'schet100',
            'placeholder' => 'Номер лицевого счета',
        ],
        'mask' => '999999999999',
    ]) */?>

    <?=$form->field($model_so100, 'schet')
        ->textInput([
            'type' => 'number',
            'id' => 'schet100',
            'placeholder' => 'Номер лицевого счета',
        ]); ?>

    <?= $form->field($model_so100, 'ins')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'ins100',
            'placeholder' => 'Дату зачисления',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?=$form->field($model_so100, 'prikaz_in')
        ->textInput([
            'type' => 'number',
            'id' => 'prikaz_in100',
            'placeholder' => 'Номер приказа зачисления',
        ]); ?>
<!--    <hr/>-->
    <?= $form->field($model_so100, 'outs')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'outs100',
            'placeholder' => 'Дата увольнения',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?=$form->field($model_so100, 'prikaz_out')
        ->textInput([
            'type' => 'number',
            'id' => 'prikaz_out100',
            'placeholder' => 'Номер приказа увольнения',
        ]); ?>

    <?= $form->field($model_so100, 'coment')->textarea([
        'rows' => 2,
        'placeholder' => 'Примечание',
        'id' => 'coment100',
    ]) ?>

    <?= $form->field($model_so100, 'pol', ['inline'=>true, 'enableLabel'=>true])->radioList([
        '1' => 'Мужской',
        '2' => 'Женский',
    ],[
        'id' => 'blog_type',
        'class' => 'btn-group btn-group-justified',
        'data-toggle' => 'buttons',
        'unselect' => null,
        'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="btn btn-default' . ($checked ? ' active' : '') . '">' .
                Html::radio($name, $checked, ['value' => $value, 'class' => 'project-status-btn']) . $label . '</label>';
            },
    ]); ?>

    <?= $form->field($model_so100, 'seria')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'seria100',
            'placeholder' => 'Серия паспорта',
        ],
        'mask' => '9999',
    ]) ?>
    <?= $form->field($model_so100, 'num_passport')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'num_passport100',
            'placeholder' => 'Номер паспорта',
        ],
        'mask' => '999999',
    ]) ?>
    <?= $form->field($model_so100, 'kem_vidan')->textarea([
        'id' => 'kem_vidan100',
        'rows' => 4,
        'placeholder' => 'Кем выдан'
    ]) ?>
    <?= $form->field($model_so100, 'date_vidacha')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'date_vidacha100',
            'placeholder' => 'Дата выдачи',
        ],
        'mask' => '99.99.9999',
    ]) ?>
    <?= $form->field($model_so100, 'inn')->widget('yii\widgets\MaskedInput', [
        'options' => [
            'id' => 'inn100',
            'placeholder' => 'ИНН',
        ],
        'mask' => '999999999999',
    ]) ?>


</div>
<?= $form->field($model_so100,'id')->hiddenInput([
    'id' => 'hidden_pole_idso',
//    'disabled' => 'disabled'
])->label(false); ?>

<?php ActiveForm::end(); ?>

<?php Modal::end(); ?>

<div class="my_table my_table2 not_selected_text_on_block" id="table_so100">
    <table class="table-striped table-bordered table-hover">

    <!--<div class="my_table my_table2 not_selected_text_on_block">-->
    <?php
    $i = 1;
//    debug($model);
    foreach($model as $q){
        if (strpos($q['inS'],'1900-01-01') !== false){
            $t = '';
        }else{
            $t = Yii::$app->formatter->asDate(trim($q['inS']));
        }
        if (strpos($q['date_vidacha'],'1900-01-01') !== false){
            $date_vidacha = '';
        }else{
            $date_vidacha = Yii::$app->formatter->asDate($q['date_vidacha']);
        }

        if (!empty($q['outS'])||!is_null($q['outS'])){
            $t2 = Yii::$app->formatter->asDate(trim($q['outS']));
            $classs = 'id="row_sotr_all"';
        }else{
            $t2 = '';
            $classs = '';
        }
        if (strpos($q['rozdso'],'1900-01-01') !== false){
            $u = '';
        }else{
            $u = Yii::$app->formatter->asDate(trim($q['rozdso']));
        }
        if (strpos($q['dol'],'null') !== false){
            $dol = '';
        }else{
            $dol = trim($q['dol']);
        }

        if (strlen(trim($q['pitanie']))>0){
            $pit = '&#10004;';
//            $pit = '+';
        }else{
            $pit = '';
        }

        echo '<tr '.$classs.' data-id="'.$q['id'].'" data-name="'.trim($q['name']).'" data-schet="'.trim($q['schet']).'" data-ins="'.$t.'" data-rozdso="'.$u.'"
             data-dol="'.$dol.'" data-pitanie="'.trim($q['pitanie']).'" data-pol="'.trim($q['pol']).'" data-tel="'.trim($q['tel']).'" data-address="'.trim($q['address']).'"
             data-coment="'.trim($q['coment']).'"
             data-snils="'.trim($q['snils']).'" data-outs="'.$t2.'" data-prikaz_in="'.$q['prikaz_in'].'" data-prikaz_out="'.$q['prikaz_out'].'" data-polis="'.trim($q['polis']).'"
             data-seria="'.$q['seria'].'" data-num_passport="'.$q['num_passport'].'" data-kem_vidan="'.trim($q['kem_vidan']).'" data-date_vidacha="'.$date_vidacha.'" data-inn="'.$q['inn'].'">
                <td style="width: 2%">'.$i.'</td>
                <td id="id_n">'.trim($q['name']).'</td>
                <td style="width: 7%">'.trim($q['schet']).'</td>
                <td style="width: 7%">'.$t.'</td>
                <td style="width: 7%">'.$u.'</td>
                <td style="width: 15%">'.$dol.'</td>
                <td style="width: 6%">'.$pit.'</td>
                <td style="width: 3%">'.trim($q['pol']).'</td>
                <td style="width: 10%">'.trim($q['tel']).'</td>

                <td style="width: 10%">'.trim($q['snils']).'</td>
            </tr>';
        $i++;
    }

    ?>

    </table>
</div>

<?php /*<td style="width: 10%">'.trim($q['address']).'</td>
                <td style="width: 10%">'.trim($q['coment']).'</td>*/ ?>

<?php
$script = <<<JS

$(function(){

    var idishka;
    if($('#b_del_so').hasClass('active')){
        $('#b_worked_so').removeClass('active');
        $('#b_add_so').attr('disabled',true);
    }/*else{
        $('#b_worked_so').addClass('active');
    }*/


    $('table tr').on('click',function(){

        $('form').trigger('reset');

        var id,form,name,schet,ins,outs,rozdso,dol,pitanie,pol,tel,address,coment,snils,prikaz_in,prikaz_out,opt,polis,seria,num_passport,kem_vidan,date_vidacha,inn = null;
        $(this).addClass('#item_yellow_tr');
        id = $(this).data('id');
        name = $(this).data('name');
        schet = $(this).data('schet');
        ins = $(this).data('ins');
        outs = $(this).data('outs');
        rozdso = $(this).data('rozdso');
        dol = $(this).data('dol');
        pitanie = $(this).data('pitanie');
        pol = $(this).data('pol');
        tel = $(this).data('tel');
        address = $(this).data('address');
        coment = $(this).data('coment');
        snils = $(this).data('snils');
        prikaz_in = $(this).data('prikaz_in');
        prikaz_out = $(this).data('prikaz_out');
        polis = $(this).data('polis');

        seria = $(this).data('seria');
        num_passport = $(this).data('num_passport');
        kem_vidan = $(this).data('kem_vidan');
        date_vidacha = $(this).data('date_vidacha');
        inn = $(this).data('inn');

        seria == 0 ? $('#seria100').val('') : $('#seria100').val(seria);
        num_passport == 0 ? $('#num_passport100').val('') : $('#num_passport100').val(num_passport);
        $('#kem_vidan100').val(kem_vidan);
        $('#date_vidacha100').val(date_vidacha);
        inn == 0 ? $('#inn100').val('') : $('#inn100').val(inn);

        //установка переключателя "на питании"
        if(pitanie.length > 1){
            $('input:radio[name="Sotrudniki[pitanie]"][value=1]').click();
        }else{
            $('input:radio[name="Sotrudniki[pitanie]"][value=2]').click();
        }

        //установка переключателя "пол"
        if(pol.indexOf('М') > -1){
        console.log('1');
            $('input:radio[name="Sotrudniki[pol]"][value=1]').click();
        }else if(pol.indexOf('Ж') > -1){
        console.log('2');
            $('input:radio[name="Sotrudniki[pol]"][value=2]').click();
        }else if(pol.length < 1){
            console.log('3');
        }

        //установка должности
        if(dol.length > 0){
//        console.log('dol.length > 0');
            $('#dol100 option:selected').text(dol);
            /*opt = $('#dol100 option');
            opt.each(function(indx, element){
              if ( $(this).text().toLowerCase() == dol.toLowerCase() ) {
                $(this).attr("selected", "selected")
              }
            });*/
        }else{
            $('#dol100 option:selected').text('Должность');
        }

        //установка даты рождения
        if(rozdso.length > 1){
            $('#rozdso100').val(rozdso);
        }else{
            $('#rozdso100').val('');
        }
        //установка номера телефона
        if(tel.length > 1){
            $('#tel100').val(tel);
        }else{
            $('#tel100').val('');
        }
        //установка address
        if(address.length > 1){
            $('#address100').val(address);
        }else{
            $('#address100').val('');
        }
        //установка schet
        if(schet > 1){
            $('#schet100').val(schet);
        }else{
            $('#schet100').val('');
        }
        //установка ins
        if(ins.length > 1){
            $('#ins100').val(ins);
        }else{
            $('#ins100').val('');
        }
        //установка outs
        if(outs.length > 1){
            $('#outs100').val(outs);
        }else{
            $('#outs100').val('');
        }
        //установка snils
        if(snils.length > 1){
            $('#snils100').val(snils);
        }else{
            $('#snils100').val('');
        }
        //установка coment
        if(coment.length > 1){
            $('#coment100').val(coment);
        }else{
            $('#coment100').val('');
        }

        if(polis.length > 1){
            $('#polis100').val(polis);
        }else{
            $('#polis100').val('');
        }


        //установка prikaz_in
        $('#but_save_so').text('Сохранить изменения');
        $('#prikaz_in100').val(prikaz_in);
        $('#prikaz_out100').val(prikaz_out);
        $('#hidden_pole_idso').val(id);
        $('#hidden_id_so').val(1);
        $('#name100').val(name);
        $('#but_del_forever_so').show();
        $('#modal_so100').modal('show');
        return false;
    });


    $('#but_save_so').click(function(){
        $('.modal').modal('hide');
        if($('#b_del_so').hasClass('active')){
            $('#hidden_id_so').val(4);
        }/*else{
            $('#hidden_id_so').val(1);
        }*/
        var iddd = $('#hidden_id_so').val();
        console.log(iddd+' iddd');//return;
        if (iddd == 1 || iddd == 5){
//            $('#hidden_id_so').val(1);
            $('.modal').modal('hide');
            var dol = $("#dol100 option:selected").text();
            var form = $('#form100,#form100_hide').serializeArray();
            form.splice(2,1);
            form.splice(3,1);
            form.push({name:'Sotrudniki[dol]',value:dol});
//            form.push({name:'id7',value:id7});
            console.log(form);//return;//////////////////////////////////////////////////////
            var arr = $('#form100');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);//return;
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }else if(iddd == 2){
            $('.modal').modal('hide');
            dol = $("#dol100 option:selected").text();
            form = $('#form100,#form100_hide').serializeArray();
//            console.log(form);
            form.splice(2,1);
            form.splice(3,1);
            form.push({name:'Sotrudniki[dol]',value:dol});
            console.log(form);//return;
            arr = $('#form100');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#b_del_so').removeClass('active');
                        $('#b_worked_so').addClass('active');
                        $('#table_sotrudniki').html(response);
                    }
                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
//                    console.log('not');
//                    alert("Ошибка");
                });


        }else if(iddd == 4){
            $('#hidden_id_so').val(6);
            $('.modal').modal('hide');
            var dol = $("#dol100 option:selected").text();
            form = $('#form100,#form100_hide').serializeArray();
            form.splice(2,1);
            form.splice(3,1);
            form.push({name:'Sotrudniki[dol]',value:dol});
            console.log(form);//return;//////////////////////////////////////////////////////
            arr = $('#form100');
            $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('yes');
//                    console.log(response);
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }

                }).fail(function() {
                    $("*").LoadingOverlay("hide");
                    $('.modal').modal('hide');
                    console.log('not');
//                    alert("Ошибка");
                });
        }
        return false;
    });

    /*$('#b_add_so').click(function(){
        $('#but_del_forever_so').hide();
        $('#hidden_id_so').val(2);
        $('form').trigger('reset');
        $('#dol100 option:selected').text('Должность');
        $('input:radio[name="Sotrudniki[pol]"][value=2]').click();
        $('input:radio[name="Sotrudniki[pitanie]"][value=2]').click();
        $('#but_save_so').text('Добавить нового сотрудника');
        $('#modal_so100').modal('show');
        return false;
    });*/

    $('#but_del_forever_so').click(function(){
        $('#name_so_for_del').text($('#name100').val());
//        idishka = $('#hidden_pole_idso').val();
        $('.modal').modal('hide');
        $('#modal_del_or_not').modal('show');
        return false;
    });

    $('#but_del_cancel_so').click(function(){
        $('.modal').modal('hide');
        return false;
    });

    $('#but_deldel_so').click(function(){
        $('#hidden_id_so').val(3);
        var form = $('#form100,#form100_hide').serializeArray();
        form.splice(2,1);
        form.splice(3,1);
        if($('#b_del_so').hasClass('active')){
            var id7 = 0;
        }else{
            id7 = 1;
        }
        form.push({name:'id7',value:id7});
//        console.log(form);return;
//        return;
        $('.modal').modal('hide');
//        console.log(idishka);
        var arr = $('#form100');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                $('.modal').modal('hide');
            });
        return false;
    });

})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>

