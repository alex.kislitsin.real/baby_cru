<?php


use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

//echo Html::a('Перейти', ['sotrudniki/ex'], ['class'=>'btn btn-lg btn-primary']);
//$query_year = "select distinct(year(datenotgo)) from gogo";
//$query_year = Yii::$app->db->createCommand($query_year)->queryAll();
//debug($query_year);
//$array_year_tabel_deti = [
//    2019 => '2019',
//            2020 => '2020',
//];

//if(in_array(2020,$array_year_tabel_deti))debug('111');

?>
<!--<div class="so_boss">
    <div class="so_1">4</div>
    <div class="so_2">5</div>

</div>-->

<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
            <?= Html::button('Добавить сотрудника', ['class'=>'btn btn-md btn-success btn-block','id' => 'b_add_so']); ?>
                <!----><?//= $this->render('form_item_reports',compact(
//                'model_id',
//                'array',
//                'model_rep')) ?>
        </div>
        <div class="rep_boss_up_panel_1" data-id7="0">
            <div class="btn-group btn-group" role="group">
                <div class="btn-group pull_right" role="group">
                    <?= Html::button('Уволенные', ['class'=>'btn btn-md btn-default','id' => 'b_del_so']); ?>
                </div>
                <div class="btn-group" role="group">
                    <?= Html::button('Работающие', ['class'=>'btn btn-md btn-default active','id' => 'b_worked_so']); ?>
                </div>
            </div>
<!--            --><?//= Html::button('Уволенные', ['class'=>'btn btn-md btn-default','id' => 'b_del_so']); ?>
<!--            --><?//= Html::button('Работающие', ['class'=>'btn btn-md btn-default active','id' => 'b_worked_so']); ?>
<!--            --><?//= $this->render('hidden_button_for_rep',compact(
//                'array_year',
//                'model_group',
//                'array_gruppa',
//                'item_gruppa',
//                'model_y',
//                'model_m',
//                'array_year_tabel_deti',
//                '_monthsList'
//            )) ?>
        </div>
    </div>
    <div class="rep_boss_up_panel_2 my_table not_selected_text_on_block">
        <table>
            <tr>
                <td style="width: 2%">№
                </td><td>Фамилия Имя Отчество</td>
                <td style="width: 7%">№ счета</td>
                <td style="width: 7%">Дата зачис.</td>
                <td style="width: 7%">Дата рожд.</td>
                <td style="width: 15%">Должность</td>
                <td style="width: 6%">На питании</td>
                <td style="width: 3%">Пол</td>
                <td style="width: 10%">Телефон</td>
                <!--<td style="width: 10%">Адрес</td>
                <td style="width: 10%">Примечание</td>-->
                <td  style="width: 10%">СНИЛС</td>
            </tr>
        </table>
    </div>
    <div class="rep_boss_down" id="table_sotrudniki">
        <?= $this->render('table',compact(
            'dol',
            'model_so100',
            'model_id',
            'model')) ?>
    </div>
</div>

<?php
$script = <<<JS

$(function(){

    $('#b_add_so').click(function(){
        $('#but_del_forever_so').hide();
        $('#hidden_id_so').val(2);
        $('#form100,#form100_hide').trigger('reset');
        $('#dol100 option:selected').text('Должность');
        $('input:radio[name="Sotrudniki[pol]"][value=2]').click();
        $('input:radio[name="Sotrudniki[pitanie]"][value=2]').click();
        $('#but_save_so').text('Добавить нового сотрудника');
        $('#modal_so100').modal('show');
        return false;
    });

    $('#b_del_so').click(function(){///button show deleted
        $(this).addClass('active');
        $('#b_worked_so').removeClass('active');
        $('#b_add_so').attr('disabled',true);
        $('#hidden_id_so').val(4);//show deleted empoyes
        var form = $('#form100,#form100_hide').serializeArray();
        var arr = $('#form100');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
//                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
//                $('.modal').modal('hide');
            });
        return false;
    });

    $('#b_worked_so').click(function(){
        $(this).addClass('active');
        $('#b_del_so').removeClass('active');
        $('#b_add_so').attr('disabled',false);
        $('#hidden_id_so').val(5);
        var form = $('#form100,#form100_hide').serializeArray();
        var arr = $('#form100');
        $(".rep_boss").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
            }).done(function(response) {
                $("*").LoadingOverlay("hide");
//                $('.modal').modal('hide');
                    if(response==400){
                        $('#modal_error').modal('show');
                    }else{
                        $('#table_sotrudniki').html(response);
                    }
            }).fail(function() {
                $("*").LoadingOverlay("hide");
//                $('.modal').modal('hide');
            });
        return false;
    });





})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>