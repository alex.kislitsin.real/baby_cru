<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 20.10.19
 * Time: 20:41
 */


?>

<div class="container_calendar">
    <div class="container_calendar1">

        <div class="container_calendar1_1 item_child_sp"></div>
        <div class="container_calendar1_3">
            <div class="container_calendar1_3_1 button_next_month" data-clickmonth="0" data-idd="" data-datamonth="<?= $data ?>"></div>
            <div class="container_calendar1_3_2">
                <?= $item_month ?>
            </div>
            <div class="container_calendar1_3_3 button_next_month" data-clickmonth="1" data-idd="" data-datamonth="<?= $data ?>"></div>
        </div>
        <div class="container_calendar1_2" id="table_cal_sp">
            <?= $this->render('_only_calso',compact(
                'data',
                'data_item',
                'model_d_date')) ?>
        </div>
        <!--                <div class="container_calendar1_3">333</div>-->
        <div class="container_calendar1_4"></div>

    </div>
</div>