<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$item = Yii::$app->controller->action->id;
$this->title = 'Списки (сотрудники)';
//debug($model_sp_begin);
?>

<div class="block_content_sp" id="boss_id">

    <?= $this->render('_sp_calso',compact(
        'model_d_antidate',
        'model_d_date',
        'model_item_date',
        'data',
        'data_item',
        '_spiski',
        'model_sp_begin',
        'model_id',
        'array_disabled_dates')) ?>



</div>


