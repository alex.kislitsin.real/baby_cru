<?php
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$model_id = new \app\models\Id();
$model_id->id = 0;

//$model_id->safeAttributes(Yii::$app->request->post());
Yii::$app->request->setBodyParams($model_id);
//debug($model_id);
//debug($model_item_date);



?>


<div class="date_picker_so">
<?php $form = ActiveForm::begin([
    'id' => 'form_sp_item_date',
    'action' => ['spso/spsoview'],
    'method' => 'POST',
    'type' => ActiveForm::TYPE_VERTICAL,
    'enableAjaxValidation' => false,//
])?>


<?= $form->field($model_item_date,'item_date')->widget(DatePicker::className(),[
    'removeButton' => false,
    'name' => 'datepicker_sp',
    'size' => 'md',
    'type' => DatePicker::TYPE_COMPONENT_PREPEND,
    'readonly' => true,
    'options' => [
        'placeholder' => 'Выбрать дату',
        'id' => 'dp_sp'
    ],
    'pluginOptions' => [
        'todayHighlight' => true,
        'todayBtn' => true,
//        'daysOfWeekDisabled' => [0, 6],
        'daysOfWeekHighlighted' => [0, 6],
        'datesDisabled' => $array_disabled_dates,
        'toggleActive'   => true,
        'autoclose'=>true,
        'startDate' => '-1Y',
        'endDate' => '+1Y',
        'format' => 'dd.mm.yyyy'
    ],
    'pluginEvents' => [
        'changeDate' => 'function(e) {

                            var date1 = $("#dp_sp").val();
                            if(date1 != ""){

                                $("#id_form_datepicker").val("2");
                                var testform = $("#form_sp_item_date").serializeArray();
                                var testform2 = $("#form_sp_item_date");
                                console.log(testform);//return;
                                    $(".block_content").LoadingOverlay("show",{image:""});$("#anim_loader").LoadingOverlay("show");
                                    $.ajax({
                                        type : testform2.attr("method"),
                                        url : testform2.attr("action"),
                                        data : testform
                                    }).done(function(response) {
                                        $(".item_child_sp").text("");
                                        $("*").LoadingOverlay("hide");
                                        $("#boss_id").html(response);
                                    }).fail(function() {
                                        $("*").LoadingOverlay("hide");
                                        console.log("not");
                                        alert("Ошибка");
                                    });
                            }
                        }',
        ]
])->label(false); ?>
<?= $form->field($model_id,'id')->hiddenInput(['id' => 'id_form_datepicker'])->label(false);?>
<?php $form = ActiveForm::end()?>
</div>
