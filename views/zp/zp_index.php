<?php
use app\models\Id;
use app\models\Zp;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$model = new Zp();
$model_id = new Id();
?>
<div class="rep_boss">
    <div class="rep_boss_up_panel">
        <div class="rep_boss_up_panel_item_rep">
<!--            <div style="background-color: #a1abff;border: 1px solid transparent;border-radius: 3px;width: 100%;height: 100%"></div>-->
        </div>
        <div class="rep_boss_up_panel_1">

        </div>
    </div>

    <div id="renderAjax_zp_table_boss" style="height: 87vh">

    </div>
</div>


<?php $form = ActiveForm::begin([
    'id' => 'form_zp_hide555',
    'method' => 'POST',
    'action' => ['zp/zp'],

]); ?>
<?= $form->field($model_id, 'id')->hiddenInput([
    'id' => 'id_hidden_modal_zp555',
])->label(false); ?>
<?= $form->field($model, 'id_so')->hiddenInput([
    'id' => 'id_hidden_modal_zp_id_so555',
])->label(false); ?>
<?php ActiveForm::end(); ?>


<?php
$scr = <<<JS
    $(function(){
        var p = prompt('пароль для входа');
        var arr = $('#form_zp_hide555');
        $('#id_hidden_modal_zp555').val(8);
        $('#id_hidden_modal_zp_id_so555').val(p);
        var form = $('#form_zp_hide555').serializeArray();
//        console.log(form);
//        return false;
        $.ajax({
            type : arr.attr('method'),
            url : arr.attr('action'),
            data : form
        }).done(function(response) {
                $("#renderAjax_zp_table_boss").html(response);
            }).fail(function() {
                console.log('error');
            });
        return false;
    });

JS;
$this->registerJs($scr,yii\web\View::POS_END);
?>
