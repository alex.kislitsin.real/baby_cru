<?php
use kartik\date\DatePicker;
//use yii\bootstrap\ActiveForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;

timurmelnikov\widgets\LoadingOverlayAsset::register($this);
//$params = [
//    'prompt' => 'выберите группу'
//];
//$model_id = new \app\models\Id();
//$model_id->id = 0;
//$model_id->safeAttributes(Yii::$app->request->post());
?>
<!--<div class="pull-right">-->

<?php $form = ActiveForm::begin([
    'id' => 'form_sp_item_group',
//    'layout'=>'horizontal',
//    'value' => '1111',
//    'action' => ['sp/test55'],
    'action' => ['sp/spview'],
//    'action' => ['sp/spitemgroupajax'],
    'method' => 'POST',
//    'size' => 'lg',
//    'fieldConfig' => [
//        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
////        'horizontalCssClasses' => [
////            'label' => 'col-sm-4',
////            'offset' => 'col-sm-offset-4',
////            'wrapper' => 'col-sm-8',
////            'error' => '',
////            'hint' => '',
////        ],
//    ],
//    'type' => ActiveForm::TYPE_INLINE,
    'enableAjaxValidation' => false,//
])?>

<?= $form->field($model_group,'name',[
    'addon' => [
        'prepend' => [
            'content' => Html::button('', [
                    'class'=>'btn btn-default btn-md',
                    'id' => 'sp_child_count',
                    'disabled' => 'disabled',
                    'style' => [
                        'font-weight' => 'bold',
                        'color' => 'black',
                        'background-color' => 'rgb(216, 250, 217)',
                        'font-size' => '18px',
                        'width' =>  '45px',
                        'height' => '40px',
                        'border-radius' => '0'
                    ]]),
            'asButton' => true
        ]
    ]
])->dropDownList($array_gruppa,[
//    'prompt' => 'выберите',
        'id' => 'drop',
//    'size' => 'lg',
//    'value' => 1
    ])->label(false) ?>

<?//= $form->field($model_id,'id')->hiddenInput()->label(false); ?>


<?php $form = ActiveForm::end()?>

<?php $count = count($model_sp_begin); ?>
<div id="id_hidden_count_in_group" hidden="hidden" data-count="<?= $count ?>"></div>

<?php
$scr2 = <<< JS

$(function() {

    $('#sp_child_count').text($('#id_hidden_count_in_group').data('count'));
//        var count = $('#id_hidden_count_in_group').data('count');
//        var fff = $("#drop option:selected").text();
//        $("#drop option:selected").text(fff+'  '+count);
//        console.log(count);

    $('#drop').on('change', function() {
        // Получаем объект формы
        var value = $(this).val();



//        var name_group = $(this).val('name');
        console.log(value);
        console.log(64561651451);

        if(value > 0){
            $("#id_form_datepicker").val("1");
            var testform = $('#form_sp_item_date,#form_sp_item_group');
            console.log(testform.serializeArray());
//            $("form,.block_listview_sp").LoadingOverlay("show");
//            $("#dp_sp,.block_listview_sp_boss,#drop").LoadingOverlay("show");
            $("#boss_id").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
            $.ajax({
                type : testform.attr('method'),
                url : testform.attr('action'),
                data : testform.serializeArray()
            }).done(function(response) {
                $(".item_child_sp").text("");
                $("*").LoadingOverlay("hide");
                $('#boss_id').html(response);
//                $('#only_spiski').html(response);
            }).fail(function() {
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });
        }
        // Запрещаем прямую отправку данных из формы
        return false;
    });
});


JS;
$this->registerJs($scr2, yii\web\View::POS_END);
?>




