<?php
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
timurmelnikov\widgets\LoadingOverlayAsset::register($this);

$item = Yii::$app->controller->action->id;
$this->title = 'Питание (дети)';
//debug($model);
//$data = '2019-08-21';
//$model_id = new \app\models\Id();
//if($_spiski==_spiski)
?>

<div class="block_content_sp" id="boss_id">
    <?= $this->render('_sp_cal',compact(
        'model_d_antidate',
        'model_d_date',
        'model_item_date',
        'data',
        'data_item',
        'model_group',
        'array_gruppa',
        'item_gruppa',
        '_spiski',
        'model_sp_begin',
        'model_id',
        'array_disabled_dates')) ?>
</div>