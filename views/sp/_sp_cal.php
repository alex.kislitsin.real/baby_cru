<?php
//$model_id = new \app\models\Id();
?>

<div id="only_spiski" class="block_listview_sp_boss">
    <?= $this->render($_spiski,compact(
        'model_d_antidate',
        'model_sp_begin',
        'data',
        'model_d_date',
        'model_id')) ?>
</div>


<div class="block_calendar_sp not_selected_text_on_block">
    <?= $this->render('_calendar',compact(
        'model_sp_begin',
        'model_d_antidate',
        'model_d_date',
        'model_item_date',
        'data',
        'data_item',
        'model_group',
        'array_gruppa',
        'item_gruppa',
        'model_id',
        'array_disabled_dates')) ?>
</div>