<?php
use yii\bootstrap\Modal;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 08.10.19
 * Time: 22:25
 */
//if($array_day)
$veruyu = verification_user();//if 555 - medsestra, else vospitatel
//debug($veruyu);
//if(empty($date_item)){
//    $date_item = '0000-00-00';
//}

//функция определяет по дате номер недели в месяце
function weekOfMonth($date) {
    $firstOfMonth = date("Y-m-01", strtotime($date));
    return (intval(date("W", strtotime($date))) - intval(date("W", strtotime($firstOfMonth))))+1;
}

//$number_day_on_week = 0;
$year = date('Y', strtotime($data));
$month = date('m', strtotime($data));
//$day = date('j', strtotime($data));
$count_days = date('t', strtotime($data));
//$number_week_day = weekOfMonth($data);
$firstDayOfMonth = date("Y-m-01", strtotime($data));
$number_day_on_week = date('N', strtotime($firstDayOfMonth));
//    debug($number_day_on_week);
$iteration = 1;
switch($number_day_on_week){
    case 1:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 2:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 3:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 4:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 5:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 6:
        ${'id'.$number_day_on_week} = $iteration;
        break;
    case 7:
        ${'id'.$number_day_on_week} = $iteration;
        break;
}
${'itemdate'.$number_day_on_week} = $year.'-'.$month.'-'.$iteration;
$iteration++;
$number_day_on_week++;
while($iteration<=$count_days){
    ${'id'.$number_day_on_week} = $iteration;
    ${'itemdate'.$number_day_on_week} = $year.'-'.$month.'-'.$iteration;

    $iteration++;
    $number_day_on_week++;
}

$number_day_on_week2 = date('N', strtotime($firstDayOfMonth));
$correct = $number_day_on_week2 - 1;
if(empty($array_day)){
    $array_day = '';
}else{
    foreach($array_day as $d){
        if($d['id']==100 || $d['id']==200){
            $dw = date('j', strtotime($d['datenotgo']));
            $dwq = $dw + $correct;
            ${'class'.$dwq} = 'cal_table_row_today';
            ${'dat'.$dwq} = trim($d['reason']);

        }else{
            $dw = date('j', strtotime($d['datenotgo']));
            $dwq = $dw + $correct;
            ${'class'.$dwq} = 'cal_table_row';
            ${'dat'.$dwq} = trim($d['reason']);

        }
    }
}

//debug($model_d_date);
$array_disabled_dates_only_day = array();
foreach($model_d_date as $mdd){
    $year222 = date('Y', strtotime($mdd));
    $month222 = date('m', strtotime($mdd));
    if($year==$year222 && $month==$month222){
        array_push($array_disabled_dates_only_day,$mdd);
    }
}
if(!empty($array_disabled_dates_only_day)){
//    debug($array_disabled_dates_only_day);
    foreach($array_disabled_dates_only_day as $addod){
        $dw5 = date('j', strtotime($addod));
        $dwq5 = $dw5 + $correct;
        ${'class'.$dwq5} = ${'class'.$dwq5}.' table_sp_color_holidays';
    }
}
$class6 = 'table_sp_color_holidays '.$class6;
$class7 = 'table_sp_color_holidays '.$class7;
$class13 = 'table_sp_color_holidays '.$class13;
$class14 = 'table_sp_color_holidays '.$class14;
$class20 = 'table_sp_color_holidays '.$class20;
$class21 = 'table_sp_color_holidays '.$class21;
$class27 = 'table_sp_color_holidays '.$class27;
$class28 = 'table_sp_color_holidays '.$class28;
$class34 = 'table_sp_color_holidays '.$class34;
$class35 = 'table_sp_color_holidays '.$class35;


//возвращаем антипраздники
$array_disabled_antidates_only_day = array();
foreach($model_d_antidate as $mdd5){
    $year222 = date('Y', strtotime($mdd5));
    $month222 = date('m', strtotime($mdd5));
    if($year==$year222 && $month==$month222){
        array_push($array_disabled_antidates_only_day,$mdd5);
    }
}
if(!empty($array_disabled_antidates_only_day)){
//    debug($array_disabled_dates_only_day);
    foreach($array_disabled_antidates_only_day as $addod5){
        $dw55 = date('j', strtotime($addod5));
        $dwq55 = $dw55 + $correct;
        ${'class'.$dwq55} = str_replace('table_sp_color_holidays','',${'class'.$dwq55});
//        ${'class'.$dwq55} = ${'class'.$dwq55}.'';
    }
}



//debug($array_disabled_dates_only_day);


//$this->render('_modal',compact(
//    'data',
//    'data_item',
//    'veruyu',
////    'data_item',
//    'model_d_date'
//));


$array_reason12 = [
    0 => 'ПРОЧЕЕ (отпуск, санаторий, выходной и т.п.)',
    4 => 'ОРЗ',
    20 => 'РВОТА, ПОНОС',
    6 => 'ВЕТРЯНКА',
    28 => 'ДРУГИЕ заболевания (отит, бронхит, гастрит и т.п.)',
];

$array_reason12_555 = [
    0 => 'ПРОЧЕЕ (отпуск, санаторий, выходной и т.п.)',
    4 => 'ОРЗ',
    20 => 'РВОТА, ПОНОС',
    6 => 'ВЕТРЯНКА',
    28 => 'ДРУГИЕ заболевания (отит, бронхит, гастрит и т.п.)',
    100 => 'ПРИДУ С ОПОЗДАНИЕМ',
    200 => 'НЕ ПРИДУ СОВСЕМ',
];

$array_reason_today = [
    100 => 'ПРИДУ С ОПОЗДАНИЕМ',
    200 => 'НЕ ПРИДУ СОВСЕМ',
];


?>


<table class="table_sp" id="id_table_sp_veruyu" data-veruyu="<?= $veruyu ?>" data-datamonth="<?= $data ?>" data-item_month="<?= $item_month ?>" data-item_year="<?= $year ?>">

    <tr>
        <td class="name_of_week_on_day">Пн</td>
        <td class="name_of_week_on_day">Вт</td>
        <td class="name_of_week_on_day">Ср</td>
        <td class="name_of_week_on_day">Чт</td>
        <td class="name_of_week_on_day">Пт</td>
        <td class="table_sp_color_holidays name_of_week_on_day">Сб</td>
        <td class="table_sp_color_holidays name_of_week_on_day">Вс</td>
    </tr>
    <tr> <!--1 неделя-->
        <td id="id1" class="<?= $class1 ?>" data-dat="<?= $dat1 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate1 ?>"><?= $id1 ?></td>
        <td id="id2" class="<?= $class2 ?>" data-dat="<?= $dat2 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate2 ?>"><?= $id2 ?></td>
        <td id="id3" class="<?= $class3 ?>" data-dat="<?= $dat3 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate3 ?>"><?= $id3 ?></td>
        <td id="id4" class="<?= $class4 ?>" data-dat="<?= $dat4 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate4 ?>"><?= $id4 ?></td>
        <td id="id5" class="<?= $class5 ?>" data-dat="<?= $dat5 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate5 ?>"><?= $id5 ?></td>
        <td id="id6" class="<?= $class6 ?>" data-dat="<?= $dat6 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate6 ?>"><?= $id6 ?></td>
        <td id="id7" class="<?= $class7 ?>" data-dat="<?= $dat7 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate7 ?>"><?= $id7 ?></td>
    </tr>
    <tr> <!--2 неделя-->
        <td id="id8" class="<?= $class8 ?>" data-dat="<?= $dat8 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate8 ?>"><?= $id8 ?></td>
        <td id="id9" class="<?= $class9 ?>" data-dat="<?= $dat9 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate9 ?>"><?= $id9 ?></td>
        <td id="id10" class="<?= $class10 ?>" data-dat="<?= $dat10 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate10 ?>"><?= $id10 ?></td>
        <td id="id11" class="<?= $class11 ?>" data-dat="<?= $dat11 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate11 ?>"><?= $id11 ?></td>
        <td id="id12" class="<?= $class12 ?>" data-dat="<?= $dat12 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate12 ?>"><?= $id12 ?></td>
        <td id="id13" class="<?= $class13 ?>" data-dat="<?= $dat13 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate13 ?>"><?= $id13 ?></td>
        <td id="id14" class="<?= $class14 ?>" data-dat="<?= $dat14 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate14 ?>"><?= $id14 ?></td>
    </tr>
    <tr> <!--3 неделя-->
        <td id="id15" class="<?= $class15 ?>" data-dat="<?= $dat15 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate15 ?>"><?= $id15 ?></td>
        <td id="id16" class="<?= $class16 ?>" data-dat="<?= $dat16 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate16 ?>"><?= $id16 ?></td>
        <td id="id17" class="<?= $class17 ?>" data-dat="<?= $dat17 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate17 ?>"><?= $id17 ?></td>
        <td id="id18" class="<?= $class18 ?>" data-dat="<?= $dat18 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate18 ?>"><?= $id18 ?></td>
        <td id="id19" class="<?= $class19 ?>" data-dat="<?= $dat19 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate19 ?>"><?= $id19 ?></td>
        <td id="id20" class="<?= $class20 ?>" data-dat="<?= $dat20 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate20 ?>"><?= $id20 ?></td>
        <td id="id21" class="<?= $class21 ?>" data-dat="<?= $dat21 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate21 ?>"><?= $id21 ?></td>
    </tr>
    <tr> <!--4 неделя-->
        <td id="id22" class="<?= $class22 ?>" data-dat="<?= $dat22 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate22 ?>"><?= $id22 ?></td>
        <td id="id23" class="<?= $class23 ?>" data-dat="<?= $dat23 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate23 ?>"><?= $id23 ?></td>
        <td id="id24" class="<?= $class24 ?>" data-dat="<?= $dat24 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate24 ?>"><?= $id24 ?></td>
        <td id="id25" class="<?= $class25 ?>" data-dat="<?= $dat25 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate25 ?>"><?= $id25 ?></td>
        <td id="id26" class="<?= $class26 ?>" data-dat="<?= $dat26 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate26 ?>"><?= $id26 ?></td>
        <td id="id27" class="<?= $class27 ?>" data-dat="<?= $dat27 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate27 ?>"><?= $id27 ?></td>
        <td id="id28" class="<?= $class28 ?>" data-dat="<?= $dat28 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate28 ?>"><?= $id28 ?></td>
    </tr>
    <tr> <!--5 неделя-->
        <td id="id29" class="<?= $class29 ?>" data-dat="<?= $dat29 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate29 ?>"><?= $id29 ?></td>
        <td id="id30" class="<?= $class30 ?>" data-dat="<?= $dat30 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate30 ?>"><?= $id30 ?></td>
        <td id="id31" class="<?= $class31 ?>" data-dat="<?= $dat31 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate31 ?>"><?= $id31 ?></td>
        <td id="id32" class="<?= $class32 ?>" data-dat="<?= $dat32 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate32 ?>"><?= $id32 ?></td>
        <td id="id33" class="<?= $class33 ?>" data-dat="<?= $dat33 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate33 ?>"><?= $id33 ?></td>
        <td id="id34" class="<?= $class34 ?>" data-dat="<?= $dat34 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate34 ?>"><?= $id34 ?></td>
        <td id="id35" class="<?= $class35 ?>" data-dat="<?= $dat35 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate35 ?>"><?= $id35 ?></td>
    </tr>
    <tr> <!--6 неделя-->
        <td id="id36" class="<?= $class36 ?>" data-dat="<?= $dat36 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate36 ?>"><?= $id36 ?></td>
        <td id="id37" class="<?= $class37 ?>" data-dat="<?= $dat37 ?>" data-id_child="<?= $id ?>" data-itemdate="<?= $itemdate37 ?>"><?= $id37 ?></td>
        <td class="table_sp_color_holidays"></td>
        <td class="table_sp_color_holidays"></td>
        <td class="table_sp_color_holidays"></td>
        <td class="table_sp_color_holidays"></td>
        <td class="table_sp_color_holidays"></td>
    </tr>

</table>

<?php
//////////////////////////////////////////////
Modal::begin([
    'id' => 'modal_cal_555',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
foreach($array_reason12_555 as $key  => $value){
    echo
        '<div class="item_reason_in_dialog_sp_cal not_selected_text_on_block" data-reason_cal="'.$key.'" data-todayinik="0">
    <li>'.($value).'</li>
</div>';
}
Modal::end();

///////////////////////////////////////////////
Modal::begin([
    'id' => 'modal_cal_not',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Отметка невозможна</div>
</div>';
//debug($data_item.' item date');
//debug(date("Y-m-d").' today');
//debug(verifi_date_today($data_item,$model_d_date,$model_d_antidate));
Modal::end();

////////////////////////////////////////////////
Modal::begin([
    'id' => 'modal_cal',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
foreach($array_reason12 as $key  => $value){
    echo
        '<div class="item_reason_in_dialog_sp_cal not_selected_text_on_block" data-reason_cal="'.$key.'" data-todayinik="0">
    <li>'.($value).'</li>
</div>';
}
Modal::end();

///////////////////////////////////////////////////

Modal::begin([
    'id' => 'modal_cal_today',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
foreach($array_reason_today as $key  => $value){
    echo
        '<div class="item_reason_in_dialog_sp_cal not_selected_text_on_block" data-reason_cal="'.$key.'" data-todayinik="1">
    <li>'.($value).'</li>
</div>';
}
Modal::end();

///////////////////////////////////////////////////

Modal::begin([
    'id' => 'modal_cal_closed_tabel',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Выберите причину:</h3>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="not_selected_text_on_block">
    <div class="alert alert-danger" role="alert">Отметка невозможна, табель закрыт!</div>
</div>';
Modal::end();

///////////////////////////////////////////////////



Modal::begin([
    'id' => 'modal_cal2',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Причина пропуска:</h3>',
    'footer' => '<button type="button" class="btn btn-danger btn-md" id="but_del_otmetka_sp">Удалить отметку</button>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="rrrrrrrr not_selected_text_on_block"><li></li></div>';
Modal::end();

Modal::begin([
    'id' => 'modal_cal2_not_delete',
    'header' => '<h3 style="padding-left: 10px" class="not_selected_text_on_block">Причина пропуска:</h3>',
//    'footer' => '<button type="button" class="btn btn-danger btn-md" id="but_del_otmetka_sp">Удалить отметку</button>',
//    'header' => '<h3 style="padding-left: 10px">Не приду совсем:</h3>',
//    'size' => Modal::SIZE_SMALL,
]);
echo '<div class="rrrrrrrr not_selected_text_on_block"><li></li></div>';
Modal::end();



?>
<?php
$script = <<<JS

//            var form5 = $('form').serializeArray();
////            console.log(form5);
//
//            dict = {};
//
//            $(form5).each(function(i, field){
//                dict[field.name] = field.value;
//            });



$(function(){




//$('[data-toggle="popover"]').popover({placement : 'top'});
function verifitoday(itemdate){
        var now2 = new Date(itemdate);
        var getyear2 = now2.getFullYear();
        var getmonth2 = now2.getMonth();
        var getday2 = now2.getDate();

        var date = $("#dp_sp").val();
        var d=new Date(date.split(".").reverse().join("-"));
        var dd=d.getDate();
        var mm=d.getMonth();
        var yy=d.getFullYear();
//        var newdate=yy+"-"+mm+"-"+dd;

        if(yy==getyear2&&mm==getmonth2&&dd==getday2){
            return true;
        }else{
            return false;
        }
}

var id_child,form,veruyu,itemdate,todayinik,data = '';

    $('td:not(.table_sp_color_holidays)').on('click',function(){
        var val_in_td = $(this).text();
        var name = $('.item_child_sp').text();
        data = $(this).data('dat');
        veruyu = $('#id_table_sp_veruyu').data('veruyu');



        if(val_in_td.length > 0 && name.length > 0){
            id_child = $(this).data('id_child');
            itemdate = $(this).data('itemdate');

//            console.log(DateDateDate(itemdate));
            console.log(veruyu+' veruyu');

            if(data.length > 0){

                $("#id_form_datepicker").val("7");
                form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                form.push({name:'id_child',value:id_child});
                form.push({name:'itemdate',value:itemdate});
//                console.log(form);
                console.log('отметка уже есть');
//                console.log(form);return
                $('.rrrrrrrr li').text(data);
                var arr = $('#form_sp_item_date');
                $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(res) {
                    console.log('yes не пустая дата');
                    console.log(res+' res');

                    if(veruyu > 500){//555
                        switch (res){
                            case 3:
                            $('#modal_cal2_not_delete').modal('show');
                            break;
                            default :
                            $('#modal_cal2').modal('show');
                            break;
                        }
                    }else{
                        switch (res){
                            case 0:
                            case 4:
                            case 1:
                            $('#modal_cal2_not_delete').modal('show');
                            break;
                            default:
                            $('#modal_cal2').modal('show');
                            break;
                        }
                    }




                }).fail(function() {
                    console.log('not не пустая дата');
    //                alert("Ошибка");
                });
                return false;

//                console.log(data);
            }else{
                console.log('можно сделать отметку');
                console.log(data);
                $("#id_form_datepicker").val("7");

                form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                form.push({name:'id_child',value:id_child});
                form.push({name:'itemdate',value:itemdate});
//                console.log(form);return
//                $(".block_calendar_sp_content2").LoadingOverlay("show");
                var arr = $('#form_sp_item_date');
                $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(res) {
                    console.log('yes пустая дата');
                    console.log(res+' res');
                    if(veruyu > 500){//555
                        switch (res){
                            case 3:
                            $('#modal_cal_not').modal('show');
                            break;
                            default :
                            $('#modal_cal_555').modal('show');
                            break;
                        }
                    }else{
                        switch (res){
                            case 0:
                            $('#modal_cal_not').modal('show');
                            break;
                            case 1:
                            $('#modal_cal_today').modal('show');
                            break;
                            case 4:
                            $('#modal_cal_closed_tabel').modal('show');
                            break;
                            default:
                            $('#modal_cal').modal('show');
                            break;
                        }
                    }
//                    alert(response);
                }).fail(function() {
//                  $("*").LoadingOverlay("hide");
                    console.log('not пустая дата');
    //                alert("Ошибка");
                });
//                $('#modal_cal').modal('show');//dialog window for insert
                return false;
            }
        }else if(val_in_td.length > 0 && !name.length > 0){
            alert('Чтобы сделать отметку - выберите ребенка, нажав на календарик')
        }
        return false;
    });




    $('.item_reason_in_dialog_sp_cal').click(function(){
        $('.modal').modal('hide');
//        $('.modal').data('bs.modal',null);
//$('#modal_cal2').modal('hide');
//$('#modal_cal_555').modal('hide');
//$('#modal_cal_today').modal('hide');
//$('#modal_cal').modal('hide');

        var reason = $(this).data('reason_cal');
        todayinik = $(this).data('todayinik');
        $("#id_form_datepicker").val("6");
        form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
        form.push({name:'id_child',value:id_child});
        form.push({name:'itemdate',value:itemdate});
        console.log(todayinik);
//        return;
//        reason = '- '+reason;
        form.push({name: "reason", value: reason});
        console.log(form);
        var arr = $('#form_sp_item_date');
//		$('.modal').modal('hide');
        $(".block_calendar_sp_content2").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
//		$('.modal').modal('hide');
        $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
//            $('.modal').modal('hide');
                $("*").LoadingOverlay("hide");

                console.log('yes');
//                $('#only_spiski').html(response);
//                $('#_calendar').html(response);


                if(verifitoday(itemdate)){
                $("#id_form_datepicker").val("11");
                form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                form.push({name:'id_child',value:id_child});
                $("#only_spiski").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    //            return;
                $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
//                    $('.modal').modal('hide');
                    console.log('yes');
                    $('#only_spiski').html(response);
    //                $('#_calendar').html(response);

    //                $('#table_cal_sp').html(response);
                }).fail(function() {
//                    $('.modal').modal('hide');
                    $("*").LoadingOverlay("hide");
                    console.log('not');
                    alert("Ошибка");
                });
                }

                $('#table_cal_sp').html(response);
            }).fail(function() {
                $('.modal').modal('hide');
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });


//		$('.modal').modal('hide');



        return false;
    });

    $('#but_del_otmetka_sp').click(function(){
    $('.modal').modal('hide');
//    $('.modal').data('bs.modal',null);
//    $('#modal_cal2').modal('hide');
//$('#modal_cal_555').modal('hide');
//$('#modal_cal_today').modal('hide');
//$('#modal_cal').modal('hide');
//        $('.modal').modal('hide');
        $("#id_form_datepicker").val("5");
        var reason = $('.rrrrrrrr li').text();
        if(reason.indexOf('ОПОЗД')>0||reason.indexOf('ПРИДУ')>0)reason=1;
        form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
        form.push({name:'id_child',value:id_child});
        form.push({name:'itemdate',value:itemdate});
        form.push({name:'reason',value:reason});
        console.log(form);
        var arr = $('#form_sp_item_date');
//		$('.modal').modal('hide');
//        console.log(data.indexOf('ОПОЗД'));
//        console.log(data.indexOf('СОВСЕМ'));
//		$('.modal').modal('hide');
//        return;
        $(".block_calendar_sp_content2").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
        $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : form
            }).done(function(response) {
//            $('.modal').modal('hide');
                $("*").LoadingOverlay("hide");

                console.log('yes');
//                $('#only_spiski').html(response);
//                $('#_calendar').html(response);


                if(verifitoday(itemdate)){
                $("#id_form_datepicker").val("11");
                form = $('#form_sp_item_group,#form_sp_item_date').serializeArray();
                form.push({name:'id_child',value:id_child});
                $("#only_spiski").LoadingOverlay("show",{image:""});$('#anim_loader').LoadingOverlay("show");
    //            return;
                $.ajax({
                    type : arr.attr('method'),
                    url : arr.attr('action'),
                    data : form
                }).done(function(response) {
                    $("*").LoadingOverlay("hide");
//                    $('.modal').modal('hide');
                    console.log('yes');
                    $('#only_spiski').html(response);
    //                $('#_calendar').html(response);

    //                $('#table_cal_sp').html(response);
                }).fail(function() {
//                    $('.modal').modal('hide');
                    $("*").LoadingOverlay("hide");
                    console.log('not');
                    alert("Ошибка");
                });
                }

                $('#table_cal_sp').html(response);
            }).fail(function() {
                $('.modal').modal('hide');
                $("*").LoadingOverlay("hide");
                console.log('not');
                alert("Ошибка");
            });

//			$('.modal').modal('hide');


            return false;
    });

    $('.item_month_calendar1').on('click',function(){
        $('.modal').modal('hide');
        $("#id_form_datepicker").val("999");
        var arr = $('#form_sp_item_date');
//        console.log(arr.serializeArray());return;
        $.ajax({
                type : arr.attr('method'),
                url : arr.attr('action'),
                data : arr.serializeArray()
            }).done(function(response) {
                console.log('yes brut');
                console.log(response);

//                $('#only_spiski').html(response);
//                $('#_calendar').html(response);
//                $("*").LoadingOverlay("hide");
                alert(response);
            }).fail(function() {
//                $("*").LoadingOverlay("hide");
                console.log('not brut');
//                alert("Ошибка");
            });

        return false;
    });
})
JS;
$this->registerJs($script,yii\web\View::POS_END);
?>