<?php
use yii\helpers\ArrayHelper;

/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 22.10.19
 * Time: 20:20
 */

//debug($model);
//exit;
$MonthNamesRus=array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");

//vul($this);
//change_db_attr();

$year = 2019;
$month = 10;
$id_group = 6;
$count_days_all = date('t',strtotime(date($year.'-'.$month.'-01')));
$monthwithzero = date('m',strtotime(date($year.'-'.$month.'-'.$count_days_all)));

$last_day_up_right = date($count_days_all.'.'.$monthwithzero.'.'.$year);

$model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
$model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

/*if(empty($model_d_date) || !in_array(date('Y-01-01'),$model_d_date)){
    $model_d_date_query = "select date_hol from holidays";
    $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
    $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');

    $json_array_dates = json_encode($model_d_date);
    $cookie = new \yii\web\Cookie([
        'name' => 'array_dates',
        'value' => $json_array_dates,
    ]);
    Yii::$app->response->cookies->add($cookie);
}*/
//$last_work_day = date($year.'-'.$monthwithzero.'-'.$count_days_all);//последний рабочий день
//while((date('N', strtotime($last_work_day)) > 5) || (in_array($last_work_day,$model_d_date))){
//    $last_work_day = date('Y-m-d',strtotime($last_work_day. " - 1 day"));
//}
$iter_date = date($year.'-'.$monthwithzero.'-'.$count_days_all);
$last_works_day = $iter_date;
$count_works_days = 0;
while($iter_date >= date($year.'-'.$monthwithzero.'-01')){
    if((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))){
        $count_works_days++;
    }
    $iter_date = date('Y-m-d',strtotime($iter_date. " - 1 day"));
}
while((date('N', strtotime($iter_date)) > 5) || (in_array($iter_date,$model_d_date))){
    $last_works_day = date('Y-m-d',strtotime($last_works_day. " - 1 day"));
}

switch($count_works_days){
    case 1:
    case 21:
    case 31:
        $count_works_days = $count_works_days.' день';
        break;
    case 2:
    case 3:
    case 4:
    case 22:
    case 23:
    case 24:
        $count_works_days = $count_works_days.' дня';
        break;
    default:
        $count_works_days = $count_works_days.' дней';
        break;
}







//$last_work_day = (date('j',strtotime($last_work_day)));

$dataProvider = new \yii\data\SqlDataProvider([
    'sql' => 'SET NOCOUNT ON; EXEC Tabel_site_deti @year=:year, @mon=:mon, @idgr=:idgr',//Tabel_site_deti табель для бухгалтерии
    'params' => [
        ':year' => $year,
        ':mon'  => $month,
        ':idgr'  => $id_group
    ],
    'pagination' => false,
    'sort' => false,
]);
$model = $dataProvider->getModels();
$model555 = ArrayHelper::index($model,'id5');//полный массив из базы

$model = $model555;
//$a2 = $model;
//$a3 = $model;
foreach($model as $g){
    array_push($model,$g);
}





$query_years_of_deti = "select distinct(year(rozd))[year] from deti
where id_gruppa = {$id_group} and (({$month}>=month([in]) and {$year}>=year([in])) or {$year}>year([in]))
and ((({$month}<=month([out]) and {$year}<=year([out])) or {$year} < year([out])) or [out] is null)";
$array_years_of_deti = Yii::$app->db->createCommand($query_years_of_deti)->queryAll();
$array_years_of_deti = ArrayHelper::getColumn($array_years_of_deti,'year');

$first_v = date('Y') - ($array_years_of_deti[0]);
if (count($array_years_of_deti)>1){
    $last_v = date('Y') - (array_pop($array_years_of_deti));
    if ($last_v<5&&$first_v<5){
        $vozrast = $last_v.'-'.$first_v.' года';
    }else{
        $vozrast = $last_v.'-'.$first_v.' лет';
    }
}else{
    if ($first_v<5){
        $vozrast = $first_v.' года';
    }else{
        $vozrast = $first_v.' лет';
    }
}




$width_col = 2.8;//good
$height_row_up = 13.9;
$height_row_st = 30.5;
$height_row_shapka1 = 27;
$height_row_shapka2 = 28.5;
$height_row_shapka3 = 24.75;
$height_row_shapka_down = 12.5;
$style_border_bottom = array(
    'borders' => array(
        'bottom' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
//    'fill' => array(
//        'type' => PHPExcel_Style_Fill::FILL_SOLID,
//        'startcolor' => array(
//            'argb' => 'FFFF0000',
//        ),
//    ),
);
$style_border_all = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
);
$allign_center_all = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
);
$allign_up_right = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_left = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$allign_up_center = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
    ),
);
$xls = new PHPExcel();

$xls->setActiveSheetIndex(0);
$sheet = $xls->getActiveSheet()->setTitle('Лист1');
$sheet->getDefaultStyle()->getFont()->setSize(9);
$sheet->getDefaultStyle()->getFont()->setName('Arial');
$sheet->getSheetView()->setZoomScale(70);
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setPrintArea('B1:AO29');
//$sheet->setBreak('AO1',PHPExcel_Worksheet::BREAK_COLUMN);
$sheet->setBreak('B29',PHPExcel_Worksheet::BREAK_ROW);
$sheet->getPageMargins()->setTop(0);
$sheet->getPageMargins()->setBottom(0);
$sheet->getPageMargins()->setLeft(0.5);
$sheet->getPageMargins()->setRight(0);


$sheet->getColumnDimension('A')->setWidth(1)->setVisible(false);
$sheet->getColumnDimension('B')->setWidth(4.3);
$sheet->getColumnDimension('C')->setWidth(17.3);
$sheet->getColumnDimension('D')->setWidth(9);
$sheet->getColumnDimension('E')->setWidth(9);
$sheet->getColumnDimension('F')->setWidth($width_col);
$sheet->getColumnDimension('G')->setWidth($width_col);
$sheet->getColumnDimension('H')->setWidth($width_col);
$sheet->getColumnDimension('I')->setWidth($width_col);
$sheet->getColumnDimension('J')->setWidth($width_col);
$sheet->getColumnDimension('K')->setWidth($width_col);
$sheet->getColumnDimension('L')->setWidth($width_col);
$sheet->getColumnDimension('M')->setWidth($width_col);
$sheet->getColumnDimension('N')->setWidth($width_col);
$sheet->getColumnDimension('O')->setWidth($width_col);
$sheet->getColumnDimension('P')->setWidth($width_col);
$sheet->getColumnDimension('Q')->setWidth($width_col);
$sheet->getColumnDimension('R')->setWidth($width_col);
$sheet->getColumnDimension('S')->setWidth($width_col);
$sheet->getColumnDimension('T')->setWidth($width_col);
$sheet->getColumnDimension('U')->setWidth($width_col);
$sheet->getColumnDimension('V')->setWidth($width_col);
$sheet->getColumnDimension('W')->setWidth($width_col);
$sheet->getColumnDimension('X')->setWidth($width_col);
$sheet->getColumnDimension('Y')->setWidth($width_col);
$sheet->getColumnDimension('Z')->setWidth($width_col);
$sheet->getColumnDimension('AA')->setWidth($width_col);
$sheet->getColumnDimension('AB')->setWidth($width_col);
$sheet->getColumnDimension('AC')->setWidth($width_col);
$sheet->getColumnDimension('AD')->setWidth($width_col);
$sheet->getColumnDimension('AE')->setWidth($width_col);
$sheet->getColumnDimension('AF')->setWidth($width_col);
$sheet->getColumnDimension('AG')->setWidth($width_col);
$sheet->getColumnDimension('AH')->setWidth($width_col);
$sheet->getColumnDimension('AI')->setWidth($width_col);
$sheet->getColumnDimension('AJ')->setWidth($width_col);
$sheet->getColumnDimension('AK')->setWidth(8);
$sheet->getColumnDimension('AL')->setWidth(9);
$sheet->getColumnDimension('AM')->setWidth(7);
$sheet->getColumnDimension('AN')->setWidth(7);
$sheet->getColumnDimension('AO')->setWidth(12);

$i = 1;
while($i<=9){
    $sheet->getRowDimension($i)->setRowHeight($height_row_up);
    $i++;
}
//$sheet->getRowDimension("1")->setRowHeight($height_row_up);
//$sheet->getRowDimension("2")->setRowHeight($height_row_up);
//$sheet->getRowDimension("3")->setRowHeight($height_row_up);
//$sheet->getRowDimension("4")->setRowHeight($height_row_up);
//$sheet->getRowDimension("5")->setRowHeight($height_row_up);
//$sheet->getRowDimension("6")->setRowHeight($height_row_up);
//$sheet->getRowDimension("7")->setRowHeight($height_row_up);
//$sheet->getRowDimension("8")->setRowHeight($height_row_up);
//$sheet->getRowDimension("9")->setRowHeight($height_row_up);
$sheet->getRowDimension("10")->setRowHeight($height_row_shapka1);
$sheet->getRowDimension("11")->setRowHeight($height_row_shapka2);
$sheet->getRowDimension("12")->setRowHeight($height_row_shapka3);


$line = 1;
$sheet->setCellValue("F{$line}", 'ТАБЕЛЬ');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(10);

$line++;//2
$sheet->setCellValue("F{$line}", 'УЧЕТА ПОСЕЩАЕМОСТИ ДЕТЕЙ');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(10);
$sheet->setCellValue("AO{$line}", 'КОДЫ')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//3
$sheet->setCellValue("F{$line}", 'за '.$_monthsList[$month].' '.$year.' г.');
$sheet->mergeCells("F{$line}:AJ{$line}");
$sheet->getStyle("F{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$sheet->getStyle("F{$line}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle("F{$line}")->getFont()->setBold(true);
$sheet->getStyle("F{$line}")->getFont()->setSize(10);
$sheet->setCellValue("AM{$line}", 'Форма по ОКУД  ')->mergeCells("AM{$line}:AN{$line}");
$sheet->getStyle("AM{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", '504608')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//4
$sheet->setCellValue("C{$line}", 'Учреждение');
$sheet->setCellValue("D{$line}", 'муниципальное казенное дошкольное образовательное учреждение "Детский сад № '.$number_sad.'" города '.trim($ncity));
$sheet->mergeCells("D{$line}:AM{$line}");
$sheet->getStyle("D{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AN{$line}", 'Дата  ');
$sheet->getStyle("AN{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", $last_day_up_right)->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);;

$line++;//5
$sheet->setCellValue("C{$line}", 'Структурное подразделение');
$sheet->mergeCells("C{$line}:D{$line}");
$sheet->setCellValue("E{$line}", 'Группа '.$id_group.' возраст '.$vozrast);
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AN{$line}", 'по ОКПО  ');
$sheet->getStyle("AN{$line}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$sheet->setCellValue("AO{$line}", '13691336')->getStyle("AO{$line}")->applyFromArray($style_border_all)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$line++;//6
$sheet->setCellValue("C{$line}", 'Вид расчета');
$sheet->setCellValue("E{$line}", $count_works_days);//кол-во раб. дней
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AO{$line}", '')->getStyle("AO{$line}")->applyFromArray($style_border_all);

$line++;//7
$sheet->setCellValue("C{$line}", 'Режим работы');
$sheet->setCellValue("E{$line}", '12 час');//кол-во раб. дней
$sheet->mergeCells("E{$line}:AM{$line}");
$sheet->getStyle("E{$line}:AM{$line}")->applyFromArray($style_border_bottom);
$sheet->setCellValue("AO{$line}", '')->getStyle("AO{$line}")->applyFromArray($style_border_all);

$line+=3;//10
$line3 = $line+2;
$sheet->setCellValue("B{$line}", "№№\nп/п");
$sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("B{$line}:B{$line3}");
$sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
$sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);

$sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
$sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("C{$line}:C{$line3}");

$sheet->setCellValue("D{$line}", "Номер\nсчета");
$sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("D{$line}:D{$line3}");

$sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
$sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("E{$line}:E{$line3}");

$sheet->setCellValue("F{$line}", "Дни посещения");
$sheet->mergeCells("F{$line}:AJ{$line}");

$sheet->setCellValue("AK{$line}", "Номер\nсчета");
$sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AK{$line}:AK{$line3}");

$sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
$sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AL{$line}:AL{$line3}");

$sheet->setCellValue("AM{$line}", "Пропущено дней");
$sheet->mergeCells("AM{$line}:AN{$line}");

$sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
$sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AO{$line}:AO{$line3}");

$line++;//11
$line2=$line+1;
$sheet->setCellValue("F{$line}", "1");
$sheet->mergeCells("F{$line}:F{$line2}");

$sheet->setCellValue("G{$line}", "2");
$sheet->mergeCells("G{$line}:G{$line2}");

$sheet->setCellValue("H{$line}", "3");
$sheet->mergeCells("H{$line}:H{$line2}");

$sheet->setCellValue("I{$line}", "4");
$sheet->mergeCells("I{$line}:I{$line2}");

$sheet->setCellValue("J{$line}", "5");
$sheet->mergeCells("J{$line}:J{$line2}");

$sheet->setCellValue("K{$line}", "6");
$sheet->mergeCells("K{$line}:K{$line2}");

$sheet->setCellValue("L{$line}", "7");
$sheet->mergeCells("L{$line}:L{$line2}");

$sheet->setCellValue("M{$line}", "8");
$sheet->mergeCells("M{$line}:M{$line2}");

$sheet->setCellValue("N{$line}", "9");
$sheet->mergeCells("N{$line}:N{$line2}");

$sheet->setCellValue("O{$line}", "10");
$sheet->mergeCells("O{$line}:O{$line2}");

$sheet->setCellValue("P{$line}", "11");
$sheet->mergeCells("P{$line}:P{$line2}");

$sheet->setCellValue("Q{$line}", "12");
$sheet->mergeCells("Q{$line}:Q{$line2}");

$sheet->setCellValue("R{$line}", "13");
$sheet->mergeCells("R{$line}:R{$line2}");

$sheet->setCellValue("S{$line}", "14");
$sheet->mergeCells("S{$line}:S{$line2}");

$sheet->setCellValue("T{$line}", "15");
$sheet->mergeCells("T{$line}:T{$line2}");

$sheet->setCellValue("U{$line}", "16");
$sheet->mergeCells("U{$line}:U{$line2}");

$sheet->setCellValue("V{$line}", "17");
$sheet->mergeCells("V{$line}:V{$line2}");

$sheet->setCellValue("W{$line}", "18");
$sheet->mergeCells("W{$line}:W{$line2}");

$sheet->setCellValue("X{$line}", "19");
$sheet->mergeCells("X{$line}:X{$line2}");

$sheet->setCellValue("Y{$line}", "20");
$sheet->mergeCells("Y{$line}:Y{$line2}");

$sheet->setCellValue("Z{$line}", "21");
$sheet->mergeCells("Z{$line}:Z{$line2}");

$sheet->setCellValue("AA{$line}", "22");
$sheet->mergeCells("AA{$line}:AA{$line2}");

$sheet->setCellValue("AB{$line}", "23");
$sheet->mergeCells("AB{$line}:AB{$line2}");

$sheet->setCellValue("AC{$line}", "24");
$sheet->mergeCells("AC{$line}:AC{$line2}");

$sheet->setCellValue("AD{$line}", "25");
$sheet->mergeCells("AD{$line}:AD{$line2}");

$sheet->setCellValue("AE{$line}", "26");
$sheet->mergeCells("AE{$line}:AE{$line2}");

$sheet->setCellValue("AF{$line}", "27");
$sheet->mergeCells("AF{$line}:AF{$line2}");

$sheet->setCellValue("AG{$line}", "28");
$sheet->mergeCells("AG{$line}:AG{$line2}");

$sheet->setCellValue("AH{$line}", "29");
$sheet->mergeCells("AH{$line}:AH{$line2}");

$sheet->setCellValue("AI{$line}", "30");
$sheet->mergeCells("AI{$line}:AI{$line2}");

$sheet->setCellValue("AJ{$line}", "31");
$sheet->mergeCells("AJ{$line}:AJ{$line2}");

$sheet->setCellValue("AM{$line}", "всего");
$sheet->mergeCells("AM{$line}:AM{$line2}");

$sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
$sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
$sheet->mergeCells("AN{$line}:AN{$line2}");


/*****************************************************************/
$arr_last_row = array_pop($model);//последняя итоговая строка вырезана из массива в переменную $arr_last_row

$model = array_slice($model,0,36,true);



if (count($model)>15){
    $arr_first15 = array_slice($model,0,15,true);//первые 15 строк массива
    if (count($model)>33){
        $arr_15_33 = array_slice($model,15,18,true);//18 rows after 15
        $arr_page3 = ArrayHelper::index((array_slice($model,33)),'id5');//на остатке все что после 33
    }else{
        $arr_15_33 = ArrayHelper::index((array_slice($model,15)),'id5');//на остатке все что после 15
    }
}else{
    $arr_first15 = $model;
}
$line++;//12
foreach($arr_first15 as $abc){
    $line++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_st);
    $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

    $sheet->setCellValue("B{$line}", trim($abc['id5']));
    $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

    if (mb_strlen(trim($abc['name']))>24){
        $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
        $sheet->setCellValue("C{$line}", $rrr);
    }else{
        $sheet->setCellValue("C{$line}", trim($abc['name']));
    }

    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

    $sheet->setCellValue("D{$line}", trim($abc['number']));
    $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

    $sheet->setCellValue("E{$line}", '');
    $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

    $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

    $sheet->setCellValue("F{$line}", trim($abc[' 1 ']));
    $sheet->setCellValue("G{$line}", trim($abc[' 2 ']));
    $sheet->setCellValue("H{$line}", trim($abc[' 3 ']));
    $sheet->setCellValue("I{$line}", trim($abc[' 4 ']));
    $sheet->setCellValue("J{$line}", trim($abc[' 5 ']));
    $sheet->setCellValue("K{$line}", trim($abc[' 6 ']));
    $sheet->setCellValue("L{$line}", trim($abc[' 7 ']));
    $sheet->setCellValue("M{$line}", trim($abc[' 8 ']));
    $sheet->setCellValue("N{$line}", trim($abc[' 9 ']));
    $sheet->setCellValue("O{$line}", trim($abc[' 10']));
    $sheet->setCellValue("P{$line}", trim($abc[' 11']));
    $sheet->setCellValue("Q{$line}", trim($abc[' 12']));
    $sheet->setCellValue("R{$line}", trim($abc[' 13']));
    $sheet->setCellValue("S{$line}", trim($abc[' 14']));
    $sheet->setCellValue("T{$line}", trim($abc[' 15']));
    $sheet->setCellValue("U{$line}", trim($abc[' 16']));
    $sheet->setCellValue("V{$line}", trim($abc[' 17']));
    $sheet->setCellValue("W{$line}", trim($abc[' 18']));
    $sheet->setCellValue("X{$line}", trim($abc[' 19']));
    $sheet->setCellValue("Y{$line}", trim($abc[' 20']));
    $sheet->setCellValue("Z{$line}", trim($abc[' 21']));
    $sheet->setCellValue("AA{$line}", trim($abc[' 22']));
    $sheet->setCellValue("AB{$line}", trim($abc[' 23']));
    $sheet->setCellValue("AC{$line}", trim($abc[' 24']));
    $sheet->setCellValue("AD{$line}", trim($abc[' 25']));
    $sheet->setCellValue("AE{$line}", trim($abc[' 26']));
    $sheet->setCellValue("AF{$line}", trim($abc[' 27']));
    $sheet->setCellValue("AG{$line}", trim($abc[' 28']));
    $sheet->setCellValue("AH{$line}", trim($abc[' 29']));
    $sheet->setCellValue("AI{$line}", trim($abc[' 30']));
    $sheet->setCellValue("AJ{$line}", trim($abc[' 31']));
    $sheet->setCellValue("AK{$line}", trim($abc['number1']));
    $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
    $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
    $sheet->setCellValue("AN{$line}", trim($abc['popo']));
    $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
}




/*вторая шапка*/

//$sheet->getPageSetup()->setRowsToRepeatAtTop(10-12);
if (count($model)>15){
    $line++;//28
    $line++;//29
    $row1shapka2 = $line;
    $row1shapka2++;
    $row1shapka3 = $row1shapka2;
    $row1shapka3++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $sheet->getRowDimension($row1shapka2)->setRowHeight($height_row_shapka2);
    $sheet->getRowDimension($row1shapka3)->setRowHeight($height_row_shapka3);
    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);

    $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("C{$line}:C{$line3}");

    $sheet->setCellValue("D{$line}", "Номер\nсчета");
    $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");

    $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
    $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("E{$line}:E{$line3}");

    $sheet->setCellValue("F{$line}", "Дни посещения");
    $sheet->mergeCells("F{$line}:AJ{$line}");

    $sheet->setCellValue("AK{$line}", "Номер\nсчета");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");

    $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
    $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AL{$line}:AL{$line3}");

    $sheet->setCellValue("AM{$line}", "Пропущено дней");
    $sheet->mergeCells("AM{$line}:AN{$line}");

    $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
    $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AO{$line}:AO{$line3}");

    $line++;//30
    $line2=$line+1;//31
    $sheet->setCellValue("F{$line}", "1");
    $sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "2");
    $sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "3");
    $sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "4");
    $sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "5");
    $sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "6");
    $sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "7");
    $sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "8");
    $sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "9");
    $sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "10");
    $sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "11");
    $sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "12");
    $sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "13");
    $sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "14");
    $sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "15");
    $sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "16");
    $sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "17");
    $sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "18");
    $sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "19");
    $sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "20");
    $sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "21");
    $sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "22");
    $sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "23");
    $sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "24");
    $sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "25");
    $sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "26");
    $sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "27");
    $sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "28");
    $sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "29");
    $sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "30");
    $sheet->mergeCells("AI{$line}:AI{$line2}");
    $sheet->setCellValue("AJ{$line}", "31");
    $sheet->mergeCells("AJ{$line}:AJ{$line2}");
    $sheet->setCellValue("AM{$line}", "всего");
    $sheet->mergeCells("AM{$line}:AM{$line2}");

    $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
    $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AN{$line}:AN{$line2}");

    $line++;//31
//    $line++;//32

//    if (isset($arr_15_33)){
//
//    }
    foreach($arr_15_33 as $abc){//2 страница ********************************************** 2
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

        $sheet->setCellValue("B{$line}", trim($abc['id5']));
        $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

        if (mb_strlen(trim($abc['name']))>24){
            $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
            $sheet->setCellValue("C{$line}", $rrr);
        }else{
            $sheet->setCellValue("C{$line}", trim($abc['name']));
        }

        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

        $sheet->setCellValue("D{$line}", trim($abc['number']));
        $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

        $sheet->setCellValue("E{$line}", '');
        $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

        $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

        $sheet->setCellValue("F{$line}", trim($abc[' 1 ']));
        $sheet->setCellValue("G{$line}", trim($abc[' 2 ']));
        $sheet->setCellValue("H{$line}", trim($abc[' 3 ']));
        $sheet->setCellValue("I{$line}", trim($abc[' 4 ']));
        $sheet->setCellValue("J{$line}", trim($abc[' 5 ']));
        $sheet->setCellValue("K{$line}", trim($abc[' 6 ']));
        $sheet->setCellValue("L{$line}", trim($abc[' 7 ']));
        $sheet->setCellValue("M{$line}", trim($abc[' 8 ']));
        $sheet->setCellValue("N{$line}", trim($abc[' 9 ']));
        $sheet->setCellValue("O{$line}", trim($abc[' 10']));
        $sheet->setCellValue("P{$line}", trim($abc[' 11']));
        $sheet->setCellValue("Q{$line}", trim($abc[' 12']));
        $sheet->setCellValue("R{$line}", trim($abc[' 13']));
        $sheet->setCellValue("S{$line}", trim($abc[' 14']));
        $sheet->setCellValue("T{$line}", trim($abc[' 15']));
        $sheet->setCellValue("U{$line}", trim($abc[' 16']));
        $sheet->setCellValue("V{$line}", trim($abc[' 17']));
        $sheet->setCellValue("W{$line}", trim($abc[' 18']));
        $sheet->setCellValue("X{$line}", trim($abc[' 19']));
        $sheet->setCellValue("Y{$line}", trim($abc[' 20']));
        $sheet->setCellValue("Z{$line}", trim($abc[' 21']));
        $sheet->setCellValue("AA{$line}", trim($abc[' 22']));
        $sheet->setCellValue("AB{$line}", trim($abc[' 23']));
        $sheet->setCellValue("AC{$line}", trim($abc[' 24']));
        $sheet->setCellValue("AD{$line}", trim($abc[' 25']));
        $sheet->setCellValue("AE{$line}", trim($abc[' 26']));
        $sheet->setCellValue("AF{$line}", trim($abc[' 27']));
        $sheet->setCellValue("AG{$line}", trim($abc[' 28']));
        $sheet->setCellValue("AH{$line}", trim($abc[' 29']));
        $sheet->setCellValue("AI{$line}", trim($abc[' 30']));
        $sheet->setCellValue("AJ{$line}", trim($abc[' 31']));
        $sheet->setCellValue("AK{$line}", trim($abc['number1']));
        $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
        $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
        $sheet->setCellValue("AN{$line}", trim($abc['popo']));
        $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
    }
}

/*3 шапка*/

//$sheet->getPageSetup()->setRowsToRepeatAtTop(10-12);
if (count($model)>33){
    $line++;//50
    $line++;//51

    $row1shapka2 = $line;
    $row1shapka2++;
    $row1shapka3 = $row1shapka2;
    $row1shapka3++;
    $sheet->getRowDimension($line)->setRowHeight($height_row_shapka1);
    $sheet->getRowDimension($row1shapka2)->setRowHeight($height_row_shapka2);
    $sheet->getRowDimension($row1shapka3)->setRowHeight($height_row_shapka3);

    $line3 = $line+2;
    $sheet->setCellValue("B{$line}", "№№\nп/п");
    $sheet->getStyle("B{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("B{$line}:B{$line3}");
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($allign_center_all);
    $sheet->getStyle("B{$line}:AO{$line3}")->applyFromArray($style_border_all);

    $sheet->setCellValue("C{$line}", "Фамилия, имя\nребенка");
    $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("C{$line}:C{$line3}");

    $sheet->setCellValue("D{$line}", "Номер\nсчета");
    $sheet->getStyle("D{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("D{$line}:D{$line3}");

    $sheet->setCellValue("E{$line}", "Плата\nпо став-\nке");
    $sheet->getStyle("E{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("E{$line}:E{$line3}");

    $sheet->setCellValue("F{$line}", "Дни посещения");
    $sheet->mergeCells("F{$line}:AJ{$line}");

    $sheet->setCellValue("AK{$line}", "Номер\nсчета");
    $sheet->getStyle("AK{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AK{$line}:AK{$line3}");

    $sheet->setCellValue("AL{$line}", "Дни\nпосеще-\nния, под-\nлежащие\nоплате");
    $sheet->getStyle("AL{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AL{$line}:AL{$line3}");

    $sheet->setCellValue("AM{$line}", "Пропущено дней");
    $sheet->mergeCells("AM{$line}:AN{$line}");

    $sheet->setCellValue("AO{$line}", "Причины\nнепосещения\n(основание)");
    $sheet->getStyle("AO{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AO{$line}:AO{$line3}");

    $line++;//
    $line2=$line+1;//
    $sheet->setCellValue("F{$line}", "1");
    $sheet->mergeCells("F{$line}:F{$line2}");
    $sheet->setCellValue("G{$line}", "2");
    $sheet->mergeCells("G{$line}:G{$line2}");
    $sheet->setCellValue("H{$line}", "3");
    $sheet->mergeCells("H{$line}:H{$line2}");
    $sheet->setCellValue("I{$line}", "4");
    $sheet->mergeCells("I{$line}:I{$line2}");
    $sheet->setCellValue("J{$line}", "5");
    $sheet->mergeCells("J{$line}:J{$line2}");
    $sheet->setCellValue("K{$line}", "6");
    $sheet->mergeCells("K{$line}:K{$line2}");
    $sheet->setCellValue("L{$line}", "7");
    $sheet->mergeCells("L{$line}:L{$line2}");
    $sheet->setCellValue("M{$line}", "8");
    $sheet->mergeCells("M{$line}:M{$line2}");
    $sheet->setCellValue("N{$line}", "9");
    $sheet->mergeCells("N{$line}:N{$line2}");
    $sheet->setCellValue("O{$line}", "10");
    $sheet->mergeCells("O{$line}:O{$line2}");
    $sheet->setCellValue("P{$line}", "11");
    $sheet->mergeCells("P{$line}:P{$line2}");
    $sheet->setCellValue("Q{$line}", "12");
    $sheet->mergeCells("Q{$line}:Q{$line2}");
    $sheet->setCellValue("R{$line}", "13");
    $sheet->mergeCells("R{$line}:R{$line2}");
    $sheet->setCellValue("S{$line}", "14");
    $sheet->mergeCells("S{$line}:S{$line2}");
    $sheet->setCellValue("T{$line}", "15");
    $sheet->mergeCells("T{$line}:T{$line2}");
    $sheet->setCellValue("U{$line}", "16");
    $sheet->mergeCells("U{$line}:U{$line2}");
    $sheet->setCellValue("V{$line}", "17");
    $sheet->mergeCells("V{$line}:V{$line2}");
    $sheet->setCellValue("W{$line}", "18");
    $sheet->mergeCells("W{$line}:W{$line2}");
    $sheet->setCellValue("X{$line}", "19");
    $sheet->mergeCells("X{$line}:X{$line2}");
    $sheet->setCellValue("Y{$line}", "20");
    $sheet->mergeCells("Y{$line}:Y{$line2}");
    $sheet->setCellValue("Z{$line}", "21");
    $sheet->mergeCells("Z{$line}:Z{$line2}");
    $sheet->setCellValue("AA{$line}", "22");
    $sheet->mergeCells("AA{$line}:AA{$line2}");
    $sheet->setCellValue("AB{$line}", "23");
    $sheet->mergeCells("AB{$line}:AB{$line2}");
    $sheet->setCellValue("AC{$line}", "24");
    $sheet->mergeCells("AC{$line}:AC{$line2}");
    $sheet->setCellValue("AD{$line}", "25");
    $sheet->mergeCells("AD{$line}:AD{$line2}");
    $sheet->setCellValue("AE{$line}", "26");
    $sheet->mergeCells("AE{$line}:AE{$line2}");
    $sheet->setCellValue("AF{$line}", "27");
    $sheet->mergeCells("AF{$line}:AF{$line2}");
    $sheet->setCellValue("AG{$line}", "28");
    $sheet->mergeCells("AG{$line}:AG{$line2}");
    $sheet->setCellValue("AH{$line}", "29");
    $sheet->mergeCells("AH{$line}:AH{$line2}");
    $sheet->setCellValue("AI{$line}", "30");
    $sheet->mergeCells("AI{$line}:AI{$line2}");
    $sheet->setCellValue("AJ{$line}", "31");
    $sheet->mergeCells("AJ{$line}:AJ{$line2}");
    $sheet->setCellValue("AM{$line}", "всего");
    $sheet->mergeCells("AM{$line}:AM{$line2}");

    $sheet->setCellValue("AN{$line}", "в том\nчисле\nзасчи-\nтыва-\nемых");
    $sheet->getStyle("AN{$line}")->getAlignment()->setWrapText(true);
    $sheet->mergeCells("AN{$line}:AN{$line2}");

    $line++;//
//    $line++;//

    foreach($arr_page3 as $abc){//2 страница ********************************************** 2
        $line++;
        $sheet->getRowDimension($line)->setRowHeight($height_row_st);
        $sheet->getStyle("B{$line}:AO{$line}")->applyFromArray($style_border_all);

        $sheet->setCellValue("B{$line}", trim($abc['id5']));
        $sheet->getStyle("B{$line}")->applyFromArray($allign_up_right);

        if (mb_strlen(trim($abc['name']))>24){
            $rrr = mb_substr(trim($abc['name']), 0, 13)." ".mb_substr(trim($abc['name']), 13);
            $sheet->setCellValue("C{$line}", $rrr);
        }else{
            $sheet->setCellValue("C{$line}", trim($abc['name']));
        }

        $sheet->getStyle("C{$line}")->getAlignment()->setWrapText(true);
        $sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);

        $sheet->setCellValue("D{$line}", trim($abc['number']));
        $sheet->getStyle("D{$line}")->applyFromArray($allign_up_center);

        $sheet->setCellValue("E{$line}", '');
        $sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);

        $sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

        $sheet->setCellValue("F{$line}", trim($abc[' 1 ']));
        $sheet->setCellValue("G{$line}", trim($abc[' 2 ']));
        $sheet->setCellValue("H{$line}", trim($abc[' 3 ']));
        $sheet->setCellValue("I{$line}", trim($abc[' 4 ']));
        $sheet->setCellValue("J{$line}", trim($abc[' 5 ']));
        $sheet->setCellValue("K{$line}", trim($abc[' 6 ']));
        $sheet->setCellValue("L{$line}", trim($abc[' 7 ']));
        $sheet->setCellValue("M{$line}", trim($abc[' 8 ']));
        $sheet->setCellValue("N{$line}", trim($abc[' 9 ']));
        $sheet->setCellValue("O{$line}", trim($abc[' 10']));
        $sheet->setCellValue("P{$line}", trim($abc[' 11']));
        $sheet->setCellValue("Q{$line}", trim($abc[' 12']));
        $sheet->setCellValue("R{$line}", trim($abc[' 13']));
        $sheet->setCellValue("S{$line}", trim($abc[' 14']));
        $sheet->setCellValue("T{$line}", trim($abc[' 15']));
        $sheet->setCellValue("U{$line}", trim($abc[' 16']));
        $sheet->setCellValue("V{$line}", trim($abc[' 17']));
        $sheet->setCellValue("W{$line}", trim($abc[' 18']));
        $sheet->setCellValue("X{$line}", trim($abc[' 19']));
        $sheet->setCellValue("Y{$line}", trim($abc[' 20']));
        $sheet->setCellValue("Z{$line}", trim($abc[' 21']));
        $sheet->setCellValue("AA{$line}", trim($abc[' 22']));
        $sheet->setCellValue("AB{$line}", trim($abc[' 23']));
        $sheet->setCellValue("AC{$line}", trim($abc[' 24']));
        $sheet->setCellValue("AD{$line}", trim($abc[' 25']));
        $sheet->setCellValue("AE{$line}", trim($abc[' 26']));
        $sheet->setCellValue("AF{$line}", trim($abc[' 27']));
        $sheet->setCellValue("AG{$line}", trim($abc[' 28']));
        $sheet->setCellValue("AH{$line}", trim($abc[' 29']));
        $sheet->setCellValue("AI{$line}", trim($abc[' 30']));
        $sheet->setCellValue("AJ{$line}", trim($abc[' 31']));
        $sheet->setCellValue("AK{$line}", trim($abc['number1']));
        $sheet->setCellValue("AL{$line}", trim($abc['пришли']));
        $sheet->setCellValue("AM{$line}", trim($abc['пропуски']));
        $sheet->setCellValue("AN{$line}", trim($abc['popo']));
        $sheet->setCellValue("AO{$line}", trim($abc['prichiny']));
    }
}

/*set itogovaya row*/
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Всего присутствует детей');
//$sheet->mergeCells("B{$line}:D{$line}");
$sheet->getStyle("C{$line}")->applyFromArray($allign_up_left);
$sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($style_border_all);
$sheet->getStyle("F{$line}:AO{$line}")->applyFromArray($allign_up_right);

$sheet->setCellValue("F{$line}", trim($arr_last_row[' 1 ']));
$sheet->setCellValue("G{$line}", trim($arr_last_row[' 2 ']));
$sheet->setCellValue("H{$line}", trim($arr_last_row[' 3 ']));
$sheet->setCellValue("I{$line}", trim($arr_last_row[' 4 ']));
$sheet->setCellValue("J{$line}", trim($arr_last_row[' 5 ']));
$sheet->setCellValue("K{$line}", trim($arr_last_row[' 6 ']));
$sheet->setCellValue("L{$line}", trim($arr_last_row[' 7 ']));
$sheet->setCellValue("M{$line}", trim($arr_last_row[' 8 ']));
$sheet->setCellValue("N{$line}", trim($arr_last_row[' 9 ']));
$sheet->setCellValue("O{$line}", trim($arr_last_row[' 10']));
$sheet->setCellValue("P{$line}", trim($arr_last_row[' 11']));
$sheet->setCellValue("Q{$line}", trim($arr_last_row[' 12']));
$sheet->setCellValue("R{$line}", trim($arr_last_row[' 13']));
$sheet->setCellValue("S{$line}", trim($arr_last_row[' 14']));
$sheet->setCellValue("T{$line}", trim($arr_last_row[' 15']));
$sheet->setCellValue("U{$line}", trim($arr_last_row[' 16']));
$sheet->setCellValue("V{$line}", trim($arr_last_row[' 17']));
$sheet->setCellValue("W{$line}", trim($arr_last_row[' 18']));
$sheet->setCellValue("X{$line}", trim($arr_last_row[' 19']));
$sheet->setCellValue("Y{$line}", trim($arr_last_row[' 20']));
$sheet->setCellValue("Z{$line}", trim($arr_last_row[' 21']));
$sheet->setCellValue("AA{$line}", trim($arr_last_row[' 22']));
$sheet->setCellValue("AB{$line}", trim($arr_last_row[' 23']));
$sheet->setCellValue("AC{$line}", trim($arr_last_row[' 24']));
$sheet->setCellValue("AD{$line}", trim($arr_last_row[' 25']));
$sheet->setCellValue("AE{$line}", trim($arr_last_row[' 26']));
$sheet->setCellValue("AF{$line}", trim($arr_last_row[' 27']));
$sheet->setCellValue("AG{$line}", trim($arr_last_row[' 28']));
$sheet->setCellValue("AH{$line}", trim($arr_last_row[' 29']));
$sheet->setCellValue("AI{$line}", trim($arr_last_row[' 30']));
$sheet->setCellValue("AJ{$line}", trim($arr_last_row[' 31']));
$sheet->setCellValue("AK{$line}", trim($arr_last_row['number1']));
$sheet->setCellValue("AL{$line}", trim($arr_last_row['пришли']));
$sheet->setCellValue("AM{$line}", trim($arr_last_row['пропуски']));
$sheet->setCellValue("AN{$line}", trim($arr_last_row['popo']));
$sheet->setCellValue("AO{$line}", trim($arr_last_row['prichiny']));

/*нижняя шапка*/
$line++;
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Руководитель учреждения');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);
$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Воспитатель');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

$sheet->getStyle("AD{$line}")->applyFromArray($allign_center_all);
$sheet->setCellValue("AD{$line}", date('j',strtotime($last_works_day)).' '.$MonthNamesRus[$month-1].' '.$year.' года');
//$sheet->setCellValue("AD{$line}", rdate('j M Y').' года');
$sheet->mergeCells("AD{$line}:AK{$line}");
$sheet->getStyle("AD{$line}:AK{$line}")->applyFromArray($style_border_bottom);

$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);

$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("C{$line}", 'Мед.сестра');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}:K{$line}")->applyFromArray($style_border_bottom);
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}:Y{$line}")->applyFromArray($style_border_bottom);

$line++;
$sheet->getRowDimension($line)->setRowHeight($height_row_shapka_down);
$sheet->setCellValue("E{$line}", '(подпись)');
$sheet->mergeCells("E{$line}:K{$line}");
$sheet->getStyle("E{$line}")->applyFromArray($allign_up_center);
$sheet->setCellValue("O{$line}", '(расшифровка подписи)');
$sheet->mergeCells("O{$line}:Y{$line}");
$sheet->getStyle("O{$line}")->applyFromArray($allign_up_center);




header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/vnd.ms-excel" );
header("Content-Disposition: attachment; filename=Табель дети ".date("d.m.Y").".xlsx");

$objWriter = new PHPExcel_Writer_Excel2007($xls);
$objWriter->save('php://output');

exit;