<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/table.css',
        'css/fonts.css',
        'css/main.css',
        'css/spiski.css',
        'css/style.css',
        'css/test333css.css',
        'css/sp/dropdown_sp_item_group.css',
        'css/sp/item_sp.css',
        'css/sp/dialog_info_not_go.css',
        'css/sp/dialog_to_reason_sp.css',
        'css/login/start.css',
        'css/sp/cal_table.css',
        'css/sp/calendar_3_column.css',
        'css/sp/so/spso.css',
        'css/reports/rep.css',
        'css/sotrudniki/sotr.css',
        'css/reports/diseases/table.css',
        'css/deti/one.css',
        'css/journal.css',
        'css/submenu.css',
        'css/correct.css',
        'css/hot_line.css',
        'css\injection\modal.css',
        'css/start/start.css',
        'css\zp.css'
    ];
    public $js = [

        'js/start.js',
        'js/search.js',
        'js/correct.js',
        'js/correct_reason.js',
        'js/main_button.js',
        'js/five.js',
        'js/antro.js',
    ];
    public $jsOptions = [
        'position'=>\yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
//        'rmrevin\yii\fontawesome\NpmFreeAssetBundle'
    ];
}
