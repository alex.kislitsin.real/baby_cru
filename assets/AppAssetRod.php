<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetRod extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/rod.css',
//        'css/fonts.css',
        'css/main.css',
//        'css/spiski.css',
//        'css/style.css',
//        'css/test333css.css',
//        'css/sp/dropdown_sp_item_group.css',
//        'css/sp/item_sp.css',
//        'css/sp/dialog_info_not_go.css',
//        'css/sp/dialog_to_reason_sp.css',
//        'css/login/start.css',
        'css/start/start.css'

    ];
    public $js = [
//        'js/sp/click_to_calen.js'
//        'js/loadingoverlay.min.js'
//        'js/4rb44yu.js'
    ];
    public $jsOptions = [
        'position'=>\yii\web\View::POS_END
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
//        'rmrevin\yii\fontawesome\NpmFreeAssetBundle'
    ];
}
