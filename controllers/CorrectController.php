<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 07.11.19
 * Time: 12:36
 */

namespace app\controllers;


use app\models\CorrectDate;
use app\models\Id;
use yii\web\Controller;
use Yii;

class CorrectController extends Controller{

    public function actionCorr(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();
        $model = new CorrectDate();
        $model_id = new Id();
        $model_id->id = 1;

        $flag = 100;

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){
                switch($model_id->id){
                    case 1:
                        if ($model->load(Yii::$app->request->post()) && $model->validate()){
                            /*$dataProvider = new \yii\data\SqlDataProvider([
                                'sql' => 'SET NOCOUNT ON; EXEC Show_correct @id=:id,@date_in=:begin,@date_out=:end',
                                'params' => [
                                    ':id'  => $model->id_child,
                                    ':begin'  => $model->datbegin,
                                    ':end'  => $model->datend
                                ],
                                'pagination' => false,
                                'sort' => false,
                            ]);
                            $array = $dataProvider->getModels();*/

                            $array = Yii::$app->db->createCommand("SELECT e.`name`,date_format(e.datenotgo,'%d.%m.%Y') AS `datenotgo`,
if(sovsem>0,(SELECT NAME FROM reason WHERE id = sovsem),e.prichina) AS `prichina`
,e.id_child,e.sovsem FROM (
select deti.name,datenotgo,reason.NAME AS `prichina`,id_child,gogo.sovsem
from gogo left outer join deti on gogo.id_child = deti.id
left outer join reason ON gogo.id = reason.id
where reason.id not in (1,2,3,100) and id_child = :id and datenotgo between :begin and :end order by datenotgo)e",[
                                'id'  => $model->id_child,
                                'begin'  => $model->datbegin,
                                'end'  => $model->datend
                            ])->queryAll();

                            $table = 'table2';

                            if (count($array) > 0){
                                return $this->renderAjax('modal_corr',compact('array'));
                            }else{
                                return 300;
                            }
                        }else{
                            return 400;
                        }

                        break;
                    case 2:
                        $array = array(
                            1 => 'Инфекции',
                            2 => 'Соматические',
                            3 => 'Травмы',
                            0 => 'ПРОЧЕЕ (отпуск, санаторий, выходной и т.п.)',
                        );
                        return $this->renderAjax('modal_corr1',compact('array'));
                        break;
                    case 3:
                        $v = (Yii::$app->request->post('v'));
                        $v = preg_replace('/[^0-9]/','',$v);
                        switch($v){
                            case 1:
                                $array = array(
                                    4 => 'ОРВИ, ОРЗ',
                                    5 => 'Грипп',
                                    6 => 'Ветряная оспа',
                                    7 => 'Скарлатина',
                                    8 => 'Гастроэнтерит',
                                    9 => 'Энтеровирусная',
                                    10 => 'Ротовирусная',
                                    11 => 'Аденовирусная',
                                    12 => 'Мононуклеоз',
                                    13 => 'Корь, краснуха',
                                    14 => 'Паротит',
                                    15 => 'Цитомегаловирус',
                                    16 => 'Инф. эритема',
                                    17 => 'Педикулез',
                                    18 => 'Гельминтозы',
                                    19 => 'Полиомиелит',
                                    20 => 'Другие (Инфекции)',
                                );
                                break;
                            case 2:
                                $array = array(
                                    21 => 'Органов дыхания',
                                    22 => 'Лор органов',
                                    23 => 'Органов ЖКТ',
                                    24 => 'Глазные',
                                    25 => 'Аллергия',
                                    26 => 'Мочеполовой с.',
                                    27 => 'Обмена веществ',
                                    28 => 'Другие (Соматические)',
                                );
                                break;
                            case 3:
                                $array = array(
                                    60 => 'Ушиб',
                                    61 => 'Перелом',
                                    62 => 'Растяжение',
                                    63 => 'Отморожение',
                                    64 => 'Ожоги',
                                    65 => 'Укусы насекомых',
                                    67 => 'Сотрясение мозга',
                                    66 => 'Другие (травмы)',
                                );
                                break;
                        }
                        return $this->renderAjax('modal_corr1',compact('array'));
                        break;
                    case 4:
                        $v = (Yii::$app->request->post('v'));
                        $v = preg_replace('/[^0-9]/','',$v);
                        switch($v){
                            case 21:
                                $array = array(
                                    29 => 'Бронхит',
                                    30 => 'Бронхопневмония',
                                    31 => 'Пневмония',
                                    32 => 'Астма бр.',
                                    33 => 'Другие (Органов дыхания)',
                                );
                                break;
                            case 22:
                                $array = array(
                                    34 => 'Фарингит',
                                    35 => 'Ринит',
                                    36 => 'Трахеит',
                                    37 => 'Отит',
                                    38 => 'Ринофарингит',
                                    39 => 'Фаринготрахеит',
                                    40 => 'Ларингит',
                                    41 => 'Ангина',
                                    42 => 'Ларинготрахеит',
                                    43 => 'Аденоиды',
                                    44 => 'Другие (Лор органов)',
                                );
                                break;
                            case 23:
                                $array = array(
                                    45 => 'Гастрит',
                                    46 => 'Другие (Органов ЖКТ)',
                                );
                                break;
                            case 24:
                                $array = array(
                                    47 => 'Коньюктивит',
                                    48 => 'Другие (Глазные)',
                                );
                                break;
                            case 25:
                                $array = array(
                                    49 => 'Дерматит',
                                    50 => 'Крапивница',
                                    51 => 'Укусы насекомых',
                                    52 => 'Астма алл.',
                                    53 => 'Другие (Аллергия)',
                                );
                                break;
                            case 26:
                                $array = array(
                                    54 => 'Пиелонефрит',
                                    55 => 'Цистит',
                                    56 => 'Другие (Мочеполовой с.)',
                                );
                                break;
                            case 27:
                                $array = array(
                                    57 => 'Недостаточное питание',
                                    58 => 'Избыточный вес',
                                    59 => 'Другие (Обмена веществ)',
                                );
                                break;

                        }
                        return $this->renderAjax('modal_corr1',compact('array'));
                        break;
                    case 5:
                        if ($model->load(Yii::$app->request->post()) && $model->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update gogo set id=:id_reason_new where id_child=:id_child and datenotgo between :start_date and :end_date and id not in (100,200)";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model->id_child,
                                        'start_date'  => $model->datbegin,
                                        'end_date'  => $model->datend,
                                        'id_reason_new'  => $model->reasonnew
                                    ]
                                )->execute();

                                $query2 = "update gogo set sovsem=:id_reason_new where id_child=:id_child and datenotgo between :start_date and :end_date and id = 200";
                                Yii::$app->db->createCommand($query2,[
                                        'id_child' => $model->id_child,
                                        'start_date'  => $model->datbegin,
                                        'end_date'  => $model->datend,
                                        'id_reason_new'  => $model->reasonnew
                                    ]
                                )->execute();

                                $reason = $model->reasonnew;
                                $medotvod = $model->medotvod;

                                if ($reason > 0 && $medotvod > 0){
                                    $medotvod = date('Y-m-d',strtotime($model->datend. $medotvod.' day'));
                                    $query3 = "update gogo set medotvod=:medotvod where id_child=:id_child and datenotgo=:end_date";
                                    Yii::$app->db->createCommand($query3,[
                                            'id_child' => $model->id_child,
                                            'end_date'  => $model->datend,
                                            'medotvod'  => $medotvod
                                        ]
                                    )->execute();
                                }

                                $array = Yii::$app->db->createCommand("SELECT e.`name`,date_format(e.datenotgo,'%d.%m.%Y') AS `datenotgo`,
if(sovsem>0,(SELECT NAME FROM reason WHERE id = sovsem),e.prichina) AS `prichina`
,e.id_child,e.sovsem FROM (
select deti.name,datenotgo,reason.NAME AS `prichina`,id_child,gogo.sovsem
from gogo left outer join deti on gogo.id_child = deti.id
left outer join reason ON gogo.id = reason.id
where reason.id not in (1,2,3,100) and id_child = :id and datenotgo between :begin and :end order by datenotgo)e",[
                                    'id'  => $model->id_child,
                                    'begin'  => $model->datbegin,
                                    'end'  => $model->datend
                                ])->queryAll();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }


                            $table = 'table2';

                            if (count($array) > 0){
                                return $this->renderAjax('modal_corr',compact('array'));
                            }else{
                                return 400;
                            }

//                            return 700;
                        }else{
                            return 400;
                        }
                        break;
                }
            }else{
                return 400;
            }




            return null;
        }


//        checking($this);


        $query = "select `id`,`name`,`start` from `deti` where `out` is null order by name";
        $array = Yii::$app->db->createCommand($query)->queryAll();
        $table = 'table';
        return $this->render('corr_view',compact('model','array','table','model_id'));
    }

} 