<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 21.12.19
 * Time: 17:10
 */

namespace app\controllers;


use app\models\Cru_add_child;
use app\models\Category_id;
use app\models\CruDate;
use app\models\Crujki;
use app\models\Gruppa;
use app\models\Id;
use app\models\Month;
use app\models\Queries;
use app\models\Search;
use app\models\SoActiveRecordCru;
use app\models\Year;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class CrujkiController extends Controller{

    public $enableCsrfValidation = false;

    public function actionCru(){

//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $model_cru = new Crujki();
        $model = new Gruppa();
        $model_id = new Id();
        $model_search = new Search();
        $model_cru_add = new Cru_add_child();
        $model_y = new Year();
        $model_m = new Month();
        $model_cru_date = new CruDate();
        $model_so = new SoActiveRecordCru();
        $qu = new Queries();
        $model_category_cru = new Category_id();


        $array_year_tabel_deti = [
            2019 => '2019',
            2020 => '2020',
            2021 => '2021',
            2022 => '2022',
            2023 => '2023',
        ];

        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_cru->load(Yii::$app->request->post());
            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3
            if ($model_id->validate()){
                switch($model_id->id){
                    case 0:
                        $array_so = $model_cru->show_crujki_so();
                        $array = $model_cru->show_crujki_edit_modal();
                        $array_drop = ArrayHelper::map($array,'id','name');

                        return $this->renderAjax('modal_edit_cru_view',compact(
                            'array_so',
                            'model_so',
                            'array_drop',
                            'model',
                            'model_cru',
                            'array',
                            'model_id'
                        ));
                        break;
                    case 1:
                        if ($model_cru->validate()&&$model_so->load(Yii::$app->request->post())&&$model_so->validate()&&$model_category_cru->load(Yii::$app->request->post())&&$model_category_cru->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "insert into crujki (category_id,name,price,pn,vt,sr,ch,pt,zarplata,id_so)
                                values (:category_id,:name,:price,:pn,:vt,:sr,:ch,:pt,:zarplata,:id_so);";
                                Yii::$app->db->createCommand($query,[
                                        'name' => $model_cru->name,
                                        'category_id' => $model_category_cru->name,
                                        'price' => $model_cru->price,
                                        'pn' => $model_cru->pn,
                                        'vt' => $model_cru->vt,
                                        'sr' => $model_cru->sr,
                                        'ch' => $model_cru->ch,
                                        'pt' => $model_cru->pt,
                                        'zarplata' => $model_cru->zp,
                                        'id_so' => $model_so->name,
                                    ]
                                )->execute();

//                            $array = Crujki::find()->asArray()->all();

                                $array = $model_cru->show_crujki_edit_modal();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }


                            return $this->renderAjax('view_table_cru',compact('array'));
                        }
                        break;
                    case 2:
                        if ($model_cru->validate()&&$model_so->load(Yii::$app->request->post())&&$model_so->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update crujki set name=:name,price=:price,pn=:pn,vt=:vt,sr=:sr,ch=:ch,pt=:pt,zarplata=:zarplata,id_so=:id_so where id=:id;";
                                Yii::$app->db->createCommand($query,[
                                        'id' => $model_cru->id,
                                        'name' => $model_cru->name,
//                                    'pedagog' => $model_cru->pedagog,
                                        'price' => $model_cru->price,
                                        'pn' => $model_cru->pn,
                                        'vt' => $model_cru->vt,
                                        'sr' => $model_cru->sr,
                                        'ch' => $model_cru->ch,
                                        'pt' => $model_cru->pt,
                                        'zarplata' => $model_cru->zp,
                                        'id_so' => $model_so->name,
                                    ]
                                )->execute();

//                            $array = Crujki::find()->asArray()->all();
                                $array = $model_cru->show_crujki_edit_modal();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }


                            return $this->renderAjax('view_table_cru',compact('array'));
                        }
                        break;
                    case 3:
                        $model->load(Yii::$app->request->post());
                        if ($model->validate()){
                            $array_add_child = $model_cru_add->show_table_modal_add($model->name);
                            return $this->renderAjax('view_table_cru_add_child',compact('array_add_child','model_cru_add'));
                        }
                        break;
                    case 4://update
                        $model_cru_add->load(Yii::$app->request->post());
                        $model->load(Yii::$app->request->post());
                        if ($model_cru_add->validate() && $model->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update crujki_add_deti set in_cru=:in_cru,out_cru=:out_cru where id_child=:id_child and id_crujok=:id_crujok and in_cru=:in_cru_old";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_cru_add->id_child,
                                        'id_crujok' => $model->name,
                                        'in_cru' => $model_cru_add->in_cru,
                                        'out_cru' => $model_cru_add->out_cru,
                                        'in_cru_old' => $model_cru_add->in_cru_old,
                                    ]
                                )->execute();

                                $array_add_child = $model_cru_add->show_table_modal_add($model->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('view_table_cru_add_child',compact('array_add_child','model_cru_add'));
                        }else{
                            return 400;
                        }
                        break;
                    case 5:
                        $model->load(Yii::$app->request->post());
                        if ($model->validate()){
                            $array_add_child = $model_cru_add->show_table_modal_add($model->name);
                            return $this->renderAjax('view_table_cru_add_child',compact('array_add_child','model_cru_add'));
                        }
                        break;
                    case 6://insert
                        $model_cru_add->load(Yii::$app->request->post());
                        $model->load(Yii::$app->request->post());
                        if ($model_cru_add->validate() && $model->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "insert into crujki_add_deti (id_child,id_crujok,in_cru,out_cru) values (:id_child,:id_crujok,:in_cru,:out_cru);";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_cru_add->id_child,
                                        'id_crujok' => $model->name,
                                        'in_cru' => $model_cru_add->in_cru,
                                        'out_cru' => $model_cru_add->out_cru,
                                    ]
                                )->execute();

                                $array_add_child = $model_cru_add->show_table_modal_add($model->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('view_table_cru_add_child',compact('array_add_child','model_cru_add'));
                        }else{
                            return 400;
                        }
                        break;
                    case 7://delete
                        $model_cru_add->load(Yii::$app->request->post());
                        $model->load(Yii::$app->request->post());
                        if ($model_cru_add->validate() && $model->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "delete from crujki_add_deti where id_child=:id_child and id_crujok=:id_crujok";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_cru_add->id_child,
                                        'id_crujok' => $model->name,
//                                    'in_cru_old' => $model_cru_add->in_cru_old,
                                    ]
                                )->execute();

                                $array_add_child = $model_cru_add->show_table_modal_add($model->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('view_table_cru_add_child',compact('array_add_child','model_cru_add'));
                        }else{
                            return 400;
                        }
                        break;
                    case 8://table_boss_cru
                        if ($model_y->load(Yii::$app->request->post())&&$model_y->validate()&&
                            $model_m->load(Yii::$app->request->post())&&$model_m->validate()&&
                            $model->load(Yii::$app->request->post())&&$model->validate()){

                            $cookie = new \yii\web\Cookie([
                                'name' => 'save_last_id_crujok',
                                'value' => $model->name,
                            ]);
                            Yii::$app->response->cookies->add($cookie);

                            $array_boss = $qu->show_tabel_cru($model_y->name,$model_m->name,$model->name);
                            return $this->renderAjax('table_boss_cru',compact(
                                'model_cru_date',
                                'model_y',
                                'model_m',
                                'array_boss'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 9://insert
                        if ($model->load(Yii::$app->request->post()) && $model->validate() &&
                            $model_cru_date->load(Yii::$app->request->post()) && $model_cru_date->validate() &&
                            $model_y->load(Yii::$app->request->post()) && $model_y->validate()&&
                            $model_m->load(Yii::$app->request->post()) && $model_m->validate()){

                            if(verifi_date_today_med($model_cru_date->date_cru) == 3){
                                return 505;
                            }

                            $transaction = Yii::$app->db->beginTransaction();
                            try {
	                            $query = "insert into crujki_gogo (datenotgo,id_child,id_crujok,id_so) values (:datenotgo,:id_child,:id_crujok,(select id_so from crujki where id = {$model->name}));";
	                            Yii::$app->db->createCommand($query,[
			                            'id_child' => $model_cru_date->id_child,
			                            'id_crujok' => $model->name,
			                            'datenotgo' => $model_cru_date->date_cru,
		                            ]
	                            )->execute();
	                            $array_boss = $qu->show_tabel_cru($model_y->name,$model_m->name,$model->name);
                                $transaction->commit();
                            } catch (Exception $e) {
                                $transaction->rollback();
                            }

                            return $this->renderAjax('table_boss_cru',compact(
                                'model_cru_date',
                                'model_y',
                                'model_m',
                                'array_boss'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 10://delete
                        if ($model->load(Yii::$app->request->post()) && $model->validate() &&
                            $model_cru_date->load(Yii::$app->request->post()) && $model_cru_date->validate() &&
                            $model_y->load(Yii::$app->request->post()) && $model_y->validate()&&
                            $model_m->load(Yii::$app->request->post()) && $model_m->validate()){


                            if(verifi_date_today_med($model_cru_date->date_cru) == 3){
                                return 505;
                            }

                            $transaction = Yii::$app->db->beginTransaction();
                            try {
                                $query = "delete from crujki_gogo where datenotgo=:datenotgo and id_child=:id_child and id_crujok=:id_crujok";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_cru_date->id_child,
                                        'id_crujok' => $model->name,
                                        'datenotgo' => $model_cru_date->date_cru,
                                    ]
                                )->execute();

                                $array_boss = $qu->show_tabel_cru($model_y->name,$model_m->name,$model->name);
                                $transaction->commit();
                            } catch (Exception $e) {
                                $transaction->rollback();
                            }


                            return $this->renderAjax('table_boss_cru',compact(
                                'model_cru_date',
                                'model_y',
                                'model_m',
                                'array_boss'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 11://one percent for all
                        if ($model_cru->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update crujki set zarplata=:zarplata";
                                Yii::$app->db->createCommand($query,[
                                        'zarplata' => $model_cru->zp,
                                    ]
                                )->execute();

//                            $array = Crujki::find()->asArray()->all();
                                $array = $model_cru->show_crujki_edit_modal();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('view_table_cru',compact('array'));
                        }else{
                            return 400;
                        }
                        break;
                    case 12://one price for all
                        if ($model_cru->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update crujki set price=:price";
                                Yii::$app->db->createCommand($query,[
                                        'price' => $model_cru->price,
                                    ]
                                )->execute();

//                            $array = Crujki::find()->asArray()->all();
                                $array = $model_cru->show_crujki_edit_modal();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('view_table_cru',compact('array'));
                        }else{
                            return 400;
                        }
                        break;
                    case 13:
                        if ($model_search->load(Yii::$app->request->post())&&$model_search->validate()){
                            if (strlen($model_search->name)>0){
                                $query = "select crujki_add_deti.*,rtrim(deti.name) as `name`,crujki.name as `namecru` from crujki_add_deti left outer join deti on deti.id = crujki_add_deti.id_child left outer join crujki on crujki_add_deti.id_crujok = crujki.id where deti.name like '%$model_search->name%' order by deti.name,crujki.name";
                                $array = Yii::$app->db->createCommand($query)->queryAll();
                                return $this->renderAjax('view_table_cru_search_child',compact('array'));
//                                return $array;view_table_cru_search_child
                            }else{
                                return $this->renderAjax('view_table_cru_search_child_empty');
                            }
                        }else{
                            return $this->renderAjax('view_table_cru_search_child_empty');
                        }

                        break;
                    case 14:
                        $model_category_cru = new Category_id();
                        $name_category_cru = (Yii::$app->request->post('name'));
                        $name_category_cru = preg_replace('/[\'\"\;]/','',$name_category_cru);
                        $name_category_cru = str_replace('--','',$name_category_cru);
                        if (!empty($name_category_cru)){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                Yii::$app->db->createCommand('insert into category_cru (name) values (:name);',['name' => $name_category_cru])->execute();
                                $data = Yii::$app->db->createCommand("select * from category_cru")->queryAll();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }
                            return json_encode($data);
                        }else{
                            return 400;
                        }
                        break;
                    case 15:
                        $id_so = (Yii::$app->request->post('idso'));
                        $id_so = preg_replace('/[^0-9]/','',$id_so);

                        if ($id_so == 112233){
                            $panel = 'panel_button';
                        }else{
                            $panel = 'panel_button_v';
                        }

                        $array_so = array();
                        $array_add_child = array();

                        $array = $model_cru->show_crujki_edit_modal();
                        $array_drop = ArrayHelper::map($array,'id','name');

                        $model->name = Yii::$app->request->cookies->getValue('save_last_id_crujok');
                        if (strlen(Yii::$app->request->cookies->getValue('save_last_id_crujok'))<1)$model->name = 1;

                        $model_y->name = date('Y');
                        $model_m->name = date('n');

                        $array_boss = $qu->show_tabel_cru($model_y->name,$model_m->name,$model->name);

                        return $this->renderAjax('cru_view1',compact(
                            'panel',
                            'array_so',
                            'model_so',
                            'model_cru_date',
                            'array_year_tabel_deti',
                            '_monthsList',
                            'model_y',
                            'model_m',
                            'array',
                            'model',
                            'model_cru',
                            'array_drop',
                            'model_id',
                            'model_search',
                            'array_add_child',
                            'model_cru_add',
                            'array_boss'));
                        break;
                    case 16:
                        $model->load(Yii::$app->request->post());
                        $id_child = (Yii::$app->request->post('id_child'));
                        $id_child = preg_replace('/[^0-9]/','',$id_child);
//                        $id_crujok = (Yii::$app->request->post('Gruppa[name]'));
//                        $id_crujok = preg_replace('/[^0-9]/','',$id_crujok);
                        $array = Yii::$app->db->createCommand("select * from crujki_add_deti left outer join deti on crujki_add_deti.id_child = deti.id where id_child=:id_child and id_crujok=:id_crujok and out_cru is not null order by out_cru",[
                            'id_child' => $id_child,
                            'id_crujok' => $model->name
                        ])->queryAll();
                        if (count($array)==0){
                            return 400;
                        }else{
                            return $this->renderAjax('table_hidden_cru',compact('array'));
                        }

                        break;
                    case 17:

	                    $model_id->load(Yii::$app->request->post());
	                    $model_cru_add->load(Yii::$app->request->post());
	                    $out_cru = null;
	                    $id_crujok = null;
	                    $id_crujok_old = null;
	                    if (isset($model_cru_add->out_cru) && isset($model_cru_add->id_crujok) && isset($model_cru_add->id_crujok_old)){
		                    try {
			                    $out_cru = \Yii::$app->formatter->asTime($model_cru_add->out_cru);
			                    $id_crujok = preg_replace('/[^0-9]/','',$model_cru_add->id_crujok);
			                    $id_crujok_old = preg_replace('/[^0-9]/','',$model_cru_add->id_crujok_old);
		                    } catch (InvalidConfigException $e) {

		                    }
	                    }
	                    if (is_null($out_cru) || is_null($id_crujok) || is_null($id_crujok_old) || !in_array($id_crujok_old,[0,1])){
	                    	return 404;
	                    }

	                    if($id_crujok_old == 1){
		                    \Yii::$app->db->createCommand("update crujki_add_deti set out_cru=:out_cru where out_cru is null",[
			                    'out_cru' => $out_cru
		                    ])->execute();
	                    }

	                    if($id_crujok_old == 0){
		                    \Yii::$app->db->createCommand("update crujki_add_deti set out_cru=:out_cru where id_crujok=:id_crujok and out_cru is null",[
			                    'out_cru' => $out_cru,
			                    'id_crujok' => $id_crujok
		                    ])->execute();
	                    }

	                    return $this->refresh();

                        break;
                }
            }

        return null;

        }

	    $file = \Yii::$app->cache->get('access');

	    if ($file){
		    $panel = 'panel_button';
	    }else{
		    $panel = 'panel_button_v';
	    }

	    $array_so = array();
	    $array_add_child = array();

	    $array = $model_cru->show_crujki_edit_modal();
	    $array_drop = ArrayHelper::map($array,'id','name');

	    $model->name = Yii::$app->request->cookies->getValue('save_last_id_crujok');
	    if (strlen(Yii::$app->request->cookies->getValue('save_last_id_crujok'))<1)$model->name = 1;

	    $model_y->name = date('Y');
	    $model_m->name = date('n');

	    $array_boss = $qu->show_tabel_cru($model_y->name,$model_m->name,$model->name);

	    return $this->render('cru_view1',compact(
		    'panel',
		    'array_so',
		    'model_so',
		    'model_cru_date',
		    'array_year_tabel_deti',
		    '_monthsList',
		    'model_y',
		    'model_m',
		    'array',
		    'model',
		    'model_cru',
		    'array_drop',
		    'model_id',
		    'model_search',
		    'array_add_child',
		    'model_cru_add',
		    'array_boss'));



//        return $this->render('cru_index');




//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);
        /*$array_so = SoActiveRecordCru::find()
//            ->select(['id','name'])
            ->where(['outS' => null])
            ->orderBy('name')
//            ->orWhere(['like','dol','услуги'])
            ->asArray()->all();
        $array_so = ArrayHelper::map($array_so,'id','name');*/


//        $array_so = $model_cru->show_crujki_so();

    }

    public function actionAccess($id)
    {
	    \Yii::$app->cache->set('access',$id);
	    return $this->redirect(['crujki/cru']);
    }

} 