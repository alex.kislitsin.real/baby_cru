<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 02.11.19
 * Time: 16:42
 */

namespace app\controllers;


use app\models\Gruppa;
use app\models\Id;
use app\models\Kal;
use app\models\Mantu;
use app\models\Optd;
use app\models\Reports;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use app\models\Queries;

class JournalController extends Controller{

    public function actionJournal(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

//        if (verification_user()!=555){
//            return $this->render('net_dostupa');
//        }

        $model_id = new Id();
        $model_rep = new Reports();
        $model_id->id = 1;
        $model_group = new Gruppa();
        $model_mantu = new Mantu();
        $model_optd = new Optd();
        $model_kal = new Kal();
        $qu = new Queries();

        $array = [
            1 => 'Журнал учёта р.Манту',
            2 => 'Журнал обследования на гельминтозы',
            3 => 'Журнал направленных в ОПТД',
        ];

        $array2 = [
            1 => 'ar',
            2 => 'p',
            3 => 'отр',
            4 => 'отказ',
        ];

        $array_status = [
            1 => 'Стоит на учёте',
            2 => 'Снят с учёта',
            5 => 'Не подлежит учёту',
        ];

        $array_gruppa = getgroup();

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
        if(empty($id_gruppa)){
            $item_gruppa = trim($array_gruppa[1]);
        }else{
            $item_gruppa = trim($array_gruppa[$id_gruppa]);
            $model_group->name = $id_gruppa;
        }




        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id7 = (Yii::$app->request->post('id7'));
            $id7 = preg_replace('/[^0-9]/','',$id7);
            switch($id7){
                case 0:
                    $status = 0;
                    break;
                case 1:
                    $status = 1;
                    break;
            }
            $model_group->load(Yii::$app->request->post());
            $model_rep->load(Yii::$app->request->post());
            $item_gruppa = trim($array_gruppa[$model_group->name]);
            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3
            if ($model_id->validate()){
                switch($model_id->id){
                    case 1://update
                        if ($model_mantu->load(Yii::$app->request->post()) && $model_mantu->validate()){
                            if (strpos($model_mantu->ye,'фильтрат') !== false)$model_mantu->ye = '';

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update mantu set dat=:date_zamera,el1=:el1,el2=:razmer,coment=:coment where id_child=:id_child and year(dat)=:year";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_mantu->id_child,
                                        'date_zamera' => $model_mantu->dat,
                                        'el1' => $model_mantu->ye,
                                        'razmer' => $model_mantu->yea,
                                        'coment' => $model_mantu->coment,
                                        'year' => date('Y',strtotime($model_mantu->dat)),
                                    ]
                                )->execute();
                                $model = $qu->show_mantu($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['dat'],'1900') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table',compact('array2','model','model_mantu','model_id','all_deti','not_all_deti','status','model_rep'));
                        }else{
                            return 400;
                        }
                        break;
                    case 2://insert
                        if ($model_mantu->load(Yii::$app->request->post()) && $model_mantu->validate()){
                            if (strpos($model_mantu->ye,'фильтрат') !== false)$model_mantu->ye = '';

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "insert into mantu (id_child,dat,el1,el2,coment) values (:id_child,:date_zamera,:el1,:razmer,:coment)";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_mantu->id_child,
                                        'date_zamera' => $model_mantu->dat,
                                        'el1' => $model_mantu->ye,
                                        'razmer' => $model_mantu->yea,
                                        'coment' => $model_mantu->coment,
                                    ]
                                )->execute();
                                $model = $qu->show_mantu($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['dat'],'1900') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table',compact('array2','model','model_mantu','model_id','all_deti','not_all_deti','status','model_rep'));
                            break;
                        }else{
                            return 400;
                        }
                    case 3://delete
                        if ($model_mantu->load(Yii::$app->request->post()) && $model_mantu->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "delete from mantu where id_child=:id and year(dat)=:year";
                                Yii::$app->db->createCommand($query,[
                                    'id' => $model_mantu->id_child,
                                    'year' => date('Y',strtotime($model_mantu->dat))
                                ])->execute();
                                $model = $qu->show_mantu($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['dat'],'1900') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep'));
                        }else{
                            return 400;
                        }
                    case 4:
                        $model = $qu->show_mantu($model_group->name);
                        $all_deti = 'Все дети в группе - '.count($model);
                        $model5 = array();
                        foreach($model as $key  => $value){
                            if(strpos($value['dat'],'1900') !== false){
                                array_push($model5,$value);
                            }
                        }
                        $not_all_deti = 'Без замера - '.count($model5);
                        return $this->renderAjax('table',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep'));
                        break;
                    case 5:
                        $y1 = date('Y');
                        $y2 = date('Y')-1;
                        $y3 = date('Y')-2;
                        $y4 = date('Y')-3;
                        $y5 = date('Y')-4;
                        echo"<table id='rrr555'>
                                <tr>
                                    <td class='not_hover_td' style='width: 2%'>№</td>
                                    <td class='not_hover_td'>Фамилия Имя</td>
                                    <td class='not_hover_td' style='width: 9%'>{$y5}</td>
                                    <td class='not_hover_td' style='width: 9%'>{$y4}</td>
                                    <td class='not_hover_td' style='width: 9%'>{$y3}</td>
                                    <td class='not_hover_td' style='width: 9%'>{$y2}</td>
                                    <td class='not_hover_td' style='width: 9%'>{$y1}</td>
                                    <td class='not_hover_td' style='width: 15%'>Примечание {$y1}</td>
                                    <td class='not_hover_td' style='width: 15%'>Медотвод</td>
                                </tr>
                            </table>";
                        exit;
                        break;
                    case 6:
                        $y1 = date('Y');
                        $y2 = date('Y')-1;
                        echo"<table id='rrr555'>
                                <tr>
                                    <td class='not_hover_td' style='width: 2%'>№</td>
                                    <td class='not_hover_td'>Фамилия Имя</td>
                                    <td class='not_hover_td' style='width: 15%'>{$y2}</td>
                                    <td class='not_hover_td' style='width: 20%'>{$y1}</td>
                                    <td class='not_hover_td' style='width: 20%'>Примечание {$y1}</td>
                                </tr>
                            </table>";
                        exit;
                        break;
                    case 7:
                        /*$dataProvider = new \yii\data\SqlDataProvider([
                            'sql' => 'SET NOCOUNT ON; EXEC ShowKalSpisokYears_site @namegroup=:item_gruppa',
                            'params' => [':item_gruppa'  => $item_gruppa],
                            'pagination' => false,
                            'sort' => false,
                        ]);
                        $model = $dataProvider->getModels();*/

                        $model = $qu->show_kal($model_group->name);

                        $all_deti = 'Все дети в группе - '.count($model);
                        $model5 = array();
                        foreach($model as $key  => $value){
                            if(strpos($value['year1'],'1900-01-01') !== false){
                                array_push($model5,$value);
                            }
                        }
                        $not_all_deti = 'Без замера - '.count($model5);
                        return $this->renderAjax('table_kal',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        break;
                    case 8://update kal
                        if ($model_kal->load(Yii::$app->request->post()) && $model_kal->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update kal set dat=:dat,coment=:coment where id_child=:id_child and year(dat)=:year";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_kal->id_child,
                                        'dat' => $model_kal->dat,
                                        'coment' => $model_kal->coment,
                                        'year' => date('Y',strtotime($model_mantu->dat)),
                                    ]
                                )->execute();
                                $model = $qu->show_kal($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['year1'],'1900-01-01') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table_kal',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        }else{
                            return 400;
                        }
                        break;
                    case 9://insert kal
                        if ($model_kal->load(Yii::$app->request->post()) && $model_kal->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "insert into kal (id_child,dat,coment) values (:id_child,:dat,:coment)";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_kal->id_child,
                                        'dat' => $model_kal->dat,
                                        'coment' => $model_kal->coment,
                                    ]
                                )->execute();
                                $model = $qu->show_kal($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }




                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['year1'],'1900-01-01') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table_kal',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        }else{
                            return 400;
                        }
                        break;
                    case 10://delete kal
                        if ($model_kal->load(Yii::$app->request->post()) && $model_kal->validate()){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "delete from kal where id_child=:id_child and year(dat)=:year";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_kal->id_child,
                                        'year' => date('Y',strtotime($model_kal->dat)),
                                    ]
                                )->execute();
                                $model = $qu->show_kal($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }



                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['year1'],'1900-01-01') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table_kal',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        }else{
                            return 400;
                        }
                        break;
                    case 11://show journal rep -> 1 after item drop down rep
                        if ($model_kal->load(Yii::$app->request->post()) && $model_kal->validate()){
                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "delete from kal where id_child=:id_child and year(dat)=:year";
                                Yii::$app->db->createCommand($query,[
                                        'id_child' => $model_kal->id_child,
                                        'year' => date('Y',strtotime($model_kal->dat)),
                                    ]
                                )->execute();
                                $model = $qu->show_kal($model_group->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            $all_deti = 'Все дети в группе - '.count($model);
                            $model5 = array();
                            foreach($model as $key  => $value){
                                if(strpos($value['year1'],'1900-01-01') !== false){
                                    array_push($model5,$value);
                                }
                            }
                            $not_all_deti = 'Без замера - '.count($model5);
                            return $this->renderAjax('table_kal',compact('array2','model','model_mantu','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        }else{
                            return 400;
                        }
                        break;
                    case 12://запуск модального окна итоги показатели манту по садику
                        /*$dataProvider = new \yii\data\SqlDataProvider([
                            'sql' => 'SET NOCOUNT ON; EXEC Show_pokazateli_mantu_all_deti',
//                            'params' => [':item_gruppa'  => $item_gruppa],
                            'pagination' => false,
                            'sort' => false,
                        ]);
                        $array_itogo = $dataProvider->getModels();*/

                        $array_itogo = $qu->show_mantu_pokazateli();
                        return $this->renderAjax('modal_mantu_itogo',compact('array_itogo'));
                        break;
                    case 13://показать журнал направленных ОПТД
                        echo"<table id='rrr555'>
                                <tr class='not_hover_td'>
                                    <td class='not_hover_td' style='width: 3%'>№</td>
                                    <td class='not_hover_td' style='width: 22%'>Фамилия Имя</td>
                                    <td class='not_hover_td' style='width: 3%'>№ гр</td>
                                    <td class='not_hover_td' style='width: 9%'>Дата напр.</td>
                                    <td class='not_hover_td' style='width: 9%'>Дата осмотра</td>
                                    <td class='not_hover_td' style='width: 22%'>Окончат.диагноз</td>
                                    <td class='not_hover_td' style='width: 14%'>Статус</td>
                                    <td class='not_hover_td' style='width: 9%'>Дата след.явки</td>
                                    <td class='not_hover_td' style='width: 9%'>Медотвод</td>
                                </tr>
                            </table>";
                        exit;
                        break;
                    case 14://показать журнал направленных ОПТД
                        $array_control_yavka = array();
                        $model = $model_optd->show_table_optd();

                        $array_all_deti_optd = array();
                        /*$array_status = [
                            1 => 'Стоит на учёте',
                            2 => 'Снят с учёта',
                            5 => 'Не подлежит учёту',
                        ];*/
                        $model_group->name = null;
                        $array_control_yavka = array();
                        return $this->renderAjax('table_mantu_n',compact('array_control_yavka','array_status','array_all_deti_optd','model_group','array_gruppa','array2','model','model_optd','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        break;
                    case 15:
                        if ($model_group->validate()){
                            $query_all_deti = "select id,id_gruppa,name from deti where [out] is null and id_gruppa=:id_gruppa";
                            $array_all_deti_optd = Yii::$app->db->createCommand($query_all_deti,['id_gruppa' => $model_group->name])->queryAll();
                            return json_encode($array_all_deti_optd);
                        }
                        break;
                    case 16:
                        if ($model_optd->load(Yii::$app->request->post()) && $model_optd->validate()){
                            $query = "insert into mantu_n (id_child,date_to,pred_diagnoz,date_osmotr_ptd,finish_diagnoz_ptd,status_ptd,date_sled_yavki,medotvod,date_vidacha,coment)values(:id_child,:date_to,:pred_diagnoz,:date_osmotr_ptd,:finish_diagnoz_ptd,:status_ptd,:date_sled_yavki,:medotvod,:date_vidacha,:coment)";
                            Yii::$app->db->createCommand($query,[
                                    'id_child' => $model_optd->name,
                                    'date_to' => $model_optd->date_to,
                                    'pred_diagnoz' => $model_optd->pred_diagnoz,
                                    'date_osmotr_ptd' => $model_optd->date_osmotr_ptd,
                                    'finish_diagnoz_ptd' => $model_optd->finish_diagnoz_ptd,
                                    'status_ptd' => $model_optd->status_ptd,
                                    'date_sled_yavki' => $model_optd->date_sled_yavki,
                                    'medotvod' => $model_optd->medotvod,
                                    'date_vidacha' => $model_optd->date_vidacha,
                                    'coment' => $model_optd->coment,
                                ]
                            )->execute();

                            $model = $model_optd->show_table_optd();

                            $array_all_deti_optd = array();
                            /*$array_status = [
                                1 => 'Стоит на учёте',
                                2 => 'Снят с учёта',
                                5 => 'Не подлежит учёту',
                            ];*/
                            $model_group->name = null;
                            $array_control_yavka = array();
                            return $this->renderAjax('table_mantu_n',compact('array_control_yavka','array_status','array_all_deti_optd','model_group','array_gruppa','array2','model','model_optd','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));

//                            return $model_optd;
                        }else{
                            return 400;
                        }
                        break;
                    case 17:
                        if ($model_optd->load(Yii::$app->request->post()) && $model_optd->validate()){
                            $query = "update mantu_n set date_to=:date_to,pred_diagnoz=:pred_diagnoz,date_osmotr_ptd=:date_osmotr_ptd,finish_diagnoz_ptd=:finish_diagnoz_ptd,status_ptd=:status_ptd,date_sled_yavki=:date_sled_yavki,medotvod=:medotvod,date_vidacha=:date_vidacha,coment=:coment where id_child=:id_child and id=:id";
                            Yii::$app->db->createCommand($query,[
                                    'id_child' => $model_optd->id_child,
                                    'id' => $model_optd->id,
                                    'date_to' => $model_optd->date_to,
                                    'pred_diagnoz' => $model_optd->pred_diagnoz,
                                    'date_osmotr_ptd' => $model_optd->date_osmotr_ptd,
                                    'finish_diagnoz_ptd' => $model_optd->finish_diagnoz_ptd,
                                    'status_ptd' => $model_optd->status_ptd,
                                    'date_sled_yavki' => $model_optd->date_sled_yavki,
                                    'medotvod' => $model_optd->medotvod,
                                    'date_vidacha' => $model_optd->date_vidacha,
                                    'coment' => $model_optd->coment,
                                ]
                            )->execute();

                            $model = $model_optd->show_table_optd();

                            $array_all_deti_optd = array();
                            /*$array_status = [
                                1 => 'Стоит на учёте',
                                2 => 'Снят с учёта',
                                5 => 'Не подлежит учёту',
                            ];*/
                            $model_group->name = null;
                            $model_optd->date_osmotr_ptd = null;
                            $model_optd->date_sled_yavki = null;
                            $model_optd->date_to = null;
                            $model_optd->date_vidacha = null;
                            $model_optd->medotvod = null;
                            $model_optd->coment = null;
                            $model_optd->name = null;
                            $model_optd->name_edit = null;
                            $model_optd->pred_diagnoz = null;
                            $model_optd->finish_diagnoz_ptd = null;

                            $array_control_yavka = array();

                            return $this->renderAjax('table_mantu_n',compact('array_control_yavka','array_status','array_all_deti_optd','model_group','array_gruppa','array2','model','model_optd','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));

//                            return $model_optd;
                        }else{
                            return 400;
                        }
                        break;
                    case 18:
                        if ($model_optd->load(Yii::$app->request->post()) && $model_optd->validate()){
                            $query = "delete from mantu_n where id_child=:id_child and id=:id";
                            Yii::$app->db->createCommand($query,[
                                    'id_child' => $model_optd->id_child,
                                    'id' => $model_optd->id,
                                ]
                            )->execute();

                            $model = $model_optd->show_table_optd();

                            $array_all_deti_optd = array();
                            /*$array_status = [
                                1 => 'Стоит на учёте',
                                2 => 'Снят с учёта',
                                5 => 'Не подлежит учёту',
                            ];*/
                            $model_group->name = null;
                            $model_optd->date_osmotr_ptd = null;
                            $model_optd->date_sled_yavki = null;
                            $model_optd->date_to = null;
                            $model_optd->date_vidacha = null;
                            $model_optd->medotvod = null;
                            $model_optd->coment = null;
                            $model_optd->name = null;
                            $model_optd->name_edit = null;
                            $model_optd->pred_diagnoz = null;
                            $model_optd->finish_diagnoz_ptd = null;

                            $array_control_yavka = array();

                            return $this->renderAjax('table_mantu_n',compact('array_control_yavka','array_status','array_all_deti_optd','model_group','array_gruppa','array2','model','model_optd','model_id','status','all_deti','not_all_deti','status','model_rep','model_kal'));
                        }else{
                            return 400;
                        }
                        break;
                    case 19:
                        $array_control_yavka = $model_optd->show_table_optd_control_yavka();
                        if (count($array_control_yavka)<1){
                            return 400;
                        }else{
//                            return $array_control_yavka;
                            return $this->renderAjax('modal_control_yavka',compact('array_control_yavka'));
                        }
                        break;



                }
            }
            return null;
        }
//        checking($this);

        $model_rep->name = 1;

        $status = 0;

        $model = $qu->show_mantu($id_gruppa);

        /*$dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SET NOCOUNT ON; EXEC ShowMantuSpisokYears @namegroup=:item_gruppa',
            'params' => [':item_gruppa'  => $item_gruppa],
            'pagination' => false,
            'sort' => false,
        ]);

        $model = $dataProvider->getModels();*/

        $all_deti = 'Все дети в группе - '.count($model);
        $model5 = array();
        foreach($model as $value){
            if(strpos($value['dat'],'1900') !== false){
                array_push($model5,$value);
            }
        }
        $not_all_deti = 'Без замера - '.count($model5);

        $array_itogo = array(
            0 => [
            'name' => '',
            'q1' => ''
            ],
        );

        return $this->render('journal',compact(
            'array_control_yavka',
            'array_itogo',
            'model_group',
            'array_gruppa',
            'array',
            'array2',
            'model_rep',
            'model',
            'model_mantu',
            'model_id',
            'all_deti',
            'not_all_deti',
            'status'
        ));
    }

} 