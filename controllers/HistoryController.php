<?php

namespace app\controllers;

use app\models\Id;
use app\models\Search;
use Yii;
use yii\data\SqlDataProvider;
use yii\web\Controller;

class HistoryController extends Controller
{


    public function actionFind(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');

        if(strlen($id_gruppa)<1){
            $gr = '';
        }else{
            $id_gruppa = preg_replace('/[^0-9]/','',$id_gruppa);
            $gr = "where id_gruppa = $id_gruppa";
        }


        if(verification_user()==555){
            $dostup = '';
        }else{
            $dostup = "where kogo in (select name from deti $gr)";
        }

        $model = new Search();
        $model_id = new Id();
        $model_id->id = 1;



        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){
                switch($model_id->id){
                    case 1:
                        $dataProvider = new SqlDataProvider([
                            'sql' => "select top(400)* from history $dostup order by dateH desc",
                            'pagination' => false
                        ]);
                        return $this->renderAjax('table_grid',compact('dataProvider'));
                        break;
                    case 2:
                        $dataProvider = new SqlDataProvider([
                            'sql' => "select * from history $dostup order by dateH desc",
                            'pagination' => false
                        ]);
                        return $this->renderAjax('table_grid',compact('dataProvider'));
                        break;
                }
            }
            return null;
        }

//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);

        $dataProvider = new SqlDataProvider([
            'sql' => "select top(400)* from history $dostup order by dateH desc",
            'pagination' => false
        ]);

        return $this->render('index_so',compact('model','model_id','dataProvider'));
    }

}
