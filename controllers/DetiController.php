<?php

namespace app\controllers;

use app\models\Deti_check;
use app\models\Gruppa;
use app\models\Id;
use Yii;
use app\models\Deti;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DetiController extends Controller
{

    public function actionChild()
    {
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

//        if (verification_user()!=555){
//            return $this->render('net_dostupa');
//        }

        $table = 'deti';
//        $table = 'deti_test';

        $model_so100 = new Deti();
        $model_id = new Id();
        $model_id->id = 1;
        $model_group = new Gruppa();
        $model_check = new Deti_check();

        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);

        if (empty($array_gruppa)){
            $query_gruppa = "select * from gruppa order by id";
            $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
            $array_gruppa = ArrayHelper::index($array_gruppa,'id');
            $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

            $json_array_group = json_encode($array_gruppa);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_group',
                'value' => $json_array_group,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3
            if ($model_id->validate()){
                if (($model_so100->load(Yii::$app->request->post()) && $model_so100->validate()) || $model_id->id == 555 || $model_id->id == 4 || $model_id->id == 5){
                    switch($model_id->id){
                        case 1://update
                            switch($model_so100->pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }

//                            return $model_so100;

                            $old_id_gruppa = $model_so100->old_id_gruppa;
                            $perevod = $model_so100->perevod;
                            $perevod2 = $model_so100->perevod2;
                            $current_gruppa = $model_so100->id_gruppa;
                            if (date('Y',strtotime($perevod)) > 1900){
                                $query = "update $table set name=:name,id_gruppa=:id_gruppa, number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,old_id_gruppa = :id_gruppa,perevod=:perevod,alergia=:alergia where id=:id";

                                Yii::$app->db->createCommand($query,[
                                        'id' => $model_so100->id,
                                        'name' => $model_so100->name,
                                        'id_gruppa' => $current_gruppa,////////////////////////////////
                                        'number_schet' => $model_so100->number_schet,
                                        'in' => $model_so100->in,
                                        'out' => $model_so100->out,
                                        'rozd' => $model_so100->rozd,
                                        'otche' => $model_so100->otche,
                                        'pol' => $pol,
                                        'polis' => $model_so100->polis,
                                        'snils' => $model_so100->snils,
                                        'adress' => $model_so100->adress,
                                        'alergia' => $model_so100->alergia,
//                                            'old_id_gruppa' => $current_gruppa,
                                        'perevod' => $perevod,

                                    ]
                                )->execute();
                            }

                            if (date('Y',strtotime($perevod2)) > 1900){
                                $query = "update $table set name=:name,number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,perevod=:perevod,alergia=:alergia where id=:id";

                                Yii::$app->db->createCommand($query,[
                                        'id' => $model_so100->id,
                                        'name' => $model_so100->name,
                                        'number_schet' => $model_so100->number_schet,
                                        'in' => $model_so100->in,
                                        'out' => $model_so100->out,
                                        'rozd' => $model_so100->rozd,
                                        'otche' => $model_so100->otche,
                                        'pol' => $pol,
                                        'polis' => $model_so100->polis,
                                        'snils' => $model_so100->snils,
                                        'adress' => $model_so100->adress,
                                        'alergia' => $model_so100->alergia,
                                        'perevod' => $perevod2,

                                    ]
                                )->execute();
                            }

                            if ((date('Y',strtotime($perevod)) == 1900) && (date('Y',strtotime($perevod2)) == 1900) && $old_id_gruppa > 0){
                                $query = "update $table set name=:name,id_gruppa=:id_gruppa, number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,
                            otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,old_id_gruppa=:old_id_gruppa,perevod=:perevod,alergia=:alergia where id=:id";

                                Yii::$app->db->createCommand($query,[
                                        'id' => $model_so100->id,
                                        'name' => $model_so100->name,
                                        'id_gruppa' => $current_gruppa,////////////////////////////////
                                        'number_schet' => $model_so100->number_schet,
                                        'in' => $model_so100->in,
                                        'out' => $model_so100->out,
                                        'rozd' => $model_so100->rozd,
                                        'otche' => $model_so100->otche,
                                        'pol' => $pol,
                                        'polis' => $model_so100->polis,
                                        'snils' => $model_so100->snils,
                                        'adress' => $model_so100->adress,
                                        'alergia' => $model_so100->alergia,
                                        'old_id_gruppa' => 0,
                                        'perevod' => $perevod,

                                    ]
                                )->execute();
                            }

                            if ((date('Y',strtotime($perevod)) == 1900) && (date('Y',strtotime($perevod2)) == 1900) && $old_id_gruppa == 0){
                                $query = "update $table set name=:name, number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,
                            otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,alergia=:alergia where id=:id";

                                Yii::$app->db->createCommand($query,[
                                        'id' => $model_so100->id,
                                        'name' => $model_so100->name,
                                        'number_schet' => $model_so100->number_schet,
                                        'in' => $model_so100->in,
                                        'out' => $model_so100->out,
                                        'rozd' => $model_so100->rozd,
                                        'otche' => $model_so100->otche,
                                        'pol' => $pol,
                                        'polis' => $model_so100->polis,
                                        'snils' => $model_so100->snils,
                                        'adress' => $model_so100->adress,
                                        'alergia' => $model_so100->alergia,
                                    ]
                                )->execute();
                            }


                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                $query2 = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
                                $model = Yii::$app->db->createCommand($query2,['id_gruppa' => $model_group->name])->queryAll();
                                $model_so100 = new Deti();
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
//                            return $query;
                            break;
                        case 2://добавление нового ребенка
                            switch($model_so100->pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }

                            /*$query = "update $table set name=:name, number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,
                            otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,old_id_gruppa=:old_id_gruppa,perevod=:perevod where id=:id";*/

                            $query = "insert into $table (name,id_gruppa,number_schet,`in`,`out`,rozd,otche,pol,polis,snils,adress,alergia) values
                            (:name,:id_gruppa,:number_schet,:in,:out,:rozd,:otche,:pol,:polis,:snils,:adress,:alergia);";

                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){

                            Yii::$app->db->createCommand($query,[
                                    'name' => $model_so100->name,
                                    'id_gruppa' => $model_so100->id_gruppa,
                                    'number_schet' => $model_so100->number_schet,
                                    'in' => $model_so100->in,
                                    'out' => $model_so100->out,
                                    'rozd' => $model_so100->rozd,
                                    'otche' => $model_so100->otche,
                                    'pol' => $pol,
                                    'polis' => $model_so100->polis,
                                    'snils' => $model_so100->snils,
                                    'adress' => $model_so100->adress,
                                    'alergia' => $model_so100->alergia,
//                                    'perevod' => $model_so100->perevod,

                                ]
                            )->execute();

                                $query2 = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
                                $model = Yii::$app->db->createCommand($query2,['id_gruppa' => $model_group->name])->queryAll();
                                $model_so100 = new Deti();
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
                            break;
                        case 3://
                            $id7 = (Yii::$app->request->post('id7'));
                            $id7 = preg_replace('/[^0-9]/','',$id7);
                            if ($model_so100->id < 1){
                                return 400;
                            }else{
                                if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                    switch($id7){
                                        case 0:
                                            $query2 = "select * from $table where `out` is not null and id_gruppa=:id_gruppa order by name";
                                            break;
                                        case 1:
                                            $query2 = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
                                            break;
                                    }
                                    $query = "delete from $table where id=:id;";
                                    Yii::$app->db->createCommand($query,['id' => $model_so100->id])->execute();
                                    $model_so100 = new Deti();
                                    $model = Yii::$app->db->createCommand($query2,['id_gruppa' => $model_group->name])->queryAll();
                                    return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                                }else{
                                    return 400;
                                }
                            }
                            break;
                        case 4://
                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                $query = "select * from $table where `out` is not null and id_gruppa=:id_gruppa order by name";
                                $model = Yii::$app->db->createCommand($query,['id_gruppa' => $model_group->name])->queryAll();
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
                            break;
                        case 5://
                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                $query = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
                                $model = Yii::$app->db->createCommand($query,['id_gruppa' => $model_group->name])->queryAll();
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
                            break;
                        case 6://обновление данных сорт. уволенного и возврат на уволенных
                            switch($model_so100->pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }

                            $query = "update $table set name=:name, number_schet=:number_schet,`in`=:in,`out`=:out,rozd=:rozd,
                            otche=:otche,pol=:pol,polis=:polis,snils=:snils,adress=:adress,alergia=:alergia where id=:id";

                            Yii::$app->db->createCommand($query,[
                                    'id' => $model_so100->id,
                                    'name' => $model_so100->name,
                                    'number_schet' => $model_so100->number_schet,
                                    'in' => $model_so100->in,
                                    'out' => $model_so100->out,
                                    'rozd' => $model_so100->rozd,
                                    'otche' => $model_so100->otche,
                                    'pol' => $pol,
                                    'polis' => $model_so100->polis,
                                    'snils' => $model_so100->snils,
                                    'adress' => $model_so100->adress,
                                    'alergia' => $model_so100->alergia,
//                                    'perevod' => $model_so100->perevod,

                                ]
                            )->execute();

                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                $query2 = "select * from $table where `out` is not null and id_gruppa=:id_gruppa order by name";
                                $model = Yii::$app->db->createCommand($query2,['id_gruppa' => $model_group->name])->queryAll();
                                $model_so100 = new Deti();
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
//                            return $query;
                            break;
                        case 555:
                            if ($model_group->load(Yii::$app->request->post()) && $model_group->validate()){
                                $id_gruppa = preg_replace('/[^0-9]/','',$model_group->name);
                                $show_del = (Yii::$app->request->post('show_del'));
                                $show_del = preg_replace('/[^0-9]/','',$show_del);
                                switch($show_del){
                                    case 0:
                                        $query = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
                                        break;
                                    case 1:
                                        $query = "select * from $table where `out` is not null and id_gruppa=:id_gruppa order by name";
                                        break;
                                }
                                $model = Yii::$app->db->createCommand($query,['id_gruppa' => $id_gruppa])->queryAll();

//                                return $model;
                                return $this->renderAjax('table',compact('model','model_so100','model_group','model_id','array_gruppa'));
                            }else{
                                return 400;
                            }
                            break;
                        case 600:
                            return $this->redirect(['crujki/cru']);

                            break;


                    }
                }else{
                    return 400;
                }

            }

            return null;

            /*$model_so100->load(Yii::$app->request->post());
            $model_so100->validate();
            $data = $model_so100->ins;
            $data = Yii::$app->formatter->asTime($data);
            return $data;*/
        }
//        checking($this);

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
        if(empty($id_gruppa)){
            $model_group->name = 1;
            $id_gruppa = 1;
        }else{
            $model_group->name = $id_gruppa;
        }

        $id_gruppa = preg_replace('/[^0-9]/','',$id_gruppa);
        $query = "select * from $table where `out` is null and id_gruppa=:id_gruppa order by name";
        $model = Yii::$app->db->createCommand($query,['id_gruppa' => $id_gruppa])->queryAll();

        return $this->render('index_so',compact('model','model_so100','array_gruppa','model_id','model_group','model_check'));
    }




}
