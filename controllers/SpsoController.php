<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 30.07.19
 * Time: 15:52
 */
namespace app\controllers;
use app\models\AjaxText;
use app\models\Disable_date;
use app\models\Gruppa;
use app\models\Id;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use Yii;
use yii\db\Connection;
use app\models\Item_date;
use app\models\Queries;

class SpsoController extends Controller{

    public function actionSpsoview(){
//        show_layout_light($this);

        /*$name_base = Yii::$app->request->cookies->getValue('name_base');
        $us = Yii::$app->request->cookies->getValue('user');
        $id_u = Yii::$app->request->cookies->getValue('id_user');
        if(empty($name_base)||empty($us)||empty($id_u)){
            return $this->redirect(['site/login']);
        }*/

//        change_db_attr();

        $model_group = new Gruppa();
        $model_item_date = new Item_date();
        $model_d_date = new Disable_date();
        $model_id = new Id();
        $qu = new Queries();

        $model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
        $model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);
        /*if(empty($model_d_date) || !in_array(date('Y-01-01'),$model_d_date)){
            $model_d_date_query = "select date_hol from holidays";
            $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
            $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');

            $json_array_dates = json_encode($model_d_date);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_dates',
                'value' => $json_array_dates,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }*/

//        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);
//
//        if (empty($array_gruppa)){
//            $query_gruppa = "select * from gruppa order by id";
//            $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
//            $array_gruppa = ArrayHelper::index($array_gruppa,'id');
//            $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');
//
//            $json_array_group = json_encode($array_gruppa);
//            $cookie = new \yii\web\Cookie([
//                'name' => 'array_group',
//                'value' => $json_array_group,
//            ]);
//            Yii::$app->response->cookies->add($cookie);
//        }

        $array_disabled_dates = array();
        foreach($model_d_date as $date){
            $date = date('d.m.Y',  strtotime($date));
            array_push($array_disabled_dates,$date);
        }

        $_monthsList = array(
            "1"=>"Январь","2"=>"Февраль","3"=>"Март",
            "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
            "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
            "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");
//
        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3

            switch($model_id->id){
                case 1://при выборе группы
                    $data = (Yii::$app->request->post('itemdate'));

                    break;
                case 2://при выборе даты
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);
                    if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
                        $_spiski = '_weekends_or_holidays';
                    }else{
                        $_spiski = '_spiskiso';
                        $model_sp_begin = $qu->show_sp_so($data);
                    }

                    return $this->renderAjax('_sp_calso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        '_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 3://при нажатии на сотрудника в списке -> сделать отметку
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));

                    $reason = (Yii::$app->request->post('reason'));
                    $reason = preg_replace('/[a-zA-Z0-9\'\"\;]/','',$reason);

                    $id = preg_replace('/[^0-9]/','',$id);

                    if (strpos($reason,'ПРИДУ') !== false){
                        $query = "insert into gogos (id,dateS,propuskS) values (:id,:datas,'не приду')";
                    }else{
                        $query = "insert into gogos (id,dateS) values (:id,:datas)";
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $model_sp_begin = $qu->show_sp_so($data);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }                    

                    return $this->renderAjax('_spiskiso',compact('model_sp_begin','model_id','data','model_d_date','model_d_antidate'));

                    break;
                case 4://при нажатии на сотрудника -> календарик
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);
                    $month = date('n', strtotime($data));
                    $year = date('Y', strtotime($data));
                    $query = "select * from gogos where id =:id and month(dateS)=:mon and year(dateS)=:yea";
                    $array_day = Yii::$app->db->createCommand($query,[
                        'id' => $id,
                        'mon' => $month,
                        'yea' => $year,
                        ]
                    )->queryAll();
                    $item_month = $_monthsList[date("n", strtotime($data))];//текущий месяц

                    return $this->renderAjax('_only_calso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'data_item',
                        'id',
                        'item_month',
                        'array_day'
                    ));
                    break;
                case 5://при нажатии на сотрудника -> календарик DELETE
                    $data = (Yii::$app->request->post('itemdate'));
                    $data = Yii::$app->formatter->asTime($data);
                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        $query = "delete from gogos where dateS=:datas and id=:id";
                        Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $month = date('n', strtotime($data));
                        $year = date('Y', strtotime($data));
                        $query = "select * from gogos where id =:id and month(dateS)=:mon and year(dateS)=:yea";
                        $array_day = Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'mon' => $month,
                                'yea' => $year,
                            ]
                        )->queryAll();
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }

                    return $this->renderAjax('_only_calso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'id',
                        'array_day'
                    ));
                    break;
                case 6://при отметке на пустой дате при выбранном сотрудника INSERT (calendar)
                    $data = (Yii::$app->request->post('itemdate'));
                    $data = Yii::$app->formatter->asTime($data);
                    $id = (Yii::$app->request->post('id_child'));
                    $today = (Yii::$app->request->post('today'));
                    $today = preg_replace('/[^0-9]/','',$today);
                    $id = preg_replace('/[^0-9]/','',$id);

                    switch($today){
                        case 1:
                            $query = "insert into gogos (id,dateS,propuskS) values (:id,:datas,'не приду')";
                            break;
                        case 2:
                            $query = "insert into gogos (id,dateS) values (:id,:datas)";
                            break;
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $month = date('n', strtotime($data));
                        $year = date('Y', strtotime($data));
                        $query = "select * from gogos where id =:id and month(dateS)=:mon and year(dateS)=:yea";
                        $array_day = Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'mon' => $month,
                                'yea' => $year,
                            ]
                        )->queryAll();
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }

                    return $this->renderAjax('_only_calso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'data_item',
                        'id',
                        'array_day'
                    ));

//                    exit;
                    break;
//                case 7://
//                    $data_item = (Yii::$app->request->post('itemdate'));
//                    $data_item = Yii::$app->formatter->asTime($data_item);
//
//                    $veruyu = verification_user();
//                    if($veruyu == 555){
//                        return verifi_date_today_med($data_item);
//                    }else{
//                        return verifi_date_today($data_item,$model_d_date);
//                    }
//                    break;
                case 8://
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        $query = "delete from gogos where dateS=:datas and id=:id";
                        Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $model_sp_begin = $qu->show_sp_so($data);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }

                    $_spiski = '_spiskiso';

                    return $this->renderAjax('spsoview',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));

                    break;
                case 9://red sp only
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    return verifi_date_today_med($data);
                    break;
                case 10://red sp only пришел / отмена пришел
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $sur = (Yii::$app->request->post('sur'));
                    $sur = preg_replace('/[^\d]/','',$sur);
                    switch ($sur){
                        case 0:
                            $query = "update gogos set surpriseS = '+' where id=:id and dateS=:datas";
                            break;
                        case 1:
                            $query = "update gogos set surpriseS = '' where id=:id and dateS=:datas";
                            break;
                        default:
                            return;
                            break;
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        Yii::$app->db->createCommand($query,[
                                'id' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $model_sp_begin = $qu->show_sp_so($data);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }

                    $_spiski = '_spiskiso';

                    return $this->renderAjax('spsoview',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 11://
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $model_sp_begin = $qu->show_sp_so($data);

                    return $this->renderAjax('_spiskiso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 12://
                    $data = (Yii::$app->request->post('datamonth'));
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $leftright = (Yii::$app->request->post('leftright'));
                    $leftright = preg_replace('/[^0-9]/','',$leftright);

                    $year = date('Y', strtotime($data));
                    $month = date('n', strtotime($data));

                    switch($leftright){
                        case 0://-1 month
                            if ($month == 1){
                                $month = 12;
                                $year--;
                            }else{
                                $month--;
                            }
                            $data = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
//                            $month = date('n',strtotime($data. " - 1 month"));
//                            $data = date('Y-m-d',strtotime($data. " - 1 month"));
                            break;
                        case 1://+1 month
                            if ($month == 12){
                                $month = 1;
                                $year++;
                            }else{
                                $month++;
                            }
                            $data = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
//                            $month = date('n',strtotime($data. " 1 month"));
//                            $data = date('Y-m-d',strtotime($data. " 1 month"));
                            break;
                    }

                    $query = "select * from gogos where id =:id and month(dateS)=:mon and year(dateS)=:yea";
                    $array_day = Yii::$app->db->createCommand($query,[
                            'id' => $id,
                            'mon' => $month,
                            'yea' => $year,
                        ]
                    )->queryAll();


                    $item_month = $_monthsList[$month];//текущий месяц

                    return $this->renderAjax('_only_calso',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'id',
                        'item_month',
                        'array_day'
                    ));

                    break;
                case 999://for tests
                    echo 'Отметка невозможна';
                    exit;
                    break;
                default:
//                    return;
                    break;
//                    return;
//
                    //exit;
            }

            return null;
        }
//        checking($this);

        $model_item_date->item_date = date("d.m.Y");//'01.11.2019';//date("d.m.Y");
        $data = date("Y-m-d");//'2019-11-01';//date("Y-m-d");

        if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
            $_spiski = '_weekends_or_holidays';
        }else{
            $_spiski = '_spiskiso';

            $model_sp_begin = $qu->show_sp_so($data);
        }

        return $this->render('spsoview',compact(
            'model_d_antidate',
            'model_d_date',
            'model_item_date',
            'data',
            '_spiski',
            'dataProvider_spiski',
            'model_sp_begin',
            'model_id',
            'array_disabled_dates'));


    }


}