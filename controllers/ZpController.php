<?php

namespace app\controllers;

use app\models\Id;
use app\models\Zp;
use Yii;
use yii\web\Controller;

class ZpController extends Controller
{


    public function actionZp(){



//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

//        if (verification_user()!=555){
//            return $this->render('net_dostupa');
//        }
        $model = new Zp();
        $model_id = new Id();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($model_id->load(Yii::$app->request->post()) && $model_id->validate()){

                $model->load(Yii::$app->request->post());

                $id_so = preg_replace('/[^0-9]/','',$model->id_so);

                switch($model_id->id){
                    case 0:

                        $query2 = "select sotrudniki.id[id_s],rtrim(sotrudniki.dol)[dol],rtrim(sotrudniki.name)[nameso],isnull(all_oklad.id_oklad,0)[idokl],all_oklad.*,dol_oklad.*,case when oklad is not null then convert(numeric (9,2),(
 (((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)
)) else null end [summ]
,case when oklad is not null then convert(numeric (9,2),(
((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)) -
((((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) + (((((oklad/100)*percent_oklad) + (((oklad/100)*percent_oklad) * ppk) + ((((oklad/100)*percent_oklad) / 100)*vred)) / 100)*sever)) - children)*0.13)
)) else null end [posle_vicheta_naloga] from sotrudniki left outer join all_oklad on sotrudniki.id = all_oklad.idso left outer join dol_oklad on all_oklad.id_oklad = dol_oklad.id where outS is null and sotrudniki.dol not like '%латные услуги%' and sotrudniki.id = :id_so order by sotrudniki.name ASC,percent_oklad DESC";

                        /*$query2 = "select sotrudniki.id[id_s],rtrim(sotrudniki.dol)[dol],rtrim(sotrudniki.name)[nameso],isnull(all_oklad.id_oklad,0)[idokl],all_oklad.*,dol_oklad.*,case when oklad is not null then convert(numeric (9,2),(((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred))) else null end [summ],case when oklad is not null then convert(numeric (9,2),((((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred))-((((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred)))*0.13)) else null end [posle_vicheta_naloga] from sotrudniki left outer join all_oklad on sotrudniki.id = all_oklad.idso left outer join dol_oklad on all_oklad.id_oklad = dol_oklad.id where outS is null and sotrudniki.dol not like '%латные услуги%' and sotrudniki.id = :id_so order by sotrudniki.name ASC,percent_oklad DESC";*/
                        $array2 = Yii::$app->db->createCommand($query2,['id_so' => $id_so])->queryAll();

                        foreach ($array2 as $value) {
                            if((Int)($value['idokl']) == 0){
                                return 400;
                            }
                        }
                        return $this->renderAjax('table_this3',compact('array2'));
                        break;
                    case 1:
                        $ppk = (Yii::$app->request->post('ppk'));
                        $idident = (Yii::$app->request->post('idident'));
                        $ppk = preg_replace('/[^0-9\.]/','',$ppk);
                        $idident = preg_replace('/[^0-9]/','',$idident);
                        $query = "update all_oklad set ppk=:ppk where idident=:idident";
                        Yii::$app->db->createCommand($query,['ppk' => $ppk,'idident' => $idident])->execute();

                        $array = $model->show_zp_all_distinct();

                        /*foreach ($array2 as $value) {
                            if((Int)($value['idokl']) == 0){
                                return 400;
                            }
                        }*/

                        /*$countSum = 0;
                        $countSum2 = 0;

                        foreach ($array as $value) {
                            $countSum += $value['summ2'];
                            $countSum2 += $value['posle'];
                        }*/
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 2:
                        $percent_oklad = (Yii::$app->request->post('percent_oklad'));
                        $idident = (Yii::$app->request->post('idident'));
                        $percent_oklad = preg_replace('/[^0-9\.]/','',$percent_oklad);
                        $idident = preg_replace('/[^0-9]/','',$idident);
                        $query = "update all_oklad set percent_oklad=:percent_oklad where idident=:idident";
                        Yii::$app->db->createCommand($query,['percent_oklad' => $percent_oklad,'idident' => $idident])->execute();

                        $array = $model->show_zp_all_distinct();
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 3://children
                        $children = (Yii::$app->request->post('children'));
                        $idident = (Yii::$app->request->post('idident'));
                        $children = preg_replace('/[^0-9\.]/','',$children);
                        $idident = preg_replace('/[^0-9]/','',$idident);
                        $query = "update all_oklad set children=:children where idident=:idident";
                        Yii::$app->db->createCommand($query,['children' => $children,'idident' => $idident])->execute();

                        $array = $model->show_zp_all_distinct();
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 4:
                        $vred = (Yii::$app->request->post('vred'));
                        $idident = (Yii::$app->request->post('idident'));
                        $vred = preg_replace('/[^0-9\.]/','',$vred);
                        $idident = preg_replace('/[^0-9]/','',$idident);
                        $query = "update all_oklad set vred=:vred where idident=:idident";
                        Yii::$app->db->createCommand($query,['vred' => $vred,'idident' => $idident])->execute();

                        $array = $model->show_zp_all_distinct();
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 5:
                        return json_encode(Yii::$app->db->createCommand("select * from dol_oklad")->queryAll());
                        break;
                    case 6:
                        $model->validate();
                        $query = "insert into all_oklad (idso,id_oklad,ppk,children,vred,percent_oklad) values (:idso,:id_oklad,:ppk,:children,:vred,:percent_oklad)";
                        Yii::$app->db->createCommand($query,[
                            'idso' => $model->id_so,
                            'id_oklad' => $model->id_oklad,
                            'ppk' => $model->ppk,
                            'children' => $model->children,
                            'vred' => $model->vred,
                            'percent_oklad' => $model->percent,
                        ])->execute();

                        $array = $model->show_zp_all_distinct();
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 7:
                        $idident = (Yii::$app->request->post('idident'));
                        $idident = preg_replace('/[^0-9]/','',$idident);
                        $query = "delete all_oklad where idident=:idident";
                        Yii::$app->db->createCommand($query,['idident' => $idident])->execute();

                        $array = $model->show_zp_all_distinct();
                        return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        break;
                    case 8:
                        if ((Int)$id_so == 1575){
                            $array = $model->show_zp_all_distinct();
                            return $this->renderAjax('zp_all_table',compact('array','model','model_id'));
                        }
                        break;



                }
            }
            exit;
        }






//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);



//        $array = $model->show_zp_all_distinct();


        /*$query5 = "select sotrudniki.id[id_s],rtrim(sotrudniki.dol)[dol],rtrim(sotrudniki.name)[nameso],isnull(all_oklad.id_oklad,0)[idokl],all_oklad.*,dol_oklad.*,case when oklad is not null then convert(numeric (9,2),(((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred))) else null end [summ],case when oklad is not null then convert(numeric (9,2),((((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred))-((((oklad/100)*percent_oklad) + (oklad * ppk) + ((oklad / 100)*sever) + ((oklad / 100)*children) + ((oklad / 100)*vred)))*0.13)) else null end [posle_vicheta_naloga] from sotrudniki left outer join all_oklad on sotrudniki.id = all_oklad.idso left outer join dol_oklad on all_oklad.id_oklad = dol_oklad.id where outS is null and sotrudniki.dol not like '%латные услуги%' order by sotrudniki.name ASC,percent_oklad DESC";
        $array5 = Yii::$app->db->createCommand($query5)->queryAll();*/




        return $this->render('zp_index');
    }

}
