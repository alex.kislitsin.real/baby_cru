<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 30.07.19
 * Time: 15:52
 */
namespace app\controllers;
use app\models\AjaxText;
use app\models\deti;
use app\models\Disable_date;
use app\models\Gruppa;
use app\models\Id;
use app\models\Item_date;
use yii\db\mssql\PDO;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use app\models\TestForm;
use yii\db\Connection;

class MyController extends Controller{

    //public $layout = 'basic';

    public function actionIndex(){
        change_db_attr();
        $query_gruppa = "select * from gruppa order by id";

        $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();

        $array_gruppa = ArrayHelper::index($array_gruppa,'id');

        $array_test1 = ArrayHelper::map($array_gruppa,'id','name');

        //$array_gruppa = ArrayHelper::getColumn($array_gruppa,'name');



        $model = new TestForm();
        if($model->load(Yii::$app->request->post())){
            //debug($model);
            //die;
            if($model->validate()){
                Yii::$app->session->setFlash('success','Данные приняты');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error','Ошибка !');
            }
        }

        return $this->render('index',compact('model','array_gruppa','array_test1'));
    }



    public function actionShow(){

        $name_base = Yii::$app->request->cookies->getValue('name_base');
        $us = Yii::$app->request->cookies->getValue('user');
        $id_u = Yii::$app->request->cookies->getValue('id_user');
        if(empty($name_base)||empty($us)||empty($id_u)){
            return $this->redirect(['site/login']);
        }

        change_db_attr();
        $model_item_date = new Item_date();
        $model_id = new Id();
        $model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);

        $nametable = 'deti';

        if(empty($model_d_date) || !in_array(date('Y-01-01'),$model_d_date)){
            $model_d_date_query = "select date_hol from holidays";
            $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
            $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');

            $json_array_dates = json_encode($model_d_date);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_dates',
                'value' => $json_array_dates,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }

        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());
            $model_item_date->load(Yii::$app->request->post());
            $item_date = $model_item_date->item_date;
            $data = $model_item_date->item_date;
            $data = Yii::$app->formatter->asTime($data);
            switch($model_id->id){
                case 0:

                    break;
                case 1:
                    $dataProvider = new \yii\data\SqlDataProvider([
                        'sql' => 'SET NOCOUNT ON; EXEC Dvizh2222 @data=:data, @nametable=:nametable',
                        'params' => [':data' => $data,':nametable'  => $nametable],
                        'pagination' => false,
                        'sort' => false,
                    ]);

                    $model_dvig = $dataProvider->getModels();
                    $model_dvig = ArrayHelper::index($model_dvig,'idid');
                    $percent = '9%';

//                    return $this->renderAjax('show',compact('dataProvider','model_dvig','model_item_date','item_date','model_d_date','model_id'));
                    return
                    $this->renderAjax('dvigenie_table',compact(
                        'dataProvider',
                        'model_dvig',
                        'percent'));

                    break;
            }
            return null;
        }

        $model_item_date->item_date = date("d.m.Y");
        $data = date("Y-m-d");

        $dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SET NOCOUNT ON; EXEC Dvizh2222 @data=:data, @nametable=:nametable',
            'params' => [':data' => $data,':nametable'  => $nametable],
            'pagination' => false,
            'sort' => false,
        ]);

        $model_dvig = $dataProvider->getModels();
        $model_dvig = ArrayHelper::index($model_dvig,'idid');

        return $this->render('show',compact('dataProvider','model_dvig','model_item_date','item_date','model_d_date','model_id'));

    }



    public function actionTest333(){

        $model_item_date = new Item_date();
        $model_item_date->item_date = date("d.m.Y");
        $model_id = new Id();
//        $model_d_date = new Disable_date();
//        change_db_attr();
//        $model_d_date_query = "select date_hol from holidays";
//        $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
//        $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');
//        change_db_attr();
//        $model = new AjaxText();
//        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

//        if (Yii::$app->request->isAjax) {
//            $model->text = 'fff';
//            return [
//                    "data" => $model,
//                    "error" => null
//                ];
//        }else{
//            $model->text = 'qqq';
////            return $model;
//        }

        return $this->render('test333',compact('model_id'));
    }


}