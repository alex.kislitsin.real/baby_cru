<?php

namespace app\controllers;

use app\models\Antro;
use app\models\Gruppa;
use app\models\Id;
use app\models\Month;
use app\models\Year;
use Yii;
use yii\web\Controller;
use app\models\Queries;

class AntroController extends Controller
{
    public function actionAntro(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $model_id = new Id();

        $model_y = new Year();
        $model_m = new Month();
        $model_group = new Gruppa();
        $model_antro = new Antro();
        $qu = new Queries();

        $array_year_tabel_deti = [
            2019 => '2019',
            2020 => '2020',
            2021 => '2021',
            2022 => '2022',
            2023 => '2023',
        ];

        $array_vremya_goda = [
            1 => 'январь - июнь',
            2 => 'июль - декабрь'
        ];

        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);


        if (Yii::$app->request->isAjax) {
//            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model_id->load(Yii::$app->request->post());
            if ($model_id->validate()){
                switch($model_id->id){
                    case 0:
                        if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                            ($model_m->load(Yii::$app->request->post()) && $model_m->validate()) &&
                            ($model_group->load(Yii::$app->request->post()) && $model_group->validate())){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $array = $qu->show_sp_antro($model_m->name,$model_y->name,$model_group->name);
                                $array2 = $qu->show_sp_antro_mebel($model_m->name,$model_y->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('antro_all_table',compact(
                                'array','array2','model_antro','model_id'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 1://insert
                        if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                            ($model_m->load(Yii::$app->request->post()) && $model_m->validate()) &&
                            ($model_group->load(Yii::$app->request->post()) && $model_group->validate()) &&
                            ($model_antro->load(Yii::$app->request->post()) && $model_antro->validate())){
                            switch($model_m->name){
                                case 1:
                                    $array_y = [1,2,3,4,5,6];
                                    break;
                                case 2:
                                    $array_y = [7,8,9,10,11,12];
                                    break;
                            }

                            if ($model_y->name < date('Y')){
                                $model_m->name == 1 ? $date = date('Y-m-d',strtotime($model_y->name.'-03-01')) : $date = date('Y-m-d',strtotime($model_y->name.'-09-01'));
                            }
                            if ($model_y->name > date('Y')){
                                $model_m->name == 1 ? $date = date('Y-m-d',strtotime($model_y->name.'-03-01')) : $date = date('Y-m-d',strtotime($model_y->name.'-09-01'));
                            }
                            if ($model_y->name == date('Y')){
                                if (in_array(date('n'),[1,2,3,4,5,6])){
                                    $current_period = 1;
                                }else{
                                    $current_period = 2;
                                }
                                if($model_m->name < $current_period){
                                    $date = date('Y-m-d',strtotime($model_y->name.'-03-01'));
                                }elseif($model_m->name == $current_period){
                                    $date = date('Y-m-d');
                                }else{
                                    $date = date('Y-m-d',strtotime($model_y->name.'-09-01'));
                                }
                            }

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query_insert = "insert into rostves (id_child,dat,rost,ves) values (:id_child,:dat,:rost,:ves)";
                                Yii::$app->db->createCommand($query_insert,[
                                        'id_child' => $model_antro->id_child,
                                        'dat' => $date,
                                        'rost' => $model_antro->rost,
                                        'ves' => $model_antro->ves,
                                    ]
                                )->execute();
                                $array = $qu->show_sp_antro($model_m->name,$model_y->name,$model_group->name);
                                $array2 = $qu->show_sp_antro_mebel($model_m->name,$model_y->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }



                            return $this->renderAjax('antro_all_table',compact(
                                'array','array2','model_antro','model_id'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 2://update
                        if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                            ($model_m->load(Yii::$app->request->post()) && $model_m->validate()) &&
                            ($model_group->load(Yii::$app->request->post()) && $model_group->validate()) &&
                            ($model_antro->load(Yii::$app->request->post()) && $model_antro->validate())){

//                            return $model_antro;

                            switch($model_m->name){
                                case 1:
                                    $array_y = [1,2,3,4,5,6];
                                    break;
                                case 2:
                                    $array_y = [7,8,9,10,11,12];
                                    break;
                            }

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query_delete = "update rostves set rost=:rost, ves=:ves where id_child=:id_child and dat=:dat";
                                Yii::$app->db->createCommand($query_delete,[
                                        'id_child' => $model_antro->id_child,
                                        'dat' => $model_antro->dat,
                                        'rost' => $model_antro->rost,
                                        'ves' => $model_antro->ves,
                                    ]
                                )->execute();
                                $array = $qu->show_sp_antro($model_m->name,$model_y->name,$model_group->name);
                                $array2 = $qu->show_sp_antro_mebel($model_m->name,$model_y->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }



                            return $this->renderAjax('antro_all_table',compact(
                                'array','array2','model_antro','model_id'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 3://only group
                        if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                            ($model_m->load(Yii::$app->request->post()) && $model_m->validate()) &&
                            ($model_group->load(Yii::$app->request->post()) && $model_group->validate())){

                            $array = $qu->show_sp_antro($model_m->name,$model_y->name,$model_group->name);

                            return $this->renderAjax('antro_table',compact(
                                'array','model_antro','model_id'
                            ));
                        }else{
                            return 400;
                        }
                        break;
                    case 4://delete
                        if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                            ($model_m->load(Yii::$app->request->post()) && $model_m->validate()) &&
                            ($model_group->load(Yii::$app->request->post()) && $model_group->validate()) &&
                            ($model_antro->load(Yii::$app->request->post()) && $model_antro->validate())){

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query_delete = "delete from rostves where id_child=:id_child and dat=:dat";
                                Yii::$app->db->createCommand($query_delete,[
                                        'id_child' => $model_antro->id_child,
                                        'dat' => $model_antro->dat,
                                    ]
                                )->execute();
                                $array = $qu->show_sp_antro($model_m->name,$model_y->name,$model_group->name);
                                $array2 = $qu->show_sp_antro_mebel($model_m->name,$model_y->name);
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }



                            return $this->renderAjax('antro_all_table',compact(
                                'array','array2','model_antro','model_id'
                            ));
                        }else{
                            return 400;
                        }
                        break;

                }
            }
            return null;
        }
//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);

        $model_id->id = 1;

        $period = 1;

        $current_month = date('n');
        if ($current_month >= 1 && $current_month < 7){//spring
            $period = 1;
            $model_m->name = 1;
        }elseif($current_month >= 7 && $current_month < 12){//ourtemn
            $period = 2;
            $model_m->name = 2;
        }

        $item_gruppa = trim($array_gruppa[1]);

        $year = date('Y');

        if (!empty($year)) {
            $transaction = Yii::$app->db->beginTransaction();
            try{
                $array = $qu->show_sp_antro($period,$year,1);
                $array2 = $qu->show_sp_antro_mebel($period,$year);
                $transaction->commit();
            }catch (Exception $e){
                $transaction->rollBack();
            }
        }else{
            exit;
        }

//        $nametable = 'deti';

        $model_y->name = $year;

        $model_group->name = 1;

        return $this->render('antro_index',compact(
            'model_antro',
            'array_gruppa',
            'model_group',
            'model_m',
            'array',
            'array2',
            'model_y',
            'model_id',
            'array_vremya_goda',
            'array_year_tabel_deti'

        ));
    }

}
