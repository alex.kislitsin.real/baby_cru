<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 17.11.19
 * Time: 14:05
 */
/**
 * LoginForm is the model behind the login form.
 *
 * @qu app\models\Queries;
 *
 */
namespace app\controllers;


use app\models\Gruppa;
use app\models\Id;
use app\models\Injection;
use app\models\Queries;
use app\models\VR;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class InjectionController extends Controller{

    public function actionInj(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();


        $model = new Injection();
        $model_vr = new Vr();
        $model_group = new Gruppa();
        $model_id = new Id();
        $qu = new Queries();

        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);

        if (empty($array_gruppa)){
            $query_gruppa = "select * from gruppa order by id";
            $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
            $array_gruppa = ArrayHelper::index($array_gruppa,'id');
            $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

            $json_array_group = json_encode($array_gruppa);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_group',
                'value' => $json_array_group,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
        if(empty($id_gruppa)){
            $model_group->name = 1;
            $id_gruppa = 1;
        }else{
            $model_group->name = $id_gruppa;
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());
            $model_group->load(Yii::$app->request->post());
            $model->load(Yii::$app->request->post());

//            $model->scenario = $model::SCENARIO1;
            if ($model_id->validate()&&$model_group->validate()){
                $id_gruppa = $model_group->name;
                switch($model_id->id){
                    case 1://
                        if ($model->validate()){
                            $query = "select * from injection where id_child=:id_child and id_inj=:id_inj";
                            $array_one = Yii::$app->db->createCommand($query,[
                                'id_child' => $model->id_child,
                                'id_inj' => $model->id_inj,
                            ])->queryAll();
                            return json_encode($array_one);
                        }else{
                            return 400;
                        }
                        break;
                    case 2:
                        if ($model->validate()){

//                            return $model;
                            $date_check = date('1900-01-01');

                            $date_v1 = $model->date_inj_v1;
                            $date_v2 = $model->date_inj_v2;
                            $date_v3 = $model->date_inj_v3;
                            $date_r1 = $model->date_inj_r1;
                            $date_r2 = $model->date_inj_r2;
                            $date_r3 = $model->date_inj_r3;

                            $b = array();
                            $ar = array($date_v1,$date_v2,$date_v3,$date_r1,$date_r2,$date_r3);
                            foreach($ar as $d){
                                if ($d > $date_check){
                                    array_push($b,$d);
                                }
                                if ($d < $date_check){
                                    return 400;
                                }
                            }
                            $res = array_unique($b);
                            if (count($b)!=count($res)){
                                return 888;
                            }

//                            return;

                            $transaction = Yii::$app->db->beginTransaction();
                            try{


                            ////////////////////////////////////////////////////////        v1
                            $vv1 = $model->vv1;
                            $date_v1 = $model->date_inj_v1;
                            if($vv1!=0){
                                //update
                                if($date_v1 > $date_check){//если не 1900-01-01
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and v=1 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_v1,
                                            'doza' => $model->doza_v1,
                                            'seriya' => $model->seriya_v1,
                                            'name_preparat' => $model->name_preparat_v1,
                                            'medotvod' => $model->medotvod_v1,
                                            'reaction' => $model->reaction_v1,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and v=1 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_v1 > $date_check){
                                    $query = "insert into injection (id_child,v,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:v,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'v' => 1,
                                            'date_inj' => $model->date_inj_v1,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_v1,
                                            'seriya' => $model->seriya_v1,
                                            'name_preparat' => $model->name_preparat_v1,
                                            'medotvod' => $model->medotvod_v1,
                                            'reaction' => $model->reaction_v1,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            ////////////////////////////////////////////////////////        v2

                            $vv2 = $model->vv2;
                            $date_v2 = $model->date_inj_v2;
                            if($vv2!=0){
                                //update
                                if($date_v2 > $date_check){//если не 1900-01-01
                                    //(id_child,r,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)
                                    //return $model;
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and v=2 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_v2,
                                            'doza' => $model->doza_v2,
                                            'seriya' => $model->seriya_v2,
                                            'name_preparat' => $model->name_preparat_v2,
                                            'medotvod' => $model->medotvod_v2,
                                            'reaction' => $model->reaction_v2,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and v=2 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_v2 > $date_check){
                                    $query = "insert into injection (id_child,v,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:v,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'v' => 2,
                                            'date_inj' => $model->date_inj_v2,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_v2,
                                            'seriya' => $model->seriya_v2,
                                            'name_preparat' => $model->name_preparat_v2,
                                            'medotvod' => $model->medotvod_v2,
                                            'reaction' => $model->reaction_v2,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            ////////////////////////////////////////////////////////        v3

                            $vv3 = $model->vv3;
                            $date_v3 = $model->date_inj_v3;
                            if($vv3!=0){
                                //update
                                if($date_v3 > $date_check){//если не 1900-01-01
                                    //(id_child,r,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)
                                    //return $model;
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and v=3 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_v3,
                                            'doza' => $model->doza_v3,
                                            'seriya' => $model->seriya_v3,
                                            'name_preparat' => $model->name_preparat_v3,
                                            'medotvod' => $model->medotvod_v3,
                                            'reaction' => $model->reaction_v3,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and v=3 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_v3 > $date_check){
                                    $query = "insert into injection (id_child,v,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:v,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'v' => 3,
                                            'date_inj' => $model->date_inj_v3,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_v3,
                                            'seriya' => $model->seriya_v3,
                                            'name_preparat' => $model->name_preparat_v3,
                                            'medotvod' => $model->medotvod_v3,
                                            'reaction' => $model->reaction_v3,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            ////////////////////////////////////////////////////////        r1

                            $rr1 = $model->rr1;
                            $date_r1 = $model->date_inj_r1;
                            if($rr1!=0){
                                //update
                                if($date_r1 > $date_check){//если не 1900-01-01
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and r=1 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_r1,
                                            'doza' => $model->doza_r1,
                                            'seriya' => $model->seriya_r1,
                                            'name_preparat' => $model->name_preparat_r1,
                                            'medotvod' => $model->medotvod_r1,
                                            'reaction' => $model->reaction_r1,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and r=1 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_r1 > $date_check){
                                    $query = "insert into injection (id_child,r,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:r,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'r' => 1,
                                            'date_inj' => $model->date_inj_r1,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_r1,
                                            'seriya' => $model->seriya_r1,
                                            'name_preparat' => $model->name_preparat_r1,
                                            'medotvod' => $model->medotvod_r1,
                                            'reaction' => $model->reaction_r1,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            ////////////////////////////////////////////////////////        r2

                            $rr2 = $model->rr2;
                            $date_r2 = $model->date_inj_r2;
                            if($rr2!=0){
                                //update
                                if($date_r2 > $date_check){//если не 1900-01-01
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and r=2 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_r2,
                                            'doza' => $model->doza_r2,
                                            'seriya' => $model->seriya_r2,
                                            'name_preparat' => $model->name_preparat_r2,
                                            'medotvod' => $model->medotvod_r2,
                                            'reaction' => $model->reaction_r2,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and r=2 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_r2 > $date_check){
                                    $query = "insert into injection (id_child,r,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:r,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'r' => 2,
                                            'date_inj' => $model->date_inj_r2,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_r2,
                                            'seriya' => $model->seriya_r2,
                                            'name_preparat' => $model->name_preparat_r2,
                                            'medotvod' => $model->medotvod_r2,
                                            'reaction' => $model->reaction_r2,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            ////////////////////////////////////////////////////////        r3

                            $rr3 = $model->rr3;
                            $date_r3 = $model->date_inj_r3;
                            if($rr3!=0){
                                //update
                                if($date_r3 > $date_check){//если не 1900-01-01
                                    $query = "update injection set date_inj=:date_inj,doza=:doza,seriya=:seriya,name_preparat=:name_preparat,medotvod=:medotvod,reaction=:reaction where id_child=:id_child and r=3 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_inj' => $model->id_inj,
                                            'id_child' => $model->id_child,
                                            'date_inj' => $model->date_inj_r3,
                                            'doza' => $model->doza_r3,
                                            'seriya' => $model->seriya_r3,
                                            'name_preparat' => $model->name_preparat_r3,
                                            'medotvod' => $model->medotvod_r3,
                                            'reaction' => $model->reaction_r3,
//                                            '' => $model->,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }else{
                                    //delete
                                    $query = "delete from injection where id_child=:id_child and r=3 and id_inj=:id_inj";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'id_inj' => $model->id_inj,
                                        ]
                                    )->execute();

                                }
                            }else{
                                //insert
                                if($date_r3 > $date_check){
                                    $query = "insert into injection (id_child,r,date_inj,id_inj,doza,seriya,name_preparat,medotvod,reaction)values(:id_child,:r,:date_inj,:id_inj,:doza,:seriya,:name_preparat,:medotvod,:reaction)";
                                    Yii::$app->db->createCommand($query,[
                                            'id_child' => $model->id_child,
                                            'r' => 3,
                                            'date_inj' => $model->date_inj_r3,
                                            'id_inj' => $model->id_inj,
                                            'doza' => $model->doza_r3,
                                            'seriya' => $model->seriya_r3,
                                            'name_preparat' => $model->name_preparat_r3,
                                            'medotvod' => $model->medotvod_r3,
                                            'reaction' => $model->reaction_r3,
//                                            '' => $model->,
                                        ]
                                    )->execute();

                                }
                            }

                            $otkaz = (Yii::$app->request->post('otkaz'));
                            $old_otkaz = (Yii::$app->request->post('old_otkaz'));

                            $otkaz = preg_replace('/[^0-9]/','',$otkaz);
                            $old_otkaz = preg_replace('/[^0-9]/','',$old_otkaz);

                            switch($old_otkaz){
                                case 0://значит отказ вставляем строку
                                    if($otkaz==1){
                                        $query = "insert into injection (id_child,date_inj,id_inj,otkaz)values(:id_child,getdate(),:id_inj,:otkaz)";
                                        Yii::$app->db->createCommand($query,[
                                                'id_child' => $model->id_child,
//                                                'date_inj' => 'getdate()',
                                                'id_inj' => $model->id_inj,
                                                'otkaz' => 1,
                                            ]
                                        )->execute();
                                    }
                                    break;
                                case 1://отметка отказ уже была
                                    if($otkaz==0){
                                        $query = "delete from injection where id_child=:id_child and otkaz=1 and id_inj=:id_inj";
                                        Yii::$app->db->createCommand($query,[
                                                'id_child' => $model->id_child,
                                                'id_inj' => $model->id_inj,
                                            ]
                                        )->execute();
                                    }
                                    break;
                            }

                            $array = $qu->show_journal_injection($id_gruppa);

                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }

                            return $this->renderAjax('table',compact('model_id','model','array','model_vr'));
//                            return $model;


                        }else{
                            return 400;
                        }
                        break;
                    case 3:
                        $cookie = new \yii\web\Cookie([
                            'name' => 'save_last_group',
                            'value' => $id_gruppa,
                        ]);
                        Yii::$app->response->cookies->add($cookie);


                        $array = $qu->show_journal_injection($id_gruppa);

                        if (count($array)<1){
                            return 400;
                        }else{
                            return $this->renderAjax('table',compact('model_id','model','array','model_vr'));
                        }
//                        return $id_gruppa;
                        break;
                    case 4:
                        /*$dataProvider = new \yii\data\SqlDataProvider([
                            'sql' => 'SET NOCOUNT ON; EXEC Show_plan_injection',
                            'pagination' => false,
                            'sort' => false,
                        ]);
                        $array = $dataProvider->getModels();*/

                        $array = $qu->show_plan_injection();

                        if (count($array)<1){
                            return 400;
                        }else{
                            return $this->renderAjax('modal_plan',compact('array'));
                        }
                        break;
                }
            }
            return null;
        }







        /*$dataProvider = new \yii\data\SqlDataProvider([
            'sql' => 'SET NOCOUNT ON; EXEC ShowInjectionProc @id_group=:id_gruppa',
            'params' => [':id_gruppa'  => $id_gruppa],
            'pagination' => false,
            'sort' => false,
        ]);
        $array = $dataProvider->getModels();*/

        $array = $qu->show_journal_injection($id_gruppa);

        return $this->render('inj',compact('model_group','array_gruppa','model_id','model','array','model_vr'));
    }

} 