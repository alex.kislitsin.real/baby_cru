<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 30.07.19
 * Time: 15:52
 */
namespace app\controllers;
use app\components\NumericCaptcha;
use app\models\Disable_date;
use app\models\Gruppa;
use app\models\Id;
use app\models\Optd;
use app\models\Sotrudniki;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use Yii;
use yii\db\Connection;
use app\models\Item_date;
use app\models\Queries;

class SpController extends Controller{

    public function actionSpview(){

//        show_layout_light($this);

//        Yii::$app->session->setFlash('success', 'Все прошло удачно');
//        vul($this);

        $name_base = Yii::$app->request->cookies->getValue('name_base');
        $us = Yii::$app->request->cookies->getValue('user');
        $id_u = Yii::$app->request->cookies->getValue('id_user');
        /*if(empty($name_base)||empty($us)||empty($id_u)){
            return $this->redirect(['site/login']);
        }

        change_db_attr();*/

        $model_group = new Gruppa();
        $model_item_date = new Item_date();
        $model_d_date = new Disable_date();
        $model_id = new Id();
        $qu = new Queries();

        $model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
        $model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

        if(empty($model_d_date) || count($model_d_date) < 1){
            return $this->redirect(['site/login']);
        }

//        $check_holidays = date('Y-m-d',strtotime(date('Y-01-01'). " 1 year"));
        /*if(count($model_d_date)<1 || !isset($model_d_date) || $model_d_date == null || $model_d_date === false){
            $model_d_date_query = "select * from holidays,works_day";
            $model_d_date_result = Yii::$app->db->createCommand($model_d_date_query)->queryAll();
            $model_d_date = ArrayHelper::getColumn($model_d_date_result,'date_hol');
            $model_d_antidate = ArrayHelper::getColumn($model_d_date_result,'date_work');
            $model_d_antidate = array_unique($model_d_antidate);
            $json_array_dates = json_encode($model_d_date);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_dates',
                'value' => $json_array_dates,
            ]);
            Yii::$app->response->cookies->add($cookie);
            $json_array_antidates = json_encode($model_d_antidate);
            $cookie2 = new \yii\web\Cookie([
                'name' => 'array_antidates',
                'value' => $json_array_antidates,
            ]);
            Yii::$app->response->cookies->add($cookie2);
        }*/

        $array_gruppa = json_decode(Yii::$app->request->cookies->getValue('array_group'), true);

        if (empty($array_gruppa)){
            $query_gruppa = "select * from gruppa order by id;";
            $array_gruppa = Yii::$app->db->createCommand($query_gruppa)->queryAll();
            $array_gruppa = ArrayHelper::index($array_gruppa,'id');
            $array_gruppa = ArrayHelper::map($array_gruppa,'id','name');

            $json_array_group = json_encode($array_gruppa);
            $cookie = new \yii\web\Cookie([
                'name' => 'array_group',
                'value' => $json_array_group,
            ]);
            Yii::$app->response->cookies->add($cookie);
        }

        $array_disabled_dates = array();
        foreach($model_d_date as $date){
            $date = date('d.m.Y',  strtotime($date));
            array_push($array_disabled_dates,$date);
        }

//        $model_dvig = array();

        $_monthsList = array(
            "1"=>"Январь","2"=>"Февраль","3"=>"Март",
            "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
            "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
            "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");

        if (Yii::$app->request->isAjax) {

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3
            if ($model_id->validate()){


            switch($model_id->id){
                case 1://при выборе группы
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

//                    if(($array_disabled_dates[$data]) || (date('N', strtotime($data)) == 6) || (date('N', strtotime($data)) == 7)){
                    if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
                        $_spiski = '_weekends_or_holidays';
                    }else{
                        $_spiski = '_spiski';

                        $model_group->load(Yii::$app->request->post());
                        $id_gruppa = $model_group->name;
                        $item_gruppa = trim($array_gruppa[$id_gruppa]);

                        $cookie = new \yii\web\Cookie([
                            'name' => 'save_last_group',
                            'value' => $id_gruppa,
                        ]);
                        Yii::$app->response->cookies->add($cookie);

//                        $qu = new Queries();
                        $model_sp_begin = $qu->show_sp($data,$id_gruppa);
                    }

                    return $this->renderAjax('spview',compact(
                        'model_dvig',
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'model_group',
                        'array_gruppa',
                        'item_gruppa',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 2://при выборе даты
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

//                    if(($array_disabled_dates[$data]) || (date('N', strtotime($data)) == 6) || (date('N', strtotime($data)) == 7)){
                    if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5)) && !in_array($data,$model_d_antidate)){
                        $_spiski = '_weekends_or_holidays';
                        $model_sp_begin = array();
                    }else{
                        $_spiski = '_spiski';

                        $model_group->load(Yii::$app->request->post());
                        $id_gruppa = $model_group->name;
                        $item_gruppa = trim($array_gruppa[$id_gruppa]);

//                        $qu = new Queries();
                        $model_sp_begin = $qu->show_sp($data,$id_gruppa);
                    }

                    return $this->renderAjax('_sp_cal',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'model_group',
                        'array_gruppa',
                        'item_gruppa',
                        '_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 3://при нажатии на ребенка в списке -> сделать отметку
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $reason = (Yii::$app->request->post('reason'));
//                    $reason = preg_replace('/[a-zA-Z0-9\'\"\;]/','',$reason);
                    $reason = preg_replace('/[^0-9]/','',$reason);
                    $id = preg_replace('/[^0-9]/','',$id);
                    $id_u = preg_replace('/[^0-9]/','',$id_u);
                    //$us = preg_replace('/[^0-9]/','',$us);
                    /*if ($us==555){///////////////////////////////////////////////////////////////////

                    }else{

                    }*/
                    $model_group->load(Yii::$app->request->post());
                    $id_gruppa = $model_group->name;
                    $id_gruppa = preg_replace('/[^0-9]/','',$id_gruppa);

//                    $model_group->load(Yii::$app->request->post());
//                    $id_gruppa = $model_group->name;
                    $item_gruppa = trim($array_gruppa[$id_gruppa]);

                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $query = "insert into gogo (id,id_child,datenotgo) values (:id,:id_child,:datas);";
                        Yii::$app->db->createCommand($query,[
                                'id' => $reason,//gogo.id
                                'id_child' => $id,
                                'datas' => $data,
                            ]
                        )->execute();

                        $model_sp_begin = $qu->show_sp($data,$id_gruppa);
                        $transaction->commit();
                    } catch (Exception $e) {
                        $transaction->rollback();
                    }



                    /*if (strpos($reason,'НЕ') !== false || strpos($reason,'ОПОЗД') !== false){
                        if (strpos($reason,'НЕ') !== false){
                            $id_gogo = 200;
                        }
                        if (strpos($reason,'ОПОЗД') !== false){
                            $id_gogo = 100;
                        }

                        $query = "insert into gogo (id_child,id_gruppa,datenotgo,today,surprise) values (:id_child,:id_gruppa,:datas,:reason,'');";
                        $query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),'оповещение',:reason,:datas);";
                    }else{

                        $query = "insert into gogo (id_child,id_gruppa,datenotgo,prichina,surprise) values (:id_child,:id_gruppa,:datas,:reason,'');";
                        $query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),'снятие с питания',:reason,:datas);";
                    }*/



                    /*Yii::$app->db->createCommand($query_h,[
                            'id_child' => $id,
                            'id_so' => $id_u,
                            'datas' => $data,
                            'reason' => $reason,
                        ]
                    )->execute();*/





                    return $this->renderAjax('_spiski',compact('model_sp_begin','model_id','data','model_d_date','model_d_antidate'));

                    break;
                case 4://при нажатии на ребенка -> календарик
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $id = (Yii::$app->request->post('id_child'));
                    $array_day = $qu->show_sp_calendar($id,$data);
                    $item_month = $_monthsList[date("n", strtotime($data))];//текущий месяц

                    return $this->renderAjax('_only_cal',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'data_item',
                        'id',
                        'item_month',
                        'array_day'
                    ));
                    break;
                case 5://при нажатии на ребенка -> календарик DELETE
                    $data = (Yii::$app->request->post('itemdate'));
                    $data = Yii::$app->formatter->asTime($data);
                    $id = (Yii::$app->request->post('id_child'));
                    $reason = (Yii::$app->request->post('reason'));
                    $id = preg_replace('/[^0-9]/','',$id);
                    $id_u = preg_replace('/[^0-9]/','',$id_u);
                    $reason = preg_replace('/[^0-9]/','',$reason);
                    if($reason==1){
                        $reas = 'удаление оповещения';
                    }else{
                        $reas = 'удаление отметки';
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        $query = "delete from gogo where datenotgo=:datas and id_child=:id_child;";
                        Yii::$app->db->createCommand($query,[
                                'id_child' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $array_day = $qu->show_sp_calendar($id,$data);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }


                    //$query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),:reas,:reason,:datas);";


                    /*Yii::$app->db->createCommand($query_h,[
                            'id_child' => $id,
                            'datas' => $data,
                            'id_so' => $id_u,
                            'reas' => $reas,
                            'reason' => '',
                        ]
                    )->execute();*/



                    return $this->renderAjax('_only_cal',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'id',
                        'array_day'
                    ));
                    break;
                case 6://при отметке на пустой дате при выбранном ребенке INSERT
                    $data = (Yii::$app->request->post('itemdate'));
                    $data = Yii::$app->formatter->asTime($data);
                    $id = (Yii::$app->request->post('id_child'));
                    $reason = (Yii::$app->request->post('reason'));
//                    $reason = preg_replace('/[a-zA-Z0-9\'\"\;]/','',$reason);
                    $reason = preg_replace('/[^0-9]/','',$reason);


                    $id = preg_replace('/[^0-9]/','',$id);
                    $id_u = preg_replace('/[^0-9]/','',$id_u);
                    $model_group->load(Yii::$app->request->post());
                    $id_gruppa = $model_group->name;
                    $id_gruppa = preg_replace('/[^0-9]/','',$id_gruppa);

                    /*if (strpos($reason,'ПРИДУ') !== false || strpos($reason,'ОПОЗД') !== false){
                        $query = "insert into gogo (id_child,id_gruppa,datenotgo,today,surprise) values (:id_child,:id_gruppa,:datas,:reason,'');";
                        $query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),'оповещение',:reason,:datas);";
                    }else{
                        $query = "insert into gogo (id_child,id_gruppa,datenotgo,prichina,surprise) values (:id_child,:id_gruppa,:datas,:reason,'');";
                        $query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),'снятие с питания',:reason,:datas);";
                    }*/

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        $query = "insert into gogo (id,id_child,datenotgo) values (:id,:id_child,:datas);";
                        Yii::$app->db->createCommand($query,[
                                'id' => $reason,//gogo.id
                                'id_child' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $array_day = $qu->show_sp_calendar($id,$data);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }



                    /*Yii::$app->db->createCommand($query_h,[
                            'id_child' => $id,
                            'id_so' => $id_u,
                            'datas' => $data,
                            'reason' => $reason,
                        ]
                    )->execute();*/



                    return $this->renderAjax('_only_cal',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'id',
                        'array_day'
                    ));

//                    exit;
                    break;
                case 7://
                    $data_item = (Yii::$app->request->post('itemdate'));
                    $data_item = Yii::$app->formatter->asTime($data_item);

                    $veruyu = verification_user();
                    if($veruyu == 555){
                        return verifi_date_today_med($data_item);
                    }else{
                        return verifi_date_today($data_item,$model_d_date,$model_d_antidate);
                    }
                    break;
                case 8://
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $reason = (Yii::$app->request->post('reason'));
                    $id_u = preg_replace('/[^0-9]/','',$id_u);
                    $reason = preg_replace('/[^0-9]/','',$reason);
                    if($reason==1){
                        $reas = 'удаление оповещения';
                    }else{
                        $reas = 'удаление отметки';
                    }

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        $query = "delete from gogo where datenotgo=:datas and id_child=:id_child;";
                        Yii::$app->db->createCommand($query,[
                                'id_child' => $id,
                                'datas' => $data,
                            ]
                        )->execute();
                        $model_group->load(Yii::$app->request->post());
                        $id_gruppa = $model_group->name;
                        $item_gruppa = trim($array_gruppa[$id_gruppa]);
                        $model_sp_begin = $qu->show_sp($data,$id_gruppa);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }


                    /*$query_h = "insert into history (kto,kogo,text_content,prichina,nadatu) values ((select name from sotrudniki where id=:id_so),(select name from deti where id=:id_child),:reas,:reason,:datas);";*/

                    /*Yii::$app->db->createCommand($query_h,[
                            'id_child' => $id,
                            'datas' => $data,
                            'id_so' => $id_u,
                            'reas' => $reas,
                            'reason' => '',
                        ]
                    )->execute();*/

                    $_spiski = '_spiski';

                    return $this->renderAjax('spview',compact(
                        'model_dvig',
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'model_group',
                        'array_gruppa',
                        'item_gruppa',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));

                    break;
                case 9://red sp only
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $veruyu = verification_user();
                    if($veruyu == 555){
                        return verifi_date_today_med($data);
                    }else{
                        return verifi_date_today($data,$model_d_date,$model_d_antidate);
                    }
                    break;
                case 10://red sp only пришел / отмена пришел
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $sur = (Yii::$app->request->post('sur'));
                    $sur = preg_replace('/[^\d]/','',$sur);
                    switch ($sur){
                        case 0:
                            $query = "update gogo set surprise = '+' where id_child=:id_child and datenotgo=:datas;";
                            break;
                        case 1:
                            $query = "update gogo set surprise = '' where id_child=:id_child and datenotgo=:datas;";
                            break;
                        default:
                            return;
                            break;
                    }

                    $transaction = Yii::$app->db->beginTransaction();
                    try{
                        Yii::$app->db->createCommand($query,[
                                'id_child' => $id,
                                'datas' => $data,
                            ]
                        )->execute();

                        $model_group->load(Yii::$app->request->post());
                        $id_gruppa = $model_group->name;
                        $item_gruppa = trim($array_gruppa[$id_gruppa]);
                        $model_sp_begin = $qu->show_sp($data,$id_gruppa);
                        $transaction->commit();
                    }catch (Exception $e){
                        $transaction->rollBack();
                    }

                    $_spiski = '_spiski';

                    return $this->renderAjax('spview',compact(
                        'model_dvig',
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'model_group',
                        'array_gruppa',
                        'item_gruppa',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 11://
                    $model_item_date->load(Yii::$app->request->post());
                    $data = $model_item_date->item_date;
                    $data = Yii::$app->formatter->asTime($data);

                    $model_group->load(Yii::$app->request->post());
                    $id_gruppa = $model_group->name;
                    $item_gruppa = trim($array_gruppa[$id_gruppa]);

                    $model_sp_begin = $qu->show_sp($data,$id_gruppa);

                    return $this->renderAjax('_spiski',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'model_item_date',
                        'data',
                        'model_group',
                        'array_gruppa',
                        'item_gruppa',
                        '_spiski',
                        'dataProvider_spiski',
                        'model_sp_begin',
                        'model_id',
                        'array_disabled_dates'));
                    break;
                case 12://
                    $data = (Yii::$app->request->post('datamonth'));
                    $data = Yii::$app->formatter->asTime($data);

                    $id = (Yii::$app->request->post('id_child'));
                    $id = preg_replace('/[^0-9]/','',$id);

                    $leftright = (Yii::$app->request->post('leftright'));
                    $leftright = preg_replace('/[^0-9]/','',$leftright);

                    $year = date('Y', strtotime($data));
                    $month = date('n', strtotime($data));

                    switch($leftright){
                        case 0://-1 month
                            if ($month == 1){
                                $month = 12;
                                $year--;
                            }else{
                                $month--;
                            }
                            $data = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
//                            $month = date('n',strtotime($data. " - 1 month"));
//                            $data = date('Y-m-d',strtotime($data. " - 1 month"));
                            break;
                        case 1://+1 month
                            if ($month == 12){
                                $month = 1;
                                $year++;
                            }else{
                                $month++;
                            }
                            $data = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
//                            $month = date('n',strtotime($data. " 1 month"));
//                            $data = date('Y-m-d',strtotime($data. " 1 month"));
                            break;
                    }

//                    return $month;


                    $query = "select gogo.*,reason.NAME AS reason from gogo LEFT OUTER JOIN reason ON if(sovsem > 0, sovsem=reason.id, gogo.id=reason.id)  where id_child =:id and month(datenotgo)=:mon and year(datenotgo)=:yea;";
                    $array_day = Yii::$app->db->createCommand($query,[
                            'id' => $id,
                            'mon' => $month,
                            'yea' => $year,
                        ]
                    )->queryAll();



                    $item_month = $_monthsList[$month];//текущий месяц
//                    $item_month = $_monthsList[date("n", strtotime($data))];//текущий месяц

                    return $this->renderAjax('_only_cal',compact(
                        'model_d_antidate',
                        'model_d_date',
                        'data',
                        'id',
                        'item_month',
                        'array_day'
                    ));
                    break;
                case 999://for tests
                    echo 'Отметка невозможна';
                    exit;
                    break;
                default:
//                    return;
                    break;
//                    return;
//
                    //exit;
                }
            }

            return null;
        }

//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);

        checking_table_deti();

        if ($model_item_date->load(Yii::$app->request->post()) && $model_item_date->validate()) {
            $item_date = $model_item_date->item_date;// you code
            $data = $model_item_date->item_date;
            $data = Yii::$app->formatter->asTime($data);
        }else{
            $model_item_date->item_date = date("d.m.Y");
            $data = date("Y-m-d");
        }

        $id_gruppa = Yii::$app->request->cookies->getValue('save_last_group');
        if(empty($id_gruppa)){
            $item_gruppa = trim($array_gruppa[1]);
            $id_gruppa = 1;
        }else{
            $item_gruppa = trim($array_gruppa[$id_gruppa]);
            $model_group->name = $id_gruppa;
        }


//        if(($array_disabled_dates[$data]) || (date('N', strtotime($data)) == 6) || (date('N', strtotime($data)) == 7)){
        if((in_array($data,$model_d_date) || (date('N', strtotime($data)) > 5))&&!in_array($data,$model_d_antidate)){
            $_spiski = '_weekends_or_holidays';
        }else{
            $_spiski = '_spiski';
//            $item_gruppa = 'Звёздочки';
//            $data = date("Y-m-d");

            $model_sp_begin = $qu->show_sp($data,$id_gruppa);
        }



        return $this->render('spview',compact(
//            'model_dvig',
            'model_d_antidate',
            'model_d_date',
            'model_item_date',
            'data',
            'model_group',
            'array_gruppa',
            'item_gruppa',
            '_spiski',
            'dataProvider_spiski',
            'model_sp_begin',
            'model_id',
            'array_disabled_dates'));


    }











    public function actionTest55(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();
//        $year = 2019;
//        $month = 8;
//        $query = "select distinct(year(rozd))[year] from deti
//        where id_gruppa = 1 and (({$month}>=month([in]) and {$year}>=year([in])) or {$year}>year([in]))
//        and ((({$month}<=month([out]) and {$year}<=year([out])) or {$year} < year([out])) or [out] is null)";
//        $array = Yii::$app->db->createCommand($query)->queryAll();
//        $array = ArrayHelper::getColumn($array,'year');
        $data = date('Y-m-d');
        $model = new Sotrudniki();
        return $this->render('test55',compact('data','array','model'));
    }





    public function actionExceltest($id){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();
        $qu = new Queries();

        $number_sad = Yii::$app->request->cookies->getValue('number_sad');
        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");
        switch($id){
            case 1:
                return $this->render('excel/excel_deti_buh_test3',compact(
                    'ncity',
                    'model',
                    'array',
                    '_monthsList',
                    'number_sad'
                ));
                break;
            case 2:
//                $model_optd = new Optd();
//                $array_control_yavka = $model_optd->show_table_optd_control_yavka();
                /*$dataProvider = new \yii\data\SqlDataProvider([
                    'sql' => 'SET NOCOUNT ON; EXEC Show_plan_injection',
                    'pagination' => false,
                    'sort' => false,
                ]);
                $array = $dataProvider->getModels();*/

                $array = $qu->show_plan_injection();

                return $this->render('excel/plan_injection_current_month',compact(
                    '_monthsList',
                    'array'
                ));
                break;

        }
        exit;
    }

}