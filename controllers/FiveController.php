<?php

namespace app\controllers;

use app\models\Id;
use app\models\Month;
use app\models\Year;
use Yii;
use yii\web\Controller;
use app\models\Queries;

class FiveController extends Controller
{


    public function actionFive(){
//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

        $model_id = new Id();
        $model_id->id = 1;
        $model_y = new Year();
        $model_m = new Month();
        $qu = new Queries();

        $model_d_date = json_decode(Yii::$app->request->cookies->getValue('array_dates'), true);
        $model_d_antidate = json_decode(Yii::$app->request->cookies->getValue('array_antidates'), true);

        $array_year_tabel_deti = [
            2019 => '2019',
            2020 => '2020',
            2021 => '2021',
            2022 => '2022',
            2023 => '2023',
        ];

        $_monthsList = array(
            1=>"Январь",2=>"Февраль",3=>"Март",
            4=>"Апрель",5=>"Май", 6=>"Июнь",
            7=>"Июль",8=>"Август",9=>"Сентябрь",
            10=>"Октябрь",11=>"Ноябрь",12=>"Декабрь");

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if (($model_y->load(Yii::$app->request->post()) && $model_y->validate()) &&
                ($model_m->load(Yii::$app->request->post()) && $model_m->validate())){
                $year = $model_y->name;
                $month = $model_m->name;
                if ($year > date('Y')){
                    return 125;
                }
                if ($year==date('Y')){
                    if ($month > date('m')){
                        return 125;
                    }
                    if ($month == date('m')){
                        $date_proc = date('Y-m-d');
                        $hour = date('G');
                        $y = date('Y',strtotime($date_proc));
                        $m = date('m',strtotime($date_proc));
                        $count_days = date('t',strtotime($date_proc));
                        $last_days = date('Y-m-d',strtotime($y.'-'.$m.'-'.$count_days));
                        if ($date_proc < $last_days){
                            if ($hour>=12){
                                $iter_date = date('Y-m-d',strtotime($date_proc. " 1 day"));
                                while($iter_date <= $last_days){
                                    if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                                        $date_proc = $iter_date;//получаем ближайший рабочий день после сегодня
                                        break;
                                    }else{
                                        $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
                                    }
                                }
                            }else{
                                while($date_proc < $last_days){
                                    if((date('N', strtotime($date_proc)) > 5 || in_array($date_proc,$model_d_date)) && !in_array($date_proc,$model_d_antidate)){
                                        $date_proc = date('Y-m-d',strtotime($date_proc. " 1 day"));
                                    }else{
                                        break;
                                    }
                                }
                            }
                        }
                        $array = $qu->show_five($model_y->name,$model_m->name);
                        return $this->renderAjax('five_table',compact(
                            'date_proc',
                            'model_d_antidate',
                            'model_d_date',
                            'array',
                            'model_y',
                            'model_m'
                        ));
                    }
                    if ($month < date('m')){
                        $date_proc = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
                        $count_days = date('t',strtotime($date_proc));
                        $date_proc = date('Y-m-d',strtotime($year.'-'.$month.'-'.$count_days));
                        $array = $qu->show_five($model_y->name,$model_m->name);
                        return $this->renderAjax('five_table',compact(
                            'date_proc',
                            'model_d_antidate',
                            'model_d_date',
                            'array',
                            'model_y',
                            'model_m'
                        ));
                    }
                }
                if ($year < date('Y')){
                    $date_proc = date('Y-m-d',strtotime($year.'-'.$month.'-01'));
                    $count_days = date('t',strtotime($date_proc));
                    $date_proc = date('Y-m-d',strtotime($year.'-'.$month.'-'.$count_days));


//                    $query = "SET NOCOUNT ON; EXEC Five_day_site @date_current=:date_current";
//                    $array = Yii::$app->db->createCommand($query,['date_current' => $date_proc])->queryAll();

                    $array = $qu->show_five($model_y->name,$model_m->name);

                    return $this->renderAjax('five_table',compact(
                        'date_proc',
                        'model_d_antidate',
                        'model_d_date',
                        'array',
                        'model_y',
                        'model_m'
                    ));
                }
            }
            return null;
        }

//        out_is_null($this);/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        checking($this);
//        $date_proc = date('Y-m-d',strtotime('2020-01-31'));
        $date_proc = date('Y-m-d');
//        $hour = date('G');
//        $hour = 10;
        $y = date('Y',strtotime($date_proc));
        $m = date('m',strtotime($date_proc));
        $n = date('n',strtotime($date_proc));
        $count_days = date('t',strtotime($date_proc));
        $last_days = date('Y-m-d',strtotime($y.'-'.$m.'-'.$count_days));

        if ($date_proc < $last_days){
            if ($hour>=12){
                $iter_date = date('Y-m-d',strtotime($date_proc. " 1 day"));
                while($iter_date <= $last_days){
                    if(((date('N', strtotime($iter_date)) < 6) && (!in_array($iter_date,$model_d_date))) || in_array($iter_date,$model_d_antidate)){
                        $date_proc = $iter_date;//получаем ближайший рабочий день после сегодня
                        break;
                    }else{
                        $iter_date = date('Y-m-d',strtotime($iter_date. " 1 day"));
                    }
                }
            }else{
                while($date_proc < $last_days){
                    if((date('N', strtotime($date_proc)) > 5 || in_array($date_proc,$model_d_date)) && !in_array($date_proc,$model_d_antidate)){
                        $date_proc = date('Y-m-d',strtotime($date_proc. " 1 day"));
                    }else{
                        break;
                    }
                }
            }
        }

//        $query = "SET NOCOUNT ON; EXEC Five_day_site @date_current=:date_current";
//        $array = Yii::$app->db->createCommand($query,['date_current' => $date_proc])->queryAll();*/

        $model_y->name = date('Y');
        $model_m->name = date('n');

        $array = $qu->show_five($model_y->name,$model_m->name);
//        $model_m->name = 1;
        //$array444 = Yii::$app->db->createCommand('select distinct(year(datenotgo))[year] from gogo')->queryAll();
//debug($array444);


        return $this->render('five_index',compact(
            'date_proc',
            'model_d_antidate',
            'model_d_date',
            'array',
            'model_y',
            'model_m',
            '_monthsList',
            'array_year_tabel_deti'

        ));
    }

}
