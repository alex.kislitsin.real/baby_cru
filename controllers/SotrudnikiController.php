<?php

namespace app\controllers;

use app\models\Id;
use Yii;
use app\models\Sotrudniki;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SotrudnikiController extends Controller
{

    public function actionSo()
    {

//        show_layout_light($this);
//        vul($this);
//        change_db_attr();

//        if (verification_user()!=555){
//            return $this->render('net_dostupa');
//        }


        $table = 'sotrudniki';

        $model_so100 = new Sotrudniki();
        $model_id = new Id();
        $model_id->id = 1;

        $dol = get_dol();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            $model_id->load(Yii::$app->request->post());//получаем общий массив со значением id 1 или 2 или 3
            if ($model_id->validate()){
                if (($model_so100->load(Yii::$app->request->post()) && $model_so100->validate()) || $model_id->id == 4 || $model_id->id == 5){
                    switch($model_id->id){
                        case 1://
                            $rozdso = Yii::$app->formatter->asTime($model_so100->rozdso);
                            $ins = Yii::$app->formatter->asTime($model_so100->ins);
                            if ($model_so100->outs === null){
                                $outs = null;
                            }else{
                                $outs = Yii::$app->formatter->asTime($model_so100->outs);
                            }

                            $id = preg_replace('/[^0-9]/','',$model_so100->id);
                            $pol = preg_replace('/[^0-9]/','',$model_so100->pol);
                            $prikaz_in = preg_replace('/[^0-9]/','',$model_so100->prikaz_in);//если пустой приходит 0
                            $prikaz_out = preg_replace('/[^0-9]/','',$model_so100->prikaz_out);//если пустой приходит 0
                            $pitanie = preg_replace('/[^0-9]/','',$model_so100->pitanie);
                            $schet = preg_replace('/[^0-9]/','',$model_so100->schet);//если пустой приходит 0

//                            $name = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->name);
                            $address = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->address);
                            $coment = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->coment);
                            $dol = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->dol);
                            if (strpos($dol,'олжность') !== false){
                                $dol = '';
                            }

                            $tel = preg_replace('/[^0-9\s\+\(\)]/','',$model_so100->tel);
                            $snils = preg_replace('/[^0-9\s\-]/','',$model_so100->snils);

                            switch($pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }
                            switch($pitanie){
                                case 1:
                                    $pitanie = 'на питании';
                                    break;
                                case 2:
                                default:
                                    $pitanie = '';
                                    break;
                            }

                            $query = "update $table set name=:name, schet=:schet, inS=:inS, outS=:outS, rozdso=:rozdso, dol=:dol,
                            pitanie=:pitanie, pol=:pol, tel=:tel, address=:address, coment=:coment, prikaz_in=:prikaz_in,prikaz_out=:prikaz_out,snils=:snils,polis=:polis,
                             seria=:seria,num_passport=:num_passport,kem_vidan=:kem_vidan,date_vidacha=:date_vidacha,inn=:inn where id=:id";

                            /*$query = "insert into sotrudniki (name,schet,inS,outS,rozdso,dol,pitanie,pol,tel,address,coment,prikaz_in,prikaz_out,snils) values
                            (:name,:schet,:inS,:outS,:rozdso,:dol,:pitanie,:pol,:tel,:address,:coment,:prikaz_in,:prikaz_out,:snils);";*/

                            Yii::$app->db->createCommand($query,[
                                    'id' => $id,
                                    'name' => $model_so100->name,
                                    'schet' => $schet,
                                    'inS' => $ins,
                                    'outS' => $outs,
                                    'rozdso' => $rozdso,
                                    'dol' => $dol,
                                    'pitanie' => $pitanie,
                                    'pol' => $pol,
                                    'tel' => $tel,
                                    'address' => $address,
                                    'coment' => $coment,
                                    'prikaz_in' => $prikaz_in,
                                    'prikaz_out' => $prikaz_out,
                                    'snils' => $snils,
                                    'polis' => $model_so100->polis,
                                    'seria' => $model_so100->seria,
                                    'num_passport' => $model_so100->num_passport,
                                    'kem_vidan' => $model_so100->kem_vidan,
                                    'date_vidacha' => $model_so100->date_vidacha,
                                    'inn' => $model_so100->inn,
                                ]
                            )->execute();

                            $query2 = "select * from $table where outS is null order by name";
                            $model = Yii::$app->db->createCommand($query2)->queryAll();
                            $model_so100 = new Sotrudniki();
                            $dol = get_dol();
                            return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
//                            return $query;
                            break;
                        case 2://добавление нового сотрудника
                            $rozdso = Yii::$app->formatter->asTime($model_so100->rozdso);
                            $ins = Yii::$app->formatter->asTime($model_so100->ins);
                            if ($model_so100->outs === null){
                                $outs = null;
                            }else{
                                $outs = Yii::$app->formatter->asTime($model_so100->outs);
                            }

//                            $id = preg_replace('/[^0-9]/','',$model_so100->id);
                            $pol = preg_replace('/[^0-9]/','',$model_so100->pol);
                            $prikaz_in = preg_replace('/[^0-9]/','',$model_so100->prikaz_in);//если пустой приходит 0
                            $prikaz_out = preg_replace('/[^0-9]/','',$model_so100->prikaz_out);//если пустой приходит 0
                            $pitanie = preg_replace('/[^0-9]/','',$model_so100->pitanie);
                            $schet = preg_replace('/[^0-9]/','',$model_so100->schet);//если пустой приходит 0

//                            $name = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->name);
                            $address = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->address);
                            $coment = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->coment);
                            $dol = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->dol);
                            if (strpos($dol,'олжность') !== false){
                                $dol = '';
                            }

                            $tel = preg_replace('/[^0-9\s\+\(\)]/','',$model_so100->tel);
                            $snils = preg_replace('/[^0-9\s\-]/','',$model_so100->snils);

                            switch($pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }
                            switch($pitanie){
                                case 1:
                                    $pitanie = 'на питании';
                                    break;
                                case 2:
                                default:
                                    $pitanie = '';
                                    break;
                            }

                            /*$query = "update sotr500 set name=:name, schet=:schet, inS=:inS, outS=:outS, rozdso=:rozdso, dol=:dol,
                            pitanie=:pitanie, pol=:pol, tel=:tel, address=:address, coment=:coment, prikaz_in=:prikaz_in,prikaz_out=:prikaz_out,snils=:snils where id=:id";*/

                            $query = "insert into $table (name,schet,inS,outS,rozdso,dol,pitanie,pol,tel,address,coment,prikaz_in,prikaz_out,snils,polis,seria,num_passport,kem_vidan,date_vidacha,inn) values
                            (:name,:schet,:inS,:outS,:rozdso,:dol,:pitanie,:pol,:tel,:address,:coment,:prikaz_in,:prikaz_out,:snils,:polis,:seria,:num_passport,:kem_vidan,:date_vidacha,:inn);";

                            Yii::$app->db->createCommand($query,[
                                    'name' => $model_so100->name,
                                    'schet' => $schet,
                                    'inS' => $ins,
                                    'outS' => $outs,
                                    'rozdso' => $rozdso,
                                    'dol' => $dol,
                                    'pitanie' => $pitanie,
                                    'pol' => $pol,
                                    'tel' => $tel,
                                    'address' => $address,
                                    'coment' => $coment,
                                    'prikaz_in' => $prikaz_in,
                                    'prikaz_out' => $prikaz_out,
                                    'snils' => $snils,
                                    'polis' => $model_so100->polis,
                                    'seria' => $model_so100->seria,
                                    'num_passport' => $model_so100->num_passport,
                                    'kem_vidan' => $model_so100->kem_vidan,
                                    'date_vidacha' => $model_so100->date_vidacha,
                                    'inn' => $model_so100->inn,
                                ]
                            )->execute();

                            $query = "select * from $table where outS is null order by name";
                            $model = Yii::$app->db->createCommand($query)->queryAll();
                            $model_so100 = new Sotrudniki();
                            $dol = get_dol();
                            return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
                            break;
                        case 3://
                            $id7 = (Yii::$app->request->post('id7'));
                            $id7 = preg_replace('/[^0-9]/','',$id7);
                            $id = preg_replace('/[^0-9]/','',$model_so100->id);
                            if ($id < 1){
                                return 400;
                            }else{
                                switch($id7){
                                    case 0:
                                        $query2 = "select * from $table where outS is not null order by name";
                                        break;
                                    case 1:
                                        $query2 = "select * from $table where outS is null order by name";
                                        break;
                                }

                                $transaction = Yii::$app->db->beginTransaction();
                                try{
                                    $query = "delete from $table where id=:id;";
                                    Yii::$app->db->createCommand($query,['id' => $id])->execute();
                                    $model = Yii::$app->db->createCommand($query2)->queryAll();
                                    $transaction->commit();
                                }catch (Exception $e){
                                    $transaction->rollBack();
                                }

                                /*$query = "delete from $table where id=:id;";
                                Yii::$app->db->createCommand($query,['id' => $id])->execute();

                                $model = Yii::$app->db->createCommand($query2)->queryAll();*/
                                $dol = get_dol();
                                $model_so100 = new Sotrudniki();
                                return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
                            }
                            break;
                        case 4://
                            $query = "select * from $table where outS is not null order by name";
                            $model = Yii::$app->db->createCommand($query)->queryAll();
                            $model_so100 = new Sotrudniki();
                            $dol = get_dol();
                            return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
                            break;
                        case 5://
                            $query = "select * from $table where outS is null order by name";
                            $model = Yii::$app->db->createCommand($query)->queryAll();
                            $model_so100 = new Sotrudniki();
                            $dol = get_dol();
                            return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
                            break;
                        case 6://обновление данных сорт. уволенного и возврат на уволенных
                            $rozdso = Yii::$app->formatter->asTime($model_so100->rozdso);
                            $ins = Yii::$app->formatter->asTime($model_so100->ins);
                            if ($model_so100->outs === null){
                                $outs = null;
                            }else{
                                $outs = Yii::$app->formatter->asTime($model_so100->outs);
                            }

                            $id = preg_replace('/[^0-9]/','',$model_so100->id);
                            $pol = preg_replace('/[^0-9]/','',$model_so100->pol);
                            $prikaz_in = preg_replace('/[^0-9]/','',$model_so100->prikaz_in);//если пустой приходит 0
                            $prikaz_out = preg_replace('/[^0-9]/','',$model_so100->prikaz_out);//если пустой приходит 0
                            $pitanie = preg_replace('/[^0-9]/','',$model_so100->pitanie);
                            $schet = preg_replace('/[^0-9]/','',$model_so100->schet);//если пустой приходит 0

//                            $name = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->name);
                            $address = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->address);
                            $coment = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->coment);
                            $dol = preg_replace('/[a-zA-Z\'\"\;]/','',$model_so100->dol);
                            if (strpos($dol,'олжность') !== false){
                                $dol = '';
                            }

                            $tel = preg_replace('/[^0-9\s\+\(\)]/','',$model_so100->tel);
                            $snils = preg_replace('/[^0-9\s\-]/','',$model_so100->snils);

                            switch($pol){
                                case 1:
                                    $pol = 'М';
                                    break;
                                case 2:
                                default:
                                    $pol = 'Ж';
                                    break;
                            }
                            switch($pitanie){
                                case 1:
                                    $pitanie = 'на питании';
                                    break;
                                case 2:
                                default:
                                    $pitanie = '';
                                    break;
                            }

                            /*$query = "update $table set name=:name, schet=:schet, inS=:inS, outS=:outS, rozdso=:rozdso, dol=:dol,
                            pitanie=:pitanie, pol=:pol, tel=:tel, address=:address, coment=:coment, prikaz_in=:prikaz_in,prikaz_out=:prikaz_out,snils=:snils,polis=:polis where id=:id";*/

                            $transaction = Yii::$app->db->beginTransaction();
                            try{
                                $query = "update $table set name=:name, schet=:schet, inS=:inS, outS=:outS, rozdso=:rozdso, dol=:dol,
                            pitanie=:pitanie, pol=:pol, tel=:tel, address=:address, coment=:coment, prikaz_in=:prikaz_in,prikaz_out=:prikaz_out,snils=:snils,polis=:polis,
                             seria=:seria,num_passport=:num_passport,kem_vidan=:kem_vidan,date_vidacha=:date_vidacha,inn=:inn where id=:id";

                                Yii::$app->db->createCommand($query,[
                                        'id' => $id,
                                        'name' => $model_so100->name,
                                        'schet' => $schet,
                                        'inS' => $ins,
                                        'outS' => $outs,
                                        'rozdso' => $rozdso,
                                        'dol' => $dol,
                                        'pitanie' => $pitanie,
                                        'pol' => $pol,
                                        'tel' => $tel,
                                        'address' => $address,
                                        'coment' => $coment,
                                        'prikaz_in' => $prikaz_in,
                                        'prikaz_out' => $prikaz_out,
                                        'snils' => $snils,
                                        'polis' => $model_so100->polis,
                                        'seria' => $model_so100->seria,
                                        'num_passport' => $model_so100->num_passport,
                                        'kem_vidan' => $model_so100->kem_vidan,
                                        'date_vidacha' => $model_so100->date_vidacha,
                                        'inn' => $model_so100->inn,
                                    ]
                                )->execute();

                                $query = "select * from $table where outS is not null order by name";
                                $model = Yii::$app->db->createCommand($query)->queryAll();
                                $transaction->commit();
                            }catch (Exception $e){
                                $transaction->rollBack();
                            }


                            $model_so100 = new Sotrudniki();
                            $dol = get_dol();
                            return $this->renderAjax('table',compact('model','model_so100','dol','model_id'));
                            break;


                    }
                }else{
                    return 400;
                }

            }

            return null;

            /*$model_so100->load(Yii::$app->request->post());
            $model_so100->validate();
            $data = $model_so100->ins;
            $data = Yii::$app->formatter->asTime($data);
            return $data;*/
        }



//        checking($this);


        $query = "select * from $table where outS is null order by `name`";
        $model = Yii::$app->db->createCommand($query)->queryAll();




        return $this->render('index_so',compact('model','model_so100','dol','model_id'));
    }

    public function actionEx(){
        return $this->render('exell');
    }

}
