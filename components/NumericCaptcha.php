<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 23.12.19
 * Time: 14:20
 */

namespace app\components;
use yii\captcha\CaptchaAction as DefaultCaptchaAction;


class NumericCaptcha extends DefaultCaptchaAction{

    protected function generateVerifyCode()
    {
        //Длина
        $length = 4;
        //Цифры, которые используются при генерации
        $digits = '0123456789';
        $code = '';
        for($i = 0; $i < $length; $i++) {
            $code .= $digits[mt_rand(0, 9)];
        }
        return $code;
    }

} 