<?php
/**
 * Created by PhpStorm.
 * User: Cosmos
 * Date: 06.09.19
 * Time: 14:29
 */
namespace app\components;
use yii\base\Widget;

class MyWidget extends Widget{

    public $name;

    public function init(){
        parent::init();
        if($this->name === null){
            $this->name = 'Quest';
        }
    }

    public function run(){
        //return "<h1>{$this->name}, Hello world !</h1>";
        return $this->render('my',['name' => $this->name]);
    }

} 